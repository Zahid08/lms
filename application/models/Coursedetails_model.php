<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Coursedetails_model extends CI_Model {
	
	
	function insertcourse($data,$courseid)
	{
	    if($courseid == 0){
	        if($this->db->insert('course', $data))
    		{
    		    return true;
    		}
    		else
    		{
    			return false;
    		}
	    }else{
	        $now = date('Y-m-d');
	        
	        
	        $updatedata = array(
    		'title'=> $data['title'],
    		'category'=>$data['category'],
    		'instructor'=> $data['instructor'],
    		'type'=>$data['type'],
    		'description'=>$data['description'],
    		'tag_line'=>$data['tag_line'],
    		'how_to_use'=>$data['how_to_use'],
    		'language'=>$data['language'],
    		'image'=>$data['image'],
    		'status'=>$data['status'],
    		'is_published'=>$data['is_published'],
    		'updated_by'=>$_SESSION['user_id'],
    		'updated_date'=>$now,
    		);
		
	        
	        
	        $this->db->where('course_id', $courseid);
    		if($this->db->update('course', $updatedata))
    		{
    		    return true;
    		}
    		else
    		{
    			return false;
    		}
	    }	
		
	}
	
	function insertprice($data)
	{
	    		
		if($this->db->insert('course_price', $data))
		{
		    return true;
		}
		else
		{
			return false;
		}
	}
	function updatemarketing($data)
	{
	    $updatedata = array(
		'metadata_keywords'=> $data['metadata_keywords'],
		'metadta_desc'=>$data['metadta_desc'],
		'updated_by'=> $data['updated_by'],
		'updated_date'=>$data['updated_date']);
		
        $this->db->where('course_id', $data['course_id']);
		if($this->db->update('course', $updatedata))
		{
		    return true;
		}
		else
		{
			return false;
		}
	}
	
	function deletecourse($deleteData,$courseid)
	{
	    
		$status = $deleteData['status'];
		$updated_by = $deleteData['updated_by'];
		$updated_date = $deleteData['updated_date'];
	    
	    $DeleteCourse = $this->db->query("UPDATE course SET status = '$status',updated_by = '$updated_by', updated_date = '$updated_date'  WHERE course_id = '$courseid' ");
	    
		if($DeleteCourse)
		{
		    return true;
		}
		else
		{
			return false;
		}
	}
	function get_all_course(){
	   
	    $query = $this->db->query("SELECT * FROM course WHERE status = '1' ");
        if ($query->num_rows() > 0) {
            
            return $query->result_array();
        } else {
            return array();
        }
	}
	function get_all_price(){
	    $query = $this->db->query("SELECT * FROM course_price WHERE status = '1' ");
        if ($query->num_rows() > 0) {
            
            return $query->result_array();
        } else {
            return array();
        }
	}
	function get_luser_course(){
	     $query = $this->db->query("SELECT * FROM users WHERE status = '1' AND role = '3' ");
        if ($query->num_rows() > 0) {
            
            return $query->result_array();
        } else {
            return array();
        }
	}
	function insert_transaction_detail($user_id,$learner_id,$course_id,$payment_mode,$amount,$transactioncode){
	    $now = date('Y-m-d');
	    $data = array(
    		'user_id'=> $learner_id,
			'course_id'=> $course_id,
			'transaction_type'=>$payment_mode,
			'amount' => $amount,
			'status'=>'success',
			'transaction_code'=> $transactioncode,
			'created_date'=>$now,
			'created_by' => $user_id
    		);
	    if($this->db->insert('transactions', $data))
		{
		    $this->session->set_flashdata('smsg', 'Payment successfully Added');
		    return true;
		}
		else
		{
			return false;
		}
	}
	function get_user_info($email){
	    $query = $this->db->query("SELECT * FROM users WHERE email_id = '".$email."' AND role = '3' ");
        if ($query->num_rows() > 0) {
            
            return $query->result_array();
        } else {
            return array();
        }
	}
	
	function get_all_users(){
	    $query = $this->db->query("SELECT * FROM users WHERE status = '1' ");
        if ($query->num_rows() > 0) {
            
            return $query->result_array();
        } else {
            return array();
        }
	}
	function insert_comment($comment,$id){
	    $data = array(
			'admin_comments' => $comment
    		);
    		 $this->db->where('transaction_id', $id);
	    if($this->db->update('transactions', $data))
		{
		    $this->session->set_flashdata('smsg', 'comment inserted successfully');
		    return true;
		}
		else
		{
			return false;
		}
	}
	function get_searched_data($status,$type){
	    $query = $this->db->query("SELECT * FROM transactions WHERE status = '".$status."' AND transaction_type = '".$type."' ");
        if ($query->num_rows() > 0) {
            
            return $query->result_array();
        } else {
            return array();
        }
	}
	
}	