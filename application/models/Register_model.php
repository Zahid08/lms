<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Register_model extends CI_Model {
	
	
	function checkDuplicate($email)
	{
		$this->db->select('email_id');
		$this->db->from('users');
		$this->db->like('email_id', $email);
		return $this->db->count_all_results();
		
	}
	
	function insertUser($data)
	{
		if($this->db->insert('users', $data))
		{
		    $getmaxId = $this->db->query("SELECT id,parent_id FROM users WHERE id = (SELECT max(id) FROM `users`)");
                foreach($getmaxId->result() as $maxId){
                        $Id = $maxId->id ;  
                        $parent_id = $maxId->parent_id;
                }
                
            $parent_Id = 0;
            $getparentId = $this->db->query("SELECT id FROM `users` WHERE unique_id = '$parent_id'");
                foreach($getparentId->result() as $parentId){
                        $parent_Id = $parentId->id ;  
                }
                
                
            $dataI = array(
            'id' => $Id,
            'parent_id' => $parent_Id
            );
            $this->db->insert('pc_table',$dataI);
			/*return  $this->db->insert_id();*/
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function insertCreditData($data)
	{
		if($this->db->insert('credits', $data))
		{
		    return true;
		}
		else
		{
			return false;
		}
	}
	
	function insertCreditParentData($data)
	{
		if($this->db->insert('generated_credits', $data))
		{
		    return true;
		}
		else
		{
			return false;
		}
	}
	function insertCreditChildData($data)
	{
		if($this->db->insert('generated_credits', $data))
		{
		    return true;
		}
		else
		{
			return false;
		}
	}
}	