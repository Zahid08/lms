<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Newchapter_model extends CI_Model {
	
	
	function insertchapter($data,$chapterid,$subchapterid,$courseid,$subtype,$chaptername,$availableFrom)
	{
	    if($subchapterid == 'nil'){
	        if($chapterid == 'nil'){
    	        if($this->db->insert('chapters', $data))
        		{
        		    
        		    return true;
        		}
        		else
        		{
        			return false;
        		}
    	    }else{
    	        if($this->db->insert('sub_chapters', $data))
        		{
        		    
        		    
        		    $userid = $_SESSION['user_id'];
				       $subChapterIdz=$this->db->query("SELECT id as subChapterId  FROM sub_chapters where created_by = '$userid' and status = 1 ORDER BY 1 DESC LIMIT 1")->result();
				       foreach($subChapterIdz as $getsubChapterId){
				           $subChapterId_zoom=$getsubChapterId->subChapterId;
				       }
        		    /* ==============if zoom===================*/
				    if($subtype == 'zoom'){
				        

                            $zoom_meeting = new Zoom_Api();
                            
                            $data1 = array();
                            $data1['topic'] 		= $chaptername;
                            $data1['start_date'] = $availableFrom;
                            $data1['duration'] 	= 30;
                            $data1['type'] 		= 2;
                            $data1['password'] 	= "12345";
                             
                            try {
                            	$response = $zoom_meeting->createMeeting($data1);
                            	$meetingid=$response->id;
                            	$topic=$response->topic;
                            	$password=$response->password;
                            	$joinurl = $response->join_url;
                            	$starturl = $response->start_url;
                            	/*echo "<pre>";
                            	print_r($response);
                            	echo "<pre>";
                            	exit;*/
                            	$zoomdata = array(
                            	    'course_id' => $courseid,
                            	    'chapter_id' => $chapterid,
                            	    'sub_chapter_id' => $subChapterId_zoom,
                            	    'meeting_id' =>$meetingid,
                            	    'topic' => $topic,
                            	    'password' => $password,
                            	    'start_url' => $starturl,
                            	    'join_url' => $joinurl
                            	    );
                            	    
                            	    $this->db->insert('zoom_live_class', $zoomdata);
                            	
                            } catch (Exception $ex) {
                                $exception = $ex;
                               
                            }
                            				    
				        
				    }
				    /*=============End of Zoom================*/

                    /*==================if bbb=================*/
                    //$insert_id = $this->db->insert_id();  //Last inserted ID

                    if ($data['sub_type']=='bbb') {

                        $backUrl=base_url().'EditItem/endbbbclass?courseid='.$courseid.'&chapterid='.$chapterid.'&itemid='.$subChapterId_zoom.'';  //Return Back url after close meeting

                        $creationParams = array(
                            'meetingId' =>rand ( 10000 , 99999 ),                           // REQUIRED
                            'meetingName' => ''. $data['subchapter_name'].'',   // REQUIRED
                            'attendeePw' => 'ap',                               // Match this value in getJoinMeetingURL() to join as attendee.
                            'moderatorPw' => 'mp',                              // Match this value in getJoinMeetingURL() to join as moderator.
                            'welcomeMsg' => '',                                 // ''= use default. Change to customize.
                            'dialNumber' => '',                                 // The main number to call into. Optional.
                            'voiceBridge' => '',                                // PIN to join voice. Optional.
                            'webVoice' => '',                                   // Alphanumeric to join voice. Optional.
                            'logoutUrl' =>$backUrl,                             // Default in bigbluebutton.properties. Optional.
                            'maxParticipants' => '-1',                          // Optional. -1 = unlimitted. Not supported in BBB. [number]
                            'record' => 'true',                                 // New. 'true' will tell BBB to record the meeting.
                            'publish' => 'true',
                            'duration' =>0,                                     // Default = 0 which means no set duration in minutes. [number]
                            //'meta_category' => '',                            // Use to pass additional info to BBB server. See API docs.
                        );


                        $itsAllGood = true;
                        try {
                            $result = $this->Bigbluebutton_model->createMeetingWithXmlResponseArray($creationParams);}
                        catch (Exception $e) {
                            echo 'Caught exception: ', $e->getMessage(), "\n";
                            $itsAllGood = false;
                        }

                        if ($itsAllGood == true) {
                            // If it's all good, then we've interfaced with our BBB php api OK:
                            if ($result == null) {
                                // If we get a null response, then we're not getting any XML back from BBB.
                                echo "Failed to get any response. Maybe we can't contact the BBB server.";
                                 return false;
                            }
                            else {
                                if ($result['returncode'] == 'SUCCESS') {
                                    $bbssettingData = array(
                                        'userid'=> $_SESSION['user_id'],
                                        'sub_chapter_id' => $subChapterId_zoom,
                                        'meetingId' => $result['meetingId'],
                                        'meetingName' =>$creationParams['meetingName'],
                                        'attendeePw' =>$creationParams['attendeePw'],
                                        'moderatorPw' =>$creationParams['moderatorPw'],
                                        'created_date' => date('Y-m-d'),
                                        'created_by' => $_SESSION['user_id']
                                    );

                                    $this->db->insert('bbb_settings', $bbssettingData);

                                    return true;
                                }
                            }
                        }
                    }else{
                        return true;
                    }
                    /*===================End of BBB================*/
        		    return true;
        		}
        		else
        		{
        			return false;
        		}
    	    }
	    }else{
	        $this->db->where('id', $subchapterid);
	        $this->db->update('sub_chapters', $data);
	        redirect('EditItem?courseid='.$courseid.'&chapterid='.$chapterid.'&itemid='.$subchapterid);
	    }
	    
	    	
		
	}
	
	function insertrecording($datainsert,$dataupdate,$subChapterId)
	{
	    $subChapterData = $this->db->query("SELECT * FROM live_class_recordings WHERE status = 1  AND sub_chapter_id= '$subChapterId' ORDER BY 1 DESC")->result();
        
        if(count($subChapterData) == 0){                                                                           
    	        if($this->db->insert('live_class_recordings', $datainsert))
        		{
        		    return true;
        		}
        		else
        		{
        			return false;
        		}
        }else{
            $this->db->where('sub_chapter_id', $subChapterId);
	        $this->db->update('live_class_recordings', $dataupdate);
	        return true;
        }    
	   
	}
	function insertlivetest($insertData,$insertsubData,$updateData,$liveTestid,$CourseId)
	{ 	    
	    $subChapterData = $this->db->query("SELECT * FROM live_test WHERE status = 1  AND id= '$liveTestid' ORDER BY 1 DESC")->result();
        if(count($subChapterData) == 0){
	            if($this->db->insert('live_test', $insertData))
        		{
        		    
        		    if($this->db->insert('sub_chapters', $insertsubData))
        	      	{
        		    return true;
        		    }
        		    else
        		    {
        			return false;
        		    }
        		    return true;
        		}

        		else
        		{
        			return false;
        		}
        }
        else{
            $this->db->where('id', $liveTestid);
	        $this->db->update('live_test', $updateData);
	        return true;
        }
	}
	function insertsubchapter($insertsubData,$chapterid,$subchapterid,$courseid)
	{
	    if($subchapterid == 'nil'){
	        if($chapterid == 'nil'){
    	        if($this->db->insert('chapters', $insertsubData))
        		{
        		    return true;
        		}
        		else
        		{
        			return false;
        		}
    	    }else{
    	        if($this->db->insert('sub_chapters', $insertsubData))
        		{
        		    return true;
        		}
        		else
        		{
        			return false;
        		}
    	    }
	    }else{
	        $this->db->where('id', $subchapterid);
	        $this->db->update('sub_chapters', $data);
	        	return false;
	    }
	    
	    	
		
	}
	function insertnewquestions($insertquestionsData)
	{
	   if($this->db->insert('question_bank', $insertquestionsData))
        {
        	return true;
        }
        else
        {
        	return false;
        }
        

	}
	
	function deleteinsertnewquestions($insertquestionsData,$liveTestQuestionID)
	{
	    $this->db->where('id', $liveTestQuestionID);
	    $this->db->delete('question_bank');
	    
	    $this->db->where('qsn_bank_id', $liveTestQuestionID);
	    $this->db->delete('live_test_question_options');
	    
	    
	   if($this->db->insert('question_bank', $insertquestionsData))
        {
        	return true;
        }
        else
        {
        	return false;
        }
        

	}

    function updatequiztestquestions($updateData ,$newQuestionId)
    {
        if ($newQuestionId){
            $this->db->where('id', $newQuestionId);
            $this->db->update('question_bank', $updateData);
            return $newQuestionId;
        }
    }
	
}