<?php 
date_default_timezone_set("Asia/Calcutta");
 class Login_model extends CI_Model  
    {  
      function can_login($email, $decode_password)  
        {  
          // fetch by email first
          $this->db->where('email_id', $email);
          $query_hashedpassword = $this->db->get('users');
          $result_hashedpassword = $query_hashedpassword->row_array(); // get the row first
          
          if($query_hashedpassword->num_rows() > 0){
              $hashed_password = $result_hashedpassword['password'];
              if($hashed_password == $decode_password)  
               {  
                   $user_id = $result_hashedpassword['id'];
                   //===================================single device sign in logic==================================
                    $token=rand();
                    $this->db->where('status', 1);
                    $this->db->where('user_id', $user_id);
                    $checktoken = $this->db->get('login_tokens');
                    if($checktoken->num_rows()>0){
                        $this->db->query("UPDATE login_tokens SET status=0 WHERE user_id = '".$user_id."' ");// Update all previous active tokens
                    }
                    $this->db->query("insert into login_tokens (user_id,token,status) values ('$user_id','$token',1) ");
                    $_SESSION['token']=$token;   // Store Token in Session Variable
                    //=====================================single device sign in logic==================================
                    return true;  
               }  
               else  
               {  
                    return false;       
               } 
                 
          }else{
              return false; 
          }
          
       }
       
       public function update_firstlogin($email) {
            $data = array(
            	'first_login' => '1'
            	);
            $this->db->set($data);
            $this->db->where('email_id', $email);
            $this->db->update('users');
        }
        public function deviceinfo($deviceinfo,$user_id) {
            $today = date('Y-m-d H:i:s');
            $getDeviceInfo = $this->db->query("SELECT * FROM `user_devices` WHERE user_id = '$user_id' AND device_info = '$deviceinfo'")->result();
            
            $data = array(
            	'user_id' => $user_id,
            	'device_info' => $deviceinfo,
            	'last_login' => $today,
            	'created_by' => $user_id,
            	);
            if(count($getDeviceInfo) == 0){
                $this->db->insert('user_devices',$data);
            }else{
                foreach($getDeviceInfo as $getDeviceInfoId){
                    $deviceId = $getDeviceInfoId->id;
                    $loginCount = $getDeviceInfoId->login_count;
                }
                $loginCount++;
                
                $this->db->query("UPDATE user_devices SET last_login = '".$today."',login_count = '".$loginCount."' WHERE id = '".$deviceId."' ");
            }
            
        }
        public function update_lastlogin($email){
            $today = date('Y-m-d H:i:s');
            $this->db->query("UPDATE users SET last_login = '".$today."' WHERE email_id = '".$email."' ");
            
        if($this->db->trans_status()){
            return 1;    
        }else{
            return 0;
        }
            
        }
        public function update_logincount($email,$login_count){
            $login_count= ++$login_count;
            $this->db->query("UPDATE users SET login_count = '".$login_count."' WHERE email_id = '".$email."' ");
        }
    }