<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Newquiztest_model extends CI_Model
{
    function insertquiztest($insertData,$insertsubData,$updateData,$quizTestid,$CourseId)
	{ 	    
	    $subChapterData = $this->db->query("SELECT * FROM quiz_test WHERE status = 1  AND id= '$quizTestid' ORDER BY 1 DESC")->result();
        if(count($subChapterData) == 0){
	            if($this->db->insert('quiz_test', $insertData))
        		{
                    $insert_id = $this->db->insert_id();  //Last inserted ID

                    $insertsubData['quiz_id']=$insert_id;

        		    if($this->db->insert('sub_chapters', $insertsubData))
        	      	{
                        return $insert_id;
        		    }
        		    else
        		    {
        			return false;
        		    }
        		    return true;
        		}

        		else
        		{
        			return false;
        		}
        }
        else{
            $this->db->where('id', $quizTestid);
	        $this->db->update('quiz_test', $updateData);
	        return $quizTestid;
        }
	}

	function insertquiztestquestions($insertquestionsData)
	{
	   if($this->db->insert('quiz_test_questions', $insertquestionsData))
        {
        	return true;
        }
        else
        {
        	return false;
        }
        

	}

    function updatequiztestquestions($updateData ,$newQuestionId)
    {
       if ($newQuestionId){
           $this->db->where('id', $newQuestionId);
           $this->db->update('quiz_test_questions', $updateData);
           return $newQuestionId;
       }
    }
	
}	