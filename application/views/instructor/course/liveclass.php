<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<?php
/*====================Course data===========================*/
$CourseId = $_GET['courseid'];
$courseData = $this->db->query("SELECT * FROM course c RIGHT JOIN users u ON c.created_by = u.id  WHERE c.status = 1 AND is_published = 1 AND course_id = '$CourseId'  ORDER BY 1 DESC")->result();
foreach($courseData as $getcourseData){
	$courseTitle = $getcourseData->title;
	$courseEmail = $getcourseData->email_id;
	$courseUsername = $getcourseData->username;
	
}
/*============================================================*/
/*==================Sub Chapters Data=====================*/
$subChapterId = $_GET['itemid'];
$subChapterData = $this->db->query("SELECT * FROM sub_chapters WHERE status = 1  AND course_id = '$CourseId' AND id= '$subChapterId'  ORDER BY 1 DESC")->result();
foreach($subChapterData as $getsubChapterData){
    $type = $getsubChapterData->type;
	$subChapterNameItem = $getsubChapterData->subchapter_name;
	$subtype = $getsubChapterData->sub_type;
	$path = $getsubChapterData->path;
	$availability = $getsubChapterData->availability;
	$available_from = $getsubChapterData->available_from;
	$available_to = $getsubChapterData->available_to;
	
	$subPreClassMsg = $getsubChapterData->pre_class_msg;
	$subPostClassMsg = $getsubChapterData->post_class_msg;
	$showRecording = $getsubChapterData->show_recording;
	$emailPush = $getsubChapterData->email_push;
	$mobilePush = $getsubChapterData->mobile_push;
	$webPush = $getsubChapterData->web_push;
}

/*==========================================================*/
?>
<!DOCTYPE html>
    <head>
        <title><?php echo $courseTitle ?></title>
		 <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="<?php echo $meta_description?>">
		<meta name="keywords" content="<?php echo $meta_keywords ?>">
		<meta name="author" content="#">
		<!-- Favicon icon -->
		<link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
		<!-- Google font-->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">

		<link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.7.7/css/bootstrap.css"/>
    	<link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.7.7/css/react-select.css"/>
        
	
    </head>
    <body>
        <style type="text/css">
      
            .navbar-inverse {
                background-color: #313131;
                border-color: #404142;
            }
            .navbar-header h4 {
                margin: 0;
                padding: 15px 15px;
                color: #c4c2c2;
            }
            .navbar-right h5 {
                margin: 0;
                padding: 9px 5px;
                color: #c4c2c2;
            }
            .navbar-inverse .navbar-collapse, .navbar-inverse .navbar-form{
                border-color: transparent;
            }
        </style>

						 
		<!-- Nav For Top Fix -->
       <!--  <nav id="nav-tool" class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <h4 style="color:white"><i class="fa fa-plus"></i> Meeting Title : Demo</h4>
                </div>
                <div class="navbar-form navbar-right">
                    <h5 style="color:white"> HOST BY : 
					<img src="" HostImageclass="img-circle" height="30" width="30"/> Kavya
					&nbsp;&nbsp;&nbsp;<a href="#" style="color:white"> Back </a></h5>
                </div>
            </div>
        </nav> -->
		
		<!-- Container That Render Jitsi -->
		<center><div id="container" style="width:100%;height:100vh">

	<!-- Meet Jitsi API -->	
	<script src="https://meet.jit.si/external_api.js"></script>
	<script>
		var domain = "meet.jit.si";
		var options = {
			userInfo : { 
				email : '<?php echo $courseEmail ?>' , 
				displayName : '<?php echo $courseUsername ?>',
				moderator: true,
			},
			roomName: "<?php echo $subChapterNameItem ?>",
			width: "100%",
			height: "100%",
			parentNode: document.querySelector('#container'),
			configOverwrite: {},
			interfaceConfigOverwrite: {
				// filmStripOnly: true
				disableInviteFunctions  : false
			}
		}
		var api = new JitsiMeetExternalAPI(domain, options);
			api.executeCommand('subject', '<?php echo $courseTitle ?>');
	</script>
</center>
		
    </body>
</html>

    