<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Competitive Exam Guide || Home</title>
    <!-- Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?php echo $meta_description?>">
		<meta name="keywords" content="<?php echo $meta_keywords ?>">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/landing/logos/favicon.ico" type="image/png" sizes="16x16">
    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/landing/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,700,600" rel="stylesheet" type="text/css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animate.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.theme.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/magnific-popup.css">
    <!-- Full Page Animation -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animsition.min.css">
    <!-- Ionic Icons -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/ionicons.min.css">
    <!-- Main Style css -->
    <link href="<?php echo base_url() ?>assets/landing/css/style.css" rel="stylesheet" type="text/css" media="all">
    
</head>

<body>

<div class="wrapper animsition" data-animsition-in-class="fade-in" data-animsition-in-duration="1000" data-animsition-out-class="fade-out" data-animsition-out-duration="1000">

        <?php include("includes/header.php"); ?>
<div class="main" id="main">
            <!-- Main Section-->
             
<div class="feature_huge" style="background-color: rgb(240, 255, 250);">
                 
    <div class="container container">
                   
        <div class="feature_list">
       
            <div class="row" >
				
					<div class="col-md-12 col-sm-12 " >
					<center><h1>Refund Policy</h1></center>	
					</div>
				    <div class="col-md-12 col-sm-12 ">
						<p style="text-align: justify;">Returns and Refunds Policy. Thank you for shopping at ceg.spayee.com<br><br>Non-tangible irrevocable goods ("Digital products")<br><br>We do not issue refunds for non-tangible irrevocable goods ("digital products") once the order is confirmed and the product is sent.<br><br>We recommend contacting us for assistance if you experience any issues receiving or downloading our products.<br><br>Contact us for any issues:<br><br>If you have any questions about our Returns and Refunds Policy, please contact us:<br><br>- By email: help@competitiveexamguide.com</p>
			       
			        </div>
			    
			   
		    </div>
		</div>
	
	</div>
</div>


            <!-- Footer Section --><?php include("includes/footer.php")?>
            <!-- Scroll To Top -->
            <a id="back-top" class="back-to-top page-scroll" href="#main">
                <i class="ion-ios-arrow-thin-up"></i>
            </a>
            
            <!-- Scroll To Top Ends-->
        </div>
        <!-- Main Section -->
</div>
    <!-- Wrapper-->

    <!-- Jquery and Js Plugins -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/plugins.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/menu.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/custom.js"></script>
    
</body>

</html>
