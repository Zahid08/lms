<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<?php

$currentDate = date('Y-m-d');
/*==========================User Info=============================*/
$userId = $_SESSION['unique_id'];
$userData = $this->db->query("SELECT * FROM users WHERE unique_id = '$userId'  ORDER BY 1 DESC")->result();

foreach($userData as $getuserData){
$userEmail = $getuserData->email_id;
$userName = $getuserData->username;
$userMobile = $getuserData->mobile;

}
/*=================================================================*/


/*========================Course Info===============================*/
$courseId = $_GET["courseid"];
$courseData = $this->db->query("SELECT * FROM course c LEFT JOIN course_price cp ON cp.course_id = c.course_id WHERE c.status = 1 AND c.is_published = 1 AND c.course_id = '$courseId'  ORDER BY 1 DESC")->result();
foreach($courseData as $getcourseData){
$courseTitle = $getcourseData->title;
$coursePayableAmount = $getcourseData->payable_price;
}
/*===================================================================*/
if(!empty($this->session->flashdata('payable_amount'))){
    $coursePayableAmount = $this->session->flashdata('payable_amount');
}else{
    $coursePayableAmount = $getcourseData->payable_price;
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Competitive Exam Guide || Promo Code</title>
    <!-- Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="description" content="<?php echo $meta_description?>">
		<meta name="keywords" content="<?php echo $meta_keywords ?>">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/landing/logos/favicon.ico" type="image/png" sizes="16x16">
    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/landing/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,700,600" rel="stylesheet" type="text/css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animate.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.theme.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/magnific-popup.css">
    <!-- Full Page Animation -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animsition.min.css">
    <!-- Ionic Icons -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/ionicons.min.css">
    <!-- Main Style css -->
    <link href="<?php echo base_url() ?>assets/landing/css/style.css" rel="stylesheet" type="text/css" media="all">
    <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
  <style>
    .form-control {
    font-size: 14px;
    border-radius: 2px;
    border: 1px solid #ccc;
    }
  </style>
   <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
</head>

<body onload="submitPayuForm()">
<?php if($this->session->flashdata('cm_msg')){ ?>						            
	    <script>
        	$(document).ready(function(){
        		$("#myModal").modal('show');
        	});
        </script>
	    <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
        				<b style="color:green"><?php echo $this->session->flashdata('cm_msg') ?></b>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>
    <div class="wrapper animsition" data-animsition-in-class="fade-in" data-animsition-in-duration="1000" data-animsition-out-class="fade-out" data-animsition-out-duration="1000">
        <?php include("includes/header.php"); ?>
        <div class="main" id="main">
            <!-- Subscribe Form -->
            <div class="cta-sub  no-color">
                <div class="container">
                    <h1 class="wow fadeInUp" data-wow-delay="0s">Promo Code</h1>
                    <b style="color:red;"><?php echo $this->session->flashdata('pm_msg'); ?></b>
                    <div class="form wow fadeInUp" data-wow-delay="0.3s">
                        <form   method="post"  action="<?php echo base_url() ?>/Payment/applypromocode/<?php echo $courseId ?>">
                            
                            <div class="row">
                                <div class="col-sm-3">
                                    <p>&nbsp;</p>
                                </div>
                                <div class="col-sm-6">
                                    <p>Have a Promo Code</p>
                                    <input class="form-control"  type="text" name="promo_code" value="<?php echo $this->input->post('promo_code'); ?>" placeholder="Enter Promo Code" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <p>&nbsp;</p>
                                </div>
                                <div class="col-sm-1">
                                    <a class="btn btn-primary btn-action " data-wow-delay="0.2s" href="<?php echo base_url() ?>/Payment/applypromocode/<?php echo $courseId ?>">Apply</a>
                                     <!--<input class="submit" class="btn btn-primary btn-action btn-fill"  type="submit" value="Apply">-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <p>&nbsp;</p>
                                </div>
                                <div class="col-sm-6">
                                     <a href="<?php echo base_url() ?>Payment?courseid=<?php echo $courseId ?>">No, I dont have Promo Code</a>
                                </div>
                            </div>
                            
                            
                        </form>
                        
                    </div>
                  
                    
                </div>
            </div>
            <!-- Footer Section -->
            <?php include("includes/footer.php")?>
            <!-- Scroll To Top -->
            <a id="back-top" class="back-to-top page-scroll" href="#main">
                <i class="ion-ios-arrow-thin-up"></i>
            </a>
            <!-- Scroll To Top Ends-->
        </div>
        <!-- Main Section -->
    </div>
    <!-- Wrapper-->

    <!-- Jquery and Js Plugins -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/plugins.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/menu.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/custom.js"></script>
</body>

</html>
