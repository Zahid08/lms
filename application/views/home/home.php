<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<?php
$courseData = $this->db->query("SELECT * FROM course c LEFT JOIN course_price cp ON cp.course_id = c.course_id WHERE c.status = 1 AND c.is_published = 1  ORDER BY 1 DESC LIMIT 3")->result();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Competitive Exam Guide || Home</title>
    
    <!-- Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="description" content="<?php echo $meta_description?>">
		<meta name="keywords" content="<?php echo $meta_keywords ?>">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/landing/logos/favicon.ico" type="image/png" sizes="16x16">
    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/landing/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
     <link rel="icon" href="<?php echo base_url() ?>assets/icon/feather/css/feather.css">
    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,700,600" rel="stylesheet" type="text/css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animate.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.theme.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/magnific-popup.css">
    <!-- Full Page Animation -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animsition.min.css">
    <!-- Ionic Icons -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/ionicons.min.css">
    <!-- Main Style css -->
    <link href="<?php echo base_url() ?>assets/landing/css/style.css" rel="stylesheet" type="text/css" media="all">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <style>
		.star {
		visibility:hidden;
		font-size:30px;
		cursor:pointer;
		}
		.star:before {
		content: "\2605";
		position: absolute;
		visibility:visible;
		}
		.star:checked:before {
		content: "\2606";
		position: absolute;
		visibility:visible;
		}
		</style>
		<script charset="UTF-8" src="//web.webpushs.com/js/push/605fbebd410166eacf80b822f6bd800c_1.js" async></script>

</head>

<body>

    <div class="wrapper animsition" data-animsition-in-class="fade-in" data-animsition-in-duration="1000" data-animsition-out-class="fade-out" data-animsition-out-duration="1000">
        <?php include("includes/header.php"); ?>
        <div class="main" id="main">
            <!-- Main Section-->
             <div class="flex-features" id="features" >
                <div class="container">
                    <div class="flex-split">
                       
                            <div class="row" >
                            
                               <div class=" f-right ">
                                <div class="right-content">
                                <?php 
											if($this->session->flashdata('txn_status') == 'success'){ ?>
											  <b style="color:green;">Thank you for your purchase. Now You can Login and Check in My Courses to access this Course.</b></br>
											<?php }else if($this->session->flashdata('txn_status') == 'failure'){ ?>
											   <b style="color:red;">Oops!!! Something went wrong, Your Transaction Failed.</b></br>
											<?php }
											?>
											
                                <br><br><h2 class="wow fadeInUp">India's Most affordable online learning platform</h2>
                                <p class="wow fadeInUp" >
                                    for Competitive Exams
                                </p>
                                <a class="btn btn-primary btn-action" href="<?php echo base_url() ?>Store">Explore Courses</a>
                             </div> 
                             </div>
                             
                        
                                <div class="f-left " >
                                   <div class="left-content">
                                     <img class="img-fluid" src="<?php echo base_url() ?>assets/landing/images/12-01-01.png" style="width:100%;height:auto"alt="">
                                </div>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="services-section text-center" id="services">
                <!-- Services section (small) with icons -->
                <div class="container">
                    <div class="row  justify-content-md-center">
                        <div class="col-md-8">
                            <div class="services-content">
                                
                            <h1 class="default-font1 size-38" style="letter-spacing: 4px; font-family: &quot;Noto Sans&quot;, sans-serif; line-height: 45px;"><span style="color: rgb(0, 150, 102);">Welcome to </span><b style="color: rgb(0, 173, 189);"><span style="color: rgb(0, 160, 174);">Competitive</span><span style="color: rgb(0, 150, 102);"> </span><font style="color: rgb(0, 184, 201);">Exam Guide</font></b><font style="color: rgb(0, 184, 201);">&nbsp;</font></h1><p class="size-18">Association with <b style="color: rgb(40, 53, 147);">Glider's Endeavour,&nbsp;</b>&nbsp;We have Created an online educational portal which aims to motivate and guide future aspirants who are going to appear in different <b>Competitive Exams.</b> We help you to streamline your preparation but also you will be able to monitor your progress. Ultimately, our aim is to instill confidence in you so that you can excel in the real exam.<br></p>
                               
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <div class="services">
                                <div class="row">
                                    <div class="col-sm-3 wow fadeInUp">
                                           <div style="text-align: center; width: 40px; height: 40px; line-height: 40px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
							                   <i class="icon ion-android-desktop size-16" style="color: #e67e22"></i>
					                   	</div>
                                        <div class="services-description">
                                            <h1>Our Vission</h1>
                                            <p>
                                                To become the India’s most trusted Education Partner for Quality Assurance.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 wow fadeInUp" >
                                        <div style="text-align: center; width: 40px; height: 40px; line-height: 40px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
						                  	<i class="icon ion-gear-b size-16" style="color: #e67e22;"></i>
						               </div>
                                        <div class="services-description">
                                            <h1>Awards & Rewards</h1>
                                            <p>
                                                India's top 100 EdTech Start Up by "AIM Startup 2019 - Dubai - UAE
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 wow fadeInUp" >
                                        <div style="text-align: center; width: 40px; height: 40px; line-height: 40px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
							                 <i class="icon ion-paintbucket size-16" style="color: #e67e22;"></i>
					                 	</div>
                                        <div class="services-description">
                                            <h1>Best Online Courses</h1>
                                            <p>
                                                Made by Ex. NDA Officer Cadet, Younng  Dynamic IIT's, & NIT's Alumni.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 wow fadeInUp" >
                                        <div style="text-align: center; width: 40px; height: 40px; line-height: 40px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
						          	<i class="icon ion-social-buffer size-16" style="color: #e67e22;"></i>
					               	</div>
                                        <div class="services-description">
                                            <h1>Book Library</h1>
                                            <p>
                                                We have created a free book library for all Competitive Exams.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pricing-section no-color text-center" id="pricing">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 ">
                            <div class="pricing-intro">
                                 <h1>Popular Online Courses</h1><br>
                               <div class="row">
                                    
                                    <div class="col-sm-3 wow fadeInUp" >
                                        <div class="table-left wow fadeInUp" >
                                            <img src="<?php echo base_url() ?>assets/theme/NDA.jpeg" style="width: 90%; height: auto;">
                                        </div>   
                                        <div class="services-description">
                                         <br> <h1>NDA</h1>
                                         </div>   
                                       
                                    </div>
                                    
                                    <div class="col-sm-3 wow fadeInUp" >
                                        <div class="table-left wow fadeInUp" >
                                            <img src="<?php echo base_url() ?>assets/theme/CDSs.jpeg" style="width: 65%; height: auto;">
                                        </div> 
                                        <div class="services-description">
                                          <br>   <h1>CDS</h1>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-3 wow fadeInUp" >
                                        <div class="table-left wow fadeInUp" >
                                            <img src="<?php echo base_url() ?>assets/theme/UPSC.jpeg" style="width: 65%; height: auto;">
                                        </div> 
                                        <div class="services-description">
                                          <br>   <h1>UPSC</h1>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-3 wow fadeInUp" >
                                        <div class="table-left wow fadeInUp" >
                                            <img src="<?php echo base_url() ?>assets/theme/BANK.jpeg" style="width: 65%; height: auto;">
                                        </div> 
                                        <div class="services-description">
                                           <br>  <h1>BANKING</h1>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                 
               </div>
            </div>
            <div class="pricing-section no-color text-center" id="pricing">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 ">
                            <div class="pricing-intro">
                                <h1 class="wow fadeInUp" >A Passionate Team</h1>
                                <p class="wow fadeInUp" >
                                   We work as equals, celebrate as a team &amp; have fun with each other.
                                </p>
                            </div>
                            <div class="row" >
                                <div class="col-sm-4">
                                    <div >
                                        <div class="icon">
                                          <br>  <img src="<?php echo base_url() ?>assets/theme/Vishal keshri.jpg" style="width: 60%; height: auto;">
                                        </div>
                                          <br>  <h2 style="font-weight: bold;">Vishal Keshri</h2><br><p>Visiting Faculty | Symbiosis Pune</p> <br>
                                            <div style="text-align: center; width: 30px; height: 30px; line-height: 30px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
                                               <a href="https://www.facebook.com/Vishal.keshri111" target="_blank"><i class="ion-social-facebook" style="color: #e67e22"></i></a>
                                            </div>
                                            <div style="text-align: center; width: 30px; height: 30px; line-height: 30px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
                                               <a href="https://www.instagram.com/vishal.keshri1/" target="_blank"><i class="ion-social-instagram" style="color: #e67e22"></i></a>
                                            </div> 
                                            <div style="text-align: center; width: 30px; height: 30px; line-height: 30px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
                                               <a href="https://twitter.com/VishalKeshri_" target="_blank"><i class="ion-social-twitter" style="color: #e67e22"></i></a>
                                            </div>
                                             <div style="text-align: center; width: 30px; height: 30px; line-height: 30px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
                                               <a href="https://www.linkedin.com/in/vishalkeshri-keshriedutech/" target="_blank"><i class="ion-social-linkedin" style="color: #e67e22"></i></a>
                                            </div>
                                    </div>
                                </div>
                                 <div class="col-sm-4">
                                    <div  >
                                        <div class="icon">
                                          <br>  <img src="<?php echo base_url() ?>assets/theme/Ankit gupta.jpg" style="width: 60%; height: auto;">
                                        </div>
                                           <br> <h2 style="font-weight: bold;">Ankit Gupta</h2><br><p>Ex. NDA Officer Cadet</p><br>
                                            <div style="text-align: center; width: 30px; height: 30px; line-height: 30px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
                                               <a href="https://www.facebook.com/ag4india" target="_blank"><i class="ion-social-facebook" style="color: #e67e22"></i></a>
                                            </div>
                                            <div style="text-align: center; width: 30px; height: 30px; line-height: 30px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
                                               <a href="https://www.instagram.com/ag4india/" target="_blank"><i class="ion-social-instagram" style="color: #e67e22"></i></a>
                                            </div> 
                                            <div style="text-align: center; width: 30px; height: 30px; line-height: 30px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
                                               <a href="#" target="_blank"><i class="ion-social-twitter" style="color: #e67e22"></i></a>
                                            </div>
                                            <div style="text-align: center; width: 30px; height: 30px; line-height: 30px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
                                               <a href="https://www.linkedin.com/in/ankit-gupta-b16b421a9/" target="_blank"><i class="ion-social-linkedin" style="color: #e67e22"></i></a>
                                            </div>
                                    </div>
                                </div>
                                 <div class="col-sm-4">
                                    <div >
                                        <div class="icon">
                                          <br>  <img src="<?php echo base_url() ?>assets/theme/Pankaj gaur.jpg" style="width: 60%; height: auto;">
                                        </div>
                                          <br>  <h2 style="font-weight: bold;">Pankaj Gaur</h2><br>
                                            <p>Alumni of VNIT Nagpur</p><br>
                                            <div style="text-align: center; width: 30px; height: 30px; line-height: 30px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
                                               <a href="#" target="_blank"><i class="ion-social-facebook" style="color: #e67e22"></i></a>
                                            </div>
                                            <div style="text-align: center; width: 30px; height: 30px; line-height: 30px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
                                               <a href="https://www.instagram.com/pankaj.gaur277/" target="_blank"><i class="ion-social-instagram"  style="color: #e67e22"></i></a>
                                            </div> 
                                            <div style="text-align: center; width: 30px; height: 30px; line-height: 30px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
                                               <a href="#" target="_blank"><i class="ion-social-twitter" style="color: #e67e22"></i></a>
                                            </div>
                                            <div style="text-align: center; width: 30px; height: 30px; line-height: 30px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
                                               <a href="#" target="_blank"><i class="ion-social-linkedin" style="color: #e67e22"></i></a>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex-features" id="features">
                <div class="container">
                    <div class="flex-split">
                        <div class="f-left " >
                            <div class="left-content">
                               <img  src="<?php echo base_url() ?>assets/landing/images/photos .png"  alt="Card image cap" style="width:90%;heigth:90%;">
                            </div>
                        </div>
                        <div class=" f-right ">
                            <div class="right-content">
                                <h2>Personalized & Interactive Student's Dashboard</h2>
                                <p>
                                   Here is a Personalized Dashboard dedicated to each student for managing & navigating through all updates & scheduled classes
                                </p>
                                <ul>
                                    <li><i class="ion-android-checkbox-outline"></i>You can directly join the live classes, reach to previous recorded classes with options on dashboard.</li>
                                    <li><i class="ion-android-checkbox-outline"></i>All notifications relating to scheduled classes, upcoming tests, special classes will be reflected here.</li>
                                    <li><i class="ion-android-checkbox-outline"></i>You can also check your referral codes & earnings. </li>
                                    <li><i class="ion-android-checkbox-outline"></i>Dashboard automatically displays subscription expiry dates & other useful ongoing offers/packages to avail. </li>
                                </ul>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pricing-section no-color " style="background-color: rgb(255, 255, 255);" >
                <!-- Services section (small) with icons -->
                <div class="container">
                    <div class="row ">
                                <div class="col-md-6 pricing-intro">
                                <h1>OUR COURSES</h1>
                                </div>
                                <div class="col-sm-6">
					            <a class="float-right btn btn-primary btn-action"  href="<?php echo base_url() ?>Store">View Courses</a>
			                    </div>
			                <div class="col-sm-12 ">
								<div class="services">
									<div class="row" id="draggablePanelList">
										<?php
										foreach($courseData as $getcourseData){
										$courseId = $getcourseData->course_id;
										?>
										<div class="col-lg-12 col-xl-4">
										    
										    <div class="card-sub" style="border: 1px solid #a6a6a6;border-radius:5px;">
												<a href="<?php echo base_url() ?>Storedetails?courseid=<?php echo $courseId ?>">
												<img class="card-img-top img-fluid" src="<?php echo base_url() ?>assets/course_images/<?php echo $getcourseData->image; ?>" alt="Card image cap" style="max-height:200px;min-height:200px;">
												</a>
												<div class="card-block">
													<p>&nbsp;</p>
													<b style="font-weight: bold;">&nbsp;
													<?php
													if(strlen($getcourseData->title) > 40){
													echo substr($getcourseData->title, 0, 40).'...';
													}else{
													echo $getcourseData->title;
													}
													?>
													</b>
													<p>&nbsp;</p>
													<p>&nbsp;&nbsp;<?php echo $getcourseData->instructor; ?></p>
													<p>&nbsp;
														<input class="star" type="checkbox" >
														<input class="star" type="checkbox" >
														<input class="star" type="checkbox" >
														<input class="star" type="checkbox" >
														<input class="star" type="checkbox" >
														&nbsp;&nbsp;
													</p>
													<br>
													<p>&nbsp;&nbsp;<del>&#8377; <?php echo $getcourseData->original_price ?></del> <b>&#8377; <?php echo $getcourseData->payable_price ?></b></p>
												</div>
												<p>&nbsp;</p>
											</div>
											
											<p>&nbsp;</p>
										</div>
										<?php }
										?>
									</div>	
										
								</div>
									
							</div>
							

                    </div>
                </div>
            </div>    
           
           
            <!-- Counter Section -->
             
            <div class="feature_huge text-center" style="background-color: rgb(240, 255, 250);">
                 
                <div class="container container">
                  <div class="pricing-intro">
                    <h1><b style="color: rgb(40, 53, 147);" style="font-weight: bold;">Our</b>&nbsp;Features</h1>
                  </div> 
                 <div class="feature_list"> 
                  
                    <div class="row text-center ">
                        
                        <div class="col-6 col-md-3 wow fadeInUp" >
                            <div class="counter-up">
                                <div class="counter-icon">
                                    <i class="ion-android-contacts"></i>
                                </div>
                                
                                <h1>WORLD-CLASS FACULTY</h1>
                               
                                    <p>Learn from the accomplished teachers with an in-depth understanding of the subject.</p>
                                
                            </div>
                        </div>    
                        <div class="col-6 col-md-3 wow fadeInUp" >
                            <div class="counter-up">
                                <div class="counter-icon">
                                    <i class="ion-ios-color-wand"></i>
                                </div>
                                
                                <h1>CUTTING EDGE CRRICULUM</h1>
								<p>Educate yourself with the top-notch study material designed by the EXPERTS.</p>
                                
                            </div>
                        </div>
                        <div class="col-6 col-md-3 wow fadeInUp" >
                            <div class="counter-up">
                                <div class="counter-icon">
                                    <i class="ion-radio-waves"></i>
                                </div>
                                <h1>LIVE CLASSES</h1>
								<p>Learn concepts, practice questions &amp; get your doubts cleared instantly in the LIVE Classes.</p>
                            </div>
                        </div>
                        <div class="col-6 col-md-3 wow fadeInUp" >
                            <div class="counter-up">
                                <div class="counter-icon">
                                    <i class="ion-chatbubbles"></i>
                                </div>
                                <h1>STUDENT DISCUSSION FORUM</h1>
								<p>Get access to 24*7 Live Discussion group with batchmates &amp; faculties.</p>
                            </div>
                        </div>
                        <div class="col-6 col-md-3 wow fadeInUp" >
                            <div class="counter-up">
                                <div class="counter-icon">
                                    <i class="ion-ios-list-outline"></i>
                                </div>
                                <h1>QUIZ &amp; ASSIGNMENTS</h1>
								<p>Practice chapter-wise Quizzes &amp; solve Assignments to learn and revise concepts.</p>
                            </div>
                        </div>
                        <div class="col-6 col-md-3 wow fadeInUp" >
                            <div class="counter-up">
                                <div class="counter-icon">
                                    <i class="ion-videocamera"></i>
                                </div>
                                <h1>VIDEO LECTURES</h1>
								<p>Learn through high-quality &amp; easy to understand video lectures.</p>
                            </div>
                        </div>
                        <div class="col-6 col-md-3 wow fadeInUp" >
                            <div class="counter-up">
                                <div class="counter-icon">
                                    <i class="ion-ios-book"></i>
                                </div>
                                <h1>E-BOOKS</h1>
								<p>Get Important topics &amp; formulas for last-minute revision in the PDF format.</p>
                            </div>
                        </div>
                        <div class="col-6 col-md-3 wow fadeInUp" >
                            <div class="counter-up">
                                <div class="counter-icon">
                                    <i class="ion-lightbulb"></i>
                                </div>
                                <h1>ALERT &amp; NOTIFICATION</h1>
								<p>Stay up to date &amp; get notified every time the course content is updated.</p>
                            </div>
                        </div>
                        <div class="col-6 col-md-3 wow fadeInUp" >
                            <div class="counter-up">
                                <div class="counter-icon">
                                    <i class="ion-thumbsup"></i>
                                </div>
                                <h1>TRUSTED CONTENT</h1>
								<p>Learn from the comprehensive &amp; interactive course content.</p>
                            </div>
                        </div>
                        <div class="col-6 col-md-3 wow fadeInUp" >
                            <div class="counter-up">
                                <div class="counter-icon">
                                    <i class="ion-card"></i>
                                </div>
                                <h1>AFFORDABLE FEE STRUCTURE</h1>
								<p>Learn from the best in the industry with an affordable payment plan.</p>
                            </div>
                        </div>
                        <div class="col-6 col-md-3 wow fadeInUp" >
                            <div class="counter-up">
                                <div class="counter-icon">
                                    <i class="ion-ios-download-outline"></i>
                                </div>
                                <h1>ONLINE &amp; OFFLINE VIDEO LECTURES</h1>
								<p>Learn even when you are offline through our in-app video lectures.</p>
                            </div>
                        </div>
                        <div class="col-6 col-md-3 wow fadeInUp" >
                            <div class="counter-up">
                                <div class="counter-icon">
                                    <i class="ion-radio-waves"></i>
                                </div>
                                <h1>LEARN ANYTIME ANYWHERE</h1>
								<p>Learn at your own pace through our easy to navigate Android &amp; iOS App.</p>
                            </div>
                        </div>
                    </div>
                  </div>  
                </div>
            </div>
            <!-- Counter Section Ends -->
            <!-- Client Section -->
              
            <div class="feature_huge" style="background-color: rgb(255, 255, 255);">
                 
               <div class="container container">
                   
               <div class="feature_list">
				<div class="row">
					<div class="col-md-5">
						<h1><b style="color: rgb(198, 40, 40);">COMPETITIVE EXAM GUIDE</b></h1>
						<p style="border-bottom: 2px solid rgb(198, 40, 40); width: 40px; display: inline-block; margin-top: 0"></p>
						<p>CEG is an online educational portal which aims to motivate and guide future aspirants who are going to appear in different competitive exams.<br></p>
					</div>
					<div class="col-md-5">
					<h1><b style="color: rgb(198, 40, 40);">ADDRESS</b></h1>
					<p style="border-bottom: 2px solid rgb(198, 40, 40); width: 40px; display: inline-block; margin-top: 0"></p>
						
						<p>Sitai Park, Flat No. 2, DP Rd, Aundh, Pune, Maharashtra 411007, India</p>
					</div>
					<div class="col-md-2">
				     <h1><b style="color: rgb(198, 40, 40);">FOLLOW US</b></h1>
				     <p style="border-bottom: 2px solid rgb(198, 40, 40); width: 40px; display: inline-block; margin-top: 0"></p>
						<br>
						<div style="text-align: center; width: 30px; height: 30px; line-height: 30px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
                         <a href="https://twitter.com/competitiveexamguide1"target="_blank"><i class="ion-social-facebook" style="color: #e67e22"></i></a>
                        </div>
                        <div style="text-align: center; width: 30px; height: 30px; line-height: 30px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
                        <a href="https://www.facebook.com/competitiveexamguide.com1"target="_blank"><i class="ion-social-instagram" style="color: #e67e22"></i></a>
                         </div> 
                        <div style="text-align: center; width: 30px; height: 30px; line-height: 30px; border-radius: 50%; border: 2px solid #e67e22; display: inline-block;">
                        <a href="mailto:you@example.com" target="_blank"><i class="ion-social-twitter" style="color: #e67e22"></i></a>
                        </div>
                        
					</div>
					
				     <div class="row" >
				               
				                 <div class="col-6 col-md-4" >
				                 <a href="https://play.google.com/store/apps/details?id=com.ceg.courses" target="_blank" >
                               <br>  <img src="assets/landing/images/googleplay.png" style="margin: 0px; float: left; width: 200px; height: auto;" ></img></a>
                                </div>     
                                 </div>
                              
					 </div>
					 
				
				</div>
			  </div>
			  </div>
            </div>

            <!-- Footer Section -->
            <?php include("includes/footer.php")?>
            <!-- Scroll To Top -->
            <a id="back-top" class="back-to-top page-scroll" href="#main">
                <i class="ion-ios-arrow-thin-up"></i>
            </a>
            <!-- Scroll To Top Ends-->
        </div>
        <!-- Main Section -->
    </div>
    <!-- Wrapper-->

    <!-- Jquery and Js Plugins -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/plugins.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/menu.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/custom.js"></script>
</body>

</html>
