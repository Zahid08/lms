<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<?php

//echo date('Y-m-d');
$courseId = $_GET['courseid'];
$courseData = $this->db->query("SELECT * FROM course c LEFT JOIN course_price cp ON cp.course_id = c.course_id WHERE c.status = 1 AND c.is_published = 1 AND c.course_id = '$courseId'  ORDER BY 1 DESC")->result();

foreach($courseData as $getcourseData){
    $meta_description = $getcourseData->description;
}
//====================Comments====================
$comments = $this->db->query("SELECT * FROM `comments` c inner join users u on u.id = c.created_by where entitytypeid = 'Learner Comment' and entityid = $courseId and c.status = 1 ORDER BY 1 DESC ")->result();
$commentcount = count($comments);

$avgComments = $this->db->query("SELECT AVG(rating) as avg FROM `comments` c inner join users u on u.id = c.created_by where entitytypeid = 'Learner Comment' and entityid = $courseId ORDER BY 1 DESC ")->result();



foreach($avgComments as $getavgComments){
    $averageComments = $getavgComments->avg;
}

$averageComments = number_format((float)round($averageComments), 2, '.', '')
//==================================================

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Competitive Exam Guide || Store Details</title>
    <!-- Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="description" content="<?php echo $meta_description ?>">
		<meta name="keywords" content="<?php echo $meta_keywords ?>">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/landing/logos/favicon.ico" type="image/png" sizes="16x16">
    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/landing/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,700,600" rel="stylesheet" type="text/css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animate.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.theme.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/magnific-popup.css">
    <!-- Full Page Animation -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animsition.min.css">
    <!-- Ionic Icons -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/ionicons.min.css">
    <!-- Main Style css -->
    <link href="<?php echo base_url() ?>assets/landing/css/style.css" rel="stylesheet" type="text/css" media="all">
    <style>
		.star {
		visibility:hidden;
		font-size:30px;
		cursor:pointer;
		}
		.star:before {
		content: "\2605";
		position: absolute;
		visibility:visible;
		}
		.star:checked:before {
		content: "\2606";
		position: absolute;
		visibility:visible;
		}
		.i-circle {
            background: #064070;
            color: #fff;
            padding: 1px 15px;
            border-radius: 80%;
            font-size: 35px;
        }
    </style>
</head>

<body>

    <div class="wrapper animsition" >
        <?php include("includes/header.php"); ?>
        <div class="main" id="main">
            <div class="flex-features" id="features">
                <div class="container">
                    <div class="flex-split">
                        <?php
						foreach($courseData as $getcourseData){
						$courseId = $getcourseData->course_id;
						?>
                        <div class="f-left wow fadeInUp" data-wow-delay="0s">
                            <div class="left-content">
                                <img class="img-fluid" src="<?php echo base_url() ?>assets/course_images/<?php echo $getcourseData->image ?>" alt="">
                            </div>
                        </div>
                        <div class="f-right wow fadeInUp" data-wow-delay="0.2s">
                            <div class="right-content">
                                <h2><?php echo $getcourseData->title ?></h2>
                                <p>
                                    <?php echo $getcourseData->tag_line ?>
                                </p>
                                <p>
                                    <?php
                                    for($i=1;$i<= $averageComments;$i++){
                                    ?>
										<input class="star" type="checkbox" >
									<?php } ?>				
													</p>
								<p>(<?php echo $averageComments ; ?>) <?php echo $commentcount ?> ratings</p>
								<p>Instructor: <?php echo $getcourseData->instructor ?></p>
								<p>Validity Period: <?php echo $getcourseData->no_of_days .' Day(s)' ?></p>
								<p><del>&#8377; <?php echo $getcourseData->original_price ?></del> <b><?php echo $getcourseData->discount ?>% OFF</b></p>
								<h2>&#8377; <?php echo $getcourseData->payable_price ?></h2>
												
                                <a href="<?php echo base_url() ?>Payment/promocode?courseid=<?php echo $courseId ?>">
                                <button type="submit" class="btn btn-primary btn-action btn-fill">Add to Cart</button>
                                </a>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="services-section " id="services">
                <!-- Services section (small) with icons -->
                <div class="container">
                    <div class="row  justify-content-md-center">
                        <div class="col-md-8">
                            <div class="services-content">
                                <h1 class="wow fadeInUp" data-wow-delay="0s">Description</h1>
                                <p class="wow fadeInUp" data-wow-delay="0.2s">
                                    <?php echo $getcourseData->description ?>
                                </p>
                                <hr>
                                <h1 class="wow fadeInUp" data-wow-delay="0s">How to Use</h1>
                                <p class="wow fadeInUp" data-wow-delay="0.2s">
                                    <?php echo $getcourseData->how_to_use ?>
                                </p>
                                <hr>
                                <h1 class="wow fadeInUp" data-wow-delay="0s">Comments</h1>
                                <p class="wow fadeInUp" data-wow-delay="0.2s">
                                    <?php
                                        foreach($comments as $getcomments){
                                            $learnerName = $getcomments->username;
                                            $learnerComment = $getcomments->comment;
                                            $rating = $getcomments->rating;
                                            $commentDate = $getcomments->created_date;
                                            ?>
                                            <h2><span class="i-circle"><?php echo ucfirst(substr($learnerName, 0, 1));?></span>&nbsp;&nbsp;&nbsp;&nbsp;<strong><?php echo $learnerName ?></strong></h2>
                                            <br>
                                            <p><?php echo $learnerComment ?></p>
                                            <?php
                                            for($i=1;$i<=$rating;$i++){ ?>
                                              <input class="star" type="checkbox" >
                                            <?php  } ?>
                                             <p><br>Commented on: <?php echo $commentDate ?></p>
                                            <hr>
                                            <?php
                                        }
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- Footer Section -->
            <?php include("includes/footer.php")?>
            <!-- Scroll To Top -->
            <a id="back-top" class="back-to-top page-scroll" href="#main">
                <i class="ion-ios-arrow-thin-up"></i>
            </a>
            <!-- Scroll To Top Ends-->
        </div>
        <!-- Main Section -->
    </div>
    <!-- Wrapper-->

    <!-- Jquery and Js Plugins -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/plugins.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/menu.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/custom.js"></script>
</body>

</html>
