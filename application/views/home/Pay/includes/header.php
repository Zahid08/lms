<div class="container">
             <nav class="navbar navbar-expand-lg navbar-light navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <a class="navbar-brand page-scroll" href="#main"><img src="<?php echo base_url() ?>assets/landing/logos/competetiveexamguidelogo.png" alt="adminity Logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                        </ul>
                        <ul class="navbar-nav my-2 my-lg-0">
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="#main">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="<?php echo base_url() ?>Store">Courses</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="#features">Blog</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="#reviews">Faculty Login</a>
                            </li>
                            <?php 
                            if($_SESSION['role'] == 3){ ?>
                                <li class="nav-item">
                                    <a class="nav-link page-scroll" href="#pricing">My Courses</a>
                                </li>
                            <?php
                            }
                            ?>
                            <?php 
                            if($_SESSION['role'] == 2){ ?>
                                <li class="nav-item">
                                    <a class="nav-link page-scroll" href="#">Dashboard</a>
                                </li>
                            <?php
                            }
                            ?>
                            <?php 
                            if($_SESSION['role'] == 1){ ?>
                                <li class="nav-item">
                                    <a class="nav-link page-scroll" href="Dashboard">Dashboard</a>
                                </li>
                            <?php
                            }
                            ?>
                            <?php
                            if($_SESSION['email'] <> NULL || $_SESSION['email'] <> ''){
                            ?>
                            <li class="nav-item">
                                <a class="btn btn-primary btn-action text-white" data-wow-delay="0.2s" href="<?php echo base_url() ?>Login/logoutuser">Logout</a>
                            </li>
                            <?php 
                            }else{
                               ?>
                            <li class="nav-item">
                                <a class="btn btn-primary btn-action text-white" data-wow-delay="0.2s" href="<?php echo base_url() ?>Login">Login</a>
                            </li>
                            <?php 
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>