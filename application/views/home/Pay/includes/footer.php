<div class="footer">
                <div class="container">
                    <div class="col-md-12 text-center">
                        <img src="<?php echo base_url() ?>assets/landing/logos/competetiveexamguidelogo.png" alt="Adminty Logo">
                        <ul class="footer-menu">
                            <li><a href="http://demo.com">Site</a></li>
                            <li><a href="#">Support</a></li>
                            <li><a href="#">Terms</a></li>
                            <li><a href="#">Privacy</a></li>
                        </ul>
                        <div class="footer-text">
                            <p>
                                Copyright © 2017 Adminty. All Rights Reserved.
                            </p>
                        </div>
                    </div>
                </div>
            </div>