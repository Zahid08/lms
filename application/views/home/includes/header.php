<style>
.navbar.past-main {
    height:80px;
}
</style>
<div  class="container " class="header spectre fixed">
             <nav class="navbar navbar-expand-lg navbar-light navbar-default navbar-fixed-top" role="navigation" >
                <div class="container conainer">
                    
                  <a class="navbar-brand " href="#main"><img src="<?php echo base_url() ?>assets/landing/logos/competetiveexamguidelogo.png" alt="Comptetive Exam Guide Logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <section class="navbar-section" style="background-color:#f47443;" >
		  	            
		                </section>
                        <ul class="navbar-nav mr-auto">
                            
                        </ul>
                        <ul class="navbar-nav my-2 my-lg-0">
                            <li class="nav-item">
                                <form class="input-group p-0" action="<?php echo base_url() ?>Courses" method="GET">
        		  	           	<div class="input-group" >
        				            <input type="text" name="query" class="nav-link page-scroll" placeholder="Search" style="border:.05rem solid #47acd1;width: 300px;">	
        				        </div>
        			            </form>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="<?php echo base_url() ?>Home">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link page-scroll" href="<?php echo base_url() ?>Store">Courses</a>
                            </li>
                            
                            
                            <?php 
                            if($_SESSION['role'] == 3){ ?>
                                <li class="nav-item">
                                    <a class="nav-link page-scroll" href="LCourses">My Courses</a>
                                </li>
                            <?php
                            }
                            ?>
                            <?php 
                            if($_SESSION['role'] == 2){ ?>
                                <li class="nav-item">
                                    <a class="nav-link page-scroll" href="ICourses">My Courses</a>
                                </li>
                            <?php
                            }
                            ?>
                            <?php 
                            if($_SESSION['role'] == 1){ ?>
                                <li class="nav-item">
                                    <a class="nav-link page-scroll" href="Dashboard">Dashboard</a>
                                </li>
                            <?php
                            }
                            ?>
                            <?php
                            if($_SESSION['email'] <> NULL || $_SESSION['email'] <> ''){
                            ?>
                            <li class="nav-item">
                                <a class="btn btn-primary btn-action text-white" data-wow-delay="0.2s" href="<?php echo base_url() ?>Login/logoutuser">Logout</a>
                            </li>
                            <?php 
                            }else{
                               ?>
                            <li class="nav-item">
                                <a class="btn btn-primary btn-action text-white"  data-wow-delay="0.2s" href="<?php echo base_url() ?>Login">Login</a>
                            </li>
                            <?php 
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>