<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Competitive Exam Guide || Home</title>
    <!-- Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $meta_description?>">
		<meta name="keywords" content="<?php echo $meta_keywords ?>">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/landing/logos/favicon.ico" type="image/png" sizes="16x16">
    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/landing/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,700,600" rel="stylesheet" type="text/css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animate.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.theme.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/magnific-popup.css">
    <!-- Full Page Animation -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animsition.min.css">
    <!-- Ionic Icons -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/ionicons.min.css">
    <!-- Main Style css -->
    <link href="<?php echo base_url() ?>assets/landing/css/style.css" rel="stylesheet" type="text/css" media="all">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
</head>

<body>
    <?php if($this->session->flashdata('rd_msg')){ ?>						            
	    <script>
        	$(document).ready(function(){
        		$("#myModal").modal('show');
        	});
        </script>
	    <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
        				<b style="color:green"><?php echo $this->session->flashdata('rd_msg') ?></b>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>

<div class="wrapper animsition" data-animsition-in-class="fade-in" data-animsition-in-duration="1000" data-animsition-out-class="fade-out" data-animsition-out-duration="1000">

<?php include("includes/header.php"); ?>
<div class="main" id="main">
            <!-- Main Section-->
             
<div class="feature_huge" style="background-color: rgb(240, 255, 250);">
                 
    <div class="container container">
                   
        <div class="feature_list">
       
            <div class="row justify-content-md-center" >
				
				<div class="col-sm-6">
					<center><h1>CONTACT US</h1></center>
					<center><p>Please fill out the form below to reach us anytime.</p></center>
			       
                    <form  method="post" action="<?php echo base_url(); ?>Contactus/contactUser">
                        
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-primary">
                                            <input type="text" name="username" class="form-control" required="" placeholder="Choose Username">
                                            <span class="form-bar"></span>
                                        </div>
                                    </div>
                                </div>    
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-primary">
                                            <input type="email" name="email" class="form-control" required="" placeholder="Your Email Address">
                                            <span class="form-bar"></span>
                                        </div>
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-primary">
                                            <input type="text" name="mob" pattern="[0-9]{10,10}" minlength="10" maxlength="10" class="form-control" required="" placeholder="Mobile">
                                            <span class="form-bar"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-primary">
                                            <textarea class="form-control max-textarea"  name="message" id="message" maxlength="500" rows="4" placeholder="Type Your Message Here..." required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="form-group form-primary">
                                           <button type="submit" class="float-right btn btn-primary btn-action ">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            
                    </form>
                </div>    
		    </div>
	    </div>
	
   </div>
</div> 
<div class="feature_huge text-center" style="background-color: rgb(255, 255, 255);">
                 
    <div class="container container">
                   
        <div class="feature_list">
				<div class="row">
					<div class="col-md-4">
						<h2 style="font-weight: bold;" >CONTACT US</h2>
					
						<p>Competitive Exam Guide</b><br>Sitai Park, Flat No. 2, DP Rd, Aundh, Pune, Maharashtra 411007, India</p>
					</div>
					<div class="col-md-4">
						<h2 style="font-weight: bold;" >OPENING HOURS</h2>
						<p >
							Monday - Saturday: 9:00 AM - 10:00 PM<br>Sunday: 08:00 AM - 02:00 PM <br>Phone Number:9028 - 300 - 404</p>
					</div>
					<div class="col-md-4">
						<h2 style="font-weight: bold;" >STAY UPDATED</h2>
					
						<p>
							Follow us on:<br>Facebook: <a href="#">@</a>competitiveexamguide1<br>
							Twitter: <a href="#">@competitiveexamguide1</a></p>
					</div>
				</div>
	    </div>
	</div>	
</div>




            <!-- Footer Section --><?php include("includes/footer.php")?>
            <!-- Scroll To Top -->
            <a id="back-top" class="back-to-top page-scroll" href="#main">
                <i class="ion-ios-arrow-thin-up"></i>
            </a>
            
            <!-- Scroll To Top Ends-->
        </div>
        <!-- Main Section -->
</div>
    <!-- Wrapper-->

    <!-- Jquery and Js Plugins -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/jquery-2.1.1.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/plugins.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/menu.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/custom.js"></script>
    
</body>

</html>
