<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<?php
$searchCriteria = $_GET['query'];
if(!empty($searchCriteria)){
  $courseData = $this->db->query("SELECT * FROM course c LEFT JOIN course_price cp ON cp.course_id = c.course_id WHERE c.status = 1 AND c.is_published = 1 AND category like '%$searchCriteria%'  ORDER BY 1 DESC")->result();
  
}else{
    $courseData = $this->db->query("SELECT * FROM course c LEFT JOIN course_price cp ON cp.course_id = c.course_id WHERE c.status = 1 AND c.is_published = 1  ORDER BY 1 DESC")->result();
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Competitive Exam Guide || Store</title>
		<!-- Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta name="description" content="<?php echo $meta_description?>">
		<meta name="keywords" content="<?php echo $meta_keywords ?>">
		<!-- Favicon icon -->
		<link rel="icon" href="<?php echo base_url() ?>assets/landing/logos/favicon.ico" type="image/png" sizes="16x16">
		<!-- Bootstrap -->
		<link href="<?php echo base_url() ?>assets/landing/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
		<!-- Font -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,700,600" rel="stylesheet" type="text/css">
		<!-- Animate CSS -->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animate.css">
		<!-- Owl Carousel -->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.carousel.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/owl.theme.css">
		<!-- Magnific Popup -->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/magnific-popup.css">
		<!-- Full Page Animation -->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/animsition.min.css">
		<!-- Ionic Icons -->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/landing/css/ionicons.min.css">
		<!-- Main Style css -->
		<link href="<?php echo base_url() ?>assets/landing/css/style.css" rel="stylesheet" type="text/css" media="all">
		<style>
		.star {
		visibility:hidden;
		font-size:30px;
		cursor:pointer;
		}
		.star:before {
		content: "\2605";
		position: absolute;
		visibility:visible;
		}
		.star:checked:before {
		content: "\2606";
		position: absolute;
		visibility:visible;
		}
		</style>
	</head>
	<body>
		<div class="wrapper animsition" data-animsition-in-class="fade-in" data-animsition-in-duration="1000" data-animsition-out-class="fade-out" data-animsition-out-duration="1000">
			<?php include("includes/header.php"); ?>
			<div class="main" id="main">
				<div class="services-section" id="services">
					<!-- Services section (small) with icons -->
					<div class="container">
						<div class="row  justify-content-md-center">
							<div class="col-md-8">
								<div class="services-content">
									<h1 class="wow fadeInUp" data-wow-delay="0s">We take care our products for more feature rich</h1>
									<p class="wow fadeInUp" data-wow-delay="0.2s">
										Adminty is one of the finest Admin dashboard template in its category. Premium admin dashboard with high end feature rich possibilities.
									</p>
								</div>
							</div>
							<div class="col-md-12 ">
								<div class="services">
									<div class="row" id="draggablePanelList">
										<?php
										foreach($courseData as $getcourseData){
										$courseId = $getcourseData->course_id;
										?>
										<div class="col-lg-12 col-xl-4">
										    
										        <div class="card-sub" style="border: 1px solid #0073aa;border-radius:5px;">
												<a href="<?php echo base_url() ?>Storedetails?courseid=<?php echo $courseId ?>">
												<img class="card-img-top img-fluid" src="<?php echo base_url() ?>assets/course_images/<?php echo $getcourseData->image; ?>" alt="Card image cap" style="max-height:200px;min-height:200px;">
												</a>
												<div class="card-block">
													<p>&nbsp;</p>
													<b style="font-weight: bold;">&nbsp;
													<?php
													if(strlen($getcourseData->title) > 40){
													echo substr($getcourseData->title, 0, 40).'...';
													}else{
													echo $getcourseData->title;
													}
													?>
													</b>
													<p>&nbsp;</p>
													<p>&nbsp;&nbsp;<?php echo $getcourseData->instructor; ?></p>
													<p>&nbsp;
														<input class="star" type="checkbox" >
														<input class="star" type="checkbox" >
														<input class="star" type="checkbox" >
														<input class="star" type="checkbox" >
														<input class="star" type="checkbox" >
														&nbsp;&nbsp;
													</p>
													<br>
													<p>&nbsp;&nbsp;<del>&#8377; <?php echo $getcourseData->original_price ?></del> <b>&#8377; <?php echo $getcourseData->payable_price ?></b></p>
												</div>
												<p>&nbsp;</p>
											</div>
											
											<p>&nbsp;</p>
										</div>
										<?php }
										?>
										
										
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Footer Section -->
				<?php include("includes/footer.php")?>
				<!-- Scroll To Top -->
				<a id="back-top" class="back-to-top page-scroll" href="#main">
					<i class="ion-ios-arrow-thin-up"></i>
				</a>
				<!-- Scroll To Top Ends-->
			</div>
			<!-- Main Section -->
		</div>
		<!-- Wrapper-->
		<!-- Jquery and Js Plugins -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/jquery-2.1.1.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/plugins.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/menu.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/landing/js/custom.js"></script>
	</body>
</html>