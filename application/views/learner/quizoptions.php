<?php
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}

?>
<?php
date_default_timezone_set("Asia/Calcutta");

$CourseId = $_GET['courseid'];
$courseData = $this->db->query("SELECT * FROM course WHERE status = 1 AND is_published = 1 AND course_id = '$CourseId'  ORDER BY 1 DESC")->result();
foreach($courseData as $getcourseData){
    $courseTitle = $getcourseData->title;
    $courseImage = $getcourseData->image;
}
$user =$this->db->query("SELECT * FROM users")->result();
foreach($user as $getuser){
    $userlastlogin = $getuser->last_login;

}
$chapterId=$_GET['chapterid'];
$ItemId = $_GET['itemid'];
$chapterItems_1 = $this->db->query("SELECT * FROM sub_chapters WHERE status = 1  AND course_id = '$CourseId'  AND id = '$ItemId' ORDER BY 1 ASC")->result();

foreach($chapterItems_1 as $getchapterItems_1){
    $subChapterId = $getchapterItems_1->id;
    $subChapterName = $getchapterItems_1->subchapter_name;
    $subChapterType = $getchapterItems_1->type;
    $subChaptersubType = $getchapterItems_1->sub_type;
    $subChapterpath = $getchapterItems_1->path;
    $subChapterFrom=$getchapterItems_1->available_from;
    $subChapterDate=$getchapterItems_1->available_date;
    $subChapterTime=date('H:i', strtotime($getchapterItems_1->available_time));
    $subchapter=$getchapterItems_1->chapterid;
    $subchapterpostmsg=$getchapterItems_1->post_class_msg;
    $subchapterpremsg=$getchapterItems_1->pre_class_msg;
    $endSession=$getchapterItems_1->end_session;

}
$subChapterData = $this->db->query("SELECT * FROM live_class_recordings WHERE status = 1  AND sub_chapter_id= '$subChapterId' ORDER BY 1 DESC")->result();
foreach($subChapterData as $getsubChapterData){
    $existinglink = $getsubChapterData->link;

}

$CourseId = $_GET['courseid'];
$quizTestid =$_GET['id'];

$quiztest = $this->db->query("SELECT * FROM quiz_test WHERE status = 1 AND course_id = '$CourseId' AND id='$quizTestid' ORDER BY 1 DESC")->result();
foreach($quiztest as $getquiztest){
    $quizName = $getquiztest->title;
    $time_limit = $getquiztest->time_limit;
    $re_take_attempts = $getquiztest->re_take_attempts;
    $is_instructions = $getquiztest->is_instructions;
    $instructions = $getquiztest->instructions;
    $available_from_date =date('Y-m-d', strtotime($getquiztest->available_from ));
    $available_from_time =date('h:i:s a', strtotime($getquiztest->available_from ));
    $available_till_date =date('Y-m-d', strtotime($getquiztest->available_till));
    $available_till_time =date('h:i:s a', strtotime($getquiztest->available_till));
}

$countTotalQuestions = $this->db->where('status', 1)->where('quiz_test_id', $quizTestid)->count_all_results('quiz_test_questions');


$getAllAttempts         =$this->db->query("SELECT * FROM quiz_test_attempt WHERE quiztest_id = ".$_GET['id']." and user_id = ".$_SESSION['user_id']." ORDER BY id DESC ")->result();
$getTotalAttempt=count($getAllAttempts);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Competitive Exam Guide || Course Details</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo $meta_description?>">
    <meta name="keywords" content="<?php echo $meta_keywords ?>">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
    <!-- Color Picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
    <!-- Mini-color css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
    <!-- sweet alert framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/sweetalert/css/sweetalert.css">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <style>
        .blink_me {
            animation: blinker 1s linear infinite;
            color: red;
        }

        @keyframes blinker {
            80% {
                opacity: 0;
            }
        }

        span.page-link {
            width: 42px;
            float: left;
            border-radius: 11px;
            margin-right: 10px;
            margin-top: -6px;
        }

        .modal-content {
            max-height: 500px;
            overflow-y: scroll;
        }
        .modal-content {
            animation: show .3s;
            transform: scale(1);
        }

        @keyframes show {
            from {

                transform: scale(0);
                opacity: 0;
                z-index: -1;
            }
            to {

                transform: scale(1);
                opacity: 1;
                z-index: 2;
            }
        }

    </style>
</head>
<body>
<?php if($this->session->flashdata('cm_msg')){ ?>
    <script>
        $(document).ready(function(){
            $("#myModal").modal('show');
        });
    </script>
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Message</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <b style="color:green"><?php echo $this->session->flashdata('cm_msg') ?></b>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
        <?php include("includes/header.php"); ?>
        <!-- Sidebar inner chat end-->
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <?php include("includes/coursesidenav.php"); ?>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- Page-header start -->
                                <div class="page-header">
                                    <div class="row align-items-end">
                                        <div class="col-lg-10">
                                            <div class="page-header-title">
                                                <div class="d-inline">
                                                    <h4><?php echo $courseTitle ?></h4>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <!-- Page-header end -->
                                <!-- Page-body start -->
                                <div class="page-body">
                                    <div  class=""id="questions">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <!-- HTML5 Export Buttons table start -->
                                                <div class="card" style="background: #88cae1;text-align: center">
                                                    <?php
                                                    if ($getTotalAttempt>0){ ?>
                                                        <a style="text-align: left;" href="<?php echo base_url() ?>CourseLearners/attemptdashboard?courseid=<?=$_GET['courseid']?>&id=<?=$_GET['id']?>">
                                                            <span class="pcoded-micon"><i class=" feather icon-arrow-left"></i></span>
                                                            <span class="pcoded-mtext">Back To Dashoard</span>
                                                        </a>
                                                    <?php  }
                                                    ?>
                                                    <div class="card-block">
                                                        <div class="dt-responsive table-responsive">
                                                            <h2><?=$quizName?></h2>
                                                            <h3>Total Questions : <?=$countTotalQuestions?></h3>
                                                            <h3>Total Time : <?=$time_limit?> miniutes</h3>
                                                            <h5 style="margin-bottom: 20px;" class="blink_me">Quiz available From : <?=$available_from_date.' From '.$available_from_time?> to <?=$available_till_date.' till '.$available_till_time;?></h5>
                                                            <?php
                                                            if ($re_take_attempts>$getTotalAttempt){  ?>
                                                                <button type="button" name="startQuiz" id="startQuizLearners" data-toggle="modal" data-target="#startQuizLearners_modal"  class="btn btn-primary btn-outline-primary "><i class=" feather icon-down"></i>Start Quiz</button>
                                                            <?php  }else{ ?>
                                                                <button type="button" name="startQuiz"  class="btn btn-primary btn-outline-primary "><i class=" feather icon-down"></i>Quiz Attempt Over</button>
                                                            <?php } ?>
                                                            <?php
                                                            if ($getTotalAttempt>0){ ?>
                                                                <a href="<?php echo base_url() ?>CourseLearners/attemptdashboard?courseid=<?=$_GET['courseid']?>&id=<?=$_GET['id']?>">
                                                                    <button type="button" name="startQuiz"  class="btn btn-primary btn-outline-primary "><i class=" feather icon-down"></i>View Solutions</button>
                                                                </a>
                                                            <?php  }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- HTML5 Export Buttons end -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Page-body end -->
                            </div>
                        </div>
                    </div>
                    <!-- Main-body end -->
                    <!-- <div id="styleSelector">
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>

<!--==========================Start Quiz===============================================-->
<div class="modal fade" id="startQuizLearners_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <lable> <h4 class="modal-title">Instrucations For Given Quiz</h4></lable>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card-block">
                <div class="dt-responsive table-responsive" style="margin-bottom: 22px;">
                    <a href="#" class="list-group-item list-group-item-action active">
                        General Instructions
                    </a>
                </div>
                <div class="list-group">
                    <p>1.The Questions Palette displayed on the right side of screen will show the status of each question one of the following sysmbols.</p>
                    <a href="#" class="list-group-item list-group-item-action"><span class="page-link" style="background-color: #80808066;color: white;">1</span> You have not visited question yet.</a>
                    <a href="#" class="list-group-item list-group-item-action"><span class="page-link" style="background: red;color: white;">2</span> You have not answer question yet.</a>
                    <a href="#" class="list-group-item list-group-item-action"><span class="page-link" style="background: #146c43;color: white;">3</span> You have answer question .</a>
                    <a href="#" class="list-group-item list-group-item-action"><span class="page-link" style="background: #563d7c;color: white;">4</span> You have not answer the question , but the questions for review.</a>
                    <a href="#" class="list-group-item list-group-item-action"><span class="page-link"style="background: #065fd4a8;color: white;">5</span> You have answer the question , but marked it for review.</a>

                    <p style="margin-top: 14px;">2.The Questions Palette displayed on the right side of screen will show the status of each question one of the following sysmbols.</p>

                    <h3>Navigate To Questions</h3>
                    <p style="margin-top: 14px;">a.The Questions Palette displayed on the right side of screen will show the status of each question one of the following sysmbols.</p>
                    <p style="margin-top: 14px;">b.The Questions Palette displayed on the right side of screen will show the status of each question one of the following sysmbols.</p>
                    <p style="margin-top: 14px;">c.The Questions Palette displayed on the right side of screen will show the status of each question one of the following sysmbols.</p>
                    <p style="margin-top: 14px;">d.The Questions Palette displayed on the right side of screen will show the status of each question one of the following sysmbols.</p>
                    <?php
                    if ($is_instructions!=1){
                        ?>
                        <div class="dt-responsive table-responsive" style="margin-bottom: 22px;">
                            <a href="#" class="list-group-item list-group-item-action active">
                                Other Important Instructions
                            </a>
                        </div>
                        <?=$instructions?>
                    <?php }?>

                </div>
                <div class="modal-footer">
                    <input type="hidden" id="quizTestId" value="<?=$_GET['id']?>">
                    <input type="hidden" id="courseId"   value="<?=$_GET['courseid']?>">
                    <button type="button" class="btn btn-primary btn-outline-primary" id="startQuiz" style="margin-right: 250px;">Start Quiz</button>
                    <button type="button" class="btn btn-secondary btn-outline-secondary " data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Recorded Vdo Link -->

<!-- Warning Section Starts -->
<!-- Older IE warning message -->

<!-- Warning Section Ends -->
<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
<!-- data-table js -->
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<!-- Bootstrap date-time-picker js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
<!-- Date-range picker js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
<!-- Date-dropper js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
<!-- Color picker js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
<!-- Mini-color js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
<!-- i18next.min.js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
<!-- Custom js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>

<script src="<?php echo base_url() ?>assets/files/assets/pages/sortable-custom.js"></script>

<!-- sweet alert js -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- sweet alert modal.js intialize js -->
<!-- modalEffects js nifty modal window effects -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/modalEffects.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/classie.js"></script>
<!-- Date-range picker js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
    $(document).ready(function(){
        $("#startQuiz").on("click", function()
        {
            var locations='<?php echo base_url() ?>/CourseLearners/quizstart?courseid=<?php echo $_GET['courseid']?>&id=<?php echo $_GET['id']?>';
            window.location.href =locations;
            //document.fullScreenElement && null !== document.fullScreenElement || !document.mozFullScreen && !document.webkitIsFullScreen ? document.documentElement.requestFullScreen ? document.documentElement.requestFullScreen() : document.documentElement.mozRequestFullScreen ? document.documentElement.mozRequestFullScreen() : document.documentElement.webkitRequestFullScreen && document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT) : document.cancelFullScreen ? document.cancelFullScreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitCancelFullScreen && document.webkitCancelFullScreen()
        });
    });

</script>
</body>
</html>