<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<?php
$user_info = $this->session->userdata($session_data);
	    $id = $user_info['user_id'];

    $courseData = $this->db->query("SELECT * FROM course c INNER JOIN transactions t ON t.course_id = c.course_id INNER JOIN users u ON u.id = t.user_id OR u.unique_id = t.user_id  WHERE c.status = 1 AND c.is_published = 1 AND u.id = '".$id."'  ORDER BY 1 DESC")->result();

    
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Competitive Exam Guide || Courses </title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="<?php echo $meta_description?>">
		<meta name="keywords" content="<?php echo $meta_keywords ?>">
		<meta name="author" content="#">
		<!-- Favicon icon -->
		<link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
		<!-- Google font-->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
		<!-- Required Fremwork -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
		<!-- themify-icons line icon -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
		<!-- ico font -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
		<!-- feather Awesome -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
		<!-- Date-time picker css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
		<!-- Date-range picker css  -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
		<!-- Date-Dropper css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
		<!-- Color Picker css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
		<!-- Mini-color css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
		<!-- sweet alert framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/sweetalert/css/sweetalert.css">
		<!-- Data Table Css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
		<!-- Style.css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
	</head>
	<body>
		<!-- Pre-loader start -->
		<div class="theme-loader">
			<div class="ball-scale">
				<div class='contain'>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Pre-loader end -->
		<div id="pcoded" class="pcoded">
			<div class="pcoded-overlay-box"></div>
			<div class="pcoded-container navbar-wrapper">
				<?php include("includes/header.php"); ?>
				<!-- Sidebar inner chat end-->
				<div class="pcoded-main-container">
					<div class="pcoded-wrapper">
						<?php include("includes/sidenav.php"); ?>
						<div class="pcoded-content">
							<div class="pcoded-inner-content">
								<!-- Main-body start -->
								<div class="main-body">
									<div class="page-wrapper">
										<!-- Page-header start -->
										<div class="page-header">
											<div class="row align-items-end">
												<div class="col-lg-10">
													<div class="page-header-title">
														<div class="d-inline">
															<h4>Courses</h4>
															<span><?php echo count($courseData) ?> Course(s)</span>
														</div>
													</div>
												</div>
											
												
											</div>
										</div>
										<!-- Page-header end -->
										<!-- Page-body start -->
										<div class="page-body">
											
											
												<div class="row" id="draggablePanelList">
												    <?php
												    if(count($courseData) == 0){ ?>
												        <center><b>No Courses Pucharched Yet.</b></center>
												    <?php }
												    ?>
												    <?php 
												    foreach($courseData as $getcourseData){
												    $courseId = $getcourseData->course_id;
												    
												    
                                                    $courseSubChapters = $this->db->query("SELECT * 
                                                                                        FROM sub_chapters 
                                                                                        WHERE status = 1  
                                                                                        AND course_id = '$courseId'  
                                                                                        ORDER BY 1 DESC LIMIT 1")->result();
                                                                                        
                                                    foreach($courseSubChapters as $getcourseSubChapters){
                                                      $ItemId = $getcourseSubChapters->id;
                                                      
                                                      
                                                    }

												    
												    $Title = $getcourseData->title; 
												    $Title_length = strlen($Title);
												    
												    if($Title_length > 30){
												        $Title = substr($Title, 0, 30)."...";
												    }
												    ?>
                                                        <div class="col-lg-12 col-xl-4">
                                                            <a href="<?php echo base_url() ?>CourseLearners?courseid=<?php echo $courseId ?>&itemid=<?php echo $ItemId?>" >
    														    <div class="card-sub">
    															<img class="card-img-top img-fluid" src="<?php echo base_url() ?>assets/course_images/<?php echo $getcourseData->image; ?>" alt="Card image cap" style="max-height:200px;min-height:200px;">
    															<div class="card-block">
    																<h5 class="card-title" data-toggle="tooltip" data-placement="top" title="<?php echo $getcourseData->title; ?>">
    																    <?php
                        													if(strlen($getcourseData->title) > 25){
                        													echo substr($getcourseData->title, 0, 25).'...';
                        													}else{
                        													echo $getcourseData->title;
                        													}
                        												?>
    																</h5>
    																<p class="card-text">Instructor: <?php echo $getcourseData->username ?></p>
    																<p class="card-text">
    																   
    																</p>
    																
    																<span><?php echo $getcourseData->created_date ?></span>
    															</div>
    															<p>&nbsp;</p>
    														</div>
    														</a>
    													</div>
                                                    <?php } ?>
													
												
												</div>
											
										</div>
										<!-- Page-body end -->
									</div>
								</div>
							</div>
							<!-- Main-body end -->
							<!-- <div id="styleSelector">
							</div>-->
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Warning Section Starts -->
		<!-- Older IE warning message -->
		
		<!-- Warning Section Ends -->
		<!-- Required Jquery -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
		<!-- jquery slimscroll js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
		<!-- modernizr js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
		<!-- data-table js -->
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
		<!-- Bootstrap date-time-picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
		<!-- Date-range picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
		<!-- Date-dropper js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
		<!-- Color picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
		<!-- Mini-color js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
		<!-- i18next.min.js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
		<!-- Custom js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
		
		<script src="<?php echo base_url() ?>assets/files/assets/pages/sortable-custom.js"></script>
		
        <!-- sweet alert js -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- sweet alert modal.js intialize js -->
        <!-- modalEffects js nifty modal window effects -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/modalEffects.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/classie.js"></script>
		<!-- Date-range picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-23581568-13');
		</script>
		<script>
		    function alert_msg(Id){
		        swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this Course again!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    /*swal("Poof! Your Course has been deleted!", {
                      icon: "success",
                    });*/
                    location.href = '<?php echo base_url() ?>CourseInfo/deletecourse?courseid='+Id;
                  } else {
                    swal("Your Course is safe!");
                  }
                });
		    }
		</script>
	</body>
</html>