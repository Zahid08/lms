<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<?php
 date_default_timezone_set("Asia/Calcutta"); 

$CourseId = $_GET['courseid'];
$courseData = $this->db->query("SELECT * FROM course WHERE status = 1 AND is_published = 1 AND course_id = '$CourseId'  ORDER BY 1 DESC")->result();
foreach($courseData as $getcourseData){
	$courseTitle = $getcourseData->title;
	$courseImage = $getcourseData->image;
}
$user =$this->db->query("SELECT * FROM users")->result();
foreach($user as $getuser){
	$userlastlogin = $getuser->last_login;
	
}
$chapterId=$_GET['chapterid'];
$ItemId = $_GET['itemid'];
$chapterItems_1 = $this->db->query("SELECT * FROM sub_chapters WHERE status = 1  AND course_id = '$CourseId'  AND id = '$ItemId' ORDER BY 1 ASC")->result();
                                     
                                        foreach($chapterItems_1 as $getchapterItems_1){ 
                                        $subChapterId = $getchapterItems_1->id;    
                                        $subChapterName = $getchapterItems_1->subchapter_name;  
                                        $subChapterType = $getchapterItems_1->type;
                                        $subChaptersubType = $getchapterItems_1->sub_type;
                                        $subChapterpath = $getchapterItems_1->path;
                                        $subChapterFrom=$getchapterItems_1->available_from;
                                        $subChapterDate=$getchapterItems_1->available_date;
                                         $subChapterTime=date('H:i', strtotime($getchapterItems_1->available_time));
                                        $subchapter=$getchapterItems_1->chapterid;
                                        $subchapterpostmsg=$getchapterItems_1->post_class_msg;
                                        $subchapterpremsg=$getchapterItems_1->pre_class_msg;
                                    $endSession=$getchapterItems_1->end_session;
                           
                                        }
 $subChapterData = $this->db->query("SELECT * FROM live_class_recordings WHERE status = 1  AND sub_chapter_id= '$subChapterId' ORDER BY 1 DESC")->result();
foreach($subChapterData as $getsubChapterData){
$existinglink = $getsubChapterData->link;
    
}                                    
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Competitive Exam Guide || Course Details</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="description" content="<?php echo $meta_description?>">
		<meta name="keywords" content="<?php echo $meta_keywords ?>">
		<meta name="author" content="#">
		<!-- Favicon icon -->
		<link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
		<!-- Google font-->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
		<!-- Required Fremwork -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
		<!-- themify-icons line icon -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
		<!-- ico font -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
		<!-- feather Awesome -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
		<!-- Date-time picker css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
		<!-- Date-range picker css  -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
		<!-- Date-Dropper css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
		<!-- Color Picker css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
		<!-- Mini-color css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
		<!-- sweet alert framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/sweetalert/css/sweetalert.css">
		<!-- Data Table Css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
		<!-- Style.css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
		<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<style>
    .blink_me {
        animation: blinker 1s linear infinite;
        color: red;
    }

    @keyframes blinker {
        80% {
            opacity: 0;
        }
    }
</style>
	</head>
	<body>
	    <?php if($this->session->flashdata('cm_msg')){ ?>						            
	    <script>
        	$(document).ready(function(){
        		$("#myModal").modal('show');
        	});
        </script>
	    <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
        				<b style="color:green"><?php echo $this->session->flashdata('cm_msg') ?></b>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>						         
		<!-- Pre-loader start -->
		<div class="theme-loader">
			<div class="ball-scale">
				<div class='contain'>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Pre-loader end -->
		<div id="pcoded" class="pcoded">
			<div class="pcoded-overlay-box"></div>
			<div class="pcoded-container navbar-wrapper">
				<?php include("includes/header.php"); ?>
				<!-- Sidebar inner chat end-->
				<div class="pcoded-main-container">
					<div class="pcoded-wrapper">
						<?php include("includes/coursesidenav.php"); ?>
						<div class="pcoded-content">
							<div class="pcoded-inner-content">
								<!-- Main-body start -->
								<div class="main-body">
									<div class="page-wrapper">
										<!-- Page-header start -->
										<div class="page-header">
											<div class="row align-items-end">
												<div class="col-lg-10">
													<div class="page-header-title">
														<div class="d-inline">
															<h4><?php echo $courseTitle ?></h4>
															<span><?php echo $subChapterName ?></span>
														</div>
													</div>
												</div>
												
												
											</div>
										</div>
										<!-- Page-header end -->
										<!-- Page-body start -->
										<div class="page-body">
										 
										    <?php if($subChapterType == 'Pdf' && ($subChaptersubType == 'upload' || $subChaptersubType == 'public_url')){ ?>
										    <center><iframe src="<?php echo $subChapterpath ?>" width="100%" height="600"></iframe></center>
										    <?php }else if($subChapterType == 'Live Class'){ ?>
										    <center>
										        <div class="col-sm-6">
										            <?php if(($subChapterDate == date('Y-m-d') || $subChapterDate > date('Y-m-d'))&& ($subChapterTime > date('H:i') || $subChapterTime < date('H:i')) && $endSession!=1){?>
										            <div id="LiveMode">
                                                        <b><?php echo $subchapterpremsg ?></b><br>
                                                        <h1 style="<?php if ($runningStatusData==1){echo "display:none";}?>">Time to Go Live</h1>
                                                        <h3 id="timeSet" style="<?php if ($runningStatusData==1){echo "display:none";}?>"></h3>
                                                        <h6 id="expired" style="<?php if ($runningStatusData==1){echo "display:none";}?>">Class Will Start at sharp <?php echo $subChapterTime ?>. Be Connected</h6>
                                                    </div>
										            <?php if($subChaptersubType == 'jitsi'){ ?>
										            <a href="<?php echo base_url() ?>Live?courseid=<?php echo $_GET['courseid'] ?>&chapterid=<?php echo $_GET['chapterid'] ?>&itemid=<?php echo $subChapterId ?>" target="_blank">
										                <button class="btn btn-primary btn-outline-primary">Join Live Class</button>
										            </a>
										            
										            <?php }else if($subChaptersubType == 'zoom'){
										            $zoomData = $this->db->query("SELECT * FROM zoom_live_class WHERE status = 1  AND sub_chapter_id= '".$_GET['itemid']."' 
                                                                                AND course_id = '".$_GET['courseid']."' AND chapter_id = '".$_GET['chapterid']."'  ORDER BY 1 DESC")->result();
                                                    foreach($zoomData as $getZoomData){
                                                        $zoomjoinlink = $getZoomData->join_url;
                                                    }
                                                                                         
										            ?>
										            <a href="<?php echo $zoomjoinlink ?>" target="_blank">
										                <button class="btn btn-primary btn-outline-primary">Join Live Class</button>
										            </a>
										            <?php } ?>
												    
													 <?php }else if(count($subChapterData) > 0)
													 {$subChapterTime = date('h:i a', strtotime($subChapterTime)); ?> 
													
													         <b><?php echo $subchapterpostmsg ?></b><br>
													       <b>Live class Ended on <?php echo  $subChapterDate.' '.$subChapterTime  ?> </b>
                                                         <br>  <a href="<?php echo base_url() ?>Recording?courseid=<?php echo $_GET['courseid'] ?>&itemid=<?php echo $_GET['itemid']?>"><button class="btn btn-primary btn-outline-primary" type="button" >View Recording</button></a>
													<?php }else if($subChapterDate > date('Y-m-d')){ 
													 $subChapterTime = date('h:i a', strtotime($subChapterTime));
													 ?>
													         <b> <?php echo $subchapterpremsg ?></b><br>
													       <b>Live class on <?php echo  $subChapterDate.' '.$subChapterTime  ?> </b>
													<?php }else if($subChapterDate < date('Y-m-d')){ 
													 $subChapterTime = date('h:i a', strtotime($subChapterTime));
													 ?>
													         <b> <?php echo $subchapterpostmsg ?></b><br>
													       <b>Live class on <?php echo  $subChapterDate.' '.$subChapterTime  ?> </b>
												        <!--<button onclick=alert() class="btn btn-primary btn-outline-primary">Join Live Class</button>-->
													   <?php }else if($subChapterDate == date('Y-m-d')&& $subChapterTime > date('H:i')){ 
													 $subChapterTime = date('h:i a', strtotime($subChapterTime));
													 ?>
													         <b> <?php echo $subchapterpremsg ?></b><br>
													       <b>Live class on <?php echo  $subChapterDate.' '.$subChapterTime  ?> </b>  
													 <?php  }
													 else if ($endSession==1){ ?>
                                                    <b><?php echo $subchapterpostmsg ?></b><br>
                                                    <b>Live class Ended on <?php echo  $subChapterDate.' '.$subChapterTime  ?> </b>
                                                    <p class="blink_me">Your Live Class has been ended.You can watch video recording after sometimes.Thanks !</p>
                                                <?php }?>
                                                    
                                                </div>
                                            </center>
										    <?php } ?>
										</div>
										<!-- Page-body end -->
									</div>
								</div>
							</div>
							<!-- Main-body end -->
							<!-- <div id="styleSelector">
							</div>-->
						</div>
					</div>
				</div>
			</div>
		</div>
			<!-- Recorded Vdo Link -->

		<!-- Warning Section Starts -->
		<!-- Older IE warning message -->
		
		<!-- Warning Section Ends -->
		<!-- Required Jquery -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
		<!-- jquery slimscroll js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
		<!-- modernizr js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
		<!-- data-table js -->
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
		<!-- Bootstrap date-time-picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
		<!-- Date-range picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
		<!-- Date-dropper js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
		<!-- Color picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
		<!-- Mini-color js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
		<!-- i18next.min.js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
		<!-- Custom js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
		
		<script src="<?php echo base_url() ?>assets/files/assets/pages/sortable-custom.js"></script>
		
        <!-- sweet alert js -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- sweet alert modal.js intialize js -->
        <!-- modalEffects js nifty modal window effects -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/modalEffects.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/classie.js"></script>
		<!-- Date-range picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-23581568-13');
		</script>
		<script>
		    function alert()
		    {
                swal({
                  title: "Live Class has Ended?",
                  text: "Try to attend next class in-time",
                  icon: "warning",
                  button: true,
                  dangerMode: true,
                })

            }
		</script>

        <script>
            var date='<?php echo $subChapterDate ?>'+' '+'<?php echo date('H:i', strtotime($subChapterTime)) ?>';
            var endSession='<?php echo $endSession ?>';
            var hostedStartStatus='<?php echo $runningStatusData ?>';
            var redirectUrl="<?php echo base_url() ?>Live/joinStudent?courseid=<?php echo $_GET['courseid'] ?>&chapterid=<?php echo $_GET['chapterid'] ?>&itemid=<?php echo $_GET['itemid'] ?>&type=<?php echo $subChaptersubType ?>&sub_chapter_id=<?php echo $subChapterId ?>
            &username=<?php echo $_SESSION['username'] ?>&unique_id=<?php echo $_SESSION['unique_id'] ?>";

            // Set the date we're counting down to
            var countDownDate = new Date(""+date+"").getTime();
           // var countDownDate = new Date("2021-04-29 14:05").getTime();

            // Update the count down every 1 second
            var x = setInterval(function() {

                // Get today's date and time
                var now = new Date().getTime();

                // Find the distance between now and the count down date
                var distance = countDownDate - now;

                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);


                document.getElementById("timeSet").innerHTML = days + "d " + hours + "h "
                    + minutes + "m " + seconds + "s ";

                // If the count down is over, write some text
                if (distance < 0) {
                    clearInterval(x);

                    var loadedHtml='<div id="status" style="margin-top: 10px">\n' +
                        '<p>Your meeting has not yet started. Waiting for a moderator to start the meeting...</p>\n' +
                        '<img class="img-fluid" src="<?php echo base_url() ?>assets/landing/images/ajax-loader.gif" alt="Theme-Logo">\n' +
                        '<p>You will be connected as soon as the meeting starts.</p>\n' +
                        '<div>';

                    if (hostedStartStatus==0){

                        document.getElementById("LiveMode").innerHTML =loadedHtml;

                        var courseId="<?php echo $_GET['courseid'] ?>";
                        var itemId="<?php echo $_GET['itemid'] ?>";

                        //Check Meeting Are Exist Or not
                        $.ajax({
                            url: '<?php echo base_url() ?>/CourseLearners/Checkmettingrunning',
                            type: 'POST',
                            data: {
                                itemId:itemId,
                                courseId:courseId,
                            },
                            success: function (hosted) {
                                if (hosted==1){
                                    setTimeout(
                                        function()
                                        {
                                            document.getElementById("LiveMode").innerHTML ='';
                                            window.location.href =redirectUrl;
                                        }, 1000);
                                }else {
                                    document.getElementById("LiveMode").innerHTML =loadedHtml;
                                }
                            }
                        });

                       // document.getElementById("LiveMode").innerHTML =loadedHtml;
                    }else{

                        var loadedHtml='<div id="status" style="margin-top: 10px">\n' +
                            '<p>Your meeting has been started.Wait for second...</p>\n' +
                            '<img class="img-fluid" src="<?php echo base_url() ?>assets/landing/images/ajax-loader.gif" alt="Theme-Logo">\n' +
                            '<div>';

                        document.getElementById("LiveMode").innerHTML =loadedHtml;

                        setTimeout(
                            function()
                            {
                                document.getElementById("LiveMode").innerHTML ='';
                                window.location.href =redirectUrl;
                            }, 1000);
                    }
                }else {
                      if (hostedStartStatus==1){

                          var loadedHtml='<div id="status" style="margin-top: 10px">\n' +
                              '<p>Your meeting has been started.Wait for second...</p>\n' +
                              '<img class="img-fluid" src="<?php echo base_url() ?>assets/landing/images/ajax-loader.gif" alt="Theme-Logo">\n' +
                              '<div>';

                          document.getElementById("LiveMode").innerHTML =loadedHtml;

                          setTimeout(
                              function()
                              {
                                  document.getElementById("LiveMode").innerHTML ='';
                                  window.location.href =redirectUrl;
                              }, 1000);
                      }
                }
            }, 1000);

        </script>
	</body>
</html>