<?php
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}

?>
<?php
date_default_timezone_set("Asia/Calcutta");

$CourseId = $_GET['courseid'];
$quizTestid =$_GET['id'];

$quiztest = $this->db->query("SELECT * FROM quiz_test WHERE status = 1 AND course_id = '$CourseId' AND id='$quizTestid' ORDER BY 1 DESC")->result();

foreach($quiztest as $getquiztest){
    $quizType = $getquiztest->quiz_type;
    $quizName = $getquiztest->title;
    $time_limit = $getquiztest->time_limit;
    $takeTime = $getquiztest->take_time;
    $show_leaderboard = $getquiztest->show_leaderboard;
    $show_rank = $getquiztest->show_rank;
    $show_solutions_for_learner = $getquiztest->show_solutions_for_learner;
}

$newQuestions           = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$_GET['id']." ORDER BY id DESC ")->result();
$getAllAttempts         =$this->db->query("SELECT * FROM quiz_test_attempt WHERE quiztest_id = ".$_GET['id']." AND user_id = ".$_SESSION['user_id']." AND mark_complete_status =1 ORDER BY id DESC ")->result();

$totalQuestions         =count($newQuestions);
$countCorrectAnswar     =0;
$totalAttemptAnswar     =0;

$attemmptId=$getAllAttempts[0]->id;
if (isset($_REQUEST['attemptId'])){
    $attemmptId=$_REQUEST['attemptId'];
}

$countTotalCorrectAnswarMark=0;
$countPenalityMark=0;
$totalQuestionsMark=0;
foreach ($newQuestions as $key=>$getnewQuestions){
    $totalQuestionsMark=$totalQuestionsMark+$getnewQuestions->mark;

    $attemptResult =$this->db->query("SELECT * FROM quiz_test_attempt_result WHERE question_id = ".$getnewQuestions->id." AND attempt_id = ".$attemmptId." ")->row();
    if ($attemptResult){

        $found='';

        if ($getnewQuestions->type==3 || $getnewQuestions->type==4){
            $choseOptionsText = $attemptResult->chose_option_text;
            $correctOptionsText =$attemptResult->correct_option_text;

            if ($choseOptionsText==$correctOptionsText){
                $found = true;
            }

        }else {
            if ($attemptResult->chose_option_id==$attemptResult->correct_chose_option_id){
                $found = true;
            }else{
                $found = false;
            }
        }

        if ($found==true){
            $countCorrectAnswar++;
            $countTotalCorrectAnswarMark=$countTotalCorrectAnswarMark+$getnewQuestions->mark;
        }else{
            $countPenalityMark=$countPenalityMark+$countTotalCorrectAnswarMark+$getnewQuestions->penalty;
        }

        $totalAttemptAnswar++;
    }
}

$inCorrectAnswar=$totalAttemptAnswar-$countCorrectAnswar;
$skippedQuestions=$totalQuestions-$totalAttemptAnswar;

$markeAnswarTotal=$countTotalCorrectAnswarMark-$countPenalityMark;

$getTime         =$this->db->query("SELECT * FROM quiz_test_attempt WHERE id = ".$attemmptId." ")->row();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Competitive Exam Guide || Course Details</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo $meta_description?>">
    <meta name="keywords" content="<?php echo $meta_keywords ?>">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
    <!-- Color Picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
    <!-- Mini-color css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
    <!-- sweet alert framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/sweetalert/css/sweetalert.css">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <script  src="https://makitweb.com/demo/html2canvas/html2canvas-master/dist/html2canvas.js"></script>


    <style>
        .blink_me {
            animation: blinker 1s linear infinite;
            color: red;
        }

        @keyframes blinker {
            80% {
                opacity: 0;
            }
        }

        span.page-link {
            width: 42px;
            float: left;
            border-radius: 11px;
            margin-right: 10px;
            margin-top: -6px;
        }

        .modal-content {
            max-height: 500px;
            overflow-y: scroll;
        }
        button#printDiv {
            margin-left: 11px;
            margin-top: -4px;
        }

        @media print {
            #questionAreaPrint {
                background-color: white;
                height: 100%;
                width: 100%;
                position: fixed;
                top: 0;
                left: 0;
                margin: 0;
                padding: 15px;
                font-size: 14px;
                line-height: 18px;
            }
        }


    </style>
</head>
<body>
<?php if($this->session->flashdata('cm_msg')){ ?>
    <script>
        $(document).ready(function(){
            $("#myModal").modal('show');
        });
    </script>
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Message</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <b style="color:green"><?php echo $this->session->flashdata('cm_msg') ?></b>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>

<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
        <?php include("includes/header.php"); ?>
        <!-- Sidebar inner chat end-->
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <?php include("includes/coursesidenav.php"); ?>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- Page-header start -->

                                <div class="page-header">
                                    <div class="row align-items-end">
                                        <div class="col-md-6">
                                            <div class="page-header-title">
                                                <div class="d-inline">
                                                    <h2>Quiz Test Name : <?=$quizName?></h2>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if ($quizType!=1) { ?>
                                            <div class="col-md-6">
                                                <ul class="pager">
                                                    <li>
                                                        <button class="btn btn-secondary"
                                                                style="float: left;margin-right: 24px;">Previous
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <a href="<?php echo base_url() ?>CourseLearners/quiz?courseid=<?= $_GET['courseid'] ?>&id=<?= $_GET['id'] ?>">
                                                            <button class="btn btn-primary">Next</button>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>

                                <!-- Page-header end -->
                                <!-- Page-body start -->
                                <div class="row" id="mainContentArea">
                                    <style>
                                        div#questionArea h6 {
                                            margin-left: 16px;
                                        }
                                        div#questionArea {
                                            margin-top: 14px;
                                            border-top: 1px solid #88cae1;
                                            padding-top: 19px;
                                            width: 100%;
                                        }
                                    </style>
                                    <div class="col-sm-12">
                                        <!-- HTML5 Export Buttons table start -->
                                        <div class="card">
                                            <div class="card-block">

                                                <div class="col-md-12 row">
                                                    <div class="col-md-6">
                                                        <?php if ($quizType!=1) { ?>
                                                            <form method="post" action="<?php echo base_url() ?>CourseLearners/searchAttempt">
                                                                <div class="col-md-10" style="float: left;padding-left: 0;">
                                                                    <p>Chose Attempt <span>
                                                                       <input type="hidden" name="courseId" value="<?=$_GET['courseid']?>">
                                                                       <input type="hidden" name="quiztestId" value="<?=$_GET['id']?>">
                                                                       <select id="allAttemptId" class="form-control" name="attemptSelected">
                                                                           <?php foreach ($getAllAttempts as $key=>$itemsAttempt){ ?>
                                                                               <option value="<?=$itemsAttempt->id?>" <?php if ($attemmptId==$itemsAttempt->id){echo "selected";}?>><?=$itemsAttempt->created_at?></option>
                                                                           <?php } ?>
                                                                       </select>
                                                                   </span></p>
                                                                </div>
                                                                <div class="col-md-2" style="float: left;margin-top: 22px;">
                                                                    <input type="submit" class="btn btn-success" value="Search">
                                                                </div>
                                                            </form>
                                                        <?php } ?>
                                                    </div>

                                                    <div class="col-md-6" style="margin-top: 25px;">
                                                        <?php if ($quizType!=1) { ?>
                                                            <!--                                                            <button class="btn btn-danger" style="margin-left:54px;" id="deleteAttempt">Delete Attempt</button>-->
                                                        <?php } ?>
                                                        <button class="btn btn-primary hidden-print"  id="printDiv"><i class="fa fa-print"></i> Print</button>
                                                    </div>
                                                </div>

                                                <?php
                                                if ($show_leaderboard==1){
                                                    ?>
                                                    <div class="col-md-12 row" style="padding-bottom: 44px;">
                                                        <div class="col-md-6" style="margin-top: 42px;">
                                                            <table class="table table-bordered">
                                                                <tbody>
                                                                <tr>
                                                                    <td>Rank base On List Attempt</td>
                                                                    <td>
                                                                        <?php
                                                                        if ($show_rank==1){
                                                                            ?>
                                                                            1/1
                                                                        <?php }else{?>
                                                                            N/N
                                                                        <?php }?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Your Marks</td>
                                                                    <td><?=$markeAnswarTotal?>/<?=$totalQuestionsMark?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Time Taken</td>
                                                                    <td><?=$getTime->take_attempt_time?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Total Question</td>
                                                                    <td><?=$totalQuestions?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Correct Question</td>
                                                                    <td><?=$countCorrectAnswar?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Incorrect Questoin</td>
                                                                    <td><?=$inCorrectAnswar?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Skipped Questoin</td>
                                                                    <td><?=$skippedQuestions?></td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="col-md-6" style="margin-top: 25px;">
                                                            <?php

                                                            $dataPoints = array(
                                                                array("y" => $skippedQuestions,"label" => "Skipped" ),
                                                                array("y" => $inCorrectAnswar,"label" => "InCorect Question" ),
                                                                array("y" => $countCorrectAnswar,"label" => "Correct Question" ),
                                                                array("y" => $totalQuestions,"label" => "Total Question" )
                                                            );

                                                            $groupSubjects = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$_GET['id']." GROUP BY subject ")->result();
                                                            $goalwiseReport=[];
                                                            foreach ($groupSubjects as $key=>$subjectItem){
                                                                $subjectWiseNewQuestions = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$_GET['id']." AND subject  LIKE '%".$subjectItem->subject."%' ")->result();
                                                                $totalQuestionsSubjectWise=count($subjectWiseNewQuestions);

                                                                $countCorrectAnswarSubjectWise     ='';
                                                                $totalAttemptAnswarSubjectWise     ='';

                                                                foreach ($subjectWiseNewQuestions as $key=>$getnewQuestionsSubjectWise){
                                                                    $attemptResult =$this->db->query("SELECT * FROM quiz_test_attempt_result WHERE question_id = ".$getnewQuestionsSubjectWise->id." AND attempt_id = ".$attemmptId." ")->row();
                                                                    if ($attemptResult){

                                                                        $found='';

                                                                        if ($getnewQuestionsSubjectWise->type==3 || $getnewQuestionsSubjectWise->type==4){
                                                                            $choseOptionsText = $attemptResult->chose_option_text;
                                                                            $correctOptionsText =$attemptResult->correct_option_text;

                                                                            if ($choseOptionsText==$correctOptionsText){
                                                                                $found = true;
                                                                            }

                                                                        }else {
                                                                            if ($attemptResult->chose_option_id==$attemptResult->correct_chose_option_id){
                                                                                $found = true;
                                                                            }else{
                                                                                $found = false;
                                                                            }
                                                                        }

                                                                        if ($found==true){
                                                                            $countCorrectAnswarSubjectWise++;
                                                                        }
                                                                        $totalAttemptAnswarSubjectWise++;
                                                                    }
                                                                }

                                                                $inCorrectAnswarSubjectWise  =$totalAttemptAnswarSubjectWise-$countCorrectAnswarSubjectWise;
                                                                $skippedQuestionsSubjectWise =$totalQuestionsSubjectWise-$totalAttemptAnswarSubjectWise;

//
//                                                            $goalwiseReport[] = array(
//                                                                    "category" =>$subjectItem->subject,
//                                                                    "first" =>$totalQuestionsSubjectWise,
//                                                                     'second'=>$countCorrectAnswarSubjectWise,
//                                                                     'third'=>$inCorrectAnswarSubjectWise,
//                                                                     'fourth'=>$skippedQuestionsSubjectWise);

                                                                $goalwiseReport[] =array("y" =>$totalQuestionsSubjectWise, "label" => $subjectItem->subject);

                                                            }

                                                            ?>
                                                            <script>
                                                                window.onload = function() {

                                                                    var chart = new CanvasJS.Chart("chartContainer", {
                                                                        animationEnabled: true,
                                                                        title:{
                                                                            text: "Total Question Attempt Report"
                                                                        },
                                                                        axisY: {
                                                                            includeZero: true,
                                                                        },
                                                                        data: [{
                                                                            type: "bar",
                                                                            indexLabel: "{y}",
                                                                            indexLabelPlacement: "inside",
                                                                            indexLabelFontWeight: "bolder",
                                                                            indexLabelFontColor: "white",
                                                                            dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                                                                        }]
                                                                    });
                                                                    chart.render();

                                                                }
                                                            </script>
                                                            <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                                                            <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-12 row well" style="margin-left: 12px;">
                                                        <script>
                                                            $(function () {
                                                                var chart1 = new CanvasJS.Chart("chartContainer1", {
                                                                    animationEnabled: true,
                                                                    theme: "light2",
                                                                    title:{
                                                                        text: "Goal Wise Report"
                                                                    },
                                                                    data: [{
                                                                        type: "column",
                                                                        yValueFormatString: "#,##0.## Questions",
                                                                        dataPoints: <?php echo json_encode($goalwiseReport, JSON_NUMERIC_CHECK); ?>
                                                                    }]
                                                                });
                                                                chart1.render();
                                                            });
                                                        </script>

                                                        <div id="chartContainer1" style="height: 370px; width: 100%;"></div>
                                                    </div>

                                                <?php } ?>

                                                <span id="questionAreaPrint">
                                                <?php
                                                $i = 1;
                                                foreach($newQuestions as $getnewQuestions){
                                                    $questionid = $getnewQuestions->id;
                                                    ?>
                                                    <div class="col-md-12 row well well-sm" style="margin-left: 12px;">
                                                    <div class="col-md-6">
                                                        <h4>Question <?=$i?></h4>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h6 style="margin-top: 10px;float: right;">Single Correct Option, <span style="color: green;">+<?=$getnewQuestions->mark?></span>, <span style="color: red;">-<?=$getnewQuestions->penalty?></span></h6>
                                                    </div>

                                                    <div id="questionArea">
                                                        <h4><?= $getnewQuestions->question_text?> </h4>
                                                        <h4 style="margin-top: 18px;font-size: 20px;">Select the correct answer using the code below.</h4>
                                                        <div class="card" style="margin-top: 28px;">
                                                            <ul class="list-group list-group-flush" >
                                                                <?php
                                                                $f = 'A';
                                                                $givenAnswar='';
                                                                $correctAnswer='';
                                                                $correctStatus=0;
                                                                $newQuestionOptions = $this->db->query("SELECT * FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$getnewQuestions->id." ")->result();

                                                                $attemptResult =$this->db->query("SELECT * FROM quiz_test_attempt_result WHERE question_id = ".$getnewQuestions->id." AND attempt_id = ".$attemmptId." ")->row();

                                                                $choseOptionsId='';
                                                                $found='';

                                                                $choseOptionsArray = unserialize($attemptResult->chose_option_id);
                                                                $correctOptionsArray = unserialize($attemptResult->correct_chose_option_id);

                                                                if ($getnewQuestions->type==3 || $getnewQuestions->type==4){
                                                                    $choseOptionsText = $attemptResult->chose_option_text;
                                                                    $correctOptionsText =$attemptResult->correct_option_text;

                                                                    if ($choseOptionsText==$correctOptionsText){
                                                                        $found = true;
                                                                    }

                                                                }else {
                                                                    if ($attemptResult) {
                                                                        if ($attemptResult->chose_option_id == $attemptResult->correct_chose_option_id) {
                                                                            $found = true;
                                                                        } else {
                                                                            $found = false;
                                                                        }
                                                                    }else{
                                                                        $found = false;
                                                                    }
                                                                }

                                                                if ($found==true){
                                                                    $correctStatus=1;
                                                                }

                                                                foreach($newQuestionOptions as $getnewQuestionOptions){

                                                                    if($getnewQuestionOptions->is_answer == 1){
                                                                        $correctAnswer .=$f .' & ';
                                                                    }

                                                                    if ($getnewQuestions->type==3){
                                                                        $correctAnswer='Check Alerting Result';
                                                                    }

                                                                    if ($getnewQuestions->type==4){
                                                                        $correctStatus=1;
                                                                    }

                                                                    ?>
                                                                    <?php
                                                                    if (in_array($getnewQuestionOptions->id, $choseOptionsArray)){
                                                                        $optionsText=$getnewQuestionOptions->option_text;
                                                                        if ($getnewQuestions->type==3 || $getnewQuestions->type==4){
                                                                            $optionsText=$attemptResult->chose_option_text;
                                                                            $givenAnswar=$attemptResult->chose_option_text;
                                                                        }else{
                                                                            $givenAnswar .=$f .' & ';
                                                                        }
                                                                        ?>
                                                                        <li class="list-group-item" style="background-color: <?php if ($correctStatus==1){echo '#32B643';}else{ echo 'red';} ?>;color:white;" ><?php echo  $f.". ".$optionsText?></li>
                                                                    <?php }else{ ?>
                                                                        <li class="list-group-item" ><?php echo $f.". ".$getnewQuestionOptions->option_text?></li>

                                                                    <?php } ?>
                                                                    <?php $f++; } ?>
                                                            </ul>
                                                        </div>

                                                        <?php
                                                        if ($show_solutions_for_learner==1){
                                                            ?>
                                                            <div class="card" style="margin-top: 28px;">
                                                            <?php
                                                            if ($correctStatus){
                                                                ?>
                                                                <h6 style="font-size: 20px;font-weight: 600;color: green;"><img src="https://api.iconify.design/emojione-v1:white-heavy-check-mark.svg" alt="adminity Logo" style="width: 30px;height: 30px;"> Your Answer Is Correct</h6>
                                                            <?php }else{?>
                                                                <?php
                                                                if (empty($givenAnswar)){ ?>
                                                                    <h6 style="font-size: 20px;font-weight: 600;color: red;"><img src="https://api.iconify.design/emojione:cross-mark-button.svg" alt="adminity Logo" style="width: 30px;height: 30px;"> You skipped it</h6>
                                                                <?php  }else{ ?>
                                                                    <h6 style="font-size: 20px;font-weight: 600;color: red;"><img src="https://api.iconify.design/emojione:cross-mark-button.svg" alt="adminity Logo" style="width: 30px;height: 30px;"> Your Answer Is Wrong</h6>
                                                                <?php }
                                                                ?>
                                                            <?php } ?>

                                                                <?php
                                                                if ($getnewQuestions->type!=4){
                                                                    ?>
                                                                    <h6 style="font-size: 17px;font-weight: 600;">Your Given Answer :
                                                                <?php
                                                                if ($getnewQuestions->type==3){
                                                                    echo $attemptResult->chose_option_text;
                                                                }else{
                                                                    ?>
                                                                    <?=substr($givenAnswar, 0, -3)?>
                                                                <?php } ?>
                                                            </h6>

                                                                    <h6 style="font-size: 17px;font-weight: 600;">Correct Answer :
                                                                    <?php
                                                                    if ($getnewQuestions->type==3 || $getnewQuestions->type==4){
                                                                        echo $attemptResult->correct_option_text;
                                                                    }else{
                                                                        ?>
                                                                        <?=  substr($correctAnswer, 0, -3);?></h6>
                                                                    <?php } ?>

                                                                <?php } ?>
                                                         </div>

                                                            <div class="card" style="margin-top: -23px;">
                                                            <h6 style="font-size: 14px;font-weight: 600;">Explanations : <?=!empty($getnewQuestions->explaination)?$getnewQuestions->explaination:'No explanations available.'?></h6>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                    <?php $i++; } ?>
</span>
                                            </div>
                                        </div>
                                        <!-- HTML5 Export Buttons end -->
                                    </div>
                                </div>
                                <!-- Page-body end -->
                            </div>
                        </div>
                    </div>
                    <!-- Main-body end -->
                    <!-- <div id="styleSelector">
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Recorded Vdo Link -->

<!-- Warning Section Starts -->
<!-- Older IE warning message -->

<!-- Warning Section Ends -->
<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
<!-- data-table js -->
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<!-- Bootstrap date-time-picker js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
<!-- Date-range picker js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
<!-- Date-dropper js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
<!-- Color picker js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
<!-- Mini-color js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
<!-- i18next.min.js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
<!-- Custom js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>

<script src="<?php echo base_url() ?>assets/files/assets/pages/sortable-custom.js"></script>

<!-- sweet alert js -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- sweet alert modal.js intialize js -->
<!-- modalEffects js nifty modal window effects -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/modalEffects.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/classie.js"></script>
<!-- Date-range picker js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>

<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-23581568-13');

    $(function() {
        $("#printDiv").click(function() {

            var divContents = $("#questionAreaPrint").html();
            var printWindow = window.open('', '', 'height=500,width=900');
            printWindow.document.write('<html><head><title>Contact Us: +91 – 8459159056 | Website:www.competitiveexamguide.com</title>' +
                '<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">');
            printWindow.document.write('</head><body ><style>.card {\n' +
                '    border-radius: 5px;\n' +
                '    -webkit-box-shadow: 0 1px 20px 0 rgb(69 90 100 / 8%);\n' +
                '    box-shadow: 0 1px 20px 0 rgb(69 90 100 / 8%);\n' +
                '    border: none;\n' +
                '    margin-bottom: 30px;\n' +
                '}</style> ');
            printWindow.document.write('<div>\n' +
                '                <img class="img-fluid" src="http://zahid.rkitsolution.net/assets/landing/logos/competetiveexamguidelogo.png" alt="Theme-Logo" width="180px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="margin-left:10px;"><?=$quizName?> </span>\n' +
                '            <div>');

            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();

        });
    });

    $(document).off('click', 'button#deleteAttempt').on('click', 'button#deleteAttempt', function (e) {

        e.preventDefault();
        if(confirm("Are you sure you want to delete this?")){
            alert("Removed");
        }
        else{
            return false;
        }
    });
</script>
<script>
</script>
</body>
</html>