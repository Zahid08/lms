<?php
$this->load->helper('url');
$currentURL = current_url();
$activateLink = substr($currentURL,39);
$CourseId = $_GET['courseid'];
$courseChapters = $this->db->query("SELECT * FROM chapters WHERE status = 1  AND course_id = '$CourseId'  ORDER BY 1 ASC")->result();
?>

<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="style.css">
<style>
    span#upcommingText {
        align-items: center;
        background: #FFFF00;
        border-radius: 5rem;
        color: #144151;
        display: inline-flex;
        display: -ms-inline-flexbox;
        -ms-flex-align: center;
        font-size: 90%;
        height: 1.2rem;
        line-height: .8rem;
        margin: .1rem;
        max-width: 100%;
        padding: 0.2rem 0.4rem;
        text-decoration: none;
        vertical-align: middle;
    }

    *{
        margin: 0;
        padding: 0;
    }
    .rate {
        float: left;
        height: 46px;
        padding: 0 10px;
    }
    .rate:not(:checked) > input {
        position:absolute;
        top:-9999px;
    }
    .rate:not(:checked) > label {
        float:right;
        width:1em;
        overflow:hidden;
        white-space:nowrap;
        cursor:pointer;
        font-size:30px;
        color:#ccc;
    }
    .rate:not(:checked) > label:before {
        content: '★ ';
    }
    .rate > input:checked ~ label {
        color: #000000;
    }
    .rate:not(:checked) > label:hover,
    .rate:not(:checked) > label:hover ~ label {
        color: #000000;
    }
    .rate > input:checked + label:hover,
    .rate > input:checked + label:hover ~ label,
    .rate > input:checked ~ label:hover,
    .rate > input:checked ~ label:hover ~ label,
    .rate > label:hover ~ input:checked ~ label {
        color: #000000;
    }
</style>
<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar main-menu">
        <div class="pcoded-navigatio-lavel"><span >Navigation</span></div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <center><img src="<?php echo base_url() ?>assets/course_images/<?php echo $courseImage ?>"style="border:2px solid #000000 " height="150px;" width="180px;">
                    <br>
                    <p class="text-white"><?php echo $courseTitle ?></p>
                </center>

            </li>
            <li class="">
                <a href="<?php echo base_url() ?>LCourses">
                    <span class="pcoded-micon"><i class=" feather icon-arrow-left"></i></span>
                    <span class="pcoded-mtext">Back</span>
                </a>
            </li>
            <li class="">
                <a href="#" data-toggle="modal" data-target="#comment">
                    <span class="pcoded-micon"><i class=" feather icon-message-square"></i></span>
                    <span class="pcoded-mtext">Review</span>
                </a>
            </li>
            <?php
            foreach($courseChapters as $getcourseChapters){
                $chapterName = $getcourseChapters->chapter_name;
                $ChapterId = $getcourseChapters->id;


                ?>


                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)">
                        <span class="pcoded-micon"><i class="feather icon-bookmark"></i></span>
                        <span class="pcoded-mtext"><?php echo $chapterName ?></span>
                        <ul class="pcoded-submenu">


                            <?php
                            $chapterItems = $this->db->query("SELECT * FROM sub_chapters WHERE status = 1  AND course_id = '$CourseId' AND chapterid = '$ChapterId' ORDER BY 1 ASC")->result();
                            foreach($chapterItems as $getchapterItems){
                                $subChapterName = $getchapterItems->subchapter_name;
                                ?>
                                <li class="">
                                    <a href="<?php echo base_url() ?>CourseLearners?courseid=<?php echo $CourseId ?>&itemid=<?php echo $getchapterItems->id ?>">
                                        <span class="pcoded-mtext" ><?php echo $subChapterName ?></span>
                                    </a>
                                </li>
                            <?php } ?>


                        </ul>
                </li>
            <?php } ?>

            <?php
            $chapterItems_1 = $this->db->query("SELECT * FROM sub_chapters WHERE status = 1  AND course_id = '$CourseId' AND chapterid = 0 ORDER BY 1 ASC")->result();
            foreach($chapterItems_1 as $getchapterItems_1){
                $subChapterName_1 = $getchapterItems_1->subchapter_name;
                $sessionEndCheck = $getchapterItems_1->end_session;
                ?>

                <li class="">
                    <?php
                    if ($getchapterItems_1->type=='Live Quiz'){
                        $quizTest = $this->db->query("SELECT * FROM quiz_test WHERE id =$getchapterItems_1->quiz_id  AND course_id = '$CourseId' ORDER BY 1 ASC")->row();
                        $options='quiz';
                        if ($quizTest->quiz_type==1){
                            $options='livequiz';
                        }
                        ?>
                        <a href="<?php echo base_url() ?>CourseLearners/<?=$options?>?courseid=<?php echo $CourseId ?>&id=<?php echo $getchapterItems_1->quiz_id ?>">
                            <span class="pcoded-micon"><i class="feather icon-book"></i></span>
                            <span class="pcoded-mtext"><?php echo $subChapterName_1 ?>
                                <?php
                                if ($quizTest->quiz_type==1){?>
                                    <span style="background: red;color: white;padding: 5px;border-radius: 9px;">Live</span>
                                <?php } ?>
                    </span>
                        </a>
                    <?php }else{ ?>
                        <a href="<?php echo base_url() ?>CourseLearners?courseid=<?php echo $CourseId ?>&itemid=<?php echo $getchapterItems_1->id ?>">
                            <?php if($getchapterItems_1->type == 'Live Class') {?>
                                <span class="pcoded-micon"><i class="feather icon-video"></i></span>
                            <?php }else if($getchapterItems_1->type == 'Inbuilt Live Class') {?>
                                <span class="pcoded-micon"><i class="feather icon-video"></i></span>
                            <?php }else if($getchapterItems_1->type == 'Jitsi Live Class') {?>
                                <span class="pcoded-micon"><i class="feather icon-video"></i></span>
                            <?php }else if($getchapterItems_1->type == 'Pdf' && $getchapterItems_1->sub_type == 'upload'){ ?>
                                <span class="pcoded-micon"><i class="feather icon-book"></i></span>
                            <?php }else if($getchapterItems_1->type == 'Pdf' && $getchapterItems_1->sub_type == 'public_url'){ ?>
                                <span class="pcoded-micon"><i class="feather icon-link"></i></span>
                            <?php }else if($getchapterItems_1->type == 'Heading'){ ?>
                                <span class="pcoded-micon"><i class="feather icon-bookmark"></i></span>
                                <?php   if ($sessionEndCheck!=1){ ?>
                                    <span id="upcommingText"><?php echo "Upcoming"; ?></span>
                                <?php }?>
                            <?php } ?>

                            <span class="pcoded-mtext"><?php echo $subChapterName_1 ?></span>
                        </a>
                    <?php }?>

                </li>
            <?php } ?>
            <!--<?php

            $quizTest = $this->db->query("SELECT * FROM quiz_test WHERE status = 1  AND course_id = '$CourseId' ORDER BY 1 ASC")->result();

            foreach($quizTest as $getquizTest){
                $quizTest = $getquizTest->title;

                ?>
                                
                                <li class="">
                                    <a href="<?php echo base_url() ?>Lquiztest?courseid=<?php echo $_GET['courseid'] ?>&quizid=<?php echo $getquizTest->id ?>">
                                       
                                        <span class="pcoded-micon"><i class="feather icon-file-text"></i></span>
                                        <span class="pcoded-mtext" ><?php echo  $getquizTest->title?></span>
                                    </a>
                                </li>
                                <?php }?>-->



        </ul>
    </div>
</nav>


<!-- Modal -->
<div class="modal fade" id="comment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><b>Comment:</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php echo base_url() ?>Salesreport/addcomments">
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="hidden" name="courseid" id="courseid" value="<?php echo $CourseId ?>">

                            <b>Comment:</b>
                            <textarea class="form-control max-textarea"  name="comment" id="comment" maxlength="255" rows="4" placeholder="Type Your Comments Here..."
                                      required></textarea>
                            <br>
                            <b>Rating:</b>
                            <p><div class="rate">
                                <input type="radio" id="star5" name="rate" value="5" />
                                <label for="star5" title="text">5 stars</label>
                                <input type="radio" id="star4" name="rate" value="4" />
                                <label for="star4" title="text">4 stars</label>
                                <input type="radio" id="star3" name="rate" value="3" />
                                <label for="star3" title="text">3 stars</label>
                                <input type="radio" id="star2" name="rate" value="2" />
                                <label for="star2" title="text">2 stars</label>
                                <input type="radio" id="star1" name="rate" value="1" />
                                <label for="star1" title="text">1 star</label>
                            </div>
                            </p>
                            <br>

                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-outline-secondary btn-close" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-outline-primary  btn-action" href="<?php echo base_url() ?>CourseLearners?courseid=<?php echo $CourseId ?>"class="btn btn-primary">Save</button>

            </div>
            </form>
        </div>
    </div>
</div>
<script>
    $('#checkbox').rating('update', 3);

    // method chaining
    var ratingValue = $('#checkbox').rating('update', 3).val();
</script>