<?php
$newQuestions = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$_GET['id']."  ORDER BY id")->result();


$CourseId=$_GET['courseid'];
$quizTestid=$_GET['id'];
$quiztest = $this->db->query("SELECT * FROM quiz_test WHERE status = 1 AND course_id = '$CourseId' AND id='$quizTestid' ORDER BY 1 DESC")->result();
$post_msg='';
foreach($quiztest as $getquiztest){
    $quizName = $getquiztest->title;
    $time_limit = $getquiztest->time_limit;
    $retake_attempts = $getquiztest->re_take_attempts;
    $available_from = $getquiztest->available_from;
    $available_from  = date('Y-m-d h:i:s a', strtotime($available_from ));
    $available_till = $getquiztest->available_till;
    $available_till = date('m/d/Y h:i:s A', strtotime($available_till));
    $instructions = $getquiztest->instructions;
    $post_msg = $getquiztest->post_msg;
    $is_instructions = $getquiztest->is_instructions;
    $is_scientific_calc = $getquiztest->is_scientific_calc;
    $show_leaderboard = $getquiztest->show_leaderboard;
    $show_rank = $getquiztest->show_rank;
    $results_mode = $getquiztest->results_mode;
    $arrange_by_topic = $getquiztest->arrange_by_topic;
    $min_time_pre_submit = $getquiztest->min_time_pre_submit;
    $quizTestid = $getquiztest->id;

    $supportLanguage=$getquiztest->support_lang;
    $support_lang_id=$getquiztest->support_lang_id;

    $is_scientific_calc=$getquiztest->is_scientific_calc;

    if ($support_lang_id==1){
        $languageName='हिंदी';
    }elseif ($support_lang_id==2){
        $languageName='English';
    }elseif ($support_lang_id==3){
        $languageName='Others';
    }


}

$firstQuestions=$newQuestions[0];

$secondQuestions                = $this->db->query("SELECT * FROM quiz_test_questions WHERE id > ".$firstQuestions->id." AND status = 1 AND quiz_test_id = ".$_GET['id']."")->row();

$secondID='';
if ($secondQuestions){
    $secondID=$secondQuestions->id;
}
$newQuestionOptions = $this->db->query("SELECT * FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$firstQuestions->id." ")->result();

$answarStatus=1;
$answarQuestions               = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$_GET['id']." AND answar_status = ".$answarStatus." ")->result();

$totalQuestions                = count($newQuestions);
$totalAnswar                   = count($answarQuestions);
$notAnswar                      =$totalQuestions-$totalAnswar;


$groupSubjects = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$_GET['id']." GROUP BY subject ")->result();

$currentDate=date('Y-m-d h:i:s');
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/quizStart.css">
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    <script> var myModal = new bootstrap.Modal(document.getElementById('basiceModal'), options) </script>

    <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>

<body>
<div class="container">
    <div class="row" style="padding-top: 11px;">
        <div class="col-6">
            <button id="myButton" onclick="openFullscreen()" style="display: none">Go Fullscreen</button>
            <h4 class="text-start"><?=$quizName?></h4>
        </div>
        <div class="col-6">
            <h4 class="text-end remain-time">Remaining Time: <span id="demo"></span></h4>
        </div>
        <div class="col-12">
            <ul class="status-bar-one">
                <?php
                foreach ($groupSubjects as $key=>$items){ ?>
                    <li class="physical"><button type="button" class="btn btn-primary btn-lg" id="subjectEvent" data-question-id="<?=$items->id?>" data-quiz-test-id="<?=$items->quiz_test_id?>" data-subject-name="<?=$items->subject?>"><?=$items->subject?></button></li>
               <?php }
                ?>
                <li class="physical">
                    <button type="button" data-toggle="modal" data-target="#questionPaperModal"   class="btn btn-primary btn-lg" id="questionPaperEvent"><i class="bi bi-clipboard-data"></i> Question Paper</button>
                </li>
                <?php
                if ($is_scientific_calc==1){
                ?>
                <li class="physical"  style="float: right;">
                    <button type="button" data-toggle="modal" data-target="#calculatorPopup"   class="btn btn-primary btn-lg" id="calculatorEvent"><i class="fa fa-calculator"></i> Calculator</button>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <input type="hidden" id="minitTextFiled" value="">
        <input type="hidden" id="secondTextField" value="">

        <input type="hidden" id="currentId" value="<?=$firstQuestions->id?>">
        <input type="hidden" id="nextQuestionId" value="<?=$secondID?>">
        <input type="hidden" id="previousQuestionId" value="">
        <input type="hidden" id="courseId" value="<?=$CourseId?>">
        <input type="hidden" id="quizTestId" value="<?=$quizTestid?>">
        <input type="hidden" id="questionNumber" value="1">

        <div class="col-8">
            <div class="question-header">
                <div class="title">
                    <h4 id="questionNumber">Question 1</h4>
                </div>

                <?php
                if ($supportLanguage){
                ?>
                <div>
                    <select class="form-control" name="type" id="type" onchange="ChangeLanguage(this.value)" required>
                        <option value=''>Select Language</option>
                        <option value='1'>हिंदी</option>
                        <option value='2'>English</option>
                    </select>
                </div>
                <?php } ?>

                <div class="marks text-end">
                    <h4 style="font-size: 17px;">Single Correct Option, <span id="mark">+<?=!empty($firstQuestions->mark)?$firstQuestions->mark:0?></span>,<span id="penality">-<?=!empty($firstQuestions->penalty)?$firstQuestions->penalty:'0'?></span></h4>
                </div>
            </div>
            <div class="content">
                <div>
                    <h4 id="questionText"><?=$firstQuestions->question_text?></h4>
                </div>

                <?php
                $optionHeader='Select the correct answer given below.';
                if ($firstQuestions->type==4){
                    $optionHeader='Write your answer given below textBox';
                }
                ?>
                <h4 id="optionsTextheader" style="margin-bottom: 15px;margin-top: 16px;"><?=$optionHeader?></h4>
                <input type="hidden" value="<?=$firstQuestions->type?>" id="quizTypeId">

                <div class="answer-form" id="OptionsArea" style="width: 100%;"  >
                    <?php
                    $f = 'A';

                    if ($firstQuestions->type==1 || $firstQuestions->type==2){
                        $type='checkbox';
                        if ($firstQuestions->type==1){
                            $type='radio';
                        }
                    foreach ($newQuestionOptions as $key=>$itemsOptions){
                    ?>
                    <label class="label-container"><?=$itemsOptions->option_text?>
                        <input type="<?=$type?>"  name="radio" data-options-id="<?=$itemsOptions->id?>" value="<?=$itemsOptions->id?>" <?php if ($itemsOptions->selected_answer==1){echo "checked";} ?>>
                        <span class="checkmark"><?=$f?></span>
                    </label>
                    <?php $f ++;} }elseif ($firstQuestions->type==4){?>
                        <textarea type="text"  name="answarQuestionSubjective" rows="6" id="answarQuestionSubjective" placeholder="Write Answer" class="form-control max-textarea" required><?=$newQuestionOptions[0]->answer_option_text?></textarea>
                        <p id="errorSubjective" style="color: red"></p>
                        <div class="subjectiveForm" style="margin-top: 0px;margin-bottom: 25px;">
                            <form class="uploadSubjectiveForm mt-2 p-0" data-id="610154140cf2d77bece15465" action="<?php echo base_url(); ?>CourseLearners/uploadSubjectiveQuestion" method="POST" enctype="multipart/form-data">
                               <?php
                                 if ($newQuestionOptions[0]->answer_option_file){ ?>
                                     <div class="image-area">
                                         <img src="<?=base_url()?>/assets/subjective/<?=$newQuestionOptions[0]->answer_option_file?>" alt="Preview">
                                         <a class="remove-image" href="#" style="display: inline;" data-quiz-option-id="<?=$newQuestionOptions[0]->id?>">×</a>
                                     </div>
                                <?php }else{ ?>
                                     <h5>Upload <small class="text-gray">You can upload maximum 100 MB of file size.</small></h5>
                                     <div class="col-8 col-sm-12">
                                         <div class="input-group">
                                             <input class="form-input" type="file" name="subjectveFile" id="fileUpload" required>
                                             <input class="form-input" type="hidden" id="dataOptionsId" value="<?=$newQuestionOptions[0]->id?>">
                                             <button type="submit" class="btn btn-outline-primary">Upload</button>
                                         </div>
                                     </div>
                                <?php } ?>
                            </form>
                        </div>
                    <?php }?>
                </div>

            </div>

            <div class="status-bar">
                <div class="save">
                    <button type="button" class="btn btn-success" id="saveAndNext">SAVE & NEXT</button>
                    <button type="button" class="btn btn-primary " id="markReviewAndNext">MARK FOR REVIEW & NEXT</button>
                    <button type="button" class="btn btn-outline-primary" id="clearResponse">CLEAR RESPONSE</button>
                    <span id="completedStatusError" style="color: green;"></span>
                </div>
            </div>
            <div class="status-bar-second">
                <button type="button" class="btn btn-outline-primary disbaleButton" id="previousQuestions">PREVIOUS</button>
                <button type="button" class="btn btn-outline-primary" id="nextQuestions">NEXT QUESTION</button>
                <button type="button" class="btn btn-success" id="submitQuestions" style="float: right">Submit</button>
            </div>
        </div>
        <div class="col-4 right-sidebar">
            <ul class="exam-overview">
                <li>
                    <label class="label-container">Answered
                        <span class="checkmark" id="answeredTotal"><?php echo $totalAnswar;?></span>
                    </label>
                <li>
                    <label class="label-container">Not Answered
                        <span class="checkmark nans" id="countTotalNotAnswar"><?php echo $notAnswar;?></span>
                    </label>
                <li>
                    <label class="label-container">Not Visited
                        <span class="checkmark nvisit">0</span>
                    </label>
                <li>
                    <label class="label-container">Marked for Review
                        <span class="checkmark mfr" id="countTotalReview">0</span>
                    </label>
                <li>
                    <label class="label-container">Answer and Marked for review (will be considered for evaluation)
                        <span class="checkmark amr" id="markAndAnswar">0</span>
                    </label>
            </ul>


            <ul class="pagination pagination" id="questionOptionsBottomArea">
                <?php
                $questionIndex=1;
                foreach ($newQuestions as $key=>$itemQuesitons): ?>
                    <li class="page-item active aria-current <?php if ($itemQuesitons->answar_status==1){echo "answarDone";} ?>">
                        <span class="page-link singleQuestionEvent"  data-question-id="<?=$itemQuesitons->id?>" id="pagainateActiveLingk-<?=$itemQuesitons->id?>"  style="<?php if ($itemQuesitons->answar_status==1){echo "background: green;";} ?>"><?=$questionIndex?></span>
                    </li>
                    <?php $questionIndex ++; endforeach;?>
            </ul>
        </div>
    </div>
</div>

<div class="modal fade" id="questionPaperModal" tabindex="-1" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">All question</h5>
<!--                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>-->
            </div>
            <div class="modal-body">
                <?php
                $itemIndex=1;
                   foreach ($newQuestions as $key=>$questionItems){
                ?>
                <h6>Question <?=$itemIndex?></h6>
                <p><?=$questionItems->question_text?></p>
                <?php $itemIndex ++;} ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!--Calcualtor Popp-->
<div class="modal fade" id="calculatorPopup" tabindex="-1" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content" style="max-width: 560px;">
            <div class="calculator">
                <input type="text" id="screen" maxlength="20">
                <div class="calc-buttons">
                    <div class="functions-one">
                        <button class="button triggers">C</button>
                        <button class="button basic-stuff">(</button>
                        <button class="button basic-stuff">)</button>
                        <button class="button numbers">7</button>
                        <button class="button numbers">8</button>
                        <button class="button numbers">9</button>
                        <button class="button numbers">4</button>
                        <button class="button numbers">5</button>
                        <button class="button numbers">6</button>
                        <button class="button numbers">1</button>
                        <button class="button numbers">2</button>
                        <button class="button numbers">3</button>
                        <button class="button basic-stuff">±</button>
                        <button class="button numbers">0</button>
                        <button class="button basic-stuff">.</button>
                    </div>
                    <div class="functions-two">
                        <button class="button triggers">&#60;=</button>
                        <button class="button complex-stuff">%</button>
                        <button class="button complex-stuff">x !</button>
                        <button class="button complex-stuff">x^</button>
                        <button class="button basic-stuff">*</button>
                        <button class="button basic-stuff">/</button>
                        <button class="button complex-stuff">ln</button>
                        <button class="button complex-stuff">e</button>
                        <button class="button basic-stuff">+</button>
                        <button class="button complex-stuff">x ²</button>
                        <button class="button complex-stuff">log</button>
                        <button class="button complex-stuff">cos</button>
                        <button class="button basic-stuff">-</button>
                        <button class="button complex-stuff">√</button>
                        <button class="button complex-stuff">sin</button>
                        <button class="button complex-stuff">tan</button>
                        <button class="button triggers">=</button>
                        <button class="button complex-stuff">&#x003C0;</button>
                        <button class="button complex-stuff">∘</button>
                        <button class="button complex-stuff">rad</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="submitQuestionsModal" tabindex="-1" aria-labelledby="submitQuestionsModalLabel" aria-hidden="false" style="background: #808080f7;">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Submited</h5>
            </div>
            <div class="modal-body">
             <table>
                 <tr>
                     <td style="width: 302px;"><strong>Total Time</strong></td>
                     <td><strong id="totalAttempTime">0</strong></td>
                 </tr>
                 <tr>
                     <td style="width: 302px;"><strong>Total Questions</strong></td>
                     <td><strong><?=$totalQuestions?></strong></td>
                 </tr>
                 <tr>
                     <td style="width: 302px;"><strong>Attempt Questions</strong></td>
                     <td><strong id="attemptQuestions">5</strong></td>
                 </tr>
                 <tr>
                     <td style="width: 302px;"><strong>Skipped Questions</strong></td>
                     <td><strong id="skippedQuesitons">5</strong></td>
                 </tr>
             </table>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="postMessageContent" value="<?=$post_msg?>">
                <a href="<?php echo base_url() ?>CourseLearners/quizstart?courseid=<?=$_GET['courseid']?>&id=<?=$_GET['id']?>">
                    <button type="button" class="btn btn-primary" id="continueAttemp">Continue Test</button>
                </a>
                <button type="button" class="btn btn-success" id="finalSubmitAttepmt">Submit</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>

<script>
    // $(document).ready(function(){
    //     document.fullScreenElement && null !== document.fullScreenElement || !document.mozFullScreen && !document.webkitIsFullScreen ? document.documentElement.requestFullScreen ? document.documentElement.requestFullScreen() : document.documentElement.mozRequestFullScreen ? document.documentElement.mozRequestFullScreen() : document.documentElement.webkitRequestFullScreen && document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT) : document.cancelFullScreen ? document.cancelFullScreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitCancelFullScreen && document.webkitCancelFullScreen()
    // });
</script>

<script>

    //var availabeTill = new Date("Jul 10, 2021 15:37:25").getTime();




    var minutes=localStorage.getItem('minutes');
    var seconds=localStorage.getItem('seconds');

    var countDownDate = new Date().getTime()+('<?php echo $time_limit;?>' * 60 * 1000);

    if (minutes){
        var countDownDate = new Date().getTime()+(minutes * 60 * 1000);
    }
    // Update the count down every 1 second


    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();
        // Find the distance between now and the count down date
        var distance =countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        localStorage.setItem('minutes', minutes);
        localStorage.setItem('seconds', seconds);

        // Output the result in an element with id="demo"
        document.getElementById("demo").innerHTML =minutes + "m " + seconds + "s ";

        $('#minitTextFiled').val(minutes);
        $('#secondTextField').val(seconds);

        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(x);
            $("button#submitQuestions").trigger("click");
            $("button#finalSubmitAttepmt").trigger("click");
        }

        var currentDate = new Date();
        var currentDateTime=currentDate.toLocaleDateString()+' '+currentDate.toLocaleTimeString();
        var currentDateConvert = new Date(currentDateTime).getTime();
        var availabeTill = new Date('<?php echo $available_till?>').getTime();

        if (currentDateConvert>availabeTill){
            $("button#submitQuestions").trigger("click");
            $("button#finalSubmitAttepmt").trigger("click");
        }
    }, 1000);



    $(document).off('click', '#questionPaperEvent').on('click', '#questionPaperEvent', function (e) {
        e.preventDefault();
        $('#questionPaperModal').modal('show');
    });

    $(document).off('click', '#calculatorEvent').on('click', '#calculatorEvent', function (e) {
        e.preventDefault();
        $('#calculatorPopup').modal('show');
    });

    $(document).off('click', '#submitQuestions').on('click', '#submitQuestions', function (e) {
        e.preventDefault();
        $('#submitQuestionsModal').modal('show');
        var attemptQuestion=$('span#answeredTotal').text();
        var skippedQuestions =$('span#countTotalNotAnswar').text();

        $('strong#attemptQuestions').html(attemptQuestion);
        $('strong#skippedQuesitons').html(skippedQuestions);

        var setMinitTotal   ='<?php echo $time_limit;?>';
        var minitAttempt    =$('input#minitTextFiled').val();
        var secondAttempt    =$('input#secondTextField').val();

        var countMinute=setMinitTotal-minitAttempt;
        var displaymin='0';
        if (countMinute>1){
            displaymin=countMinute;
        }
        var countSecond=60-secondAttempt;

       var intotalMin=displaymin+'m'+' '+countSecond+ 's';

        $('strong#totalAttempTime').html(intotalMin);

        clearInterval(x);
        document.getElementById("demo").innerHTML = "Completed";

    });

    $(document).off('click', 'button#finalSubmitAttepmt').on('click', 'button#finalSubmitAttepmt', function (e) {
        e.preventDefault();

        localStorage.setItem('minutes', '');
        localStorage.setItem('seconds', '');

        var totalTimeTaken=$('strong#totalAttempTime').text();
        var quizTestId=$('input#quizTestId').val();
        var courseId=$('input#courseId').val();
        var redirectUrl= '<?php echo base_url() ?>/CourseLearners/attemptdashboard?courseid='+courseId+'&id='+quizTestId+'';

        var subjectiveUrl  = '<?php echo base_url() ?>/CourseLearners/attemptSubjective?courseid='+courseId+'&id='+quizTestId+'';
        var quizTestMessage=$('#postMessageContent').val();

        if (quizTestId){
            $.ajax({
                url: '<?php echo base_url() ?>/CourseLearners/updateQuizTotalAttemptTime',
                type: 'POST',
                data: {
                    totalTimeTaken: totalTimeTaken,
                    quizTestId: quizTestId,
                },
                success: function (response) {
                    if (response==1){

                        if (quizTestMessage=='') {
                            $('.modal-footer').html("<p style='font-size: 19px;\n" +
                                "color: green;'>You have completed your Test....</p>");
                        }else {
                            $('.modal-footer').html(quizTestMessage);
                        }

                        setTimeout(function(){
                            window.location.href=redirectUrl;
                        },1000);
                    }else {
                        if (quizTestMessage=='') {
                            $('.modal-footer').html("<p style='font-size: 19px;\n" +
                                "color: green;'>You are completed your quiz test...</p>");
                        }else {
                            $('.modal-footer').html(quizTestMessage);
                        }

                        setTimeout(function(){
                            window.location.href=subjectiveUrl;
                        },1000);
                    }
                }
            });
        }
    });

    $(document).off('click', '#markReviewAndNext').on('click', '#markReviewAndNext', function (e) {
        e.preventDefault();

        var currentId=$('input#currentId').val();
        var nextQuestionId=$('input#nextQuestionId').val();

        $('span#pagainateActiveLingk-' + currentId + '').parent().addClass('markReview');
        $('span#pagainateActiveLingk-' + currentId + '').css('background','#FFC300');

        if ($('span#pagainateActiveLingk-' + currentId + '').parent().hasClass("answarDone")) {

            $('span#pagainateActiveLingk-' + currentId + '').parent().removeClass('markReview');
            $('span#pagainateActiveLingk-' + currentId + '').parent().addClass('markReviewAndAnswar');

            $('span#pagainateActiveLingk-' + currentId + '').css('background','deepskyblue');

            var markAndAnswar = $('nav#markColorLengt ul > .markReviewAndAnswar').length;
            $('span#markAndAnswar').html(markAndAnswar);

        }else {

            var markColor = $('nav#markColorLengt ul > .markReview').length;
            $('span#countTotalReview').html(markColor);
        }

    });

    CKEDITOR.replace( 'answarQuestionSubjective');

    $(document).off('click', '#saveAndNext').on('click', '#saveAndNext', function (e) {
        e.preventDefault();

        var quizTestId=$('input#quizTestId').val();
        var currentId=$('input#currentId').val();
        var quizTyeId   =$('input#quizTypeId').val();



        if (quizTyeId==3){
            var valueOfSubjective = $('input#answerQuestionsFillIntheBlank').val()||'';
        }

        if (quizTyeId==4){
            var valueOfSubjective = CKEDITOR.instances['answarQuestionSubjective'].getData()||'';
            if (valueOfSubjective==''){
                $('p#errorSubjective').html('Answer can not blak');
                return false;
            }else {
                $('p#errorSubjective').html('');
            }
        }

        var OptionsSelectedId=[];
        $.each($("input[name='radio']:checked"), function(){
            OptionsSelectedId.push($(this).val())
        });

        if (currentId && OptionsSelectedId) {
            $.ajax({
                url: '<?php echo base_url() ?>/CourseLearners/givenAnswar',
                type: 'POST',
                data: {
                    OptionsSelectedId: OptionsSelectedId,
                    questionID: currentId,
                    quizTestId: quizTestId,
                    quizTyeId: quizTyeId,
                    valueOfSubjective: valueOfSubjective,
                },
                success: function (response) {
                    var objArray = jQuery.parseJSON(response);
                    $('span#answeredTotal').html(objArray.answar);
                    $('span#countTotalNotAnswar').html(objArray.notAnswar);

                    $('span#pagainateActiveLingk-' + currentId + '').css('background','green');
                    $('span#pagainateActiveLingk-' + currentId + '').parent().addClass('answarDone');
                }
            });
        }
    });


    $(document).off('click', '#clearResponse').on('click', '#clearResponse', function (e) {
        e.preventDefault();
        var quizTestId=$('input#quizTestId').val();
        var currentId=$('input#currentId').val();
        if (currentId) {
            $.ajax({
                url: '<?php echo base_url() ?>/CourseLearners/removedAnswar',
                type: 'POST',
                data: {
                    questionID: currentId,
                    quizTestId: quizTestId,
                },
                success: function (response) {

                    $('input[name="radio"]').prop('checked', false);

                    var objArray = jQuery.parseJSON(response);
                    $('span#answeredTotal').html(objArray.answar);
                    $('span#countTotalNotAnswar').html(objArray.notAnswar);

                    $('span#pagainateActiveLingk-' + currentId + '').css('background','red');
                    $('span#pagainateActiveLingk-' + currentId + '').parent().removeClass('answarDone');
                }
            });
        }
    });

    var nextQuestionNumber=2;
    $(document).off('click', '#nextQuestions , #markReviewAndNext , #saveAndNext').on('click', '#nextQuestions , #markReviewAndNext , #saveAndNext', function (e) {
        e.preventDefault();

        var nextQuestionId=$('input#nextQuestionId').val();
        var currentId=$('input#currentId').val();
        var courseId=$('input#courseId').val();
        var quizTestId=$('input#quizTestId').val();
        var questionNumber=$('input#questionNumber').val();

        if (nextQuestionId && courseId && quizTestId) {
            $.ajax({
                url: '<?php echo base_url() ?>/CourseLearners/nextQuizQuestions',
                type: 'POST',
                data: {
                    nextQuestionId: nextQuestionId,
                    questionNumber: questionNumber,
                    quizTestId: quizTestId,
                    courseId: courseId,
                },
                success: function (response) {
                    var objArray = jQuery.parseJSON(response);

                    $('h4#optionsTextheader').html(objArray.optionsMessage);
                    $('input#quizTypeId').val(objArray.type);

                    $('span#mark').html('+'+ objArray.mark);
                    $('span#penality').html('-'+ objArray.penality);

                    $('input#nextQuestionId').val(objArray.nextId);
                    $('input#previousQuestionId').val(currentId);
                    $('input#currentId').val(nextQuestionId);
                    $('#questionText').html(objArray.questionText);
                    $('#OptionsArea').html(objArray.optionsHtml);

                    if (objArray.nextId==''){
                        $('button#nextQuestions').addClass('disbaleButton');
                    }

                    $('button#previousQuestions').removeClass('disbaleButton');

                    $('h4#questionNumber').html("Questions "+objArray.positions+"");

                    CKEDITOR.replace( 'answarQuestionSubjective');

                    nextQuestionNumber++;
                }
            });
        }else {
            $('span#completedStatusError').html('Completed All Question');
            setTimeout(function(){
                $('span#completedStatusError').html('');
            },5000);
        }
    });

    $(document).off('click', '#previousQuestions').on('click', '#previousQuestions', function (e) {
        e.preventDefault();

        var previousQuestionId      =$('input#previousQuestionId').val();
        var courseId                =$('input#courseId').val();
        var quizTestId              =$('input#quizTestId').val();
        var questionNumber          =$('input#questionNumber').val();
        var currentId               =$('input#currentId').val();

        var previousQUesitonNumber=nextQuestionNumber-2;
        if (previousQuestionId && courseId && quizTestId) {
            $.ajax({
                url: '<?php echo base_url() ?>/CourseLearners/previousQuizQuestions',
                type: 'POST',
                data: {
                    previousQuestionId: previousQuestionId,
                    questionNumber: questionNumber,
                    quizTestId: quizTestId,
                    courseId: courseId,
                },
                success: function (response) {
                    var objArray = jQuery.parseJSON(response);

                    $('h4#optionsTextheader').html(objArray.optionsMessage);
                    $('input#quizTypeId').val(objArray.type);

                    $('span#mark').html('+'+ objArray.mark);
                    $('span#penality').html('-'+ objArray.penality);

                    $('input#nextQuestionId').val(currentId);
                    $('input#previousQuestionId').val(objArray.previousQuestionId);
                    $('input#currentId').val(previousQuestionId);

                    $('#questionText').html(objArray.questionText);
                    $('#OptionsArea').html(objArray.optionsHtml);

                    if (objArray.previousQuestionId==''){
                        $('button#previousQuestions').addClass('disbaleButton');
                    }
                    $('button#nextQuestions').removeClass('disbaleButton');

                    $('h4#questionNumber').html("Questions "+previousQUesitonNumber+"");

                    CKEDITOR.replace( 'answarQuestionSubjective');

                    nextQuestionNumber--;
                }
            });
        }else {
            $('span#completedStatusError').html('Completed Your Previous Request');
            setTimeout(function(){
                $('span#completedStatusError').html('');
            },5000);
        }
    });

   //Single Question Click Events
    $(document).off('click', 'span.singleQuestionEvent').on('click', 'span.singleQuestionEvent', function (e) {
        e.preventDefault();

        var courseId                =$('input#courseId').val();
        var quizTestId              =$('input#quizTestId').val();
        var questionId              =$(this).data('question-id');
        var quizTestMessage=$('#postMessageContent').val();

        if (courseId && quizTestId) {
            $.ajax({
                url: '<?php echo base_url() ?>/CourseLearners/singleQuizQuestions',
                type: 'POST',
                data: {
                    questionId: questionId,
                    quizTestId: quizTestId,
                    courseId: courseId,
                },
                success: function (response) {
                    var objArray = jQuery.parseJSON(response);

                    $('h4#optionsTextheader').html(objArray.optionsMessage);
                    $('input#quizTypeId').val(objArray.type);

                    $('span#mark').html('+'+ objArray.mark);
                    $('span#penality').html('-'+ objArray.penality);

                    $('input#nextQuestionId').val(objArray.nextId);
                    $('input#previousQuestionId').val(objArray.previousId);
                    $('input#currentId').val(objArray.currentId);
                    $('#questionText').html(objArray.questionText);
                    $('#OptionsArea').html(objArray.optionsHtml);


                    if (objArray.nextId==''){
                        $('button#nextQuestions').addClass('disbaleButton');
                    }else {
                        $('button#nextQuestions').removeClass('disbaleButton');
                    }

                    if (objArray.previousId==''){
                        $('button#previousQuestions').addClass('disbaleButton');
                    }else {
                        $('button#previousQuestions').removeClass('disbaleButton');
                    }

                    $('h4#questionNumber').html("Questions "+objArray.positions+"");

                    //CKEDITOR.replace( 'answarQuestionSubjective');

                    nextQuestionNumber++;
                }
            });
        }else {
            $('span#completedStatusError').html('Completed Your Previous Request');
            setTimeout(function(){
                $('span#completedStatusError').html('');
            },5000);
        }
    });

</script>

<script>
    function ChangeLanguage(value) {

        var LanguageType=value;
        var currentId=$('input#currentId').val();
        var courseId=$('input#courseId').val();
        var quizTestId=$('input#quizTestId').val();
        var questionNumber=$('input#questionNumber').val();

        if (currentId && courseId && quizTestId) {
            $.ajax({
                url: '<?php echo base_url() ?>/CourseLearners/CurrentQuizQuestions',
                type: 'POST',
                data: {
                    currentId: currentId,
                    questionNumber: questionNumber,
                    quizTestId: quizTestId,
                    courseId: courseId,
                    LanguageType: LanguageType,
                },
                success: function (response) {
                    var objArray = jQuery.parseJSON(response);
                    $('#questionText').html(objArray.questionText);
                    $('#OptionsArea').html(objArray.optionsHtml);
                    $('h4#optionsTextheader').html(objArray.optionsMessage);
                    $('input#quizTypeId').val(objArray.type);
                }
            });
        }else {
            $('span#completedStatusError').html('Completed All Question');
            setTimeout(function(){
                $('span#completedStatusError').html('');
            },5000);
        }

    }

    $(document).off('click', 'a.remove-image').on('click', 'a.remove-image', function (e) {
        e.preventDefault();
        var removedOptionsId=$(this).data('quiz-option-id');
        if (removedOptionsId) {
            $.ajax({
                url: '<?php echo base_url() ?>/CourseLearners/removedSubjectiveImage',
                type: 'POST',
                data: {
                    removedOptionsId: removedOptionsId,
                },
                success: function (response) {
                    var htmlUpload=' <h5>Upload <small class="text-gray">You can upload maximum 100 MB of file size.</small></h5>\n' +
                        '                                     <div class="col-8 col-sm-12">\n' +
                        '                                         <div class="input-group">\n' +
                        '                                             <input class="form-input" type="file" name="subjectveFile" id="fileUpload" required>\n' +
                        '                                             <button type="submit" class="btn btn-outline-primary">Upload</button>\n' +
                        '                                         </div>\n' +
                        '                                     </div>';

                    $('form.uploadSubjectiveForm.mt-2.p-0').html(htmlUpload);
                }
            });
        }
    });

    $(document).off('submit', '.uploadSubjectiveForm').on('submit', '.uploadSubjectiveForm', function (e) {
        e.preventDefault();
        var url=$(this).attr('action');
        var parentThis = $('input#fileUpload');
        var fileData  = parentThis.prop("files")[0];
        var fileName  = fileData.name;
        var fileError = 0;
        var optionsId=$('input#dataOptionsId').val();

        console.log('File Data');
        console.log(fileData);

        var fileExt = fileName.split('.').pop().toLowerCase();
        if(jQuery.inArray(fileExt, ['gif', 'png', 'jpg', 'jpeg', 'pdf']) == -1)
        {
            fileError = 1;
            alert("Uploaded an invalid file type");

        }

        // Validation: File Size
        var fileSize = fileData.size || fileData.fileSize;
        if(fileSize > (1024 * 1024 * 2))
        {
            fileError = 1;
            alert('Please select file size less than 2 MB');
        }

        if(fileError == 0){
            var formData = new FormData();
            formData.append("file", fileData);
            formData.append("fileName", fileName);
            formData.append("questionID", optionsId);

            $.ajax({
                url: url,
                contentType: false,
                cache: false,
                processData: false,
                method: "POST",
                data: formData,
                success: function (response) {
                    var imgUrl='<?php echo base_url() ?>/assets/subjective/'+response+'';
                    var imgBlock='<div class="image-area">\n' +
                        '  <img src="'+imgUrl+'"  alt="Preview">\n' +
                        '  <a data-quiz-option-id="'+optionsId+'" class="remove-image" href="#" style="display: inline;">&#215;</a>\n' +
                        '</div>';

                    $('form.uploadSubjectiveForm.mt-2.p-0').html(imgBlock);
                }
            });


        }

    });




    /*Calculator Js*/
    var display = document.getElementById("screen");
    var buttons = document.getElementsByClassName("button");

    Array.prototype.forEach.call(buttons, function(button) {
        button.addEventListener("click", function() {
            if (button.textContent != "=" &&
                button.textContent != "C" &&
                button.textContent != "x" &&
                button.textContent != "÷" &&
                button.textContent != "√" &&
                button.textContent != "x ²" &&
                button.textContent != "%" &&
                button.textContent != "<=" &&
                button.textContent != "±" &&
                button.textContent != "sin" &&
                button.textContent != "cos" &&
                button.textContent != "tan" &&
                button.textContent != "log" &&
                button.textContent != "ln" &&
                button.textContent != "x^" &&
                button.textContent != "x !" &&
                button.textContent != "π" &&
                button.textContent != "e" &&
                button.textContent != "rad"
                && button.textContent != "∘") {
                display.value += button.textContent;
            } else if (button.textContent === "=") {
                equals();
            } else if (button.textContent === "C") {
                clear();
            } else if (button.textContent === "x") {
                multiply();
            } else if (button.textContent === "÷") {
                divide();
            } else if (button.textContent === "±") {
                plusMinus();
            } else if (button.textContent === "<=") {
                backspace();
            } else if (button.textContent === "%") {
                percent();
            } else if (button.textContent === "π") {
                pi();
            } else if (button.textContent === "x ²") {
                square();
            } else if (button.textContent === "√") {
                squareRoot();
            } else if (button.textContent === "sin") {
                sin();
            } else if (button.textContent === "cos") {
                cos();
            } else if (button.textContent === "tan") {
                tan();
            } else if (button.textContent === "log") {
                log();
            } else if (button.textContent === "ln") {
                ln();
            } else if (button.textContent === "x^") {
                exponent();
            } else if (button.textContent === "x !") {
                factorial();
            } else if (button.textContent === "e") {
                exp();
            } else if (button.textContent === "rad") {
                radians();
            } else if (button.textContent === "∘") {
                degrees();
            }
        });
    });


    function syntaxError() {
        if (eval(display.value) == SyntaxError || eval(display.value) == ReferenceError || eval(display.value) == TypeError) {
            display.value == "Syntax Error";
        }
    }


    function equals() {
        if ((display.value).indexOf("^") > -1) {
            var base = (display.value).slice(0, (display.value).indexOf("^"));
            var exponent = (display.value).slice((display.value).indexOf("^") + 1);
            display.value = eval("Math.pow(" + base + "," + exponent + ")");
        } else {
            display.value = eval(display.value)
            checkLength()
            syntaxError()
        }
    }

    function clear() {
        display.value = "";
    }

    function backspace() {
        display.value = display.value.substring(0, display.value.length - 1);
    }

    function multiply() {
        display.value += "*";
    }

    function divide() {
        display.value +=  "/";
    }

    function plusMinus() {
        if (display.value.charAt(0) === "-") {
            display.value = display.value.slice(1);
        } else {
            display.value = "-" + display.value;
        }
    }

    function factorial() {
        var number = 1;
        if (display.value === 0) {
            display.value = "1";
        } else if (display.value < 0) {
            display.value = "undefined";
        } else {
            var number = 1;
            for (var i = display.value; i > 0; i--) {
                number *=  i;
            }
            display.value = number;
        }
    }

    function pi() {
        display.value = (display.value * Math.PI);
    }

    function square() {
        display.value = eval(display.value * display.value);
    }

    function squareRoot() {
        display.value = Math.sqrt(display.value);
    }

    function percent() {
        display.value = display.value / 100;
    }

    function sin() {
        display.value = Math.sin(display.value);
    }

    function cos() {
        display.value = Math.cos(display.value);
    }

    function tan() {
        display.value = Math.tan(display.value);
    }

    function log() {
        display.value = Math.log10(display.value);
    }

    function ln() {
        display.value = Math.log(display.value);
    }

    function exponent() {
        display.value += "^";
    }

    function exp() {
        display.value = Math.exp(display.value);
    }

    function radians() {
        display.value = display.value * (Math.PI / 180);
    }

    function degrees() {
        display.value = display.value * (180 / Math.PI);
    }


    $(document).off('click', '#subjectEvent').on('click', '#subjectEvent', function (e) {
        e.preventDefault();
        var subjectName=$(this).data('subject-name');
        var quizTestId=$('input#quizTestId').val();
        if (quizTestId) {
            $.ajax({
                url: '<?php echo base_url() ?>/CourseLearners/subjectiveQuiz',
                type: 'POST',
                data: {
                    subjectName: subjectName,
                    quizTestId: quizTestId,
                },
                success: function (response) {
                    var objArray = jQuery.parseJSON(response);

                    $('h4#optionsTextheader').html(objArray.optionsMessage);
                    $('input#quizTypeId').val(objArray.type);

                    $('span#mark').html('+'+ objArray.mark);
                    $('span#penality').html('-'+ objArray.penality);

                    $('input#nextQuestionId').val(objArray.nextId);
                    $('input#previousQuestionId').val(objArray.previousId);
                    $('input#currentId').val(objArray.currentId);
                    $('#questionText').html(objArray.questionText);
                    $('#OptionsArea').html(objArray.optionsHtml);


                    if (objArray.nextId==''){
                        $('button#nextQuestions').addClass('disbaleButton');
                    }else {
                        $('button#nextQuestions').removeClass('disbaleButton');
                    }

                    if (objArray.previousId==''){
                        $('button#previousQuestions').addClass('disbaleButton');
                    }else {
                        $('button#previousQuestions').removeClass('disbaleButton');
                    }

                    $('h4#questionNumber').html("Questions "+objArray.positions+"");

                    //CKEDITOR.replace( 'answarQuestionSubjective');

                    nextQuestionNumber++;
                }
            });

        }
    });

    $(document).ready(function(){
        $("button#myButton").trigger('click');
    });

    /* Get the element you want displayed in fullscreen */
    var elem = document.documentElement;
    /* Function to open fullscreen mode */
    function openFullscreen() {
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.mozRequestFullScreen) { /* Firefox */
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
            elem.webkitRequestFullscreen();
        } else if (elem.msRequestFullscreen) { /* IE/Edge */
            elem.msRequestFullscreen();
        }
    }

    /* Function to close fullscreen mode */
    function closeFullscreen() {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
</script>
