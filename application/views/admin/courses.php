<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
  

$courseData = $this->db->query("SELECT * FROM course WHERE status = 1 AND is_published = 1  ORDER BY 1 DESC")->result();

$totalcourseData = $this->db->query("SELECT count(1) as totalcourseData FROM course WHERE status = 1 AND is_published = 1  ORDER BY 1 DESC")->result();
foreach($totalcourseData as $gettotalcourseData){
    $totalcourseData = $gettotalcourseData->totalcourseData;
}


//search criteria
if(isset($_POST["search"])){
$where = " ";
    
    if(!empty($_POST['course_title'])){
        $title = $_POST['course_title'];
        $where .= " AND title = '$title'";
    }
    
    if(!empty($_POST['category'])){
        $category = $_POST['category'];
        $where .= " AND category = '$category'";
    }
    
    if(!empty($_POST['instructor'])){
        $instructor = $_POST['instructor'];
        $where .= " AND instructor = '$instructor'";
    }
    
    if(!empty($_POST['price']) && !empty($_POST['price_value'])){
        $price = $_POST['price'];
        $price_value = $_POST['price_value'];
        $where .= " AND payable_price $price $price_value";
    }
    
    if(!empty($_POST['from_date']) && empty($_POST['to_date'])){
        $fromDate = $_POST['from_date'];
        $fromDate = date("Y-m-d", strtotime($fromDate));
        $where .= " AND created_date >= '$fromDate'";
    }
    
    if(!empty($_POST['to_date']) && empty($_POST['from_date'])){
        $toDate = $_POST['to_date'];
         
        $toDate = date("Y-m-d", strtotime($toDate));
        $where .= " AND created_date <= '$toDate'";
    }
    
    if(!empty($_POST['to_date']) && !empty($_POST['from_date'])){
        $toDate = $_POST['to_date'];
        $toDate = date("Y-m-d", strtotime($toDate));
        $fromDate = $_POST['from_date'];
        $fromDate = date("Y-m-d", strtotime($fromDate));
        $where .= " AND created_date BETWEEN '$fromDate' AND '$toDate'";
    }    
$courseData = $this->db->query("SELECT * FROM course c LEFT JOIN course_price cp ON cp.course_id = c.course_id  WHERE c.status = 1 AND is_published = 1  ".$where." ORDER BY 1 DESC")->result();

}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Competitive Exam Guide || Courses </title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="description" content="<?php echo $meta_description?>">
        <meta name="keywords" content="<?php echo $meta_keywords ?>">
        <meta name="author" content="#">
		<!-- Favicon icon -->
		<link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
		<!-- Google font-->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
		<!-- Required Fremwork -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
		<!-- themify-icons line icon -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
		<!-- ico font -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
		<!-- feather Awesome -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
		<!-- Date-time picker css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
		<!-- Date-range picker css  -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
		<!-- Date-Dropper css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
		<!-- Color Picker css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
		<!-- Mini-color css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
		<!-- sweet alert framework -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/sweetalert/css/sweetalert.css">
		<!-- Data Table Css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
		<!-- Style.css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
		
	</head>
	<body>
	    <?php if($this->session->flashdata('marketing_msg')){ ?>						            
	    <script>
        	$(document).ready(function(){
        		$("#myModal").modal('show');
        	});
        </script>
	    <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
        				<b style="color:green"><?php echo $this->session->flashdata('marketing_msg') ?></b>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>
		<?php if($this->session->flashdata('df_msg')){ ?>						            
	    <script>
        	$(document).ready(function(){
        		$("#myModal").modal('show');
        	});
        </script>
	    <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
        				<b style="color:green"><?php echo $this->session->flashdata('df_msg') ?></b>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>
		<?php if($this->session->flashdata('ds_msg')){ ?>						            
	    <script>
        	$(document).ready(function(){
        		$("#myModal").modal('show');
        	});
        </script>
	    <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
        				<b style="color:green"><?php echo $this->session->flashdata('ds_msg') ?></b>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>
		<!-- Pre-loader start -->
		<div class="theme-loader">
			<div class="ball-scale">
				<div class='contain'>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Pre-loader end -->
		<div id="pcoded" class="pcoded">
			<div class="pcoded-overlay-box"></div>
			<div class="pcoded-container navbar-wrapper">
				<?php include("includes/header.php"); ?>
				<!-- Sidebar inner chat end-->
				<div class="pcoded-main-container">
					<div class="pcoded-wrapper">
						<?php include("includes/sidenav.php"); ?>
						<div class="pcoded-content">
							<div class="pcoded-inner-content">
								<!-- Main-body start -->
								<div class="main-body">
									<div class="page-wrapper">
										<!-- Page-header start -->
										<div class="page-header">
											<div class="row align-items-end">
												<div class="col-lg-7">
													<div class="page-header-title">
														<div class="d-inline">
															<h4>Courses(<?php echo $totalcourseData?>)</h4>
														</div>
													</div>
												</div>
												<div class="col-lg-2">
													<button type="button" class="btn btn-primary btn-outline-primary" data-toggle="collapse" data-target="#demo">Advanced Search</button>
												</div>
												<div class="col-lg-1">
													<a href="<?php echo base_url() ?>CourseInfo"><button   class="btn btn-primary btn-outline-primary" ><i class="icofont icofont-plus"></i>Create Course</button></a>
												</div>
												
												
											</div>
										</div>
										<!-- Page-header end -->
										<!-- Page-body start -->
										<div class="page-body">
											
											<div id="demo" class="collapse">
												<form method="post" autocomplete="off">
													<div class="row">
														<div class="col-sm-12 col-xl-3 m-b-30">
                                                        <h4 class="sub-title">From Date:</h4>
                                                         <input type="text" name="from_date" id="frmDate" value="<?php echo $_POST['from_date'] ?>" placeholder="From Date" class="form-control" />
                                                    </div>
                                                    <div class="col-sm-12 col-xl-3 m-b-30">
                                                        <h4 class="sub-title">To Date:</h4>
                                                         <input type="text" name="to_date" id="toDate" value="<?php echo $_POST['to_date'] ?>" placeholder="To Date" class="form-control" />
                                                    </div>
														
														<div class="col-sm-12 col-xl-6 m-b-30">
															<h4 class="sub-title">Price:</h4>
															<div class="row">
																<div class="col-sm-8">
																	<select name="price" class="form-control">
																		<option value='>' <?php if($_POST["price"] == '>'){ ?> selected <?php } ?>>Greater than</option>
																		<option value='<' <?php if($_POST["price"] == '<'){ ?> selected <?php } ?>>Less than</option>
																		<option value='=' <?php if($_POST["price"] == '='){ ?> selected <?php } ?>>Equal to</option>
																	</select>
																</div>
																<div class="col-sm-4">
																	<input type="number" class="form-control" name="price_value" value="<?php echo $_POST['price_value'];  ?>" placeholder="Price">
																</div>
															</div>
															
														</div>
														<div class="col-sm-12 col-xl-4 m-b-30">
															<h4 class="sub-title">Course Title:</h4>
															<input type="text" class="form-control" name="course_title" value="<?php echo $_POST['course_title'] ?>"  placeholder="Course Title">
														</div>
														<div class="col-sm-12 col-xl-4 m-b-30">
															<h4 class="sub-title">Category:</h4>
															<input type="text" class="form-control" name="category" value="<?php echo $_POST['category'] ?>" placeholder="Category">
														</div>
														<div class="col-sm-12 col-xl-4 m-b-30">
															<h4 class="sub-title">Instructor:</h4>
															<input type="text" class="form-control" name="instructor" value="<?php echo $_POST['instructor'] ?>" placeholder="Instructor">
														</div>
														<div class="col-sm-8">
														</div>
														<div class="col-sm-1">
															<button  class="btn btn-primary btn-outline-primary" type="submit" name="search" >Search</button>
														</div>
														<div class="col-sm-1">
															<button  class="btn btn-primary btn-outline-primary" type="reset">Clear</button>
														</div>
														<div class="col-sm-12">
															&nbsp;
														</div>
														
													</div>
												</form>
											</div>
											
										<!--	<center><b style="color:green;"><?php echo $this->session->flashdata('marketing_msg'); ?></b></center><br>
											<center><b style="color:red;"><?php echo $this->session->flashdata('df_msg'); ?></b></center><br>
											<center><b style="color:green;"><?php echo $this->session->flashdata('ds_msg'); ?></b></center><br>-->
											
											<div class="row" id="draggablePanelList">
												<?php
												foreach($courseData as $getcourseData){
												$courseId = $getcourseData->course_id;
												
												
												?>
												<div class="col-lg-12 col-xl-4">
													<div class="card-sub">
														<img class="card-img-top img-fluid" src="<?php echo base_url() ?>assets/course_images/<?php echo $getcourseData->image; ?>" alt="Card image cap" style="max-height:200px;min-height:200px;">
														<div class="card-block">
															<h5 class="card-title" data-toggle="tooltip" data-placement="top" title="<?php echo $getcourseData->title; ?>">
															<?php
															if(strlen($getcourseData->title) > 25){
															echo substr($getcourseData->title, 0, 25).'...';
															}else{
															echo $getcourseData->title;
															}
															?>
															</h5>
															<p class="card-text">Enrolled Students: 23</p>
															<p class="card-text">Instructor: <?php echo $getcourseData->instructor ?></p>
															<!--<p class="card-text">
																Course Type:
																<?php if($getcourseData->type == 1){
																echo 'Normal';
																}elseif($getcourseData->type == 2){
																echo 'SD Card';
																}
																?>
															</p>
															<p class="card-text">
																Is Published:
																<?php if($getcourseData->is_published == 1){
																echo 'Yes';
																}elseif($getcourseData->is_published == 2){
																echo 'No';
																}
																?>
															</p>
															<span><?php echo $getcourseData->created_date ?></span>-->
														</div>
														<hr>
														<center>
														<a href="<?php echo base_url() ?>CourseInfo?courseid=<?php echo $courseId ?>"><button class="btn btn-inverse btn-outline-inverse btn-icon" data-toggle="tooltip" data-placement="top" title="Details"><i class="icofont icofont-info-circle"></i></button></a>
														<a href="<?php echo base_url() ?>Coursebuilder?courseid=<?php echo $courseId ?>"><button class="btn btn-inverse btn-outline-inverse btn-icon" data-toggle="tooltip" data-placement="top" title="Course Builder"><i class="icofont icofont-settings-alt"></i></button></a>
														<a href="<?php echo base_url() ?>Learners?courseid=<?php echo $courseId ?>"><button class="btn btn-inverse btn-outline-inverse btn-icon" data-toggle="tooltip" data-placement="top" title="Learners"><i class="icofont icofont-users-social"></i></button></a>
														<a href="<?php echo base_url() ?>Accesscodes?courseid=<?php echo $courseId ?>"><button class="btn btn-inverse btn-outline-inverse btn-icon" data-toggle="tooltip" data-placement="top" title="Access Codes"><i class="icofont icofont-ui-text-chat"></i></button></a>
														<button  onclick ="alert_msg('<?php echo $courseId ?>')" value="<?php echo $courseId ?>" name="del_course" id="del_course" class="btn btn-inverse btn-outline-inverse btn-icon" data-toggle="tooltip" data-placement="top" title="Delete"><i class="icofont icofont-delete-alt"></i></button>
														</center>
														<p>&nbsp;</p>
													</div>
												</div>
												<?php } ?>
												
												
											</div>
											
										</div>
										<!-- Page-body end -->
									</div>
								</div>
							</div>
							<!-- Main-body end -->
							<!-- <div id="styleSelector">
							</div>-->
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Warning Section Starts -->
		<!-- Older IE warning message -->
		
		<!-- Warning Section Ends -->
		<!-- Required Jquery -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
		<!-- jquery slimscroll js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
		<!-- modernizr js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
		<!-- data-table js -->
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
		<!-- Bootstrap date-time-picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
		<!-- Date-range picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
		<!-- Date-dropper js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
		<!-- Color picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
		<!-- Mini-color js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
		<!-- i18next.min.js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
		<!-- Custom js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
		
		<script src="<?php echo base_url() ?>assets/files/assets/pages/sortable-custom.js"></script>
		
		<!-- sweet alert js -->
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<!-- sweet alert modal.js intialize js -->
		<!-- modalEffects js nifty modal window effects -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/modalEffects.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/classie.js"></script>
		<!-- Date-range picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-23581568-13');
		</script>
		<script>
		function alert_msg(Id){
		swal({
		title: "Are you sure?",
		text: "Once deleted, you will not be able to recover this Course again!",
		icon: "warning",
		buttons: true,
		dangerMode: true,
		})
		.then((willDelete) => {
		if (willDelete) {
		/*swal("Poof! Your Course has been deleted!", {
		icon: "success",
		});*/
		location.href = '<?php echo base_url() ?>CourseInfo/deletecourse?courseid='+Id;
		} else {
		swal("Your Course is safe!");
		}
		});
		}
		</script>
		<script>
            $(document).ready(function () {
                $('input[id$=frmDate]').datepicker({
                    dateFormat: 'dd-mm-yy'
                });
                $('input[id$=toDate]').datepicker({
                    dateFormat: 'dd-mm-yy'
                });
            });
        </script>

	</body>
</html>