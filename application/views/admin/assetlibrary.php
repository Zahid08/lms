<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
   
?>
<?php 
$recordings=$this->db->query('SELECT s.path
                        	,s.subchapter_name
                        	,s.created_date
                        	,s.updated_by
                        	,s.id
                        	,s.created_by
                        	,s.updated_date
                        	,u.username as username
                        FROM sub_chapters s
                        INNER JOIN users u ON u.id = s.created_by ORDER BY 1 DESC')->result();

if(isset($_POST['assetlibrary_search'])){
    $where = "where 1 = 1 ";
    if(!empty($_POST['asset_type'])){
        $asset_type = $_POST['asset_type'];
        $where .= " AND type = '$asset_type'";
    }
    
    if(!empty($_POST['sub_chapter_name'])){
        $subChapterName = $_POST['sub_chapter_name'];
        $where .= " AND s.subchapter_name = '$subChapterName'";
    }
    
    if(!empty($_POST['created_from_date']) && empty($_POST['created_to_date'])){
        $createdfromDate = $_POST['created_from_date'];
        $createdfromDate = date("Y-m-d", strtotime($createdfromDate));
        $where .= " AND s.created_date >= '$createdfromDate'";
    }
    
    if(!empty($_POST['created_to_date']) && empty($_POST['created_from_date'])){
        $createdtoDate = $_POST['created_to_date'];
         
        $createdtoDate = date("Y-m-d", strtotime($createdtoDate));
        $where .= " AND s.created_date <= '$createdtoDate'";
    }
    
    if(!empty($_POST['created_to_date']) && !empty($_POST['created_from_date'])){
        $createdtoDate = $_POST['created_to_date'];
        $createdtoDate = date("Y-m-d", strtotime($createdtoDate));
        $createdfromDate = $_POST['created_from_date'];
        $createdfromDate = date("Y-m-d", strtotime($createdfromDate));
        $where .= " AND s.created_date BETWEEN '$createdfromDate' AND '$createdtoDate'";
    }
    
    
    if(!empty($_POST['modified_from_date']) && empty($_POST['modified_to_date'])){
        $modifiedfromDate = $_POST['modified_from_date'];
        $modifiedfromDate = date("Y-m-d", strtotime($modifiedfromDate));
        $where .= " AND s.updated_date >= '$modifiedfromDate'";
    }
    
    if(!empty($_POST['modified_to_date']) && empty($_POST['modified_from_date'])){
        $modifiedtoDate = $_POST['modified_to_date'];
         
        $modifiedtoDate = date("Y-m-d", strtotime($modifiedtoDate));
        $where .= " AND s.updated_date <= '$modifiedtoDate'";
    }
    
    if(!empty($_POST['modified_to_date']) && !empty($_POST['modified_from_date'])){
        $modifiedtoDate = $_POST['modified_to_date'];
        $modifiedtoDate = date("Y-m-d", strtotime($modifiedtoDate));
        $modifiedfromDate = $_POST['modified_from_date'];
        $modifiedfromDate = date("Y-m-d", strtotime($modifiedfromDate));
        $where .= " AND s.updated_date BETWEEN '$modifiedfromDate' AND '$modifiedtoDate'";
    }
    
    
    
    
    
    
    $recordings=$this->db->query("SELECT s.path
                        	,s.subchapter_name
                        	,s.created_date
                        	,s.updated_by
                        	,s.id
                        	,s.created_by
                        	,s.updated_date
                        	,u.username as username
                        FROM sub_chapters s
                        INNER JOIN users u ON u.id = s.created_by $where ORDER BY 1 DESC")->result();
}  


$totalrecordings=$this->db->query("SELECT count(1) as totalrecordings FROM sub_chapters ")->result();
foreach($totalrecordings as $gettotalrecordings){
$totalrecordings = $gettotalrecordings->totalrecordings;
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Competitive Exam Guide || Asset Library</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo $meta_description?>">
	<meta name="keywords" content="<?php echo $meta_keywords ?>">
	<meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
    <!-- Color Picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
    <!-- Mini-color css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
</head>

<body>
<style>
    span.chip {
        align-items: center;
        background: #7cc4de;
        border-radius: 5rem;
        color: #144151;
        display: inline-flex;
        display: -ms-inline-flexbox;
        -ms-flex-align: center;
        font-size: 90%;
        height: 1.2rem;
        line-height: .8rem;
        margin: .1rem;
        max-width: 100%;
        padding: 0.2rem 0.4rem;
        text-decoration: none;
        vertical-align: middle;
    }
    .blink_me {
        animation: blinker 1s linear infinite;
        color: red;
        margin: 0;
    }

    @keyframes blinker {
        80% {
            opacity: 0;
        }
    }
</style>

<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

        <?php include("includes/header.php"); ?>
        <!-- Sidebar inner chat end-->
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <?php include("includes/sidenav.php"); ?>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- Page-header start -->
                                <div class="page-header">
                                    <div class="row align-items-end">
                                        <div class="col-lg-7">
                                            <div class="page-header-title">
                                                <div class="d-inline">
                                                    <h4>Asset Library (<?php echo $totalrecordings ?>)</h4>
                                                    <!--<span><?php echo $totalrecordings ?></span>-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
													<button type="button" class="btn btn-primary btn-outline-primary" data-toggle="collapse" data-target="#demo">Advanced Search</button>
										</div>
                                        <div class="col-lg-2">

                                            <button class="btn btn-primary btn-outline-primary"><i class="icofont icofont-upload-alt"></i>Import Quiz/Live Class</button>
                                        </div>
                                        
                                    </div>
                                </div>
                                <!-- Page-header end -->
                                    <!-- Page-body start -->
                                <div class="page-body">
                                 <div id="demo" class="collapse">
									<form method="post">
                                        <div class="row">
                                            <div class="col-sm-12 col-xl-3 m-b-30">
                                                <h4 class="sub-title">Created From Date:</h4>
                                                 <input type="text" name="created_from_date" id="created_frmDate" value="<?php echo $_POST['created_from_date'] ?>" placeholder="From Date" class="form-control" />
                                            </div>
                                            <div class="col-sm-12 col-xl-3 m-b-30">
                                                <h4 class="sub-title">Created To Date:</h4>
                                                 <input type="text" name="created_to_date" id="created_toDate" value="<?php echo $_POST['created_to_date'] ?>" placeholder="To Date" class="form-control" />
                                            </div> 
                                            <div class="col-sm-12 col-xl-3 m-b-30">
                                                <h4 class="sub-title">Modified From Date:</h4>
                                                 <input type="text" name="modified_from_date" id="modified_frmDate" value="<?php echo $_POST['modified_from_date'] ?>" placeholder="From Date" class="form-control" />
                                            </div>
                                            <div class="col-sm-12 col-xl-3 m-b-30">
                                                <h4 class="sub-title">Modified To Date:</h4>
                                                 <input type="text" name="modified_to_date" id="modified_toDate" value="<?php echo $_POST['modified_to_date'] ?>" placeholder="To Date" class="form-control" />
                                            </div>
                                            <div class="col-sm-12 col-xl-4 m-b-30">
                                                <h4 class="sub-title">Asset Type:</h4>
                                                <select name="asset_type" class="form-control">
                                                    <?php
                                                    $distinctTypes=$this->db->query("SELECT distinct type FROM sub_chapters ")->result();
                                                    foreach($distinctTypes as $getdistinctTypes){ ?>
                                                    <option value='<?php echo $getdistinctTypes->type ?>'><?php echo $getdistinctTypes->type ?></option>
                                                    <?php }
                                                    ?>
                                                    
                                                </select>
                                            </div>
                                            
                                            <div class="col-sm-12 col-xl-4 m-b-30">
                                                <h4 class="sub-title">Title:</h4>
                                                <input type="text" name="sub_chapter_name" id="sub_chapter_name" class="form-control" placeholder="Title" value="<?php echo $_POST['sub_chapter_name'] ?>">
                                            </div>
                                            
                                            <div class="col-sm-1">
                                                <button type="submit" name="assetlibrary_search" id="assetlibrary_search" class="btn btn-primary btn-outline-primary">Search</button>
                                            </div>
                                            <div class="col-sm-1">
                                                <button type="reset" class="btn btn-primary btn-outline-primary">Clear</button>
                                            </div>
                                            <div class="col-sm-12">
                                                &nbsp;
                                            </div>
                                            </div>
                                        </form>
										</div>
										<div>
                                             <div class="col-sm-12">
                                                <!-- HTML5 Export Buttons table start -->
                                                <div class="card">
                                                    <div class="card-block">
                                                        <div class="dt-responsive table-responsive">
                                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Created Date</th>
                                                                        <th>Modified date</th>
                                                                        <th>Title</th>
                                                                        <th>Created By</th>
                                                                        <th>Used In</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php 
                                                                        foreach($recordings as $getrecordings){

                                                                            $liveRecordingData = $this->db->query("SELECT * FROM live_class_recordings WHERE status = 1  AND sub_chapter_id= '$getrecordings->id' ORDER BY 1 DESC")->result();
                                                                            
                                                                            foreach($liveRecordingData as $getliveRecordingData){
                                                                                $existinglink = $getliveRecordingData->link;
                                                                            }

                                                                            $subChapterType=$this->db->query("SELECT *  FROM sub_chapters where id = '$getrecordings->id' ORDER BY 1 DESC LIMIT 1")->result();
                                                                            foreach($subChapterType as $getsubChapterType){
                                                                                $subChapterType=$getsubChapterType->sub_type;
                                                                            }

                                                                        ?>
                                                                    <tr>
                                                                       
                                                                        <td>
                                                                       <?php if($getrecordings->created_date==''){?>
                                                                        <center><?= '----' ?></center>
                                                                        <?php }else {?>
                                                                        <?php 
                                                                        echo date('d-m-Y H:i:s', strtotime($getrecordings->created_date)) ?>
                                                                       
                                                                        <?php }?>
                                                                        </td>
                                                                        <td><?php if($getrecordings->updated_date==''){?>
                                                                        <center><?= '----' ?></center>
                                                                        <?php }else {?>
                                                                        <?php echo date('d-m-Y H:i:s', strtotime($getrecordings->updated_date)) ?>
                                                                        <?php }?>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                            if ($subChapterType=='bbb'){ ?>
                                                                                <?php
                                                                                if(count($liveRecordingData) > 0) { ?>
                                                                                    <p style="margin-bottom: 6px;font-weight: 600;">Recording - <?= $getrecordings->subchapter_name ?></p>
                                                                                    <span class="chip"><i class="feather icon-video" style="margin-right: 6px;"></i> <span class="icon-text" style="text-transform:capitalize;">video</span></span>
                                                                                    <!--<span class="chip"><span style="text-transform:capitalize;">Upload</span></span><br>-->
                                                                                    <span class="text-warning" style="color: green!important;font-size: 16px;font-weight: 600;">Completed</span>
                                                                                <?php }else{ ?>
                                                                                    <p style="margin-bottom: 6px;font-weight: 600;">Recording - <?= $getrecordings->subchapter_name ?></p>
                                                                                    <span class="chip"><i class="feather icon-video" style="margin-right: 6px;"></i> <span class="icon-text" style="text-transform:capitalize;">video</span></span>
                                                                                    <!--<span class="chip"><span style="text-transform:capitalize;">Upload</span></span><br>-->
                                                                                    <span class="text-warning" style="color: #ffb700!important;font-size: 16px;font-weight: 600;">Processing....</span>
                                                                                <?php } ?>
                                                                            <?php  }else{ ?>
                                                                                <?php if($getrecordings->subchapter_name=='') {?>
                                                                                    <?= 'recorded videos' ?>
                                                                                <?php }else{?>
                                                                                    <?= $getrecordings->subchapter_name ?>
                                                                                <?php }?>

                                                                            <?php }?>
                                                                        </td>
                                                                        <td><?=$getrecordings->username ?></td>
                                                                        <td>1</td>
                                                                        <td>
                                                                            
                                                                        <button class="btn btn-primary btn-outline-primary btn-icon" data-target="#details_<?= $getrecordings->id ?>"  data-toggle="modal" data-placement="top" title="Details"><i class="icofont icofont-info-circle"></i></button>
                                                                        <div class="modal fade" id="details_<?=$getrecordings->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                                                          <div class="modal-dialog modal-lg" role="document">
                                                                            <div class="modal-content">
                                                                              <div class="modal-header">
                                                                                    <h5 class="modal-title" id="exampleModalLongTitle">View Recording</h5>
                                                                                      <?php
                                                                                      if ($subChapterType=='bbb'){
                                                                                      if (!count($liveRecordingData) > 0){
                                                                                          echo "<p class='blink_me'>Video. Uploading.Wait Sometimes For Finish</p>";
                                                                                      }
                                                                                      }else{
                                                                                          ?>
                                                                                          <?=$getrecordings->path ?>
                                                                                          <?=$getrecordings->link ?>
                                                                                      <?php }?>

                                                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                      <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                              </div>
                                                                              <div class="modal-body">
                                                                                  <div class="col-sm-12">
                                                                                      <?php
                                                                                      if ($subChapterType=='bbb'){
                                                                                          if ($existinglink && count($liveRecordingData) > 0){
                                                                                              ?>
                                                                                              <iframe width="700" height="500" src="<?= $existinglink  ?> " frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                                                          <?php }else{ ?>
                                                                                              <img class="img-fluid" src="<?php echo base_url() ?>assets/landing/images/blankVideo.png" alt="Theme-Logo">
                                                                                          <?php  } }else{ ?>
                                                                                          <?php if($getrecordings->link==''){ ?>
                                                                                              <center>
                                                                                                  <iframe width="700" height="500" src="<?= $getrecordings->path ?> " frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                                                              </center>
                                                                                          <?php }else{ ?>
                                                                                              <center>
                                                                                                  <iframe width="700" height="500" src="<?= $getrecordings->link ?> " frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                                                              </center>
                                                                                          <?php } ?>
                                                                                      <?php } ?>
                                                                                  </div>  
                                                                            </div>
                                                                         </div>  
                                                                         </div>
                                                                         </div>
                                                                            <button class="btn btn-danger btn-outline-danger btn-icon" data-toggle="tooltip" data-placement="top" title="Delete"><i class="icofont icofont-delete-alt"></i></button>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- HTML5 Export Buttons end -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                            </div>
                        </div>
                        <!-- Main-body end -->
                       <!-- <div id="styleSelector">

                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
    <!-- data-table js -->
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <!-- Bootstrap date-time-picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <!-- Date-dropper js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
    <!-- Color picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
    <!-- Mini-color js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
    
    <script src="../files/assets/pages/sortable-custom.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
<script>
    $(document).ready(function () {
        $('input[id$=created_frmDate]').datepicker({
            dateFormat: 'dd-mm-yy'
        });
        $('input[id$=created_toDate]').datepicker({
            dateFormat: 'dd-mm-yy'
        });
        $('input[id$=modified_frmDate]').datepicker({
            dateFormat: 'dd-mm-yy'
        });
        $('input[id$=modified_toDate]').datepicker({
            dateFormat: 'dd-mm-yy'
        });
    });
</script>
</body>

</html>
