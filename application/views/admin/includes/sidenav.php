<?php 
$this->load->helper('url');
$currentURL = current_url();
$activateLink = substr($currentURL,39);
?>
<!--<style>
    .pcoded-mtext{
        color:black;
    }
    .pcoded .pcoded-navbar[navbar-theme="theme1"] .pcoded-item>li.active>a {
    background: #88cae1;
    color: #dcdcdc;
    border-bottom-color: #88cae1;
    }
    .pcoded[theme-layout="vertical"] .pcoded-navbar .pcoded-item[item-border="true"]>li>a {
        border-bottom-width: 1px;
    }
    .pcoded .pcoded-navbar[navbar-theme="theme1"] .pcoded-item>li>a {
        border-bottom-color: #88cae1;
    }
</style>-->

<nav class="pcoded-navbar" >
                        <div class="pcoded-inner-navbar main-menu" >
                            <div class="pcoded-navigatio-lavel" >Navigation</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <ul class="pcoded-item pcoded-left-item">
                                <?php if($activateLink == 'Home'){ ?>
                                    <li class="active">
                                        <a href="<?php echo base_url() ?>Home">
                                            <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                                            <span class="pcoded-mtext">Home</span>
                                        </a>
                                    </li>
                                <?php }else{ ?>
                                    <li class="">
                                        <a href="<?php echo base_url() ?>Home">
                                            <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                                            <span class="pcoded-mtext">Home</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if($activateLink == 'Dashboard'){ ?>
                                    <li class="active">
                                        <a href="<?php echo base_url() ?>Dashboard">
                                            <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                                            <span class="pcoded-mtext">Dashboard</span>
                                        </a>
                                    </li>
                                <?php }else{ ?>
                                    <li class="">
                                        <a href="<?php echo base_url() ?>Dashboard">
                                            <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                                            <span class="pcoded-mtext">Dashboard</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                
                                <?php if($activateLink == 'Salesreport'){ ?>
                                    <li class="active">
                                        <a href="<?php echo base_url() ?>Salesreport">
                                            <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                            <span class="pcoded-mtext">Salesreport</span>
                                        </a>
                                    </li>
                                <?php }else{ ?>
                                    <li class="">
                                        <a href="<?php echo base_url() ?>Salesreport">
                                            <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                            <span class="pcoded-mtext">Salesreport</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                
                                
                                
                                <?php if($activateLink == 'Courses' || $activateLink == 'Assetlibrary' || $activateLink == 'Liveclass'){ ?>
                                <li class="pcoded-hasmenu active pcoded-trigger">
                                <?php }else{ ?>
                                <li class="pcoded-hasmenu">
                                <?php } ?>
                                 <!--<li class="pcoded-hasmenu active pcoded-trigger">
                                  <li class="pcoded-hasmenu"> -->  
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-book"></i></span>
                                        <span class="pcoded-mtext">Content</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <?php if($activateLink == 'Courses'){ ?>
                                            <li class="active">
                                                <a href="<?php echo base_url() ?>Courses">
                                                    <span class="pcoded-mtext">Courses</span>
                                                </a>
                                            </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="<?php echo base_url() ?>Courses">
                                                <span class="pcoded-mtext">Courses</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                        <?php if($activateLink == 'Assetlibrary'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>Assetlibrary">
                                                <span class="pcoded-mtext">Asset Library</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                            <li class="">
                                                <a href="<?php echo base_url() ?>Assetlibrary">
                                                    <span class="pcoded-mtext">Asset Library</span>
                                                </a>
                                            </li>
                                        <?php } ?>
                                        <?php if($activateLink == 'Liveclass'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>Liveclass">
                                                <span class="pcoded-mtext">Live Class</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="<?php echo base_url() ?>Liveclass">
                                                <span class="pcoded-mtext">Live Class</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                        <?php if($activateLink == 'Questionbank'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>Questionbank">
                                                <span class="pcoded-mtext">Question Bank</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="<?php echo base_url() ?>Questionbank">
                                                <span class="pcoded-mtext">Question Bank</span>
                                            </a>
                                        </li>
                                        <?php } ?>

                                        <?php if($activateLink == 'QuizReview'){ ?>
                                            <li class="active">
                                                <a href="<?php echo base_url() ?>QuizReview">
                                                    <span class="pcoded-mtext">Quiz Review</span>
                                                </a>
                                            </li>
                                        <?php }else{ ?>
                                            <li class="">
                                                <a href="<?php echo base_url() ?>QuizReview">
                                                    <span class="pcoded-mtext">Quiz Review</span>
                                                </a>
                                            </li>
                                        <?php } ?>


<!--                                        --><?php //if($activateLink == 'Quiz'){ ?>
<!--                                        <li class="active">-->
<!--                                            <a href="--><?php //echo base_url() ?><!--Quiz">-->
<!--                                                <span class="pcoded-mtext">Quiz</span>-->
<!--                                            </a>-->
<!--                                        </li>-->
<!--                                        --><?php //}else{ ?>
<!--                                        <li class="">-->
<!--                                            <a href="--><?php //echo base_url() ?><!--Quiz">-->
<!--                                                <span class="pcoded-mtext">Quiz</span>-->
<!--                                            </a>-->
<!--                                        </li>-->
<!--                                        --><?php //} ?>

                                        <?php if($activateLink == 'LiveTest'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>LiveTest">
                                                <span class="pcoded-mtext">Live Test</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="<?php echo base_url() ?>LiveTest">
                                                <span class="pcoded-mtext">Live Test</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                        
                                        <?php if($activateLink == 'Comments'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>Comments">
                                                <span class="pcoded-mtext">Rating and Reviews</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="<?php echo base_url() ?>Comments">
                                                <span class="pcoded-mtext">Rating and Reviews</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <!--==<?php if($activateLink == 'Users/learners' || $activateLink == 'Users/admins' || $activateLink == 'Users/instructors'){ ?>
                                <li class="pcoded-hasmenu active pcoded-trigger">
                                <?php }else{ ?>
                                <li class="pcoded-hasmenu">
                                <?php } ?>-->
                                 <li class="pcoded-hasmenu active pcoded-trigger">
                                  <li class="pcoded-hasmenu">  
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                                        <span class="pcoded-mtext">Users</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <?php if($activateLink == 'Users/learners'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>Users/learners">
                                                <span class="pcoded-mtext">Learners</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="<?php echo base_url() ?>Users/learners">
                                                <span class="pcoded-mtext">Learners</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                        <?php if($activateLink == 'Users/admins'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>Users/admins">
                                                <span class="pcoded-mtext">Admins</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="<?php echo base_url() ?>Users/admins">
                                                <span class="pcoded-mtext">Admins</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                        <?php if($activateLink == 'Users/instructors'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>Users/instructors">
                                                <span class="pcoded-mtext">Instructors</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="<?php echo base_url() ?>Users/instructors">
                                                <span class="pcoded-mtext">Instructors</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <!--==<?php if($activateLink == 'Promocode' || $activateLink == 'Refernearn' || $activateLink == 'Messenger'){ ?>
                                <li class="pcoded-hasmenu active pcoded-trigger">
                                <?php }else{ ?>
                                <li class="pcoded-hasmenu">
                                <?php } ?>-->
                                 <li class="pcoded-hasmenu active pcoded-trigger">
                                  <li class="pcoded-hasmenu">  
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-trending-up"></i></span>
                                        <span class="pcoded-mtext">Marketing</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <?php if($activateLink == 'Promocode'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>Promocode">
                                                <span class="pcoded-mtext">Promo Code</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="<?php echo base_url() ?>Promocode">
                                                <span class="pcoded-mtext">Promo Code</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                        <?php if($activateLink == 'Refernearn'){ ?>
                                        <li class="active">
                                            <a href="Refernearn">
                                                <span class="pcoded-mtext">Refer & Earn</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="Refernearn">
                                                <span class="pcoded-mtext">Refer & Earn</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                        <?php if($activateLink == 'Messenger'){ ?>
                                        <li class="active">
                                            <a href="Messenger">
                                                <span class="pcoded-mtext">Messenger</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class=" ">
                                            <a href="Messenger">
                                                <span class="pcoded-mtext">Messenger</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-message-circle"></i></span>
                                        <span class="pcoded-mtext">Integration</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <?php if($activateLink == 'Payment'){ ?>
                                        <li class="active">
                                            <a href="Payment">
                                                <span class="pcoded-mtext">Payment</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="Payment">
                                                <span class="pcoded-mtext">Payment</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                        <?php if($activateLink == 'Sms'){ ?>
                                        <li class="active">
                                            <a href="Sms">
                                                <span class="pcoded-mtext">SMS</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="Sms">
                                                <span class="pcoded-mtext">SMS</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                        
                                    </ul>
                                </li>
                                <!--= <?php if($activateLink == 'Paidcredits' || $activateLink == 'Pendingcredits'){ ?>
                                <li class="pcoded-hasmenu active pcoded-trigger">
                                <?php }else{ ?>
                                <li class="pcoded-hasmenu">
                                <?php } ?>-->
                                 <li class="pcoded-hasmenu active pcoded-trigger">
                                  <li class="pcoded-hasmenu">  
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-credit-card" ></i></span>
                                        <span class="pcoded-mtext">Credits</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <?php if($activateLink == 'Pendingcredits'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>Pendingcredits">
                                                <span class="pcoded-mtext">Pending Credits</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="<?php echo base_url() ?>Pendingcredits">
                                                <span class="pcoded-mtext">Pending Credits</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                        
                                        <?php if($activateLink == 'Paidcredits'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>Paidcredits">
                                                <span class="pcoded-mtext">Paid Credits</span>
                                            </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class="">
                                            <a href="<?php echo base_url() ?>Paidcredits">
                                                <span class="pcoded-mtext">Paid Credits</span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                     </ul>
                                    </li>
                                 
                                    <?php if($activateLink == 'Setting'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>Setting">
                                                <span class="pcoded-micon"><i class="feather icon-settings"></i></span>
                                                <span class="pcoded-mtext">Setting</span>
                                            </a>
                                        </li>
                                    <?php }else{ ?>
                                            <li class="">
                                                <a href="<?php echo base_url() ?>Setting">
                                                    <span class="pcoded-micon"><i class="feather icon-settings"></i></span>
                                                    <span class="pcoded-mtext">Setting</span>
                                                </a>
                                            </li>
                                    <?php } ?>
                                    <?php if($activateLink == 'Contact'){ ?>
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>Contact">
                                                <span class="pcoded-micon"><i class="feather icon-mail"></i></span>
                                                <span class="pcoded-mtext">Contact</span>
                                            </a>
                                        </li>
                                    <?php }else{ ?>
                                            <li class="">
                                                <a href="<?php echo base_url() ?>Contact">
                                                    <span class="pcoded-micon"><i class="feather icon-mail"></i></span>
                                                    <span class="pcoded-mtext">Contact</span>
                                                </a>
                                            </li>
                                    <?php } ?>
                                  </li>
                                
                                </ul>
                            </ul>
                        </div>
                    </nav>