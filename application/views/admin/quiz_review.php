<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
   
?>
<?php 
date_default_timezone_set("Asia/Calcutta"); 
$now = date('Y-m-d H:i:s');

$attemptAnswerQuestions=$this->db->query("SELECT * FROM quiz_test_attempt_result WHERE question_type = 3 OR question_type = 4 ORDER BY  id DESC" )->result();

$totalquestions=$this->db->query("SELECT count(1) as totalquestion FROM live_test  ")->result();
foreach($totalquestions as $gettotalquestions){
$total_question = $gettotalquestions->totalquestion;
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Competitive Exam Guide || Question Bank</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo $meta_description?>">
	<meta name="keywords" content="<?php echo $meta_keywords ?>">
	<meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
    <!-- Color Picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
    <!-- Mini-color css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    
</head>

<body>
<style>
     .chip {
        align-items: center;
        background: #f0f1f4;
        border-radius: 5rem;
        color: #667189;
        display: inline-flex;
        display: -ms-inline-flexbox;
        -ms-flex-align: center;
        font-size: 90%;
        height: 1.2rem;
        line-height: .8rem;
        margin: .1rem;
        max-width: 100%;
        padding: .2rem .4rem;
        text-decoration: none;
        vertical-align: middle;
    }
     .modal-content {
         border-radius: 18px;
     }
</style>
    <?php if($this->session->flashdata('delmsg')){ ?>
    <script>
    $(document).ready(function(){
    $("#myModal").modal('show');
    });
    </script>
    <div id="myModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Message</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <b style="color:green"><?php echo $this->session->flashdata('delmsg') ?></b>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

        <?php include("includes/header.php"); ?>
        <!-- Sidebar inner chat end-->
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <?php include("includes/sidenav.php"); ?>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- Page-header start -->
                                <div class="page-header">
                                    <div class="row align-items-end">
                                        <div class="col-lg-7">
                                            <div class="page-header-title">
                                                <div class="d-inline">
                                                    <h4>Quiz Review Subjective Items</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Page-header end -->


                                <div class="page-body">
										<div>
                                             <div class="col-sm-12">
                                                <!-- HTML5 Export Buttons table start -->
                                                <div class="card">
                                                    <div class="card-block">
                                                        <div class="dt-responsive table-responsive">
                                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Submited on</th>
                                                                        <th>Learners on</th>
                                                                        <th>Quiz</th>
                                                                        <th>Course</th>
                                                                        <th>Reviewed</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                        foreach($attemptAnswerQuestions as $questionAnserItem){
                                                                            $username=$email='';
                                                                            if ($questionAnserItem->user_id) {
                                                                                $userInformations = $this->db->query("SELECT * FROM users WHERE id = " . $questionAnserItem->user_id . " ")->row();
                                                                                $username=$userInformations->username;
                                                                                $email=$userInformations->email_id;
                                                                            }
                                                                            $nameQuiz=$courseName='';
                                                                            if ($questionAnserItem->quiz_test_id){
                                                                                $quizInformations = $this->db->query("SELECT * FROM quiz_test WHERE id = " . $questionAnserItem->quiz_test_id . " ")->row();
                                                                                $nameQuiz=$quizInformations->title;
                                                                                $courseId=$quizInformations->course_id;
                                                                                $courseInformations = $this->db->query("SELECT * FROM course WHERE course_id = " . $courseId. " ")->row();
                                                                                if ($courseInformations){
                                                                                    $courseName=$courseInformations->title;
                                                                                }
                                                                            }

                                                                            $questionName='';
                                                                            $mark=$penality=0;
                                                                            if ($questionAnserItem->question_id) {
                                                                                $questionInformations = $this->db->query("SELECT * FROM quiz_test_questions WHERE id = " . $questionAnserItem->question_id . " ")->row();
                                                                                $questionName=$questionInformations->question_text;
                                                                                $mark=$questionInformations->mark;
                                                                                $penality=$questionInformations->penalty;
                                                                            }

                                                                            $adminName='';
                                                                            if ($questionAnserItem->review_id) {
                                                                                $adminInformations = $this->db->query("SELECT * FROM users WHERE id = " . $questionAnserItem->review_id . " ")->row();
                                                                                $adminName=$adminInformations->username;
                                                                            }
                                                                        ?>
                                                                    <tr>
                                                                        <td><?php echo date('Y-m-d', strtotime($questionAnserItem->created_at)) ?></td>
                                                                        <td>
                                                                            <a title=<?=$email?>
                                                                               href="<?php echo base_url() ?>Users/edit_user/?edit_id=<?php echo $questionAnserItem->user_id ?> "
                                                                               class="chip" target="_blank">
                                                                                <img src="https://competitiveexamguide.com/class/assets/profile_image/avatar.jpg" alt="" style="height: 22px;">
                                                                                <span class="text-ellipsis d-inline-block" style="max-width: 90px;"><?=$username?>t</span>
                                                                            </a>
                                                                        </td>

                                                                        <td>
                                                                            <?php  echo $nameQuiz; ?>
                                                                        </td>

                                                                        <td>
                                                                            <?php echo $courseName; ?>
                                                                        </td>

                                                                        <td>
                                                                            <?php
                                                                            if ($questionAnserItem->review_id){
                                                                                echo "<span>Yes</span>";
                                                                                echo  '<a title='.$email.'href="#" class="chip" target="_blank">
                                                                                <img src="https://competitiveexamguide.com/class/assets/profile_image/avatar.jpg" alt="" style="height: 22px;">
                                                                                <span class="text-ellipsis d-inline-block" style="max-width: 90px;">'.$adminName.'t</span>
                                                                                </a>';
                                                                            }else{
                                                                                echo "No";
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                            if (empty($questionAnserItem->review_id)):
                                                                            ?>
                                                                            <?php $questionsid = $questionAnserItem->id ?>
                                                                         <button  type="button"  data-toggle="modal" data-target="#demo1<?=$questionAnserItem->id ?>" name="delete" id="delete" class="btn btn-inverse btn-outline-inverse btn-icon "  title="Result"><i class="feather icon-award"></i></button>
                                                                         <div class="modal fade" id="demo1<?=$questionAnserItem->id ?>" tabindex="-1" role="dialog" >
                                                                              <div class="modal-dialog modal-lg" role="document" >
                                                                                <div class="modal-content">
                                                                                  <div class="modal-header">
                                                                                    <h4 class="modal-title">Quiz Review</h4>
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                  </div>
                                                                                    <div class="modal-body">
                                                                                        <div class="mb-2 border-bottom">
                                                                                            <p class="mb-0">Course: <b><?=$courseName?></b></p>
                                                                                            <p class="mb-0">Quiz: <b><?=$nameQuiz?></b></p>
                                                                                            <p class="mb-0">Learner: <b> <?=$username?> &lt;<?=$email?>&gt;</b></p>
                                                                                        </div>
                                                                                        <form  method="post" action="<?php echo base_url(); ?>QuizReview/markSubmit" autocomplete="off" enctype="multipart/form-data">
                                                                                            <h6 class="bg-gray p-1">Question 1 <span class="float-right">+<?=$mark?>, -<?=$penality?></span></h6>
                                                                                            <div class="question" data-id="610154140cf2d77bece15465"><p style="margin:0cm 0cm 8pt 36pt"><span style="font-size:11pt"><span style="line-height:107%"><span style="font-family:Calibri,sans-serif"><?=$questionName?>.</span></span></span></p></div>
                                                                                            <div><b>Learner Answer</b><br><div class="border rounded p-2">
                                                                                                    <p><?=$questionAnserItem->chose_option_text?></p>
                                                                                                    <p>Uploaded File</p>
                                                                                                    <?php if ($questionAnserItem->chose_option_file): ?>
                                                                                                        <a href="<?=base_url()?>/assets/subjective/<?=$questionAnserItem->chose_option_file?>" download>
                                                                                                            <img src="<?=base_url()?>/assets/subjective/<?=$questionAnserItem->chose_option_file?>" alt="Preview" width="104" height="142" style="border: 1px solid gray; border-radius: 10px;">
                                                                                                        </a>
                                                                                                    <?php endif; ?>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div><b>Correct Answer</b><br><div class="border rounded p-2"><p><?=$questionAnserItem->correct_option_text?></p></div></div>
                                                                                            <div class="mt-2 text-right">Marks: <input name="mark" step="0.01" class="form-input col-1 d-inline-block" required="" type="number" max="1" min="-0"></div>
                                                                                            <input type="hidden" value="<?=$questionAnserItem->quiz_test_id?>" name="quiz_id">
                                                                                            <input type="hidden" value="<?=$questionAnserItem->question_id?>" name="quiz_test_question_id">
                                                                                            <input type="hidden" value="<?=$questionAnserItem->id?>" name="quiz_attempt_result_id">
                                                                                            <input type="hidden" value="<?=$questionAnserItem->attempt_id?>" name="quiz_attempt_id">
                                                                                            <div class="text-center mt-2"><button class="btn btn-success" style="width: 300px;max-width:100%;">Submit</button></div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                              </div>
                                                                            </div>
                                                                            <?php endif; ?>

                                                                         </td>

                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- HTML5 Export Buttons end -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                            </div>
                        </div>
                        <!-- Main-body end -->
                       <!-- <div id="styleSelector">

                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
    <!-- data-table js -->
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <!-- Bootstrap date-time-picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <!-- Date-dropper js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
    <!-- Color picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
    <!-- Mini-color js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
    
    <script src="../files/assets/pages/sortable-custom.js"></script>
    <!-- Date-range picker js -->
    	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<!-- sweet alert modal.js intialize js -->
		<!-- modalEffects js nifty modal window effects -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/modalEffects.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/classie.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>

</html>
