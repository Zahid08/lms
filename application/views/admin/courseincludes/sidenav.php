<?php 
$this->load->helper('url');
$currentURL = current_url();
$activateLink = substr($currentURL,39);

$CourseId = $_GET['courseid'];
$courseInfo = $this->db->query("SELECT * FROM course WHERE  course_id = '$CourseId'  ORDER BY 1 ASC")->result();
foreach($courseInfo as $getcourseInfo){
    $courseImage = $getcourseInfo->image;
    $courseTitle = $getcourseInfo->title;
}
?>
<nav class="pcoded-navbar">
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="pcoded-navigatio-lavel">Navigation</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="">
                                    <center>
                                        <img  src="<?php echo base_url() ?>assets/course_images/<?php echo $courseImage ?>" style="border:2px solid #000000 "height="150px;" width="180px;">
                                        <br>
                                    <b class="text-white">&nbsp;<?php echo $courseTitle ?>&nbsp;</b>
                                    </center>
                                </li>
                                <p>&nbsp;</p>
                                <?php if($activateLink == 'CourseInfo?courseid='.$CourseId){ ?>
                                    <li class="active">
                                        <a href="<?php echo base_url() ?>CourseInfo?courseid=<?php echo $CourseId ?>">
                                            <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                                            <span class="pcoded-mtext">Details</span>
                                        </a>
                                    </li>
                                <?php }else{ ?>
                                    <li class="">
                                         <a href="<?php echo base_url() ?>CourseInfo?courseid=<?php echo $CourseId ?>">
                                            <span class="pcoded-micon"><i class="feather icon-menu"></i></span>
                                            <span class="pcoded-mtext">Details</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                
                                <?php if($activateLink == 'Coursebuilder?courseid='.$CourseId){ ?>
                                    <li class="active">
                                        <a target="_blank" href="<?php echo base_url() ?>Coursebuilder?courseid=<?php echo $CourseId ?>">
                                            <span class="pcoded-micon"><i class="feather icon-settings"></i></span>
                                            <span class="pcoded-mtext">Course Builder</span>
                                        </a>
                                    </li>
                                <?php }else{ ?>
                                    <li class="">
                                         <a target="_blank" href="<?php echo base_url() ?>Coursebuilder?courseid=<?php echo $CourseId ?>">
                                            <span class="pcoded-micon"><i class="feather icon-settings"></i></span>
                                            <span class="pcoded-mtext">Course Builder</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                
                                <?php if($_SESSION['role'] == 1){ ?>
                                <?php if($activateLink == 'Learners?courseid='.$CourseId){ ?>
                                    <li class="active">
                                        <a target="_blank" href="<?php echo base_url() ?>Learners?courseid=<?php echo $CourseId ?>">
                                            <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                                            <span class="pcoded-mtext">Learners</span>
                                        </a>
                                    </li>
                                <?php }else{ ?>
                                    <li class="">
                                         <a target="_blank" href="<?php echo base_url() ?>Learners?courseid=<?php echo $CourseId ?>">
                                            <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                                            <span class="pcoded-mtext">Learners</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                
                                <?php if($activateLink == 'Accesscode?courseid='.$CourseId){ ?>
                                    <li class="active">
                                        <a target="_blank" href="<?php echo base_url() ?>Accesscodes?courseid=<?php echo $CourseId ?>">
                                            <span class="pcoded-micon"><i class="fa fa-gift" style="color:black"></i></span>
                                            <span class="pcoded-mtext">Access Codes</span>
                                        </a>
                                    </li>
                                <?php }else{ ?>
                                    <li class="">
                                         <a target="_blank" href="<?php echo base_url() ?>Accesscodes?courseid=<?php echo $CourseId ?>">
                                            <span class="pcoded-micon"><i class="fa fa-gift"style="color:black"></i></span>
                                            <span class="pcoded-mtext">Access Codes</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php } ?>
                                
                                <li class="">
                                    <a href="<?php echo base_url() ?>Courses">
                                        <span class="pcoded-micon"><i class=" feather icon-arrow-left"></i></span>
                                        <span class="pcoded-mtext">Back</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>