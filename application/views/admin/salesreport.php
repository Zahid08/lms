<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<?php
$id=$_SESSION['id'];
 date_default_timezone_set("Asia/Calcutta");  

$totalTransactions = $this->db->query("SELECT count(1) as totaltransactions  FROM transactions  ")->result();
foreach($totalTransactions as $gettotalTransactions){
$totalTrans = $gettotalTransactions->totaltransactions;
}


$totalPaidTransactions=$this->db->query("SELECT count(1) as totalpaidtransactions FROM transactions t WHERE status='success' ")->result();
foreach($totalPaidTransactions as $gettotalPaidTransactions){
$totalPaidTrans = $gettotalPaidTransactions->totalpaidtransactions;
}

$failedTransactions=$totalTrans-$totalPaidTrans;

$totalSales=$this->db->query("SELECT SUM(amount) as totalsales FROM transactions WHERE status='success' ")->result();
foreach($totalSales as $gettotalSales){
$Sales = $gettotalSales->totalsales;
}


$transactions=$this->db->query("SELECT t.created_date,t.status,t.amount,t.transaction_code,u.email_id,c.title,t.transaction_id 
                                FROM transactions t 
                                INNER JOIN users u ON u.id = t.user_id OR u.unique_id = t.user_id
                                INNER JOIN course c ON c.course_id = t.course_id ORDER BY 1 DESC")->result();
if(isset($_POST['salesreport_search'])){
    $where = "where 1 = 1 ";
    if(!empty($_POST['trans_type'])){
        $trans_type = $_POST['trans_type'];
        $where .= " AND transaction_type = '$trans_type'";
    }
    
    if(!empty($_POST['status'])){
        $status = $_POST['status'];
        $where .= " AND t.status = '$status'";
    }
    
    if(!empty($_POST['from_date']) && empty($_POST['to_date'])){
        $fromDate = $_POST['from_date'];
        $fromDate = date("Y-m-d", strtotime($fromDate));
        $where .= " AND t.created_date >= '$fromDate'";
    }
    
    if(!empty($_POST['to_date']) && empty($_POST['from_date'])){
        $toDate = $_POST['to_date'];
         
        $toDate = date("Y-m-d", strtotime($toDate));
        $where .= " AND t.created_date <= '$toDate'";
    }
    
    if(!empty($_POST['to_date']) && !empty($_POST['from_date'])){
        $toDate = $_POST['to_date'];
        $toDate = date("Y-m-d", strtotime($toDate));
        $fromDate = $_POST['from_date'];
        $fromDate = date("Y-m-d", strtotime($fromDate));
        $where .= " AND t.created_date BETWEEN '$fromDate' AND '$toDate'";
    }
    
    
    
    
    $transactions=$this->db->query("SELECT t.created_date,t.status,t.amount,t.transaction_code,u.email_id,c.title,t.transaction_id 
                                FROM transactions t 
                                INNER JOIN users u ON u.id = t.user_id
                                INNER JOIN course c ON c.course_id = t.course_id $where ORDER BY 1 DESC")->result();
}                              
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Competitive Exam Guide || SalesReport </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo $meta_description?>">
	<meta name="keywords" content="<?php echo $meta_keywords ?>">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
    <!-- Color Picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
    <!-- Mini-color css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
    <!-- Switch component css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/switchery/css/switchery.min.css">
	<!-- Tags css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-tagsinput/css/bootstrap-tagsinput.css">
		<!-- sweet alert framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/sweetalert/css/sweetalert.css">
    <!-- animation nifty modal window effects css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/component.css">
    
    
</head>

<body>
    <?php if($this->session->flashdata('smsg')){ ?>						            
	    <script>
        	$(document).ready(function(){
        		$("#myModal").modal('show');
        	});
        </script>
	    <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
        				<b style="color:green"><?php echo $this->session->flashdata('smsg') ?></b>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>
		<?php if($this->session->flashdata('dc_msg')){ ?>						            
	    <script>
        	$(document).ready(function(){
        		$("#myModal").modal('show');
        	});
        </script>
	    <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
        				<b style="color:green"><?php echo $this->session->flashdata('dc_msg') ?></b>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>
		<?php if($this->session->flashdata('cm_msg')){ ?>						            
	    <script>
        	$(document).ready(function(){
        		$("#myModal").modal('show');
        	});
        </script>
	    <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
        				<b style="color:green"><?php echo $this->session->flashdata('cm_msg') ?></b>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>
		<?php if($this->session->flashdata('msg')){ ?>						            
	    <script>
        	$(document).ready(function(){
        		$("#myModal").modal('show');
        	});
        </script>
	    <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
        				<b style="color:green"><?php echo $this->session->flashdata('msg') ?></b>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

        <?php include("includes/header.php"); ?>
        <!-- Sidebar inner chat end-->
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <?php include("includes/sidenav.php"); ?>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- Page-header start -->
                                <div class="page-header">
                                    <div class="row align-items-end">
                                        <div class="col-lg-8">
                                            <div class="page-header-title">
                                                <div class="d-inline">
                                                    <h4>Overview</h4>
                                                    <span>Sales Report</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               <!-- <center><b style="color:green;"><?php echo $this->session->flashdata('smsg'); ?></b></center>-->
                                <!-- Page-header end -->
                                <div class="row">
                                <!-- statustic-card start -->
                                    <div class="col-xl-3 col-md-6">
                                                <div class="card bg-c-blue text-white">
                                                    <div class="card-block">
                                                        <div class="row align-items-center">
                                                            <div class="col">
                                                                <p class="m-b-5">Total Transaction</p>
                                                                <h4 class="m-b-0"><?php echo $totalTrans ?></h4>
                                                            </div>
                                                            <div class="col col-auto text-right">
                                                                <i class="feather icon-user f-50 text-c-blue"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    <div class="col-xl-3 col-md-6">
                                                <div class="card bg-c-blue text-white">
                                                    <div class="card-block">
                                                        <div class="row align-items-center">
                                                            <div class="col">
                                                                <p class="m-b-5">Paid Transaction</p>
                                                                <h4 class="m-b-0"><?php echo $totalPaidTrans ?></h4>
                                                            </div>
                                                            <div class="col col-auto text-right">
                                                                <i class="feather icon-credit-card f-50 text-c-blue"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    <div class="col-xl-3 col-md-6">
                                                <div class="card bg-c-blue text-white">
                                                    <div class="card-block">
                                                        <div class="row align-items-center">
                                                            <div class="col">
                                                                <p class="m-b-5">Failed Transaction</p>
                                                                <h4 class="m-b-0"><?php echo $failedTransactions ?></h4>
                                                            </div>
                                                            <div class="col col-auto text-right">
                                                                <i class="feather icon-book f-50 text-c-blue"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    <div class="col-xl-3 col-md-6">
                                                <div class="card bg-c-blue text-white">
                                                    <div class="card-block">
                                                        <div class="row align-items-center">
                                                            <div class="col">
                                                                <p class="m-b-5">Total Sales</p>
                                                                <h4 class="m-b-0"><?php
																		if($Sales=='')
                                                                    {
                                                                      echo 0;
                                                                     }
                                                                     else
                                                                     {
                                                                        echo $Sales;
                                                                     }?></h4>
                                                            </div>
                                                            <div class="col col-auto text-right">
                                                                <i class="feather icon-shopping-cart f-50 text-c-blue"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    <!-- statustic-card start -->
                                </div>
                                

                                    <!-- Page-body start -->
                                    <div class="page-body">
                                        <form  method="post" autocomplete="off">
                                        <div class="form-row">
                                            <div class="col-sm-12 col-xl-3 m-b-30">
                                                <h4 class="sub-title">From Date:</h4>
                                                 <input type="text" name="from_date" id="frmDate" value="<?php echo $_POST['from_date'] ?>" placeholder="From Date" class="form-control" />
                                            </div>
                                            <div class="col-sm-12 col-xl-3 m-b-30">
                                                <h4 class="sub-title">To Date:</h4>
                                                 <input type="text" name="to_date" id="toDate" value="<?php echo $_POST['to_date'] ?>" placeholder="To Date" class="form-control" />
                                            </div>
                                            <div class="col-sm-12 col-xl-3 m-b-30">
                                                <h4 class="sub-title">Status:</h4>
                                                <select name="status" class="form-control">
                                                    <option value='success'>Success</option>
                                                    <option value='failure'>Failed</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-12 col-xl-3 m-b-30">
                                                <h4 class="sub-title">Type:</h4>
                                                <select name="trans_type" class="form-control">
                                                    <option value='paid'>Paid</option>
                                                    <option value='free'>Free</option>
                                                    <option value='payu'>Pay U</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-8">
                                            </div>
                                            <div class="col-sm-1">
                                                <button name="salesreport_search" type="submit"  class="btn btn-primary btn-outline-primary">Search</button>
                                            </div>
                                        
                                            </form>
                                            <div class="col-sm-1">
                                                <button type="button" class="btn btn-primary btn-outline-primary" data-toggle="modal" data-target="#addPayment">Create</button>
                                                
                                               
                                                            
                                                <!-- Modal -->
                                                    <div class="modal fade" id="addPayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Add Payment</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                            </button>
                                                          </div>
                                                          <div class="modal-body">
                                                            <form method="post" name="Mform" id="form" action="<?php base_url('Salesreport/index') ?>">
                                                                        <div class="form-group">
                                                                            <label>E-mail ID :</label>
                                                                             <input list="learner_emails" name="email_id" class="form-control"  placeholder="Email Id" id="email_id">
                                                                                <datalist id="learner_emails">
                                                                                    <?php foreach ($luser as $l){ ?>
                                                                                        <option value="<?php echo $l['email_id'] ; ?>">
                                                                                    <?php } ?>
                                                                                </datalist>
                                                                         </div>
                                                                         <div class="form-group">
                                                                            <label>Course Name :</label>
                                                                            <select name="course_id" class="form-control" id="course" onchange="courseName()">
                                                                            <?php    foreach($sales_report as $sales){
                                                                               
                                                                            ?>
                                                                           
                                                                            <option placeholder="Choose a Value" value="<?php echo $sales['course_id'] ; ?>"><?php echo $sales['title'] ;  ?></option>
                                                                        <?php }   ?>
                                                                             </select>
                                                                         </div>
                                                                         <div class="form-group">
                                                                            <label>Payment Mode :</label>
                                                                            <select name="payment_mode" class="form-control"required>
                                                                            <option value='paid'>Paid</option>
                                                                            <option value='free'>Free</option>
                                                                            </select>
                                                                         </div>
                                                                         <div class="form-group">
                                                                            <label>Amount :</label>
                                                                            <input type="number" class="form-control" placeholder="Amount" id="amount" name="amount" required>
                                                                         </div>
                                                                         <div class="form-group">
                                                                            <label>Transaction Code :</label>
                                                                            <input name="tran_code" id="tran_code" placeholder="Transactioncode" class="form-control" type="text" size="40">
                                                                            <input type="button" class="btn btn-sm btn-primary" value="Generate" onclick="transectioncode(4);" >
                                                                         </div>
                                                                    
                                                                  </div>
                                                                  <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    <button type="submit" name="submit" class="btn btn-primary">Save changes</button>
                                                                  </div>
                                                                  </form>
                                                                </div>
                                                              </div>
                                                            </div>
                                            </div>
                                            <div class="col-sm-1">
                                                <button type = "clear" class="btn btn-primary btn-outline-primary">Clear</button>
                                            </div>
                                            <div class="col-sm-12">
                                                &nbsp;
                                            </div>
                                            <div class="col-sm-12">
                                                <!-- HTML5 Export Buttons table start -->
                                                <div class="card">
                                                    <div class="card-block">
                                                        <div class="dt-responsive table-responsive">
                                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                              <!--  <center><b style="color:green;"><?php echo $this->session->flashdata('dc_msg'); ?></b></center>
                                                             <center><b style="color:green;"><?php echo $this->session->flashdata('cm_msg'); ?></b></center>
                                                              <center><b style="color:green;"><?php echo $this->session->flashdata('msg'); ?></b></center>-->
                                                                <thead
                                                                <tr>
                                            						<th>Date</th>
                                            						<th>Status</th>
                                            						<th>Amount</th>
                                            						<th>Channel/OrderId/TaxId</th>
                                            						<th>User</th>
                                            						<th>Items</th>
                                            						<th>Action</th>
                                            					</tr>
                                            					</thead>
                                                                <tbody>
                                                                    <?php 
                                                                    foreach($transactions as $t){ 
                                                                    $orgDate = $t->created_date;  
                                                                    $createdDate = date("d-m-Y", strtotime($orgDate));   
                                                                    
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $createdDate ?></td> 
                                                                        <td><?php echo $t->status ?></td> 
                                                                        <td><?php echo $t->amount ?></td> 
                                                                        <td><?php echo $t->transaction_code ?></td>
                                                                        <td><?php echo $t->email_id ?></td>
                                                                        <td><?php echo $t->title ?></td>
                                                                        <td>
                                                                            <?php 
                                                                             $entityid = $t->transaction_id;
            																	        $existingComments=$this->db->query("SELECT *
                                                                                            FROM comments WHERE status = 1 AND entitytypeid = 'Sales Report' AND entityid = '$entityid'")->result();
                                                                                            
                                                                            ?>
                                                                            <button type="button"  data-target="#comment_<?php echo $t->transaction_id ?>" class="btn btn-inverse btn-outline-inverse btn-icon " data-toggle="modal" data-placement="top" title="Comment"><i class="icofont icofont-comment"></i></button>
                                                                            <?php 
                                                                            if(count($existingComments) > 0){ ?>
                                                                                <span class="badge bg-c-green"><?php echo count($existingComments)?></span>
                                                                            <?php
                                                                            }
                                                                            ?>
                                                                            
                                                                            <!-- Modal -->
                                                                        <div class="modal fade" id="comment_<?php echo $t->transaction_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                                                          <div class="modal-dialog modal-lg" role="document">
                                                                            <div class="modal-content">
                                                                              <div class="modal-header">
                                                                                    <h5 class="modal-title" id="exampleModalLongTitle">Comment</h5>
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                      <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                  </div>
                                                                              
                                                                              
                                                                               
                                                                              
                                                                              <div class="modal-body">
                                                                                  <form method="post" action="<?php echo base_url() ?>Salesreport/deletecomment">
                                                                                  <div class="row">
                                                                                        <div class="col-sm-11">
            																	        <?php
            																	       if(count($existingComments) > 0){
                                                                                                foreach($existingComments as $getexistingComments){ ?>
                                                                                               <div class="card text-white bg-primary" >
                                                                                                  <div class="card-body">
                                                                                                    <button type="submit"><b>X</b></button>
                                                                                                    <input type="hidden" name ="commentid" id="commentid" value="<?php echo $getexistingComments->id ?>">
    															                                    <p class="card-text"><?php echo $getexistingComments->comment ?></p>
                                                                                                  </div>
                                                                                                </div>
				                     
                                                                                                <?php    
                                                                                                }
                                                                                            }
                                                                                            
            																	        ?>
            																		</div>
            																	</div>
            																	</form>
            																	
                                                                                  <form method="post" action="<?php echo base_url() ?>Salesreport/addcomment">
                                                                                  <div class="row">
                                                                                      <div class="col-sm-10">
                                                                                          <input type="hidden" name="trnsid" id="trnsid" value="<?php echo $t->transaction_id ?>">
            																	        
                                                                                          <b>Comment:</b>
            																			<textarea class="form-control max-textarea"  name="comment" id="comment" maxlength="255" rows="4" placeholder="Type Your Comments Here..." 
            																		required></textarea>
            																		<br>
            																		<button type="submit" class="btn btn-primary">Save</button>
                                                                                      </div>
                                                                                  </div>
                                                                                  </form>
                                                                              </div>
                                                                              
                                                                            </div>
                                                                          </div>
                                                                        </div>
                                                                        
                                                                        </td>
                                                                    </tr>
                                                                       
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- HTML5 Export Buttons end -->
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                            </div>
                        </div>
                        <!-- Main-body end -->
                       <!-- <div id="styleSelector">

                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>


    
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
    <!-- Switch component js -->
	<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/switchery/js/switchery.min.js"></script>
	<!-- Tags js -->
	<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
								
    <!-- Max-length js -->
	<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-maxlength/js/bootstrap-maxlength.js"></script>
	<!-- data-table js -->
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <!-- Bootstrap date-time-picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <!-- Date-dropper js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
    <!-- Color picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
    <!-- Mini-color js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
    <!--<script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>-->
    <!-- sweet alert js -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <!--modalEffects js nifty modal window effects -->
   <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/modalEffects.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/classie.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
<script>
function courseName(){
    var x = document.getElementById("course").value;
    <?php foreach($course_price as $price) { ?>
    if(x == <?php echo $price['course_id'];?>){
        document.getElementById("amount").value = <?php echo $price['payable_price'];  ?>;
    }
    
    <?php } ?>
    
}
function transectioncode(length){
var chars = "1234567890";
    var code = "";
    var str = "COMPETIT";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        code += chars.charAt(i);
    }
    var oricode = str.concat(code);
    Mform.tran_code.value = oricode;
}
</script>
<script>
    $(document).ready(function () {
        $('input[id$=frmDate]').datepicker({
            dateFormat: 'dd-mm-yy'
        });
        $('input[id$=toDate]').datepicker({
            dateFormat: 'dd-mm-yy'
        });
    });
</script>

</body>

</html>
