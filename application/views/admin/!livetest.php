<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
   
?>
<?php 
date_default_timezone_set("Asia/Calcutta"); 
$now = date('Y-m-d H:i:s');

$questions=$this->db->query("SELECT * FROM live_test WHERE status = 1 ORDER BY  id DESC" )->result(); 
foreach($questions as $getquestions){
}
 

$totalquestions=$this->db->query("SELECT count(1) as totalquestion FROM live_test  ")->result();
foreach($totalquestions as $gettotalquestions){
$total_question = $gettotalquestions->totalquestion;
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Competitive Exam Guide || Question Bank</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo $meta_description?>">
	<meta name="keywords" content="<?php echo $meta_keywords ?>">
	<meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
    <!-- Color Picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
    <!-- Mini-color css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    
</head>

<body>
    <?php if($this->session->flashdata('delmsg')){ ?>
    <script>
    $(document).ready(function(){
    $("#myModal").modal('show');
    });
    </script>
    <div id="myModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Message</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <b style="color:green"><?php echo $this->session->flashdata('delmsg') ?></b>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

        <?php include("includes/header.php"); ?>
        <!-- Sidebar inner chat end-->
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <?php include("includes/sidenav.php"); ?>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- Page-header start -->
                                <div class="page-header">
                                    <div class="row align-items-end">
                                        <div class="col-lg-7">
                                            <div class="page-header-title">
                                                <div class="d-inline">
                                                    <h4>Live Test</h4>
                                                    <span>Live Test[<b><?php echo $total_question ?></b>]</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
													<button type="button" class="btn btn-primary btn-outline-primary" data-toggle="collapse" data-target="#demo">Advanced Search</button>
										</div>
                                        
                                        
                                        <div class="col-lg-2">
                                            <button class="btn btn-primary btn-outline-primary"><i class="icofont icofont-upload-alt"></i>Import Quiz/Live Class</button>
                                        </div>
                                        
                                    </div>
                                </div>
                                <!-- Page-header end -->

                                
                                <div class="page-body">
                                 <div id="demo" class="collapse">
									<form method="post">
                                        <div class="row">
                                            <div class="col-sm-12 col-xl-6 m-b-30">
                                                <h4 class="sub-title">Created Date:</h4>
                                                <div id="reportrange" name="creted_date" class="f-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                                    <i class="glyphicon glyphicon-calendar icofont icofont-ui-calendar"></i>
                                                    <span></span> <b class="caret"></b>
                                                </div>
                                            </div> 
                                            <div class="col-sm-12 col-xl-6 m-b-30">
                                                <h4 class="sub-title">Modified Date:</h4>
                                                <div id="reportrange" name="modified_date" class="f-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                                    <i class="glyphicon glyphicon-calendar icofont icofont-ui-calendar"></i>
                                                    <span></span> <b class="caret"></b>
                                                </div>
                                            </div> 
                                            
                                            <div class="col-sm-12 col-xl-4 m-b-30">
                                                <h4 class="sub-title">Question Type:</h4>
                                                <select name="question_type" class="form-control">
                                                    <option value='1'>Single Correct Option</option>
                                                    <option value='2'>Multi  Correct Option</option>
                                                    <option value='3'>Fill in the Blank</option>
                                                    <option value='4'>Paragraph</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-12 col-xl-4 m-b-30">
                                                <h4 class="sub-title">Topic:</h4>
                                                <input class="form-control" name="topic" id="topic" placeholder="Topic">
                                            </div>
                                            
                                            <div class="col-sm-12 col-xl-4 m-b-30">
                                                <h4 class="sub-title">Subject:</h4>
                                                <input class="form-control" name="subject" id="subject" placeholder="Subject">
                                            </div>
                                            
                                            <div class="col-sm-12">
                                                <h4 class="sub-title">Question Text:</h4>
                                                <input class="form-control" name="question_text" id="question_text" placeholder="Question Text">
                                            </div>
                                            
                                            <div class="col-sm-1">
                                                <button class="btn btn-primary btn-outline-primary">Search</button>
                                            </div>
                                            <div class="col-sm-1">
                                                <button class="btn btn-primary btn-outline-primary">Clear</button>
                                            </div>
                                            <div class="col-sm-12">
                                                &nbsp;
                                            </div>
                                            </div>
                                        </form>
										</div>
										<div>
                                             <div class="col-sm-12">
                                                <!-- HTML5 Export Buttons table start -->
                                                <div class="card">
                                                    <div class="card-block">
                                                        <div class="dt-responsive table-responsive">
                                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Created on</th>
                                                                        <th>Modified on</th>
                                                                        <th>title</th>
                                                                        <th>Course</th>
                                                                        <th>Timing</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php 
                                                                        foreach($questions as $getquestions){
                                                                        ?>
                                                                    <tr>
                                                                       
                                                                        <td><?php echo date('Y-m-d', strtotime($getquestions->created_date)) ?></td>
                                                                        <td><?php echo date('Y-m-d', strtotime($getquestions->updated_date)) ?></td>
                                                                        <td><?php echo $getquestions->title ?></td>
                                                                        <td><?php 
                                                                        $course=$this->db->query("SELECT * FROM course")->result();
                                                                               foreach($course as $getcourse){
                                                                               ?>
                                                                               <?php if($getcourse->course_id==$getquestions->course_id){?>
                                                                                <?php echo $getcourse->title ?>
                                                                                <?php }?>
                                                                              <?php }?></td>
                                                                        <td><?php echo 'Available From'.': '.date('Y-m-d H:i:s', strtotime($getquestions->available_from)).'<br>'.'Available Till'.': '.date('Y-m-d H:i:s', strtotime($getquestions->available_till)) ?></td>
                                                                        <td> <?php $questionsid = $getquestions->id ?>

                                                                         <button  type="button"  data-toggle="modal" data-target="#demo1<?=$getquestions->id ?>" name="delete" id="delete" class="btn btn-inverse btn-outline-inverse btn-icon "  title="Result"><i class="feather icon-award"></i></button>
                                                                         <div class="modal fade" id="demo1<?=$getquestions->id ?>" tabindex="-1" role="dialog" >
                                                                              <div class="modal-dialog modal-lg" role="document" >
                                                                                <div class="modal-content">
                                                                                  <div class="modal-header">
                                                                                    <h4 class="modal-title">Result</h4>
                                                                                    <?php 
                                                                        $course=$this->db->query("SELECT * FROM course")->result();
                                                                               foreach($course as $getcourse){
                                                                               ?>
                                                                               <?php if($getcourse->course_id==$getquestions->course_id){?>
                                                                                <?php echo $getcourse->title ?>
                                                                                <?php }?>
                                                                              <?php }?>
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                  </div>
                                                                                  <form  method="post" action="<?php echo base_url(); ?>LiveTest/result" autocomplete="off" enctype="multipart/form-data">
                                                                                    <center><div class="modal-body" >
                                                                                      <div class="row">
                                                                                        <div class="col-sm-12">
                                                                                          <div class="row">
                                                                                            <div class="col-2 float-right">
                                                                                                <lable class="float-left">Test Name:</lable>
                                                                                              <?php echo $getquestions->title ?>
                                                                              
                                                                                            </div>
                                                                                            
                                                                                          </div>
                                                                                        </div>
                                                                                      </div>
                                                                                     </div> 
                                                                                    <div class="modal-footer">
                                                                                      <button type="button" class="btn btn-secondary btn-outline-secondary " data-dismiss="modal">Close</button>
                                                                                      <button type="submit" class="btn btn-primary btn-outline-primary ">Save </button>
                                                                                    </div></center>
                                                                                  </form>
                                                                                </div>
                                                                              </div>
                                                                            </div>
                                                                         </td>
                                                                        
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- HTML5 Export Buttons end -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                            </div>
                        </div>
                        <!-- Main-body end -->
                       <!-- <div id="styleSelector">

                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
    <!-- data-table js -->
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <!-- Bootstrap date-time-picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <!-- Date-dropper js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
    <!-- Color picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
    <!-- Mini-color js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
    
    <script src="../files/assets/pages/sortable-custom.js"></script>
    <!-- Date-range picker js -->
    	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<!-- sweet alert modal.js intialize js -->
		<!-- modalEffects js nifty modal window effects -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/modalEffects.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/classie.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>

</html>
