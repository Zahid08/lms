<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<?php
$CourseId = $_GET['courseid'];
$users = $this->db->query("SELECT t.created_date as enrollment_date,u.username,u.email_id,t.transaction_code FROM users u INNER JOIN   transactions t ON t.user_id = u.id WHERE course_id='$CourseId' ")->result();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Competitive Exam Guide || Courses Learners </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo $meta_description?>">
        <meta name="keywords" content="<?php echo $meta_keywords ?>">
        <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
    <!-- font awesome -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/font-awesome/css/font-awesome.min.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
    <!-- Color Picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
    <!-- Mini-color css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
</head>

<body>
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

        <?php include("includes/header.php"); ?>
        <!-- Sidebar inner chat end-->
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <?php include("courseincludes/sidenav.php"); ?>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="main-body">
                            <div class="page-wrapper">
                                <!-- Page-header start -->
                                <div class="page-header">
                                    <div class="row align-items-end">
                                        <div class="col-lg-9">
                                            <div class="page-header-title">
                                                <div class="d-inline">
                                                    <h4>Users</h4>
                                                    
                                                    <span> Learners</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Page-header end -->
                                    <!-- Page-body start -->
                                    <div class="page-body">
                                        <form method="post" action="<?php base_url('Users/learners') ?>">
                                        <div class="form-row">
                                            <div class="col-sm-12 col-xl-6 m-b-30">
                                                <h4 class="sub-title">Name:</h4>
                                                <input type="text" class="form-control" name="name" value="<?php echo $_POST['username'] ?>" placeholder="User Name">
                                            </div>
                                            <div class="col-sm-12 col-xl-6 m-b-30">
                                                <h4 class="sub-title">Email:</h4>
                                                <input type="email" class="form-control" name="email" value="<?php echo $_POST['email_id'] ?>" placeholder="Email Id">
                                            </div>
                                            
                                            <div class="col-sm-1">
                                                <button type="submit" name="submit" class="btn btn-primary btn-outline-primary">Search</button>
                                            </div>
                                            <div class="col-sm-1">
                                                <button class="btn btn-primary btn-outline-primary">Create</button>
                                            </div>
                                            <div class="col-sm-1">
                                                <button type="reset" class="btn btn-primary btn-outline-primary">Clear</button>
                                            </div>
                                            <div class="col-sm-12">
                                                &nbsp;
                                            </div>
                                            </div>
                                            </form>
                                            <div class="row">
                                             <div class="col-sm-12">
                                                <!-- HTML5 Export Buttons table start -->
                                                <div class="card">
                                                    <div class="card-block">
                                                        <div class="dt-responsive table-responsive">
                                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                                
                                                                <thead>
                                                                    <tr>
                                                                        
                                                                        <th>Enrolment Date</th>
                                                                        <th>Name</th>
                                                                        <th>Email</th>
                                                                        <th>Transaction Code</th>
                                                                        <th>Access Code</th>
                                                                        
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                   <?php
																		foreach($users as $getUsers) {
																		?>
                                                                    <tr>
                                                                       
                                                                        <td><?php echo  date ( "d-m-Y" , strtotime ($getUsers->enrollment_date))?></td>
                                                                        <td><?php echo $getUsers->username ?></td>
                                                                        <td><?php echo  $getUsers->email_id ?></td>
                                                                        <td><?php echo  $getUsers->transaction_code ?></td>
                                                                        <td><?php echo  '-' ?></td>
                                                                         
                                                                    </tr>
                                                                
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- HTML5 Export Buttons end -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                            </div>
                        </div>
                        <!-- Main-body end -->
                       <!-- <div id="styleSelector">

                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
    <!-- data-table js -->
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <!-- Bootstrap date-time-picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <!-- Date-dropper js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
    <!-- Color picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
    <!-- Mini-color js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
    
    
    <script src="../files/assets/pages/sortable-custom.js"></script>
                <!-- sweet alert js -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- sweet alert modal.js intialize js -->
    <!-- Date-range picker js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
</body>

</html>
