<?php 
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<?php
$courseId = $this->input->get('courseid');
if($courseId <> NULL && $courseId <> ''){
    $courseData = $this->db->query("SELECT * FROM course where course_id = $courseId ORDER BY 1 DESC")->result();
    foreach($courseData as $getcourseData){
        $metadata_keywords = $getcourseData->metadata_keywords;
        $metadata_desc = $getcourseData->metadta_desc;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Competitive Exam Guide || Marketing </title>
		<!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 10]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<!-- Meta -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="<?php echo $meta_description?>">
        <meta name="keywords" content="<?php echo $meta_keywords ?>">
        <meta name="author" content="#">
		<!-- Favicon icon -->
		<link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
		<!-- Google font-->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
		<!-- Required Fremwork -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
		<!-- themify-icons line icon -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
		<!-- ico font -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
		<!-- font awesome -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/font-awesome/css/font-awesome.min.css">
		<!-- feather Awesome -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
		<!-- Switch component css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/switchery/css/switchery.min.css">
		<!-- Tags css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-tagsinput/css/bootstrap-tagsinput.css">
		<!-- Style.css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
		<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	</head>
	<body class="fix-menu">
	     <?php if($this->session->flashdata('price_msg')){ ?>						            
	    <script>
        	$(document).ready(function(){
        		$("#myModal").modal('show');
        	});
        </script>
	    <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
        				<b style="color:green"><?php echo $this->session->flashdata('price_msg') ?></b>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>
		<!-- Pre-loader end -->
		<div id="pcoded" class="pcoded">
			<div class="pcoded-overlay-box"></div>
			<div class="pcoded-container navbar-wrapper">
				<?php include("includes/header.php"); ?>
				<!-- Sidebar inner chat end-->
				<div class="pcoded-main-container">
					<div class="pcoded-wrapper">
						<?php include("includes/sidenav.php"); ?>
						<div class="pcoded-content">
							<div class="pcoded-inner-content">
								<!-- Main-body start -->
								<div class="main-body">
									<div class="page-wrapper">
										<!-- Page-header start -->
										<div class="page-header">
											<div class="row align-items-end">
												<div class="col-lg-10">
													<div class="page-header-title">
														<div class="d-inline">
															<h4>Course Information</h4>
															<span>Details</span>
														</div>
													</div>
												</div>
												
												
											</div>
										</div>
										<!-- Page-header end -->
										<!-- Page body start -->
										<div class="page-body">
											<div class="row">
												<div class="col-sm-12">
													<!-- Switches card start -->
													<div class="card" style="display:none">
														<div class="card-block">
															<div class="row">
																<div class="col-sm-12 col-xl-4 m-b-30">
																	<h4 class="sub-title">Single Switche</h4>
																	<input type="checkbox" class="js-single" checked="">
																</div>
																<div class="col-sm-12 col-xl-4 m-b-30">
																	<h4 class="sub-title">Multiple Switches</h4>
																	<input type="checkbox" class="js-switch" checked="">
																	<input type="checkbox" class="js-switch" checked="">
																	<input type="checkbox" class="js-switch" checked="">
																</div>
																<div class="col-sm-12 col-xl-4 m-b-30">
																	<h4 class="sub-title">Enable Disable Switches</h4>
																	<input type="checkbox" class="js-dynamic-state" checked="">
																	<button class="btn btn-primary js-dynamic-enable">Enable</button>
																	<button class="btn btn-inverse js-dynamic-disable m-t-10">Disable</button>
																</div>
															</div>
															<div class="row">
																<div class="col-sm-8">
																	<h4 class="sub-title">Color Switches</h4>
																	<input type="checkbox" class="js-default" checked="">
																	<input type="checkbox" class="js-primary" checked="">
																	<input type="checkbox" class="js-success" checked="">
																	<input type="checkbox" class="js-info" checked="">
																	<input type="checkbox" class="js-warning" checked="">
																	<input type="checkbox" class="js-danger" checked="">
																	<input type="checkbox" class="js-inverse" checked="">
																</div>
																<div class="col-sm-4">
																	<h4 class="sub-title">Switch Sizes</h4>
																	<input type="checkbox" class="js-large" checked="">
																	<input type="checkbox" class="js-medium" checked="">
																	<input type="checkbox" class="js-small" checked="">
																</div>
															</div>
														</div>
													</div>
													<!-- Switches card end -->
													<div class="row">
														<div class="col-sm-8">
														</div>
														<div class="col-sm-1">
															<a href="<?php echo base_url() ?>CourseInfo?courseid=<?php echo $courseId ?>"><button class="btn btn-primary btn-outline-primary">Details</button></a>
														</div>
														<div class="col-sm-1">
															<a href="<?php echo base_url() ?>Pricing?courseid=<?php echo $courseId ?>"><button class="btn btn-primary btn-outline-primary">Pricing</button></a>
														</div>
														<div class="col-sm-1">
															<a href="<?php echo base_url() ?>Marketing?courseid=<?php echo $courseId ?>"><button class="btn btn-primary btn-outline-primary">Marketing</button></a>
														</div>
														<div class="col-sm-12">
															&nbsp;
														</div>
														<div class="col-sm-12">
															<div class="card">
															    <form method="post" action="<?php echo base_url() ?>/Marketing/addmarketing">
																<div class="card-body">
																	<h5 class="card-title">Marketing</h5>
																	<span class="text-muted">Fill the Course Details .</span>
																	<p>&nbsp;</p>
																	<!--<center><b style="color:green;"><?php echo $this->session->flashdata('price_msg'); ?></b></center>-->
																	<div class="row">
																	    <input type="hidden" class="form-control" name="courseid" value="<?php echo $courseId ?>" required>
																		<div class="col-sm-12">
																		    <b>Course Page Keywords:</b>
																			<input type="text" class="form-control" name="course_page_keywords" value="<?php echo $metadata_keywords ?>" placeholder="Course Page Keywords" required>
																		</div>
																	
																	</div>
																	
																	<div class="row">
																	    <small>&nbsp;</small>
																	</div>
																	<div class="row">
																		<div class="col-sm-12">
																		    <b>Course Page Description:</b>
																			<textarea class="form-control max-textarea" name="course_page_description" value="<?php echo $metadata_desc ?>" maxlength="255" rows="4" placeholder="Type Description Here..." accept="image/*" required></textarea>
																		</div>
																	</div>
																	<div class="row ">
																	    <p>&nbsp;</p>
																	    <div class="col-sm-12">
                    														<center><button type="submit" class="btn btn-primary btn-outline-primary">Save</button></center>
                    													</div>
																	</div>
																</div>
																</form>
																<!-- Page body end -->
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- Required Jquery -->
								<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
								<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
								<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
								<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
								<!-- jquery slimscroll js -->
								<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
								<!-- modernizr js -->
								<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
								<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
								<!-- Switch component js -->
								<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/switchery/js/switchery.min.js"></script>
								<!-- Tags js -->
								<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
								<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
								<!-- Max-length js -->
								<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-maxlength/js/bootstrap-maxlength.js"></script>
								<!-- i18next.min.js -->
								<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
								<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
								<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
								<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
								<!-- Custom js -->
								<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/swithces.js"></script>
								<script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
								<script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
								<script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
								<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
								<!-- Global site tag (gtag.js) - Google Analytics -->
								<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
								<script>
								window.dataLayer = window.dataLayer || [];
								function gtag(){dataLayer.push(arguments);}
								gtag('js', new Date());
								gtag('config', 'UA-23581568-13');
								</script>
							</body>
						</html>