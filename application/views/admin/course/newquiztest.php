<?php
$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();
foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
?>
<?php
date_default_timezone_set("Asia/Calcutta");
$CourseId = $_GET['courseid'];

$courseData = $this->db->query("SELECT * FROM course WHERE status = 1 AND is_published = 1 AND course_id = '$CourseId'  ORDER BY 1 DESC")->result();
foreach($courseData as $getcourseData){
    $courseTitle = $getcourseData->title;
    $courseImage = $getcourseData->image;
}
$CourseId = $_GET['courseid'];
$quizTestid =$_GET['id'];

$languageName='';
$quiztest = $this->db->query("SELECT * FROM quiz_test WHERE status = 1 AND course_id = '$CourseId' AND id='$quizTestid' ORDER BY 1 DESC")->result();
foreach($quiztest as $getquiztest){
    $quizName = $getquiztest->title;
    $time_limit = $getquiztest->time_limit;
    $retake_attempts = $getquiztest->re_take_attempts;
    $available_from = $getquiztest->available_from;
    $available_from  = date('Y-m-d h:i:s a', strtotime($available_from ));
    $available_till = $getquiztest->available_till;
    $available_till = date('Y-m-d h:i:s a', strtotime($available_till));
    $instructions = $getquiztest->instructions;
    $post_msg = $getquiztest->post_msg;
    $is_instructions = $getquiztest->is_instructions;
    $is_scientific_calc = $getquiztest->is_scientific_calc;
    $show_leaderboard = $getquiztest->show_leaderboard;
    $show_rank = $getquiztest->show_rank;
    $results_mode = $getquiztest->results_mode;
    $arrange_by_topic = $getquiztest->arrange_by_topic;

    $showsolutionsLeraner = $getquiztest->show_solutions_for_learner;
    $showSolutionsAfterAttempt = $getquiztest->show_solutions_after_attempt;

    $min_time_pre_submit = $getquiztest->min_time_pre_submit;
    $quizTestid = $getquiztest->id;

    $quizType = $getquiztest->quiz_type;

    $supportLanguage=$getquiztest->support_lang;
    $support_lang_id=$getquiztest->support_lang_id;

    if ($support_lang_id==1){
        $languageName='हिंदी';
    }elseif ($support_lang_id==2){
        $languageName='English';
    }elseif ($support_lang_id==3){
        $languageName='Others';
    }

}
$quizTestid =$_GET['id'];
$quizquestion=$this->db->query("SELECT * FROM quiz_test_questions WHERE quiz_test_id='$quizTestid' AND status=1 ORDER BY 1 DESC ")->result();
foreach($quizquestion as $getquizquestion){
    $id=$getquizquestion->id;
    $quiz_test_id=$getquizquestion->quiz_test_id;
    $type=$getquizquestion->type;
    $question=$getquizquestion->question_text;
    $subject=$getquizquestion->subject;
    $topic=$getquizquestion->topic;
}
$quizquestions=$this->db->query("SELECT * FROM quiz_test_question_options WHERE status=1")->result();
foreach($quizquestions as $getquizquestions){
}
$tolatquizquestion=$this->db->query("SELECT count(1) as tolatquizquestion FROM quiz_test_questions WHERE quiz_test_id='$quizTestid'  ")->result();
foreach($tolatquizquestion as $gettolatquizquestion){
    $tolatquizquestion=$gettolatquizquestion->tolatquizquestion;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Competitive Exam Guide || Question Paper </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo $meta_description?>">
    <meta name="keywords" content="<?php echo $meta_keywords ?>">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
    <!-- Color Picker css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
    <!-- Mini-color css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
    <!-- sweet alert framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/sweetalert/css/sweetalert.css">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.16.1/full/ckeditor.js"></script>
    <style>
        .modal-lg {
            max-width: 60%;
        }
        .modal-body{
            margin:2%;
        }
        div.disbaleItem {
            pointer-events: none;
            cursor: not-allowed;
        }
        div.cusotmMessageBlock {
            display: none;
        }
        input {
            margin-top: 9px;
        }
        lable#correctionsId {
            position: absolute;
            left: 38px;
            top: 12px;
        }
        input.optionChekbox {
            float: left;
            width: 45px;
            margin-top: 18px;
        }
        div#addquiztestQuestion_modal .modal-content {
            border-radius: 15px;
        }
        div.hideFillInTheBlankOptions {
            display: none;
        }

        div#cke_3_contents {
            height: 134px!important;
        }
        span#cke_3_bottom {
            display: none!important;
        }
        .cke_bottom {
            display: none!important;
        }

        .cke_top, .cke_contents, .cke_bottom {
            height: 134px!important;
        }
        .cke_dialog_container {
            overflow: visible!important;
            z-index: 999999999999999999999999999!important;
        }
        table.cke_dialog.cke_browser_webkit.cke_ltr {
            border: 1px solid gray;
        }
        select.form-control:not([size]):not([multiple]) {
            height: auto!important;
        }
        th, td {
            white-space: normal!important;
        }
        .chip {
            align-items: center;
            background: #f0f1f4;
            border-radius: 5rem;
            color: #667189;
            display: inline-flex;
            display: -ms-inline-flexbox;
            -ms-flex-align: center;
            height: 1.2rem;
            line-height: .8rem;
            margin: .1rem;
            max-width: 100%;
            padding: .2rem .4rem;
            text-decoration: none;
            vertical-align: middle;
            font-size: 16px;
            font-weight: bold;
            padding-left: 0px!important;
        }
        .modal-content {
            animation: show .3s;
            transform: scale(1);
        }

        @keyframes show {
            from {

                transform: scale(0);
                opacity: 0;
                z-index: -1;
            }
            to {

                transform: scale(1);
                opacity: 1;
                z-index: 2;
            }
        }
    </style>

</head>
<body>
<?php if($this->session->flashdata('endmsg')){ ?>
    <script>
        $(document).ready(function(){
            $("#myModal").modal('show');
        });
    </script>
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Message</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <b style="color:green"><?php echo $this->session->flashdata('endmsg') ?></b>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if($this->session->flashdata('delmsg')){ ?>
    <script>
        $(document).ready(function(){
            $("#myModal").modal('show');
        });
    </script>
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Message</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <b style="color:green"><?php echo $this->session->flashdata('delmsg') ?></b>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php if($this->session->flashdata('amsg')){ ?>
    <script>
        $(document).ready(function(){
            $("#myModal").modal('show');
        });
    </script>
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Message</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <b style="color:green"><?php echo $this->session->flashdata('amsg') ?></b>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if($this->session->flashdata('msg')){ ?>
    <script>
        $(document).ready(function(){
            $("#myModal").modal('show');
        });
    </script>
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Message</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <b style="color:green"><?php echo $this->session->flashdata('msg') ?></b>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
        <?php include("includes/header.php"); ?>
        <!-- Sidebar inner chat end-->
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper" style="width:100%;">
                <?php include("includes/coursesidenav.php"); ?>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <!-- Main-body start -->
                        <div class="row" >
                            <div class="col-sm-1"></div>
                            <div class="main-body col-sm-11">
                                <div class="page-wrapper">
                                    <!-- Page-header start -->
                                    <div class="page-header">
                                        <div class="row align-items-end">
                                            <div class="col-lg-10">
                                                <div class="page-header-title">
                                                    <div class="d-inline">
                                                        <h4><?php echo $courseTitle ?></h4>
                                                        <!--<span>23 Course(s)</span>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="page-body">
                                        <div class="card">
                                            <div class="card-body">

                                                <div class="row">
                                                    <div class="col-lg-9">
                                                    </div>
                                                </div>

                                                <!--==========================Add questions Model===============================================-->
                                                <div class="modal fade" id="addquiztestQuestion_modal" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <lable> <h4 class="modal-title">Add New Question</h4></lable>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <form  method="post" action="<?php echo base_url(); ?>Newquiztest/addquiztestQuestion" autocomplete="off" enctype="multipart/form-data">
                                                                <center> <div class="modal-body">
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <div class="row">
                                                                                    <div class="col-md-2 col-sm-12">
                                                                                        <lable class="float-left">Type</lable>
                                                                                    </div>
                                                                                    <div class="col-md-10 col-sm-12">
                                                                                        <select class="form-control" name="type" id="type" onchange="OptionType(this.value)" required>
                                                                                            <option value="1">Single Correct Option</option>
                                                                                            <option value="2">Multiple Correct Options</option>
                                                                                            <option value="3">Numerical / Fill in the Blank</option>
                                                                                            <option value="4" >Subjective </option>
                                                                                        </select>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row" >
                                                                            <div class="col-md-2 col-sm-12">
                                                                                <br><lable class="float-left">Subject *</lable>
                                                                            </div>
                                                                            <div class="col-md-10 col-sm-12">
                                                                                <br><input type="text" name="subjectname" id="subjectname" placeholder="subject" class="form-control" required>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-2 col-sm-12">
                                                                                <br><lable class="float-left">Topic *</lable>
                                                                            </div>
                                                                            <div class="col-md-10 col-sm-12">
                                                                                <br>  <input type="text" name="Topic" id="Topic" placeholder="Topic" class="form-control" required>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-2 col-sm-12">
                                                                                <br> <lable class="float-left">Question Text *</lable>
                                                                            </div>
                                                                            <div class="col-md-10 col-sm-12">
                                                                                <br>  <textarea onclick="CKEDITOR.replace( 'QuestionText' );" type="text" name="QuestionText" rows="4" id="QuestionText" placeholder="QuestionText" class="form-control max-textarea" required></textarea>
                                                                            </div>
                                                                        </div>

                                                                        <?php
                                                                        if ($supportLanguage==1){
                                                                            ?>
                                                                            <div class="row">
                                                                                <div class="col-md-2 col-sm-12">
                                                                                    <br> <lable class="float-left"><?=$languageName?> Question Text *</lable>
                                                                                </div>
                                                                                <div class="col-md-10 col-sm-12">
                                                                                    <br>  <textarea onclick="CKEDITOR.replace( 'hindiQuestionText' );" type="text" name="QuestionTextLang" rows="4" id="hindiQuestionText" placeholder="QuestionText" class="form-control max-textarea" required></textarea>
                                                                                </div>
                                                                            </div>
                                                                        <?php }?>

                                                                        <div id="single" class="row"  >
                                                                            <div class="col-sm-12">
                                                                                <br><h4 class="btn btn-primary  btn-md btn-block   text-left m-b-20 ">Options</h4>
                                                                            </div>

                                                                            <div  class=" row col-sm-12" style="padding-right: 0px;">
                                                                                <div class="col-md-2 col-sm-12" style="padding: 0px">
                                                                                    <br>
                                                                                    <input type="checkbox" name="option1" id="option1" value="1" class="form-control optionChekbox" >
                                                                                    <lable class="float-left" id="correctionsId" style="top: 34px!important;">Correct Option 1</lable>
                                                                                </div>
                                                                                <div class="col-md-9 col-sm-12" style="padding-right: 0;">
                                                                                    <br>
                                                                                    <textarea onclick="CKEDITOR.replace( 'optionText1' );"  type="text" name="optionText1" rows="4" id="optionText1" placeholder="Option" class="form-control max-textarea"></textarea>
                                                                                    <br>
                                                                                </div>

                                                                                <div class="col-1" id="single" style="padding-left: 0;">
                                                                                    <button type="button" name="add" id="add" class="btn btn-primary btn-outline-primary btn-icon addNewQuestionOptions" title="Add Options" style="margin-top: 23px;"><h3>+</h3></button><br>
                                                                                </div>
                                                                            </div>

                                                                            <?php
                                                                            if ($supportLanguage==1){
                                                                                ?>
                                                                                <div style="margin-top: 21px;" class="col-sm-12" id="row1">
                                                                                    <div class="row">
                                                                                        <div class="col-md-2 col-sm-12" style="padding: 0px">
                                                                                            <lable class="float-left" id="correctionsId" style=""><?=$languageName?> Correct Option 1</lable>
                                                                                        </div>
                                                                                        <div class="col-md-9 col-sm-12" style="padding-left: 11px;">
                                                                                            <textarea onclick="CKEDITOR.replace( 'optionText1Lang' );" type="text" name="optionText1Lang" rows="4" id="optionText1Lang" placeholder="Option" class="form-control max-textarea"></textarea>
                                                                                        </div>
                                                                                        <div class="">
                                                                                            <button type="button" name="remove" id="2" class="btn btn-primary btn-outline-primary btn-icon btn_remove addNewQuestionOptions">X</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            <?php }?>

                                                                            <div style="margin-top: 21px;" class="col-sm-12" id="row2">
                                                                                <div class="row">
                                                                                    <div class="col-md-2 col-sm-12" style="padding: 0px">
                                                                                        <input type="checkbox" name="option2" id="option2" value="1" class="form-control optionChekbox">
                                                                                        <lable class="float-left" id="correctionsId" style="">Correct Option 2</lable>
                                                                                    </div>
                                                                                    <div class="col-md-9 col-sm-12" style="padding-left: 11px;">
                                                                                        <textarea onclick="CKEDITOR.replace( 'optionText2' );" type="text" name="optionText2" rows="4" id="optionText2" placeholder="Option" class="form-control max-textarea"></textarea>
                                                                                    </div>
                                                                                    <div class="">
                                                                                        <button type="button" name="remove" id="2" class="btn btn-primary btn-outline-primary btn-icon btn_remove addNewQuestionOptions">X</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <br>

                                                                            <?php
                                                                            if ($supportLanguage==1){
                                                                                ?>
                                                                                <div style="margin-top: 21px;" class="col-sm-12" id="row2">
                                                                                    <div class="row"><div class="col-md-2 col-sm-12" style="padding: 0px">
                                                                                            <lable class="float-left" id="correctionsId" style=""><?=$languageName?> Correct Option 2</lable>
                                                                                        </div>
                                                                                        <div class="col-md-9 col-sm-12" style="padding-left: 11px;">
                                                                                            <textarea onclick="CKEDITOR.replace( 'optionText2Lang' );" type="text" name="optionText2Lang" rows="4" id="optionText2Lang" placeholder="Option" class="form-control max-textarea"></textarea>
                                                                                        </div>
                                                                                        <div class="">
                                                                                            <button type="button" name="remove" id="2" class="btn btn-primary btn-outline-primary btn-icon btn_remove addNewQuestionOptions">X</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                            <?php }?>


                                                                            <div style="margin-top: 21px;" class="col-sm-12" id="row3">
                                                                                <div class="row">
                                                                                    <div class="col-md-2 col-sm-12" style="padding: 0px">
                                                                                        <input type="checkbox" name="option3" id="option3" value="1" class="form-control optionChekbox">
                                                                                        <lable class="float-left" id="correctionsId" style="">Correct Option 3</lable>
                                                                                    </div>
                                                                                    <div class="col-md-9 col-sm-12" style="padding-left: 11px;">
                                                                                        <textarea onclick="CKEDITOR.replace( 'optionText3' );" type="text" name="optionText3" rows="4" id="optionText3" placeholder="Option" class="form-control max-textarea"></textarea>
                                                                                    </div>
                                                                                    <div class="">
                                                                                        <button type="button" name="remove" id="3" class="btn btn-primary btn-outline-primary btn-icon btn_remove addNewQuestionOptions">X</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <br>

                                                                            <?php
                                                                            if ($supportLanguage==1){
                                                                                ?>
                                                                                <div style="margin-top: 21px;" class="col-sm-12" id="row3">
                                                                                    <div class="row">
                                                                                        <div class="col-md-2 col-sm-12" style="padding: 0px">
                                                                                            <lable class="float-left" id="correctionsId" style=""><?=$languageName?> Correct Option 3</lable>
                                                                                        </div>
                                                                                        <div class="col-md-9 col-sm-12" style="padding-left: 11px;">
                                                                                            <textarea onclick="CKEDITOR.replace( 'optionText3Lang' );" type="text" name="optionText3Lang" rows="4" id="optionText3Lang" placeholder="Option" class="form-control max-textarea"></textarea>
                                                                                        </div>
                                                                                        <div class="">
                                                                                            <button type="button" name="remove" id="3" class="btn btn-primary btn-outline-primary btn-icon btn_remove addNewQuestionOptions">X</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                            <?php }?>

                                                                            <div style="margin-top: 21px;" class="col-sm-12" id="row4">
                                                                                <div class="row">
                                                                                    <div class="col-md-2 col-sm-12" style="padding: 0px">
                                                                                        <input type="checkbox" name="option4" id="option4" value="1" class="form-control optionChekbox">
                                                                                        <lable class="float-left" id="correctionsId" style="">Correct Option 4</lable>
                                                                                    </div>
                                                                                    <div class="col-md-9 col-sm-12" style="padding-left: 11px;">
                                                                                        <textarea onclick="CKEDITOR.replace( 'optionText4' );" type="text" name="optionText4" rows="4" id="optionText4" placeholder="Option" class="form-control max-textarea"></textarea>
                                                                                    </div>
                                                                                    <div class="">
                                                                                        <button type="button" name="remove" id="4" class="btn btn-primary btn-outline-primary btn-icon btn_remove addNewQuestionOptions">X</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <?php
                                                                            if ($supportLanguage==1){
                                                                                ?>
                                                                                <div style="margin-top: 21px;" class="col-sm-12" id="row4">
                                                                                    <div class="row">
                                                                                        <div class="col-md-2 col-sm-12" style="padding: 0px">
                                                                                            <lable class="float-left" id="correctionsId" style=""><?=$languageName?> Correct Option 4</lable>
                                                                                        </div>
                                                                                        <div class="col-md-9 col-sm-12" style="padding-left: 11px;">
                                                                                            <textarea onclick="CKEDITOR.replace( 'optionText4Lang' );" type="text" name="optionText4Lang" rows="4" id="optionText4Lang" placeholder="Option" class="form-control max-textarea"></textarea>
                                                                                        </div>
                                                                                        <div class="">
                                                                                            <button type="button" name="remove" id="4" class="btn btn-primary btn-outline-primary btn-icon btn_remove addNewQuestionOptions">X</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            <?php }?>

                                                                        </div>

                                                                        <!--for Blank-->
                                                                        <div id="blank" class="row" style="display:none;">
                                                                            <div  class=" row col-sm-12" >
                                                                                <div class="col-2" style="margin-top: 16px;">
                                                                                    <br> <lable class="float-left">Answer</lable>
                                                                                </div>
                                                                                <div class="col-10" style="padding: 0; padding-left: 6px;">
                                                                                    <br>  <input type="text" name="optionText7" id="optionText7" placeholder="Answer" class="form-control" >
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <hr/>
                                                                        <div class="row">
                                                                            <div class="col-2">
                                                                                <br> <lable class="float-left">Explaination</lable>
                                                                            </div>
                                                                            <div class="col-10">
                                                                                <br>  <textarea onclick="CKEDITOR.replace( 'explaination' );" type="text" name="explaination" rows="4" id="explaination" placeholder="QuestionText" class="form-control max-textarea" required>No Explanations Available</textarea>
                                                                            </div>
                                                                        </div>

                                                                        <?php
                                                                        if ($supportLanguage==1){
                                                                            ?>
                                                                            <div class="row">
                                                                                <div class="col-2">
                                                                                    <br> <lable class="float-left"><?=$languageName?> Explaination</lable>
                                                                                </div>
                                                                                <div class="col-10">
                                                                                    <br>  <textarea onclick="CKEDITOR.replace( 'explainationLang' );" type="text" name="explainationLang" rows="4" id="explainationLang" placeholder="QuestionText <?=$languageName?>" class="form-control max-textarea" required>No Explanations Available</textarea>
                                                                                </div>
                                                                            </div>
                                                                        <?php }?>

                                                                        <div class="row">
                                                                            <div class="col-lg-12">
                                                                                <br><button style="margin: 0px;" type="button" data-toggle="collapse" data-target="#showadvancedoptions"  class="btn btn-primary btn-outline-primary btn-md btn-block waves-effect waves-light text-left m-b-20 " >Show Advanced Options</button>
                                                                                <br>
                                                                            </div>
                                                                        </div>


                                                                        <div  class="collapse"id="showadvancedoptions">
                                                                            <div class="row">
                                                                                <div class="col-2">
                                                                                    <br> <lable class="float-left">Order</lable>
                                                                                </div>
                                                                                <div class="col-10">
                                                                                    <br>  <input type="number" name="Order" id="Order" value="<?php echo $tolatquizquestion+1 ?>"placeholder="Order"  class="form-control" >
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-2">
                                                                                    <br> <lable class="float-left">Marks</lable>
                                                                                </div>
                                                                                <div class="col-10">
                                                                                    <br>  <input type="number" name="Marks" id="Marks" placeholder="Mark(s)"  class="form-control" >
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-2">
                                                                                    <br> <lable class="float-left">Penalty</lable>
                                                                                </div>
                                                                                <div class="col-10">
                                                                                    <br>  <input type="text" name="Penalty" id="Penalty" placeholder="Penalty" class="form-control" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <input type="hidden" name="courseid" id="courseid" value="<?php echo $_GET['courseid'] ?>" class="form-control">
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <input type="hidden" name="quizTestid" id="quizTestid" value="<?php echo $_GET['id'] ?>" class="form-control">
                                                                            <input type="hidden" name="quizType" id="quizType" value="<?php echo $quizType ?>" class="form-control">
                                                                        </div>
                                                                    </div>


                                                                    <div class="modal-footer">
                                                                        <span id="bottomError" style="color: red;"></span>
                                                                        <button type="button" class="btn btn-secondary btn-outline-secondary " data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-primary btn-outline-primary " id="adquestionFormSubmit">Save </button>
                                                                    </div>
                                                        </div></center>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--=============================================================================================-->

                                            <!--=============================================================================================-->
                                            <form  method="post" action="<?php echo base_url(); ?>Newquiztest/addquiztest" autocomplete="off" enctype="multipart/form-data">

                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <button type="button" data-toggle="collapse" data-target="#quiz_setting" class="btn btn-primary btn-outline-primary btn-md btn-block waves-effect waves-light text-left m-b-20 "><i class="feather icon-chevrons-down"></i>Quiz Setting</button>
                                                    </div>
                                                </div>
                                                <div  class="collapse"id="quiz_setting">
                                                    <div class="row">
                                                        <div class="col-lg-10">

                                                        </div>
                                                        <div class="col-lg-2" style="text-align: center">
                                                            <button type="button" name="edit_item" id="edit_item" onclick="edititem_test()" value="<?= $quizTestid ?>"  class="btn btn-primary btn-outline-primary ">Edit</button>
                                                            <button type="submit" name="save_item" id="save_item" onclick="alert('Are you sure, You want to Modify Data??')"  class="btn btn-primary btn-outline-primary" style="display:none;">Save & Submit</button>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12"><br>
                                                            <input type="text" name="chaptername" id="chaptername" value="<?php echo $quizName ?>" placeholder="Title" class="form-control"  readonly required>
                                                            <br>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <b>Time Limit (in minutes)</b>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <input class="form-control"  type="number" min="0" name="time_limit" id="time_limit" value="<?php echo $time_limit ?>" placeholder="Total Time (in minutes)"readonly required>
                                                        </div>
                                                        <br>

                                                        <?php if ($quizType!=1){?>
                                                            <div class="col-sm-12">
                                                                <br>
                                                                <b>Re-take Attempts</b>
                                                            </div>
                                                            <br>
                                                            <div class="col-sm-12">
                                                                <input class="form-control"  type="number" min="0" name="re_take_attempts" id="re_take_attempts" value="<?php echo $retake_attempts ?>"  placeholder="Re-take Attempts" readonly required>
                                                            </div>
                                                            <br>
                                                        <?php } ?>


                                                        <div class='col-sm-12'>
                                                            <br>
                                                            <b>Test Open Window</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <input type="text" name="available_from" id="available_from" min="<?php echo date('Y-m-d h:i:s') ?>" value="<?php echo $available_from ?>"  onfocus="this.type='datetime-local'" placeholder="Available From" class="form-control" readonly required>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" name="available_till" id="available_till" min="<?php  echo date('Y-m-d h:i:s') ?>" value="<?php echo $available_till ?>" onfocus="this.type='datetime-local'" placeholder="Available Till" class="form-control"  readonly required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br><br>

                                                        <div class="col-sm-12 disbaleItem" id="disbaleItem">
                                                            <br>
                                                            <b>Show Default Instruction</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <label class="radio-inline">
                                                                <input type="radio" name="ShowDefaultInstruction" id="no" value="0"<?php if($is_instructions == '0'){ ?> checked <?php } ?>readonly required> No
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="ShowDefaultInstruction" id="yes" value="1" <?php if($is_instructions == '1'){ ?> checked <?php } ?>readonly required> Yes
                                                            </label>
                                                            <br/><br/>
                                                            <b>Instructions to be shown at Start Of Quiz</b><br/><br/>
                                                            <textarea style="margin-top: 19px;" class="form-control max-textarea"  name="message" id="message" maxlength="500" rows="4" value="<?php echo $getquiztest->instructions ?>" placeholder="Instruction"  required ><?php echo $getquiztest->instructions ?></textarea>
                                                        </div>


                                                        <div class="col-sm-12">
                                                            <br>
                                                            <b>Show Scientific Calculator</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <label class="radio-inline">
                                                                <input type="radio" name="ShowScientificCalculator" id="n" value="0" <?php if($is_scientific_calc == '0'){ ?> checked <?php } ?>readonly required> No
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio"  name="ShowScientificCalculator" id="y" value="1" <?php if($is_scientific_calc == '1'){ ?> checked <?php } ?>readonly required> Yes
                                                            </label>
                                                            <br>
                                                        </div>




                                                        <div class="col-sm-12 " id="">
                                                            <br>
                                                            <b>Post Submit Msg</b>  <br/><br/>
                                                            <textarea class="form-control max-textarea"  name="post_message" id="post_message" maxlength="500" rows="4" value="<?php echo $getquiztest->post_msg ?>" placeholder="msg" required><?php echo $getquiztest->post_msg ?></textarea>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <br>
                                                            <b>Show Leaderboard</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <label class="radio-inline">
                                                                <input type="radio" name="ShowLeaderboard" id="n"value="0" <?php if($show_leaderboard == '0'){ ?> checked <?php } ?>readonly required > No
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio"  name="ShowLeaderboard" id="y"value="1" <?php if($show_leaderboard == '1'){ ?> checked <?php } ?>readonly required> Yes
                                                            </label>
                                                            <br>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <br>
                                                            <b>Show Rank</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <label class="radio-inline">
                                                                <input type="radio" name="ShowRank" id="n" value="0" <?php if($show_rank == '0'){ ?> checked <?php } ?>readonly required> No
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio"  name="ShowRank" id="y" value="1"<?php if($show_rank == '1'){ ?> checked <?php } ?> readonly required> Yes
                                                            </label>
                                                            <br>
                                                        </div>

                                                        <?php if ($quizType!=1){?>
                                                            <div class="col-sm-12">
                                                                <br>
                                                                <b>Minimum Time Before Submit</b>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <input class="form-control"  type="number" min="0" name="available_time" id="available_time" value="<?php echo $getquiztest->min_time_pre_submit ?>" placeholder="Total Time (in minutes)" readonly required >
                                                            </div>
                                                            <br><?php }?>

                                                        <div class="col-sm-12">
                                                            <br>
                                                            <b>Arrange questions by topic</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <label class="radio-inline">
                                                                <input type="radio" name="Arrangequestionsbytopic" id="n"   value="0"<?php if($arrange_by_topic == '0'){ ?> checked <?php } ?>readonly required > No
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio"  name="Arrangequestionsbytopic" id="y"  value="1"<?php if($arrange_by_topic == '1'){ ?> checked <?php } ?>readonly required> Yes
                                                            </label>
                                                            <br>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <br>
                                                            <b>Show Solutions To Learner</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <label class="radio-inline">
                                                                <input type="radio" name="ShowSolutionsLearners" id="n"   value="0"<?php if($showsolutionsLeraner == '0'){ ?> checked <?php } ?>readonly required > No
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio"  name="ShowSolutionsLearners" id="y"  value="1"<?php if($showsolutionsLeraner == '1'){ ?> checked <?php } ?>readonly required> Yes
                                                            </label>
                                                            <br>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <br>
                                                            <b>Show Solutions After</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <label class="radio-inline">
                                                                <input type="radio" name="ShowSolutionsAfterAttempt" id="n"   value="0"<?php if($showSolutionsAfterAttempt == '0'){ ?> checked <?php } ?>readonly required checked>Complete Attempt
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio"  name="ShowSolutionsAfterAttempt" id="y"  value="1"<?php if($showSolutionsAfterAttempt == '1'){ ?> checked <?php } ?>readonly required> Each Question Attempt
                                                            </label>
                                                            <br>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <input type="hidden" name="courseid" id="courseid" value="<?php echo $CourseId ?>" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <input type="hidden" name="chapterid" id="chapterid" value="<?php echo $_GET['chapterid'] ?>" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <input type="hidden" name="quizTestid" id="quizTestid" value="<?php echo $_GET['id'] ?>" class="form-control">
                                                                <input type="hidden" name="quizType" id="quizTyp" value="<?php echo $quizType ?>" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br><br>

                                            </form>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <button type="button" data-toggle="collapse" data-target="#questions" class="btn btn-primary btn-outline-primary btn-md btn-block waves-effect waves-light text-left m-b-20 "><i class="feather icon-chevrons-down"></i>Question[<?php echo $tolatquizquestion?>]</button>
                                                </div>
                                            </div>

                                            <?php
                                            $newQuestions = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$_GET['id']." ORDER BY id ASC ")->result();
                                            if ($newQuestions){
                                                ?>
                                                <div  class=""id="questions">
                                                    <div class="row">

                                                        <div class="row col-md-12 col-sm-12">
                                                            <div class="col-md-3 col-sm-3" style="display: inline-block;margin-right: 20px;">
                                                                <button type="button" name="addquiztestQuestion" id="addquiztestQuestion" data-toggle="modal" data-target="#addquiztestQuestion_modal"  class="btn btn-primary btn-outline-primary "> <i class=" feather icon-plus"></i> Add New Question</button>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4" style="display: inline-block;margin-right: 20px;">
                                                                <button type="button" name="ImportquiztestQuestion" id="ImportquiztestQuestion" data-toggle="modal" data-target="#ImportquiztestQuestion_modal"  class="btn btn-primary btn-outline-primary "> <i class=" feather icon-down"></i>Import From Question Bank</button>
                                                            </div>
                                                            <div class="col-md-3 col-sm-3" style="display: inline-block;margin-right: 20px;">
                                                                <button type="button" name="uploadQuestionBank" id="uploadQuestionBank" data-toggle="modal" data-target="#uploadQuestionBank_modal"  class="btn btn-primary btn-outline-primary" style="border-radius: 13px;"><i class="icofont icofont-upload-alt"></i>Upload</button>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12" style="margin-top: 23px;" id="questionBlock">
                                                            <!-- HTML5 Export Buttons table start -->
                                                            <div class="card">
                                                                <div class="card-block" style="padding: 0;">
                                                                    <div class="dt-responsive table-responsive">
                                                                        <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                                            <thead>
                                                                            <tr>
                                                                                <th style="width:3%">#</th>
                                                                                <th style="width:98%">Question</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <?php
                                                                            $i = 0;
                                                                            $editIndex = 0;
                                                                            foreach($newQuestions as $key=>$getnewQuestions){
                                                                                $key++;
                                                                                $editIndex++;
                                                                                $questionid = $getnewQuestions->id;
                                                                                ?>
                                                                                <tr>
                                                                                    <td><b><?php echo $key.'.' ?></b></td>
                                                                                    <td>
                                                                                        <div class="mb-2">
                                                                                            <span class="chip">Created: &nbsp;<b class="subject"> <?=date('Y-m-d h:i:s a  ', strtotime($getnewQuestions->created_date))?> </b></span>
                                                                                            <span class="chip">[ Subject: &nbsp;<b class="subject"><?=$getnewQuestions->subject?></b>  &nbsp;]</span>
                                                                                            <span class="chip">[ Type: &nbsp;<b class="subject">
                                                                                    <?php
                                                                                    if ($getnewQuestions->type==1){
                                                                                        echo "Single Correct Option";
                                                                                    }elseif ($getnewQuestions->type==2){
                                                                                        echo "Multiple Correct Options";
                                                                                    }elseif ($getnewQuestions->type==3){
                                                                                        echo "Numerical / Fill in the Blank";
                                                                                    }elseif ($getnewQuestions->type==4){
                                                                                        echo "Subjective";
                                                                                    }
                                                                                    ?>
                                                                                </b>  &nbsp;] </span>
                                                                                        </div>

                                                                                        <?php
                                                                                        echo $getnewQuestions->question_text;
                                                                                        $newQuestionOptions = $this->db->query("SELECT * FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$getnewQuestions->id." ")->result();
                                                                                        if ($newQuestionOptions){
                                                                                            ?>
                                                                                            <div class="card">
                                                                                                <ul class="list-group list-group-flush" >
                                                                                                    <?php
                                                                                                    $f = 'A';
                                                                                                    foreach($newQuestionOptions as $getnewQuestionOptions){

                                                                                                        ?>

                                                                                                        <?php if($getnewQuestionOptions->is_answer == 1 || $getnewQuestionOptions->option_no == 7 || $getnewQuestionOptions->option_no == 8){  ?>

                                                                                                            <li class="list-group-item" style="background-color:#32B643;color:white;" ><?php echo  $f.". ".$getnewQuestionOptions->option_text?></li>
                                                                                                        <?php }else{ ?>
                                                                                                            <li class="list-group-item" ><?php echo $f.". ".$getnewQuestionOptions->option_text?></li>

                                                                                                        <?php } ?>


                                                                                                        <?php $f++; } ?>
                                                                                                </ul>
                                                                                            </div>
                                                                                        <?php } ?>


                                                                                        <div class="row col-md-12">
                                                                                            <div class="col-sm-12 col-md-8">
                                                                                                <span class="chip" >Explanations:</span> <?=' '.''.$getnewQuestions->explaination .' '?>
                                                                                            </div>

                                                                                            <div class="col-sm-12 col-md-3">
                                                                                                <button style="float: right;margin-left: 20px;" type="button" value="<?= $questionid ?>,<?=$CourseId?>,<?= $quizTestid ?>" data-toggle="modal" data-target="#editquiztestquestion_modal<?=  $getnewQuestions->id ?>"   class="btn btn-primary btn-outline-primary" id="editQuestions" data-option-type="<?=$getnewQuestions->type?>" data-index="<?=$editIndex?>">Edit</button>

                                                                                                <button style="float: right;" onclick ="alert_del_msg('<?= $quizTestid ?>', '<?= $questionid?>', '<?=$CourseId?>')"value="<?= $questionid ?>,<?=$CourseId?>,<?= $quizTestid ?>" type="button" name="deleteitem" id="deleteitem"  class="btn btn-primary btn-outline-primary">Delete</button>

                                                                                                <!--=====edit===-->
                                                                                                <div class="modal fade" id="editquiztestquestion_modal<?=  $getnewQuestions->id ?>" tabindex="-1" role="dialog">
                                                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                                                        <div class="modal-content" style="border-radius: 11px;">
                                                                                                            <div class="modal-header">
                                                                                                                <lable><h4 class="modal-title">Edit Question</h4></lable>
                                                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 28px;">
                                                                                                                    <span aria-hidden="true">&times;</span>
                                                                                                                </button>
                                                                                                            </div>

                                                                                                            <form  method="post" action="<?php echo base_url(); ?>Newquiztest/editquiztestquestions" autocomplete="off" enctype="multipart/form-data">
                                                                                                                <center>
                                                                                                                    <input type="hidden" name="newQuestionId" value="<?=$getnewQuestions->id?>">
                                                                                                                    <div class="modal-body">

                                                                                                                        <div class="row">
                                                                                                                            <div class="row col-sm-12">
                                                                                                                                <div class="col-sm-12 col-md-2 padding-right-0">
                                                                                                                                    <lable class="float-left">Type</lable>
                                                                                                                                </div>
                                                                                                                                <div class="col-sm-12 col-md-9">
                                                                                                                                    <select class="form-control options-type-update" name="type" id="type" onchange="OptionType1(this.value)" required>
                                                                                                                                        <option value="">Select Type</option>
                                                                                                                                        <option value="1" <?php if ($getnewQuestions->type == 1){echo "selected";} ?>>Single Correct Option</option>
                                                                                                                                        <option value="2" <?php if ($getnewQuestions->type ==2){echo "selected";} ?>>Multiple Correct Options</option>
                                                                                                                                        <option value="3" <?php if ($getnewQuestions->type ==3){echo "selected";} ?>>Numerical / Fill in the Blank</option>
                                                                                                                                        <option value="4" <?php if ($getnewQuestions->type ==4){echo "selected";} ?>>Subjective </option>
                                                                                                                                    </select>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>

                                                                                                                        <div class="row" >
                                                                                                                            <div class="col-md-2 col-sm-12 padding-right-0">
                                                                                                                                <br><lable class="float-left">Subject</lable>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-9 col-sm-12">
                                                                                                                                <br><input type="text" name="subjectname" id="subjectname" value="<?php echo $getnewQuestions->subject  ?>" placeholder="subject" class="form-control" required>
                                                                                                                            </div>
                                                                                                                        </div>


                                                                                                                        <div class="row">
                                                                                                                            <div class="col-md-2 col-sm-12 padding-right-0">
                                                                                                                                <br> <lable class="float-left">Topic</lable>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-9 col-sm-12">
                                                                                                                                <br>  <input type="text" name="Topic" id="Topic" placeholder="Topic" value="<?php echo $getnewQuestions->topic ?>" class="form-control" required>
                                                                                                                            </div>
                                                                                                                        </div>

                                                                                                                        <div class="row">
                                                                                                                            <div class="col-md-2 col-sm-12 padding-right-0" >
                                                                                                                                <br> <lable class="float-left">Question Text</lable>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-9 col-sm-12">
                                                                                                                                <br>  <textarea type="text" name="QuestionText" rows="4" id="QuestionTextUpdate<?=$editIndex?>" placeholder="QuestionText" class="form-control max-textarea" required><?php echo $getnewQuestions->question_text ?></textarea>
                                                                                                                            </div>
                                                                                                                        </div>

                                                                                                                        <?php
                                                                                                                        if ($supportLanguage==1){
                                                                                                                            ?>
                                                                                                                            <div class="row">
                                                                                                                                <div class="col-2" style="max-width: 14%;">
                                                                                                                                    <br> <lable class="float-left"><?=$languageName?> Question Text</lable>
                                                                                                                                </div>
                                                                                                                                <div class="col-9" style="max-width: 75%;">
                                                                                                                                    <br>  <textarea type="text" name="QuestionTextLang" rows="4" id="QuestionTextUpdateLang<?=$editIndex?>" placeholder="QuestionText" class="form-control max-textarea" required><?php echo $getnewQuestions->question_text_lang ?></textarea>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        <?php }?>

                                                                                                                        <?php

                                                                                                                        $newQuestionOptions = $this->db->query("SELECT * FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$getnewQuestions->id." ")->result();
                                                                                                                        foreach($newQuestionOptions as $getnewQuestionOptions){}?>

                                                                                                                        <div id="editsingle" class="row col-sm-12 col-md-12">
                                                                                                                            <div  class="col-sm-12 col-md-12" id="optionblock">
                                                                                                                                <br><h4 class="btn btn-primary  btn-md btn-block   text-left m-b-20 ">Options</h4>
                                                                                                                            </div>
                                                                                                                            <?php
                                                                                                                            $i=1;
                                                                                                                            $newQuestionOptions = $this->db->query("SELECT * FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$getnewQuestions->id." ")->result();
                                                                                                                            foreach($newQuestionOptions as $getnewQuestionOptions){?>

                                                                                                                                <input type="hidden" value="<?=$getnewQuestionOptions->id?>" name="questionoptionId<?=$i?>">
                                                                                                                                <div style="margin-top: 21px;" class="col-sm-12" id="row2">
                                                                                                                                    <div class="row">
                                                                                                                                        <div class="col-md-2 col-sm-12" style="padding: 0px" id="correctoptions">
                                                                                                                                            <input type="checkbox" name="option<?=$i?>" id="option<?=$i?>" value="1" <?php if($getnewQuestionOptions->is_answer=='1'){ ?> checked <?php } ?>  class="form-control optionChekbox">
                                                                                                                                            <lable class="float-left" id="correctionsId" style="">Correct Option <?=$i?></lable>
                                                                                                                                        </div>
                                                                                                                                        <div class="col-md-9 col-sm-12" style="padding-left: 11px;">
                                                                                                    <textarea  type="text" name="optionText<?=$i?>" rows="4" id="optionTextUpdate<?=$i?><?=$editIndex?>" placeholder="Option" class="form-control max-textarea">
                                                                                                        <?php echo $getnewQuestionOptions->option_text ?>
                                                                                                    </textarea>
                                                                                                                                        </div>
                                                                                                                                        <div class="" id="removedId">
                                                                                                                                            <button type="button" name="remove" id="2" class="btn btn-primary btn-outline-primary btn-icon btn_remove">X</button>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <br>

                                                                                                                                <?php
                                                                                                                                if ($supportLanguage==1){
                                                                                                                                    ?>
                                                                                                                                    <div style="margin-top: 21px;" class="col-sm-12" id="row2">
                                                                                                                                        <div class="row">
                                                                                                                                            <div class="col-md-2 col-sm-12" style="padding: 0px">
                                                                                                                                                <lable class="float-left" id="correctionsId" style=""><?=$languageName?> Correct Option <?=$i?></lable>
                                                                                                                                            </div>
                                                                                                                                            <div class="col-md-9 col-sm-12" style="padding-left: 11px;">
                                                                                                    <textarea  type="text" name="optionText<?=$i?>Lang" rows="4" id="optionTextUpdateLang<?=$i?><?=$editIndex?>" placeholder="Option" class="form-control max-textarea">
                                                                                                        <?php echo $getnewQuestionOptions->option_text_lang ?>
                                                                                                    </textarea>
                                                                                                                                            </div>
                                                                                                                                            <div class="">
                                                                                                                                                <button type="button" name="remove" id="2" class="btn btn-primary btn-outline-primary btn-icon btn_remove">X</button>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <br>
                                                                                                                                <?php }?>

                                                                                                                                <?php $i++; }?>
                                                                                                                        </div>
                                                                                                                        <!--for Blank-->
                                                                                                                        <div id="editblank" class="row" style="display:none;">
                                                                                                                            <div  class=" row col-sm-12" >
                                                                                                                                <div class="col-md-2">
                                                                                                                                    <br> <lable class="float-left">Answer</lable>
                                                                                                                                </div>
                                                                                                                                <div class="col-md-9" style="padding: 0">
                                                                                                                                    <br>  <input type="text" name="optionText7" id="optionText7" value="<?php if($getnewQuestionOptions->is_answer=='1'){?><?php echo $getnewQuestionOptions->option_text ?><?php }?>" class="form-control" >
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>

                                                                                                                        <!--for Paragraph-->
                                                                                                                        <div class="row " id="editpara" style="display:none">
                                                                                                                            <div  class=" row col-sm-12" >
                                                                                                                                <div class="col-2">
                                                                                                                                    <br> <lable class="float-left">Answer</lable>
                                                                                                                                </div>
                                                                                                                                <div class="col-10">
                                                                                                                                    <br>  <textarea type="text" name="optionText8" rows="4" id="optionText8" value="<?php if($getnewQuestionOptions->is_answer=='1'){?><?php echo $getnewQuestionOptions->option_text ?><?php }?>" placeholder="Enter Your Text..." class="form-control max-textarea" >value="<?php echo $getnewQuestions->option_text ?>"</textarea>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <hr/>
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-md-2 col-sm-12 padding-right-0">
                                                                                                                                <br><lable class="float-left">Explaination</lable>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-9 col-sm-12">
                                                                                                                                <br>  <textarea type="text" name="explaination" rows="4" id="explainationUpdate<?=$editIndex?>" placeholder="QuestionText" value="<?php echo $getnewQuestionOptions->explaination ?>" class="form-control max-textarea" ><?php echo $getnewQuestions->explaination ?></textarea>
                                                                                                                            </div>
                                                                                                                        </div>

                                                                                                                        <?php
                                                                                                                        if ($supportLanguage==1){
                                                                                                                            ?>
                                                                                                                            <div class="row">
                                                                                                                                <div class="col-md-2 col-sm-12 padding-right-0">
                                                                                                                                    <br><lable class="float-left"><?=$languageName?> Explaination</lable>
                                                                                                                                </div>
                                                                                                                                <div class="col-md-9 col-sm-12">
                                                                                                                                    <br>  <textarea type="text" name="explainationLang" rows="4" id="explainationUpdateLang<?=$editIndex?>" placeholder="QuestionText" class="form-control max-textarea" ><?php echo $getnewQuestions->explaination_lang ?></textarea>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        <?php }?>

                                                                                                                        <div class="row">
                                                                                                                            <div class="col-lg-11">
                                                                                                                                <br><button type="button" data-toggle="collapse" data-target="#editshowadvancedoptions<?=  $getnewQuestions->id ?>"  class="btn btn-primary btn-outline-primary btn-md btn-block waves-effect waves-light text-left m-b-20 ">Show Advanced Options</button>
                                                                                                                                <br>
                                                                                                                            </div>
                                                                                                                        </div>


                                                                                                                        <div  class="collapse"id="editshowadvancedoptions<?=  $getnewQuestions->id ?>">
                                                                                                                            <div class="row">
                                                                                                                                <div class="col-2" style="max-width: 14%;">
                                                                                                                                    <br> <lable class="float-left">Order</lable>
                                                                                                                                </div>
                                                                                                                                <div class="col-9" style="max-width: 75%;">
                                                                                                                                    <br>  <input type="number" name="Order" id="Order" placeholder="Order" value="<?php echo $getnewQuestions->order ?>" class="form-control" >
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="row">
                                                                                                                                <div class="col-2" style="max-width: 14%;">
                                                                                                                                    <br> <lable class="float-left">Marks</lable>
                                                                                                                                </div>
                                                                                                                                <div class="col-9" style="max-width: 75%;">
                                                                                                                                    <br>  <input type="number" name="Marks" id="Marks" placeholder="Mark(s)" value="<?php echo $getnewQuestions->mark ?>" class="form-control" >
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="row">
                                                                                                                                <div class="col-2" style="max-width: 14%;">
                                                                                                                                    <br><lable class="float-left">Penalty</lable>
                                                                                                                                </div>
                                                                                                                                <div class="col-9" style="max-width: 75%;">
                                                                                                                                    <br>  <input type="text" name="Penalty" id="Penalty" placeholder="Penalty" value="<?php echo $getnewQuestions->penalty ?>" class="form-control" >
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div class="row">
                                                                                                                        <div class="col-sm-12">
                                                                                                                            <input type="hidden" name="courseid" id="courseid" value="<?php echo $_GET['courseid'] ?>" class="form-control">
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div class="row">
                                                                                                                        <div class="col-sm-12">
                                                                                                                            <input type="hidden" name="quizTestid" id="quizTestid" value="<?php echo $_GET['id'] ?>" class="form-control">
                                                                                                                            <input type="hidden" name="quizType" id="quizType" value="<?php echo $quizType ?>" class="form-control">
                                                                                                                        </div>
                                                                                                                    </div>


                                                                                                                    <div class="modal-footer">
                                                                                                                        <button type="button" class="btn btn-secondary btn-outline-secondary " data-dismiss="modal">Close</button>
                                                                                                                        <button type="submit" class="btn btn-primary btn-outline-primary ">Save </button>
                                                                                                                    </div></center>
                                                                                                            </form>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <!---==edit==-->
                                                                                        </div>


                                                                                    </td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- HTML5 Export Buttons end -->
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }else{?>
                                                <div  class=""id="questions">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <!-- HTML5 Export Buttons table start -->
                                                            <div class="card" style="background: #88cae1;padding: 24px 0px 24px 0px!important;">
                                                                <div class="dt-responsive table-responsive">
                                                                    <div class="col-md-3 col-sm-3" style="display: inline-block;margin-right: 20px;">
                                                                        <button type="button" name="addquiztestQuestion" id="addquiztestQuestion" data-toggle="modal" data-target="#addquiztestQuestion_modal"  class="btn btn-primary btn-outline-primary "> <i class=" feather icon-plus"></i> Add New Question</button>
                                                                    </div>
                                                                    <div class="col-md-4 col-sm-4" style="display: inline-block;margin-right: 20px;">
                                                                        <button type="button" name="ImportquiztestQuestion" id="ImportquiztestQuestion" data-toggle="modal" data-target="#ImportquiztestQuestion_modal"  class="btn btn-primary btn-outline-primary "> <i class=" feather icon-down"></i>Import From Question Bank</button>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-3" style="display: inline-block;margin-right: 20px;">
                                                                        <button type="button" name="uploadQuestionBank" id="uploadQuestionBank" data-toggle="modal" data-target="#uploadQuestionBank_modal"  class="btn btn-primary btn-outline-primary" style="border-radius: 13px;"><i class="icofont icofont-upload-alt"></i>Upload</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- HTML5 Export Buttons end -->
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }?>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!--==========================Import From Previous Question questions Model===============================================-->
<div class="modal fade" id="ImportquiztestQuestion_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <lable> <h4 class="modal-title">Import Question From Question Bank</h4></lable>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <?php
                    $questions=$this->db->query("SELECT * FROM question_bank WHERE status = 1 ORDER BY  id DESC" )->result();
                    foreach($questions as $getquestions){
                    }
                    ?>
                    <table id="basic-btn-import" class="table table-striped table-bordered nowrap">
                        <thead>
                        <tr>
                            <th width="5%" style="text-align: center;vertical-align: middle;"><input type="checkbox" name="all" id="checkall" /></th>
                            <th>Created Date</th>
                            <th>Question Text</th>
                            <th>Subject</th>
                            <th>Topic</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($questions as $getquestions){
                            ?>
                            <tr>
                                <td width="5%" style="text-align: center;vertical-align: middle;"><input type="checkbox" name="type" class="cb-element" value="<?=$getquestions->id?>" /></td>
                                <td><?php echo date('Y-m-d H:i:s', strtotime($getquestions->created_date)) ?></td>
                                <td><?php echo $getquestions->question_text ?></td>
                                <td><?php echo $getquestions->subject ?></td>
                                <td><?php echo $getquestions->topic ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="quizTestId" value="<?=$_GET['id']?>">
                    <input type="hidden" id="courseId"   value="<?=$_GET['courseid']?>">
                    <input type="hidden" value="" id="importDataSelectedItems">
                    <button type="button" class="btn btn-secondary btn-outline-secondary " data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-outline-primary" id="importQuestions">Import</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="uploadQuestionBank_modal" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-md modal-dialog-center" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <lable> <h4 class="modal-title">Upload Question</h4></lable>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <center>
                <form  method="post" action="<?php echo base_url(); ?>Newquiztest/importquestions" autocomplete="off" enctype="multipart/form-data">
                    <div class="modal-body" >
                        <div id="accordion">
                            <div class="card" style="padding: 0;margin: 0;">
                                <div class="card-header" id="headingThree" style="padding: 0;margin: 0;">
                                    <label class="btn btn-link collapsed"  data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" style="    text-decoration: none;">
                                        <img src="https://iibmhm9725.spayee.com/resources/images/icons8-microsoft-excel-48.png" style="max-height: unset;">
                                        Upload simple questions with textual content.
                                    </label>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="border p-1" style="font-size: small;"><b>Upload Instructions</b>
                                        <ol style=" margin-top: 12px;">
                                            <li style="text-align: left;">Download <a target="_blank" href="<?php echo base_url() ?>assets/files/QuestionBank.csv">sample question file.</a></li>
                                            <li style="text-align: left;">Add the questions in the same format as shown in the sample questions.</li>
                                            <li style="text-align: left;">1=Single Correct Option,2=Multiple Correct Options,3=Numerical / Fill in the Blank,4=Subjective</li>
                                            <li style="text-align: left;">if options 1 correct answer then RIGHT ANSWER=1 need to put</li>
                                        </ol>
                                    </div>

                                    <div class="input-group" style="padding: 31px;">
                                        <input type="hidden" name="quizTestId" id="quizTestId" value="<?=$_GET['id']?>">
                                        <input type="hidden" name="courseId" id="courseId"   value="<?=$_GET['courseid']?>">
                                        <input class="form-input" type="file" name="file_upload" accept=".csv" required="" style="border: 1px solid #e85600;padding: 8px;">
                                        <button type="submit" class="btn btn-primary btn-outline-primary" style="margin-top: 9px;">Upload</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-outline-secondary " data-dismiss="modal">Close</button>
                    </div>
                </form>
            </center>
        </div>
    </div>
</div>

<!--=============================================================================================-->

<!-- Warning Section Starts -->
<!-- Older IE warning message -->

<!-- Warning Section Ends -->
<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
<!-- data-table js -->
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<!-- Bootstrap date-time-picker js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
<!-- Date-range picker js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
<!-- Date-dropper js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
<!-- Color picker js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
<!-- Mini-color js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
<!-- i18next.min.js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
<!-- Custom js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
<script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>

<script src="<?php echo base_url() ?>assets/files/assets/pages/sortable-custom.js"></script>

<!-- sweet alert js -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- sweet alert modal.js intialize js -->
<!-- modalEffects js nifty modal window effects -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/modalEffects.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/classie.js"></script>
<!-- Date-range picker js -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
    $('#checkall').change(function() {
        $('.cb-element').prop('checked', this.checked);
    });

    var DataSelectedList=[];
    $('.cb-element').change(function() {
        if ($('.cb-element:checked').length == $('.cb-element').length) {
            $('#checkall').prop('checked', true);
        } else {
            $('#checkall').prop('checked', false);
        }

        if ($(this).is(':checked')){
            DataSelectedList.push($(this).val());
        }else {
            DataSelectedList.pop($(this).val());
        }

        $('#importDataSelectedItems').empty().val(DataSelectedList);
        //list.push($(this).val());
    });

    $(document).on('click', '#editQuestions', function(){

        var type=$(this).data('option-type');
        var index=$(this).data('index');

        $('.options-type-update').val(type).trigger("change");

        if (index){
            CKEDITOR.replace( 'QuestionTextUpdate'+index+'' );
            CKEDITOR.replace( 'QuestionTextUpdateLang'+index+'' );
            CKEDITOR.replace( 'optionTextUpdate1'+index+'' );
            CKEDITOR.replace( 'optionTextUpdate2'+index+'' );
            CKEDITOR.replace( 'optionTextUpdate3'+index+'' );
            CKEDITOR.replace( 'optionTextUpdate4'+index+'' );

            CKEDITOR.replace( 'optionTextUpdateLang1'+index+'' );
            CKEDITOR.replace( 'optionTextUpdateLang2'+index+'' );
            CKEDITOR.replace( 'optionTextUpdateLang3'+index+'' );
            CKEDITOR.replace( 'optionTextUpdateLang4'+index+'' );

            CKEDITOR.replace( 'explainationUpdate'+index+'' );
            CKEDITOR.replace( 'explainationUpdateLang'+index+'' );
        }
    });

    $('#importQuestions').click(function(){
        var list = [];
        var quezTestId=$('#quizTestId').val();
        var courseId=$('#courseId').val();

        var importDataSelectedItems=$('#importDataSelectedItems').val();


        //Check Meeting Are Exist Or not
        if (importDataSelectedItems) {
            $.ajax({
                url: '<?php echo base_url() ?>/Newquiztest/importQuiz',
                type: 'POST',
                data: {
                    questionList: importDataSelectedItems,
                    quezTestId: quezTestId,
                    courseId: courseId,
                },
                success: function (response) {
                    if (response==1){
                        location.reload();
                    }
                }
            });
        }else {
            alert("Please Select At Least One Questions");
        }

    });


    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-23581568-13');
</script>

<script>
    function OptionType(type) {
        if(type ==3 || type ==4){
            document.getElementById("blank").style.display = "block";
            document.getElementById("single").style.display = "none";
            // $('textarea#optionText1').prop('required',false);
            // $('textarea#optionText2').prop('required',false);
            // $('textarea#optionText3').prop('required',false);
            // $('textarea#optionText4').prop('required',false);
        }else if (type==1 || type==2) {
            // $('textarea#optionText1').prop('required',true);
            // $('textarea#optionText2').prop('required',true);
            // $('textarea#optionText3').prop('required',true);
            // $('textarea#optionText4').prop('required',true);
            $('div#blank').hide();
            $('div#single').show();
        }else {
            $('div#blank').hide();
            $('div#single').hide();
        }
    }

</script>

<script>
    function OptionType1(type) {
        if(type ==3){
            $('div#editblank').show();
            $('div#editsingle').hide();
        }else if (type==1 || type==2) {
            $('div#editblank').hide();
            $('div#editsingle').show();
        }else {
            $('div#editblank').hide();
            $('div#editsingle').hide();
        }
    }
</script>
<script >

    function edititem_test(){
        $(':input').prop('readonly', false);
        document.getElementById("save_item").style.display = "block";
        document.getElementById("edit_item").style.display = "none";
    }

</script>
<script>
    $(document).ready(function(){
        var selectType=$('select#type').val();
        if (selectType==1){
            // $('textarea#optionText1').prop('required',true);
            // $('textarea#optionText2').prop('required',true);
            // $('textarea#optionText3').prop('required',true);
            // $('textarea#optionText4').prop('required',true);
        }

        var i=1;

        $('#add').click(function(){
            if(i<=5){
                i++;
                $('#single').append('<div style="margin-top: 21px;" class="col-sm-12" id="row'+i+'"><div class="row"><div class="col-2" style="padding: 0px"><input type="checkbox" name="option'+i+'" id="option'+i+'" value="1" class="form-control optionChekbox" > <lable class="float-left" id="correctionsId" style="">Correct Option '+i+'</lable></div><div class="col-8" style="padding-left: 11px;"><input type="text" placeholder="Option" name="optionText'+i+'" id="optionText'+i+'" maxlength="255" rows="2"class="form-control max-textarea" ></div><div class=""><button type="button" name="remove" id="'+i+'" class="btn btn-primary btn-outline-primary btn-icon btn_remove">X</button></div></div></div><br>');
            }
            else{
                alert('You cannot Add More than 6 Options');
            }
        });

        $(document).on('click', '.btn_remove', function(){
            var button_id = $(this).attr("id");
            $('#row'+button_id+'').remove();
        });

    });
</script>
<script>
    $(document).ready(function(){
        var i=1;

        $('#edit').click(function(){
            if(i<=5){
                i++;
                $('#editsingle').append('<br><div class="col-sm-12" id="row'+i+'"><div class="row"><div class="col-1"><input type="checkbox" name="option'+i+'" id="option'+i+'" value="<?php if($getnewQuestionOptions->is_answer=='1'){?><?php echo $getnewQuestionOptions->option_text ?><?php }?> class="form-control" ></div><div class="col-2">Correct Option '+i+'</div><div class="col-6"><input type="text" placeholder="Option" name="optionText'+i+'" id="optionText'+i+'" maxlength="255" rows="2" value="<?php if($getnewQuestionOptions->is_answer=='1'){?><?php echo $getnewQuestionOptions->option_text ?><?php }?>"class="form-control max-textarea" ></div><div class="col-1"><button type="button" name="remove" id="'+i+'" class="btn btn-primary btn-outline-primary btn-icon btn_remove">X</button></div></div></div><br>');
            }
            else{
                alert('You cannot Add More than 6 Options');
            }
        });

        $(document).on('click', '.btn_remove', function(){
            var button_id = $(this).attr("id");
            $('#row'+button_id+'').remove();
        });

    });
</script>


<script>
    function alert_del_msg(quiztestid,questionid,CourseId)
    {
        swal({
            title: "Are you sure?",
            text: "You wants to delete this item !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willEdit) => {
                if (willEdit) {
                    /*swal("Poof! Your Course has been deleted!", {
                      icon: "success",
                    });*/
                    location.href = '<?php echo base_url() ?>Newquiztest/deletequestion?quiztestid='+quiztestid+'&questionid='+questionid+'&courseid='+CourseId;
                } else {
                    swal("User is not edited!");
                }
            });
    }
</script>

<script>

    CKEDITOR.replace( 'message' );
    CKEDITOR.replace( 'post_message' );


    $(document).on('click', '#edit_item', function(){
        $('div#disbaleItem').removeClass('disbaleItem');
    });

    $(function() {
        $('input[name="showCustomMessage"]').change(function() {
            if ($(this).is(":checked")) {
                $(this).val(1);
                $('div.cusotmMessageBlock').show();
            } else {
                $(this).val(0);
                $('div.cusotmMessageBlock').hide();
            }
        });

    });

    $(document).ready(function(){

        var listOptions=[];

        $('input#option1').click(function() {
            if($(this).is(':checked')){
                listOptions.push('options1');
            }
            else {
                listOptions.pop('options1');
            }
        });

        $('input#option2').click(function() {
            if($(this).is(':checked')){
                listOptions.push('option2');
            }
            else {
                listOptions.pop('option2');
            }
        });

        $('input#option3').click(function() {
            if($(this).is(':checked')){
                listOptions.push('option3');
            }
            else {
                listOptions.pop('option3');
            }
        });

        $('input#option4').click(function() {
            if($(this).is(':checked')){
                listOptions.push('option4');
            }
            else {
                listOptions.pop('option4');
            }
        });

        $('#adquestionFormSubmit').click(function(){
            var selectType=$('select#type').val();
            if (selectType==1 || selectType==2){
                if (listOptions.length<1){
                    $('#bottomError').html("Select at least one correct options");
                    return false;
                }else {
                    $('#bottomError').html("");
                    return true;
                }
            }else {
                $('#bottomError').html("");
                return true;

            }
        });
    });

</script>

</body>
</html>