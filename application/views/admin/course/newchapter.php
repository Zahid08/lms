<?php
date_default_timezone_set("Asia/Calcutta");

$metadata = $this->db->query("SELECT * FROM `metadata` ")->result();

foreach($metadata as $getmetadata){
    $meta_description = $getmetadata->description;
    $meta_keywords = $getmetadata->keywords;
}
    
?>
<?php

$CourseId = $_GET['courseid'];
$courseData = $this->db->query("SELECT * FROM course WHERE status = 1 AND is_published = 1 AND course_id = '$CourseId'  ORDER BY 1 DESC")->result();
foreach($courseData as $getcourseData){
	$courseTitle = $getcourseData->title;
	$courseImage = $getcourseData->image;
	
}
$subChapterId = $_GET['itemid'];
$subChapterData = $this->db->query("SELECT * FROM sub_chapters WHERE status = 1  AND course_id = '$CourseId' AND id= '$subChapterId' ORDER BY 1 DESC")->result();
foreach($subChapterData as $getsubChapterData){
}
$liveTestid =$_GET['id'];
$live_test = $this->db->query("SELECT * FROM live_test WHERE status = 1  AND course_id = '$CourseId' AND id= '$liveTestid' ORDER BY 1 DESC")->result();
foreach($live_test as $getlive_test){
}
$quizTestid =$_GET['id'];
$quiz_test = $this->db->query("SELECT * FROM quiz_test WHERE status = 1  AND course_id = '$CourseId' AND id= '$quizTestid' ORDER BY 1 DESC")->result();
foreach($quiz_test as $getquiz_test){
    $quiz_testid=$getquiz_test->id;
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Competitive Exam Guide || New Chapter</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="<?php echo $meta_description?>">
		<meta name="keywords" content="<?php echo $meta_keywords ?>">
		<meta name="author" content="#">
		<!-- Favicon icon -->
		<link rel="icon" href="<?php echo base_url() ?>assets/files/assets/images/favicon.ico" type="image/x-icon">
		<!-- Google font-->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
		<!-- Required Fremwork -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap/css/bootstrap.min.css">
		<!-- themify-icons line icon -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/themify-icons/themify-icons.css">
		<!-- ico font -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/icofont/css/icofont.css">
		<!-- feather Awesome -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/icon/feather/css/feather.css">
		<!-- Date-time picker css -->
	<!--	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">-->
		<!-- Date-range picker css  -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css">
		<!-- Date-Dropper css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datedropper/css/datedropper.min.css">
		<!-- Color Picker css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/spectrum/css/spectrum.css">
		<!-- Mini-color css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/css/jquery.minicolors.css">
		<!-- sweet alert framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/sweetalert/css/sweetalert.css">
		<!-- Data Table Css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/css/buttons.dataTables.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
		<!-- Style.css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/files/assets/css/jquery.mCustomScrollbar.css">
		<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        


	 
	</head>
	<body>
    <style>
        div#anotherLanguageDIv{
            display: none;
        }
        button.close {
            font-size: 35px;
        }
        .modal-content {
             border-radius: 14px;
         }

    </style>
	    <?php if($this->session->flashdata('msg')){ ?>						            
	    <script>
        	$(document).ready(function(){
        		$("#myModal").modal('show');
        	});
        </script>
	    <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Message</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
        				<b style="color:green"><?php echo $this->session->flashdata('msg') ?></b>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>
		<!-- Pre-loader start -->
		<div class="theme-loader">
			<div class="ball-scale">
				<div class='contain'>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
					<div class="ring">
						<div class="frame"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Pre-loader end -->
		<div id="pcoded" class="pcoded">
			<div class="pcoded-overlay-box"></div>
			<div class="pcoded-container navbar-wrapper">
				<?php include("includes/header.php"); ?>
				<!-- Sidebar inner chat end-->
				<div class="pcoded-main-container">
					<div class="pcoded-wrapper" style="width:100%;">
						<?php include("includes/coursesidenav.php"); ?>
						<div class="pcoded-content">
							<div class="pcoded-inner-content">
								<!-- Main-body start -->
								<div class="row" >
								<div class="col-sm-1"></div>
								<div class="main-body col-sm-11">
									<div class="page-wrapper">
										<!-- Page-header start -->
										<div class="page-header">
											<div class="row align-items-end">
												<div class="col-lg-10">
													<div class="page-header-title">
														<div class="d-inline">
															<h4><?php echo $courseTitle ?></h4>
															<!--<span>23 Course(s)</span>-->
														</div>
													</div>
												</div>
												
												
											</div>
										</div>
										<!-- Page-header end -->
										<!-- Page-body start -->
									
								        
										<div class="page-body">
										   <?php  if($_GET['chapterid'] == 'nil'){?> 

												   <div class="card">
													<div class="card-body">
														<div class="form-row">
														<!--	<div class="col-sm-3">
															    <h4>Upload New Item</h4>
															    <br>
															    <input type="radio" name="chaptername" id="pdf"  class="btn btn-default waves-effect" data-toggle="modal" data-target="#pdf_modal"> <b>Upload:</b> PDF/AUDIO/VIDEO.
															    <div class="modal fade" id="pdf_modal" tabindex="-1" role="dialog">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title">UPLOAD: (Max 250 MB)</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <form  method="post" action="<?php echo base_url(); ?>Newchapter/addchapter" autocomplete="off" enctype="multipart/form-data">
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="text" name="chaptername" id="chaptername" placeholder="Title" class="form-control" required>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <br>
                                                                                <label class="radio-inline">
                                                                                  <input type="radio" name="pdf_sub_type" id="upload_file" value="upload" onclick="Pdf()"> Upload
                                                                                </label>
                                                                                <label class="radio-inline">
                                                                                  <input type="radio" name="pdf_sub_type" id="upload_link" value="public_url" onclick="Pdf()"> Public URL
                                                                                </label>
                                                                                <br>
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="file" name="pdf_file" id="pdf_file" class="form-control"  style="display:none;">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="text" name="pdf_link" id="pdf_link" placeholder="Paste Link Here" class="form-control" style="display:none;">
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                                <label class="radio-inline">
                                                                                  <input type="radio" name="availability" id="always_available" value="1" onclick="Availability()" checked> Always Available
                                                                                </label>
                                                                                <label class="radio-inline">
                                                                                  <input type="radio" name="availability" id="time_based" value="2" onclick="Availability()"> Time Based
                                                                                </label>
                                                                                <br>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6">
                                                                                        <input type="text" name="available_from" id="available_from" onfocus="this.type='date'" placeholder="Available From" class="form-control" style="display:none;" >
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <input type="text" name="available_to" id="available_to" onfocus="this.type='date'" placeholder="Available To" class="form-control" style="display:none;" >
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="type" id="type" value="Pdf" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="courseid" id="courseid" value="<?php echo $CourseId ?>" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="chapterid" id="chapterid" value="<?php echo $_GET['chapterid'] ?>" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="subchapterid" id="subchapterid" value="nil" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                                                        <button type="submit" name="submit"class="btn btn-primary waves-effect waves-light ">Save </button>
                                                                                    </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
															    <br><br>
															    <!--<input type="radio" name="chaptername" id="video"> <b>Video:</b> Add a PDF file in the course.
															    <br><br>
															    <input type="radio" name="chaptername" id="audio"> <b>Audio:</b> All uploaded videos are completely secure and non downloadable. It can also be used to embed youtube and Vimeo videos.
															    <br><br>
															    <input type="radio" name="chaptername" id="file"> <b>File:</b> Add any file type for learners to download.
														</div>-->
															<!--<div class="col-sm-1">
    															
                                                            </div>-->
															<div id="mode" class="col-sm-8" >
															    <h4>Create New Item</h4>
															    <br>
															    <input type="radio" name="chaptername" id="heading" data-toggle="modal" data-target="#heading_modal"> <b>Heading:</b> Define your chapter or section headings.
															    <br><br>
															    <div class="modal fade" id="heading_modal" tabindex="-1" role="dialog">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title">Heading</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <form  method="post" action="<?php echo base_url(); ?>Newchapter/addchapter" autocomplete="off">
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="text" name="chaptername" id="chaptername" placeholder="Title" class="form-control" required>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="type" id="type" value="Heading" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="courseid" id="courseid" value="<?php echo $CourseId ?>" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="chapterid" id="chapterid" value="<?php echo $_GET['chapterid'] ?>" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="subchapterid" id="subchapterid" value="nil" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Save </button>
                                                                                    </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
															    <!--<input type="radio" name="chaptername" id="text"> <b>Text:</b> Create your textual lessons in the course. It can also be used to embed iFrame, add HTML code through the Source option.
															    <br><br>-->
															    <!--<input type="radio" name="chaptername" id="link"> <b>Link:</b> Add Link which will be embedded in iFrame.
															    <br><br>-->
															   <!-- <input type="radio" name="chaptername" id="live_class" data-toggle="modal" data-target="#liveclass_modal"> <b>Live Class:</b> Use it to conduct live classes and webinars with your enrolled learners.
                                                                <div class="modal fade" id="liveclass_modal" tabindex="-1" role="dialog">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title">Live Class</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <form  method="post" action="<?php echo base_url(); ?>Newchapter/addchapter" autocomplete="off">
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="text" name="chaptername" id="chaptername" placeholder="Title" class="form-control" required>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="type" id="type" value="Live Class" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                                <label class="radio-inline">
                                                                                  <input type="radio" name="availability" id="time_based" value="2" onclick="Availability()" checked> Time Based
                                                                                </label>
                                                                                <br>
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="text" name="available_from" id="available_from" min="<?php echo date('Y-m-d') ?>" onfocus="this.type='date'" placeholder="Available From" class="form-control"  >
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                                 <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="text" name="pre_class_msg" id="pre_class_msg" placeholder="Pre Class Message" class="form-control" >
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                                 <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="text" name="post_class_msg" id="post_class_msg" placeholder="Post Class Message" class="form-control" >
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                                <div class="row">
                                                                                    <div class='col-sm-12'>
                                                                                        <b>Show Recording (if available) to Learner:</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        <label class="radio-inline">
                                                                                          <input type="radio" name="is_record" id="is_record_yes" value="1" checked> Yes
                                                                                        </label>
                                                                                        <label class="radio-inline">
                                                                                          <input type="radio" name="is_record" id="is_record_no" value="2" > No
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <b>Notify Users on Class Start:</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        <label class="radio-inline">
                                                                                          <input type="checkbox" name="email_push" id="email_push" value="1" > Email
                                                                                        </label>
                                                                                        <label class="radio-inline">
                                                                                          <input type="checkbox" name="mobile_push" id="mobile_push" value="1" > Mobile Push
                                                                                        </label>
                                                                                        <label class="radio-inline">
                                                                                          <input type="checkbox" name="web_push" id="web_push" value="1" > Web push
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="courseid" id="courseid" value="<?php echo $CourseId ?>" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="chapterid" id="chapterid" value="<?php echo $_GET['chapterid'] ?>" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="subchapterid" id="subchapterid" value="nil" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Saves </button>
                                                                                    </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
															</div>
															<div class="col-sm-1">
    															
                                                            </div>-->
															<!--<div class="col-sm-3">
															    <br><br><br><br><br><br>
																<input type="radio" name="chaptername" id="assetlibrary"> <b>Choose From asset Library</b>
                                                            </div>-->
                                                            <!--<iframe   src="https://drive.google.com/file/d/1U1eW_SAPYEXHaXNkdaH2CAl2pBnfW7fr/preview" width="640" height="480"
                                                            frameborder="0" scrolling="no" seamless></iframe>-->
															<!--<div class="col-sm-1">
																<button type="button" name="edit" id="edit"  class="btn btn-primary btn-outline-primary">Edit</button>
																<button type="submit" name="save" id="save" onclick="alert('Are you sure, You want to Modify this Data??')"  class="btn btn-primary btn-outline-primary" style="display:none;">Save</button>
															</div>-->
															   
														</div>
												
												</div>
										    
											</div>
										   
										   <?php }else{ ?>
										    
										        <div class="card">
													<div class="card-body">
														<div class="form-row">
															<div class="col-sm-3">
															    <h4>Upload New Item</h4>
															    <br>
															    <input type="radio" name="chaptername" id="pdf"  class="btn btn-default waves-effect" data-toggle="modal" data-target="#pdf_modal"> <b>Upload:</b> PDF/AUDIO/VIDEO.
															    <div class="modal fade" id="pdf_modal" tabindex="-1" role="dialog">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title">UPLOAD: (Max 250 MB)</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <form  method="post" action="<?php echo base_url(); ?>Newchapter/addchapter" autocomplete="off" enctype="multipart/form-data">
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="text" name="chaptername" id="chaptername" placeholder="Title" class="form-control" required>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <br>
                                                                                <label class="radio-inline">
                                                                                  <input type="radio" name="sub_type" id="upload_file" value="upload" onclick="Pdf()"> Upload
                                                                                </label>
                                                                                <label class="radio-inline">
                                                                                  <input type="radio" name="sub_type" id="upload_link" value="public_url" onclick="Pdf()"> Public URL
                                                                                </label>
                                                                                <br>
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="file" name="pdf_file" id="pdf_file" class="form-control"  style="display:none;">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="text" name="pdf_link" id="pdf_link" placeholder="Paste Link Here" class="form-control" style="display:none;">
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                                <label class="radio-inline">
                                                                                  <input type="radio" name="availability" id="always_available" value="1" onclick="Availability()" checked> Always Available
                                                                                </label>
                                                                                <label class="radio-inline">
                                                                                  <input type="radio" name="availability" id="time_based" value="2" onclick="Availability()"> Time Based
                                                                                </label>
                                                                                <br>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6">
                                                                                        <input type="text" name="available_from" id="available_from"  placeholder="Available From" class="form-control" style="display:none;" >
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <input type="text" name="available_to" id="available_to"  placeholder="Available To" class="form-control" style="display:none;" >
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="type" id="type" value="Pdf" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="courseid" id="courseid" value="<?php echo $CourseId ?>" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="chapterid" id="chapterid" value="<?php echo $_GET['chapterid'] ?>" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="subchapterid" id="subchapterid" value="nil" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                                                        <button type="submit" name="submit"class="btn btn-primary waves-effect waves-light ">Save </button>
                                                                                    </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
															    <br><br>
															    <!--<input type="radio" name="chaptername" id="video"> <b>Video:</b> Add a PDF file in the course.
															    <br><br>
															    <input type="radio" name="chaptername" id="audio"> <b>Audio:</b> All uploaded videos are completely secure and non downloadable. It can also be used to embed youtube and Vimeo videos.
															    <br><br>
															    <input type="radio" name="chaptername" id="file"> <b>File:</b> Add any file type for learners to download.
															--></div>
															<div class="col-sm-1">
    															
                                                            </div>
															<div id="mode" class="col-sm-4" >
															    <h4>Create New Item</h4>
															    <br>
															    <input type="radio" name="chaptername" id="heading" data-toggle="modal" data-target="#heading_modal"> <b>Heading:</b> Define your chapter or section headings.
															    <br><br>
															    <div class="modal fade" id="heading_modal" tabindex="-1" role="dialog">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title">Heading</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <form  method="post" action="<?php echo base_url(); ?>Newchapter/addchapter" autocomplete="off">
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="text" name="chaptername" id="chaptername" placeholder="Title" class="form-control" required>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="type" id="type" value="Heading" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="courseid" id="courseid" value="<?php echo $CourseId ?>" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="chapterid" id="chapterid" value="<?php echo $_GET['chapterid'] ?>" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="subchapterid" id="subchapterid" value="nil" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Save </button>
                                                                                    </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
															    <!--<input type="radio" name="chaptername" id="text"> <b>Text:</b> Create your textual lessons in the course. It can also be used to embed iFrame, add HTML code through the Source option.
															    <br><br>-->
															    <!--<input type="radio" name="chaptername" id="link"> <b>Link:</b> Add Link which will be embedded in iFrame.
															    <br><br>-->
															    <input type="radio" name="chaptername" id="live_class" data-toggle="modal" data-target="#liveclass_modal"> <b>Live Class:</b> Use it to conduct live classes and webinars with your enrolled learners.
                                                                <div class="modal fade" id="liveclass_modal" tabindex="-1" role="dialog">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title">Live Class</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <form  method="post" action="<?php echo base_url(); ?>Newchapter/addchapter" autocomplete="off">
                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <div class="col-sm-3">
                                                                                        <label class="radio-inline">
                                                                                          <input type="radio" name="sub_type" id="bbb" value="bbb" checked> Inbuilt Live
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <label class="radio-inline">
                                                                                          <input type="radio" name="sub_type" id="jitsi" value="jitsi" > Jitsi
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <label class="radio-inline">
                                                                                          <input type="radio" name="sub_type" id="zoom" value="zoom"> Zoom Live
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                               <hr>
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="text" name="chaptername" id="chaptername" placeholder="Title" class="form-control" required>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="type" id="type" value="Live Class" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                                
                                                                                  <div class="row">
                                                                                      <div class="col-sm-3">
                                                                                    <label class="radio-inline">
                                                                                      <input type="radio" name="availability" id="time_based" value="2" onclick="Availability()" checked> Time Based
                                                                                    </label>
                                                                                    </div>
                                                                                <br>
                                                                                 
                                                                                    <div class="col-sm-6">
                                                                                        <input type="text" name="available_date" id="available_date"   placeholder="Available Date" class="form-control"  >
                                                                                    </div>
                                                                                
                                                                                    <div class="col-sm-3">
                                                                                        <input type="text" name="available_time" id="available_time"  onfocus="this.type='time'" min="09:00" max="19:00" placeholder="Available time" class="form-control"  >
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                                 <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="text" name="pre_class_msg" id="pre_class_msg" placeholder="Pre Class Message" class="form-control" >
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                                 <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="text" name="post_class_msg" id="post_class_msg" placeholder="Post Class Message" class="form-control" >
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                                <div class="row">
                                                                                    <div class='col-sm-12'>
                                                                                        <b>Show Recording (if available) to Learner:</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        <label class="radio-inline">
                                                                                          <input type="radio" name="is_record" id="is_record_yes" value="1" checked> Yes
                                                                                        </label>
                                                                                        <label class="radio-inline">
                                                                                          <input type="radio" name="is_record" id="is_record_no" value="2" > No
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <b>Notify Users on Class Start:</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        <label class="radio-inline">
                                                                                          <input type="checkbox" name="email_push" id="email_push" value="1" > Email
                                                                                        </label>
                                                                                        <label class="radio-inline">
                                                                                          <input type="checkbox" name="mobile_push" id="mobile_push" value="1" > Mobile Push
                                                                                        </label>
                                                                                        <label class="radio-inline">
                                                                                          <input type="checkbox" name="web_push" id="web_push" value="1" > Web push
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="courseid" id="courseid" value="<?php echo $CourseId ?>" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="chapterid" id="chapterid" value="<?php echo $_GET['chapterid'] ?>" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="subchapterid" id="subchapterid" value="nil" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Save </button>
                                                                                    </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <br><br>
                                                                <input type="radio" name="chaptername" id="New_live_test"  data-toggle="modal" data-target="#New_live_test_modal" > <b>Live Test:</b>Learners can attempt it during specified time window. Result is manually generated by admins. Leadership board is visible to learners after result declaration.
															    
															    <div class="modal fade" id="New_live_test_modal" tabindex="-1" role="dialog">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title">Live Test</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <form  method="post" action="<?php echo base_url(); ?>Newquiztest/addquiztest?quiztestid=<?php echo $quiz_testid ?>&quizType=1" autocomplete="off" enctype="multipart/form-data">
                                                                                <div class="modal-body">

                                                                                    <div class="row">
                                                                                        <div class="col-sm-12">
                                                                                            <input type="text" name="chaptername" id="chaptername" placeholder="Title" class="form-control" required>
                                                                                        </div>

                                                                                        <div class="col-sm-12">
                                                                                            <br>
                                                                                            Time Limit (in minutes)&nbsp;&nbsp;<span style="font-size: 12px;">0 in case of no time limit</span>
                                                                                            <br>
                                                                                            <div class="row">
                                                                                                <div class="col-sm-12">

                                                                                                    <input class="form-control"  type="number" min="0" name="time_limit" id="time_limit" placeholder="Total Time (in minutes)" required>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>


                                                                                        <div class="col-sm-12"><br>
                                                                                            <input type="checkbox" value="0" id="anotherLanguageSupport" name="another-language">&nbsp;Another Language Support
                                                                                        </div>

                                                                                        <div class="col-sm-12" id="anotherLanguageDIv"><br>
                                                                                            <select name="language" class="form-control">
                                                                                                <option value=''>Select Language</option>
                                                                                                <option value='1'>हिंदी</option>
                                                                                                <option value='2'>English</option>
                                                                                                <option value='3'>Others</option>
                                                                                            </select>
                                                                                        </div>

                                                                                        <div class="col-sm-12">
                                                                                            <br>
                                                                                            Test Open Window&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                            <br>
                                                                                            <div class="row">
                                                                                                <div class="col-sm-6">
                                                                                                    <input type="text" name="available_from" id="available_from" min="<?php echo date('Y-m-d h:i:s') ?>" value=""  onfocus="this.type='datetime-local'" placeholder="Available From" class="form-control"  required>
                                                                                                </div>
                                                                                                <div class="col-sm-6">
                                                                                                    <input type="text" name="available_till" id="available_till" min="<?php  echo date('Y-m-d h:i:s') ?>" value="" onfocus="this.type='datetime-local'" placeholder="Available Till" class="form-control"   required>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="row">
                                                                                            <div class="col-sm-12">
                                                                                                <input type="hidden" name="courseid" id="courseid" value="<?php echo $CourseId ?>" class="form-control">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <div class="col-sm-12">
                                                                                                <input type="hidden" name="chapterid" id="chapterid" value="<?php echo $_GET['chapterid'] ?>" class="form-control">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <div class="col-sm-12">
                                                                                                <input type="hidden" name="quizTestid" id="quizTestid" value="<?php echo $quiz_testid ?>" class="form-control">
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>

                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light ">Save </button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>    
															    <br><br>
                                                                <input type="radio" name="chaptername" id="quiz_test"  data-toggle="modal" data-target="#quiz_test_modal" > <b>Quiz Test:</b>Learners can attempt it any time and view instant results.
															    
															    <div class="modal fade" id="quiz_test_modal" tabindex="-1" role="dialog">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title">New Quiz</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <form  method="post" action="<?php echo base_url(); ?>Newquiztest/addquiztest?quiztestid=<?php echo $quiz_testid ?>&quizType=0" autocomplete="off" enctype="multipart/form-data">
                                                                            <div class="modal-body">
                                                                              
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="text" name="chaptername" id="chaptername" placeholder="Title" class="form-control" required>
                                                                                    </div>
                                                                                
                                                                                    <div class="col-sm-12">
                                                                                    <br>    
                                                                                        Time Limit (in minutes)&nbsp;&nbsp;<span style="font-size: 12px;">0 in case of no time limit</span>
                                                                                    <br>
                                                                                        <div class="row">
                                                                                        <div class="col-sm-12">
                                                                                    
								                                                         <input class="form-control"  type="number" min="0" name="time_limit" id="time_limit" placeholder="Total Time (in minutes)" required>
                                                                                        </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-sm-12">
                                                                                    <br>    
                                                                                        Re-take Attempts &nbsp; <span style="font-size: 12px;">(Keep blank for unlimited attempts)</span>
                                                                                    <br>
                                                                                        <div class="row">
                                                                                        <div class="col-sm-12">
                                                                                    
								                                                         <input class="form-control"  type="number" min="0" name="re_take_attempts" id="re_take_attempts" placeholder="Re-take Attempts" required>
                                                                                        </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-sm-12">
                                                                                    <br>
                                                                                    Test Open Window&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    <br>
                                                                                     <div class="row">
                                                                                              <div class="col-sm-6">
                                                                                                  <input type="text" name="available_from" id="available_from" min="<?php echo date('Y-m-d h:i:s') ?>" value=""  onfocus="this.type='datetime-local'" placeholder="Available From" class="form-control"  required>
                                                                                             </div>
                                                                                             <div class="col-sm-6">
                                                                                                 <input type="text" name="available_till" id="available_till" min="<?php  echo date('Y-m-d h:i:s') ?>" value="" onfocus="this.type='datetime-local'" placeholder="Available Till" class="form-control"   required>
                                                                                             </div>
                                                                                     </div>    
                                                                                    </div>

                                                                                <div class="col-sm-12"><br>
                                                                                    <input type="checkbox" value="0" id="anotherLanguageSupport" name="another-language">&nbsp;Another Language Support
                                                                                </div>

                                                                                    <div class="col-sm-12" id="anotherLanguageDIv"><br>
                                                                                        <select name="language" class="form-control">
                                                                                            <option value=''>Select Language</option>
                                                                                            <option value='1'>हिंदी</option>
                                                                                            <option value='2'>English</option>
                                                                                            <option value='3'>Others</option>
                                                                                        </select>
                                                                                    </div>

                                                                                    <div class="col-sm-12"><br>
                                                                                        <input type="checkbox" value="0" id="autoPreefineRandomChose" name="random-chose">&nbsp;Automically chose random question based on pre defined filters.
                                                                                    </div>

                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="courseid" id="courseid" value="<?php echo $CourseId ?>" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="chapterid" id="chapterid" value="<?php echo $_GET['chapterid'] ?>" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <input type="hidden" name="quizTestid" id="quizTestid" value="<?php echo $quiz_testId ?>" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                    
                                                                                </div>
                                                                            </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Save </button>
                                                                                    </div>
                                                                            </form>

                                                                        </div>
                                                                    </div>
                                                                </div>    
															    

															

															</div>
														    

															<div class="col-sm-1">
    															
                                                            </div>
															<div class="col-sm-3">
															    <br><br><br><br><br><br>
																<input type="radio" name="chaptername" id="assetlibrary"> <b>Choose From asset Library</b>
                                                            </div>
                                                            <!--<iframe   src="https://drive.google.com/file/d/1U1eW_SAPYEXHaXNkdaH2CAl2pBnfW7fr/preview" width="640" height="480"
                                                            frameborder="0" scrolling="no" seamless></iframe>-->
															<!--<div class="col-sm-1">
																<button type="button" name="edit" id="edit"  class="btn btn-primary btn-outline-primary">Edit</button>
																<button type="submit" name="save" id="save" onclick="alert('Are you sure, You want to Modify this Data??')"  class="btn btn-primary btn-outline-primary" style="display:none;">Save</button>
															</div>-->
															   
														</div>
												
												</div>
										    
											</div>
										   <?php } ?>
											
										</div>
									    
										<!-- Page-body end -->
									</div>
								</div>
							</div>
							</div>
							<!-- Main-body end -->
							<!-- <div id="styleSelector">
							</div>-->
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Warning Section Starts -->
		<!-- Older IE warning message -->
		
		<!-- Warning Section Ends -->
		<!-- Required Jquery -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/popper.js/js/popper.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
		<!-- jquery slimscroll js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
		<!-- modernizr js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/modernizr.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/modernizr/js/css-scrollbars.js"></script>
		<!-- data-table js -->
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/jszip.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/pdfmake.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/js/vfs_fonts.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/jszip.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/vfs_fonts.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
		<!-- Bootstrap date-time-picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
		<!--<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>-->
		<!-- Date-range picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
		<!-- Date-dropper js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/datedropper/js/datedropper.min.js"></script>
		<!-- Color picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/spectrum/js/spectrum.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jscolor/js/jscolor.js"></script>
		<!-- Mini-color js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-minicolors/js/jquery.minicolors.min.js"></script>
		<!-- i18next.min.js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next/js/i18next.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
		<!-- Custom js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/pages/advance-elements/custom-picker.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/pcoded.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/vartical-layout.min.js"></script>
		<script src="<?php echo base_url() ?>assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/script.js"></script>
		
		<script src="<?php echo base_url() ?>assets/files/assets/pages/sortable-custom.js"></script>
		
        <!-- sweet alert js -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- sweet alert modal.js intialize js -->
        <!-- modalEffects js nifty modal window effects -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/modalEffects.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/files/assets/js/classie.js"></script>
		<!-- Date-range picker js -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-23581568-13');
		</script>
		
		<script>
		jQuery( function() {
            jQuery( '#available_date' ).datepicker({ 
            format: 'yyyy-mm-dd'
            });
		} );
  </script>
  
  <script>
      jQuery( function() {
    jQuery( '#available_till' ).datetimepicker({ 
     
    });
    jQuery( '#available_from' ).datetimepicker({ 
   
    });
   jQuery( '#available_till1' ).datetimepicker({ 
    
    });
    jQuery( '#available_from1' ).datetimepicker({ 
     
    });
   jQuery( '#available_till2' ).datetimepicker({ 
      
    });
   jQuery( '#available_from2' ).datetimepicker({ 
     
    });
    jQuery( '#available_to' ).datetimepicker({ 
   
    });
    
   
   

  } );
  </script>
		<script>
		
		function Pdf(){
		    if ($("#upload_file").is(":checked")) {
               // do something
               $('#pdf_file').prop('required',true);
               document.getElementById("pdf_link").style.display = "none";
               document.getElementById("pdf_file").style.display = "block";
            }else if ($("#upload_link").is(":checked")) {
               // do something
               $('#pdf_link').prop('required',true);
               document.getElementById("pdf_link").style.display = "block";
               document.getElementById("pdf_file").style.display = "none";
            }
		}
		
		function Availability(){
		     if ($("#always_available").is(":checked")) {
               // do something
               document.getElementById("available_from").style.display = "none";
              document.getElementById("available_date").style.display = "none";
              document.getElementById("available_from1").style.display = "none";
              document.getElementById("available_till").style.display = "none";
              document.getElementById("available_from2").style.display = "none";
              document.getElementById("available_till1").style.display = "none";
              document.getElementById("available_date").style.display = "none";
              document.getElementById("available_till2").style.display = "none";
               document.getElementById("available_to").style.display = "none";
            }else if ($("#time_based").is(":checked")) {
               // do something
               $('#available_from').prop('required',true);
              $('#available_date').prop('required',true);
              $('#available_from1').prop('required',true);
              $('#available_till').prop('required',true);
              $('#available_from2').prop('required',true);
              $('#available_till1').prop('required',true);
              $('#available_till2').prop('required',true);
               $('#available_time').prop('required',true);
               $('#available_to').prop('required',true);
                document.getElementById("available_from").style.display = "block";
              document.getElementById("available_date").style.display = "block";
              document.getElementById("available_from1").style.display = "block";
              document.getElementById("available_till").style.display = "block";
              document.getElementById("available_from2").style.display = "block";
              document.getElementById("available_till2").style.display = "block";
              document.getElementById("available_till").style.display = "block";
              document.getElementById("available_time").style.display = "block";
               document.getElementById("available_to").style.display = "block";
            }
		}


        $(function() {
            $('input[name="another-language"]').change(function() {
                if ($(this).is(":checked")) {
                    $(this).val(1);
                    $('div#anotherLanguageDIv').show();
                } else {
                    $(this).val(0);
                    $('div#anotherLanguageDIv').hide();
                }
            });

            $('input[name="random-chose"]').change(function() {
                if ($(this).is(":checked")) {
                    $(this).val(1);
                } else {
                    $(this).val(0);
                }
            });

        });

		</script>
	

  
  
	</body>
</html>