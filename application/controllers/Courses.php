<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses extends CI_Controller {
    
	public function index()
	{
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
		    $this->load->view('admin/courses');
	    }else{
	        redirect('Login');
	    }
	}
}
