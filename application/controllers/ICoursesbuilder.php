<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ICoursesbuilder extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		$this->load->model('Coursedetails_model');
		$this->load->helper(array('form', 'url'));
	    $this->load->library('form_validation');
	}
	public function index()
	{
	     if(($_SESSION['role'] == 1 || $_SESSION['role'] == 2) && $_SESSION['status'] == 1){
		    $this->load->view('instructor/course/coursebuilder');
	     }else{
	         redirect('Login');
	     }
	}
	
	
}
