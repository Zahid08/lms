<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quiz extends CI_Controller {
    function __construct()
{
parent::__construct();
$this->load->model('Newquiztest_model');
$this->load->helper(array('form', 'url'));
$this->load->library('form_validation');
}
	public function index()
	{
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
	        $this->load->view('admin/quiz');
	    }else{
	        redirect('Login');
	    }
		
	}
public function addquiztestQuestion(){
   
$courseid = $this->security->xss_clean($this->input->post('courseid'));

$quizTestid = $this->security->xss_clean($this->input->post('quizTestid'));
$type  = $this->security->xss_clean($this->input->post('type'));
$subjectname = $this->security->xss_clean($this->input->post('subjectname'));
$Topic   = $this->security->xss_clean($this->input->post('Topic'));
$QuestionText = $this->security->xss_clean($this->input->post('QuestionText'));

$Explaination = $this->security->xss_clean($this->input->post('explaination'));




$Order = $this->security->xss_clean($this->input->post('Order'));
$Marks= $this->security->xss_clean($this->input->post('Marks'));
$Penalty = $this->security->xss_clean($this->input->post('Penalty'));

$now = date('Y-m-d H:i:s');

$insertquestionsData = array('quiz_test_id'=>$quizTestid,
'type' => $type,
'subject'=>$subjectname,
'topic'=>$Topic,
'question_text'=>$QuestionText,
'explaination' => $Explaination,
'order'=>$Order,
'mark'=>$Marks,
'penalty'=>$Penalty,
'created_date'=>$now,
'created_by'=>$_SESSION['user_id'],
'updated_date'=>$now,
'updated_by'=>$_SESSION['user_id']
);


$insertNewquestions=$this->Newquiztest_model->insertquiztestquestions($insertquestionsData);

if($insertNewquestions)
{
    //after question has been inserted successfully getting if it and inserting its options
    $CreatedBy = $_SESSION['user_id'];
    $newQuestionID = $this->db->query("SELECT id FROM quiz_test_questions WHERE status = 1 AND created_by = '$CreatedBy' ORDER BY 1 DESC LIMIT 1")->result();
        foreach($newQuestionID as $getnewQuestionID){
        $QuestionID = $getnewQuestionID->id;
        }

        for($i=1;$i<=8;$i++){
        	    if(!empty($this->input->post('optionText'.$i))){
        	        $insertOptions = array('quiz_test_question_id' => $QuestionID
                    ,'option_no' => $i
                    ,'option_text' => $this->input->post('optionText'.$i)
                    ,'is_answer' => $this->input->post('option'.$i) 
                    ,'status' => 1 //byDefault Active
                    ,'created_date'=>$now
                    ,'created_by'=>$_SESSION['user_id']
                    ,'updated_date'=>$now
                    ,'updated_by'=>$_SESSION['user_id']
                    );
                $this->db->insert('quiz_test_question_options', $insertOptions);
        	    }  
        	}
            
    
$this->session->set_flashdata('msg', 'Question Added Successfully.');
redirect('Quiz');


}


else 
{
$this->session->set_flashdata('msg','Unable to Added question. Please try again.');
redirect('Quiz');
}
}
public function deletequestion()
	{
	    
         $questionsid=$_GET['questionsid'];
	     $this->db->query("UPDATE quiz_test_question_options SET status = 2 WHERE quiz_test_question_id = $questionsid");
	    $this->db->query("UPDATE quiz_test_questions SET status = 2 WHERE id = $questionsid ");
	   
	     $this->session->set_flashdata('delmsg','Deleted Question Successfully .');
	   redirect('Quiz');
         
	     
	}
}