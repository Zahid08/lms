<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EditItem extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		$this->load->model('Newchapter_model');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Bigbluebutton_model');
	    $this->load->library('form_validation');
	}

	//http://localhost/lms/EditItem/cronJob
    //THis one should set on server end
    public function cronJob()
    {
        $subChapterDataAll = $this->db->query("SELECT * FROM sub_chapters where end_session =1")->result();
        foreach ($subChapterDataAll as $getsubChapterData) {
            $subChapterId = $getsubChapterData->id;
            $liveRecordingData = $this->db->query("SELECT * FROM live_class_recordings WHERE status = 1  AND sub_chapter_id= '$subChapterId' ORDER BY 1 DESC")->result();
            if(!count($liveRecordingData) > 0) {
                $bbsettingData = $this->db->query("SELECT * FROM bbb_settings where sub_chapter_id = '$subChapterId' ORDER BY 1 DESC LIMIT 1")->result();
                foreach ($bbsettingData as $item) {
                    $mettingId = $item->meetingId;
                }
                //Get Recording
                $recordingsParams = array(
                    'meetingId' =>$mettingId, 			// OPTIONAL - comma separate if multiples
                );
                // Now get recordings info and display it:
                $itsAllGood = true;
                try {$result = $this->Bigbluebutton_model->getRecordingsWithXmlResponseArray($recordingsParams);}
                catch (Exception $e) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    $itsAllGood = false;
                }
                if ($itsAllGood == true) {
                    // If it's all good, then we've interfaced with our BBB php api OK:
                    if ($result == null) {
                        // If we get a null response, then we're not getting any XML back from BBB.
                        echo "Failed to get any response. Maybe we can't contact the BBB server.";
                    }
                    else {
                        if ($result['returncode'] == 'SUCCESS' && $result['messageKey']=='') {
                            $recordList=$result[0];
                            $recordID=$recordList['recordId'];
                            $startTime=$recordList['startTime'];
                            $endTIme=$recordList['endTime'];
                           // https://live.competitiveexamguide.com/bigbluebutton/api/getRecordings?meetingID=$mettingId&recordID={recordId}&checksum=%27b0016e53d08c44758aec9d06f3ca206a012e3ff1%27
                            // $makeUrl='https://live.competitiveexamguide.com/recording/'.$recordID.'.mp4';
                            $makeUrl=$recordList['playbackFormatUrl'];
                            $now = date('Y-m-d H:i:s');
                            $insertData = array(
                                'sub_chapter_id'=> $subChapterId,
                                'link'=> $makeUrl,
                                'startTime'=> $startTime,
                                'is_download'=> 1,
                                'endTime'=> $endTIme,
                                'status' => 1,
                                'created_datetime'=>$now);
                            $this->db->insert('live_class_recordings', $insertData);
                            echo  $recordList."Inserted";
                        }
                    }
                }
            }else{
                echo "All Data Inserted";
            }
        }
    }
    //http://localhost/lms/EditItem/checkSIngleRecording?courseid=2&chapterid=0&itemid=6
    public function checkSIngleRecording()
    {
        $subChapterId=$_REQUEST['itemid'];
        $subChapterData = $this->db->query("SELECT * FROM sub_chapters where id = '$subChapterId' ORDER BY 1 DESC LIMIT 1")->result();
        foreach ($subChapterData as $getsubChapterData) {
            $chapterId = $getsubChapterData->chapterid;
        }
        $bbsettingData = $this->db->query("SELECT * FROM bbb_settings where sub_chapter_id = '$subChapterId' ORDER BY 1 DESC LIMIT 1")->result();
        foreach ($bbsettingData as $item) {
            $mettingId = $item->meetingId;
        }
        $recordingsParams = array(
            'meetingId' =>$mettingId, 			// OPTIONAL - comma separate if multiples
        );
        // Now get recordings info and display it:
        $itsAllGood = true;
        try {$result = $this->Bigbluebutton_model->getRecordingsWithXmlResponseArray($recordingsParams);}
        catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            $itsAllGood = false;
        }
        if ($itsAllGood == true) {
            // If it's all good, then we've interfaced with our BBB php api OK:
            if ($result == null) {
                // If we get a null response, then we're not getting any XML back from BBB.
                echo "Failed to get any response. Maybe we can't contact the BBB server.";
            }
            else {
                // We got an XML response, so let's see what it says:
               echo  "<pre>";
               print_r($result);
                if ($result['returncode'] == 'SUCCESS') {
                    // Then do stuff ...
                    echo "<p>Meeting info was found on the server.</p>";
                }
                else {
                    echo "<p>Failed to get meeting info.</p>";
                }
            }
        }
    }
    //After BBB End Meeting Call This funtions and updated value
    public function endbbbclass()
    {
        $subChapterId=$_REQUEST['itemid'];
        if ($subChapterId && $_SESSION['role'] != 3) {
            $subChapterData = $this->db->query("SELECT * FROM sub_chapters where id = '$subChapterId' ORDER BY 1 DESC LIMIT 1")->result();
            foreach ($subChapterData as $getsubChapterData) {
                $chapterId = $getsubChapterData->chapterid;
            }
            $bbsettingData = $this->db->query("SELECT * FROM bbb_settings where sub_chapter_id = '$subChapterId' ORDER BY 1 DESC LIMIT 1")->result();
            foreach ($bbsettingData as $item) {
                $mettingId = $item->meetingId;
            }
            $now = date('Y-m-d H:i:s');
            $courseId = $_GET['courseid'];
            $subChapterId = $_GET['itemid'];
            $this->db->query("UPDATE sub_chapters SET end_session = 1,updated_date ='$now' WHERE id = '" . $subChapterId . "' ");
            if ($_SESSION['role'] == 1) {
                $this->session->set_flashdata('endmsg', 'class Ended  Successfully .');
                redirect('EditItem?courseid=' . $courseId . '&chapterid=' . $chapterId . '&itemid=' . $subChapterId);
            } else if ($_SESSION['role'] == 2) {
                $this->session->set_flashdata('endmsg', 'iclass Ended  Successfully .');
                redirect('EditItem?courseid=' . $courseId . '&chapterid=' . $chapterId . '&itemid=' . $subChapterId);
            }
        }else{
            $courseId = $_GET['courseid'];
            if ($courseId) {
                $redirectUrl = base_url() . 'CourseLearners?courseid=' . $courseId . '&itemid=' . $subChapterId . '';
                redirect($redirectUrl); // Redirect User to Logout Page to destroy session
            }else{
                echo "SomethingsWrong";
            }
        }
    }
    //call for others info
	public function edititem()
	{
	   
	    $courseId = $_GET['courseid'];
	    $subChapterId = $_GET['itemid'];
	    $this->db->query("UPDATE sub_chapters SET status = 2 WHERE id = '". $subChapterId."' ");
	    if($_SESSION['role'] == 1){
	         $this->session->set_flashdata('delmsg','Deleted Chapter Successfully .');
	        redirect('Coursebuilder?courseid='.$courseId);
	       
	    }else if($_SESSION['role'] == 2){
	         $this->session->set_flashdata('delmsg','Deleted IChapter Successfully .'); 
	        redirect('ICoursesbuilder?courseid='.$courseId);
	    }
	    
	     
	}
	public function editchapters()
	{
	    $courseChapters = $_GET['chapterid'];
	    $courseId = $_GET['courseid'];
	    $this->db->query("UPDATE sub_chapters SET status = 2 WHERE chapterid = '". $courseChapters."' ");
	    $this->db->query("UPDATE chapters SET status = 2 WHERE id ='". $courseChapters."'");
	    if($_SESSION['role'] == 1){
	         $this->session->set_flashdata('delmsg','Deleted Chapter Successfully .');
	        redirect('Coursebuilder?courseid='.$courseId);
	       
	    }else if($_SESSION['role'] == 2){
	        $this->session->set_flashdata('delmsg','Deleted IChapter Successfully .');
	        redirect('ICoursesbuilder?courseid='.$courseId);
	    }
	    
	}
	public function endclass()
	{
	   
	    $subChapterData = $this->db->query('SELECT * FROM sub_chapters')->result();
	   foreach($subChapterData as $getsubChapterData){
	       $chapterId=$getsubChapterData->chapterid;
	   }
	    $courseId = $_GET['courseid'];
	    $subChapterId = $_GET['itemid'];
	    $this->db->query("UPDATE sub_chapters SET end_session = 1 WHERE id = '". $subChapterId."' ");
	    if($_SESSION['role'] == 1){
	         $this->session->set_flashdata('endmsg','class Ended  Successfully .');
	        redirect('EditItem?courseid='.$courseId.'&chapterid='.$chapterId.'&itemid='.$subChapterId);
	       
	    }else if($_SESSION['role'] == 2){
	         $this->session->set_flashdata('endmsg','iclass Ended  Successfully .'); 
	        redirect('EditItem?courseid='.$courseId.'&chapterid='.$chapterId.'&itemid='.$subChapterId);
	    }
	    
	     
	}
	public function index()
	{
	     if(($_SESSION['role'] == 1 ) && $_SESSION['status'] == 1 ){
		    $this->load->view('admin/course/edititem');
	     }
	      else if(($_SESSION['role'] == 2) && $_SESSION['status'] == 1 ){
		    $this->load->view('instructor/course/edititem');
	     }
	     else{
	         redirect('Login');
	     }
	}

	
}
