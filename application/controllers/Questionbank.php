<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionbank extends CI_Controller {
    function __construct()
{
parent::__construct();
$this->load->model('Newchapter_model');
$this->load->helper(array('form', 'url'));
$this->load->library('form_validation');
}
	public function index()
	{
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
	        $this->load->view('admin/questionbank');
	    }else{
	        redirect('Login');
	    }
		
	}
	public function addnewquestions(){
   
$courseid = $this->security->xss_clean($this->input->post('courseid'));

$liveTestid = $this->security->xss_clean($this->input->post('liveTestid'));
$type  = $this->security->xss_clean($this->input->post('type'));
$subjectname = $this->security->xss_clean($this->input->post('subjectname'));
$Topic   = $this->security->xss_clean($this->input->post('Topic'));
$QuestionText = $this->security->xss_clean($this->input->post('QuestionText'));

$Explaination = $this->security->xss_clean($this->input->post('explaination'));




$Order = $this->security->xss_clean($this->input->post('Order'));
$Marks= $this->security->xss_clean($this->input->post('Marks'));
$Penalty = $this->security->xss_clean($this->input->post('Penalty'));

$now = date('Y-m-d H:i:s');

$insertquestionsData = array('live_test_id'=>$liveTestid,
'type' => $type,
'subject'=>$subjectname,
'topic'=>$Topic,
'question_text'=>$QuestionText,
'explaination' => $Explaination,
'order'=>$Order,
'mark'=>$Marks,
'penalty'=>$Penalty,
'created_date'=>$now,
'created_by'=>$_SESSION['user_id'],
'updated_date'=>$now,
'updated_by'=>$_SESSION['user_id'],
'status'=>1,
);

$insertNewquestions=$this->Newchapter_model->insertnewquestions($insertquestionsData);
if($insertNewquestions)
{
    //after question has been inserted successfully getting if it and inserting its options
    $CreatedBy = $_SESSION['user_id'];
    $newQuestionID = $this->db->query("SELECT id FROM question_bank WHERE status = 1 AND created_by = '$CreatedBy' ORDER BY 1 DESC LIMIT 1")->result();
        foreach($newQuestionID as $getnewQuestionID){
        $QuestionID = $getnewQuestionID->id;
        }


        for($i=1;$i<=8;$i++){
        	    if(!empty($this->input->post('optionText'.$i))){
        	        $insertOptions = array('qsn_bank_id' => $QuestionID
                    ,'option_no' => $i
                    ,'option_text' => $this->input->post('optionText'.$i)
                    ,'is_answer' => $this->input->post('option'.$i) 
                    ,'status' => 1 //byDefault Active
                    ,'created_date'=>$now
                    ,'created_by'=>$_SESSION['user_id']
                    ,'updated_date'=>$now
                    ,'updated_by'=>$_SESSION['user_id']
                    );
                $this->db->insert('question_bank_options', $insertOptions);
        	    }  
        	}
            
     
$this->session->set_flashdata('msg', 'Question Added Successfully.');
redirect('Questionbank');


}


else 
{
$this->session->set_flashdata('amsg','Unable to Added question. Please try again.');
redirect('Questionbank');
}
}
public function deletequestion()
	{
	    
	    $questionID = $_GET['questionid'];
	    
	     $this->db->query("UPDATE question_bank_options SET status = 2 WHERE qsn_bank_id = $questionID");
	    $this->db->query("UPDATE question_bank SET status = 2 WHERE id = $questionID ");
	   
	     $this->session->set_flashdata('delmsg','Deleted Question Successfully .');
	     redirect('Questionbank');
         
	     
	}

    public function editquiztestquestions(){

        $quizTestid = $this->security->xss_clean($this->input->post('quizTestid'));
        $type  = $this->security->xss_clean($this->input->post('type'));

        $subjectname = $this->security->xss_clean($this->input->post('subjectname'));
        $Topic   = $this->security->xss_clean($this->input->post('Topic'));
        $QuestionText = $this->security->xss_clean($this->input->post('QuestionText'));

        $Explaination = $this->security->xss_clean($this->input->post('explaination'));




        $Order = $this->security->xss_clean($this->input->post('Order'));
        $Marks= $this->security->xss_clean($this->input->post('Marks'));
        $Penalty = $this->security->xss_clean($this->input->post('Penalty'));

        $newQuestionId = $this->security->xss_clean($this->input->post('newQuestionId'));

        $now = date('Y-m-d H:i:s');

        $QuestionTextlang = $this->security->xss_clean($this->input->post('QuestionTextLang'));
        $ExplainationLang = $this->security->xss_clean($this->input->post('explainationLang'));

        $updateQuestionData = array('live_test_id'=>$quizTestid,
            'type' => $type,
            'subject'=>$subjectname,
            'topic'=>$Topic,
            'question_text'=>$QuestionText,
            'question_text_lang'=>$QuestionTextlang,
            'explaination' => $Explaination,
            'explaination_lang' => $ExplainationLang,
            'order'=>$Order,
            'mark'=>$Marks,
            'penalty'=>$Penalty,
            'created_date'=>$now,
            'created_by'=>$_SESSION['user_id'],
            'updated_date'=>$now,
            'updated_by'=>$_SESSION['user_id']
        );


        $updateQuestionsId=$this->Newchapter_model->updatequiztestquestions($updateQuestionData,$newQuestionId);

        if ($updateQuestionsId){

            for($i=1;$i<=8;$i++){
                if(!empty($this->input->post('optionText'.$i))){
                    $updateQuestionOpitonId= $this->input->post('questionoptionId'.$i);
                    $updateQuestionsOptionns = array('qsn_bank_id' => $newQuestionId
                    ,'option_no' => $i
                    ,'option_text' => $this->input->post('optionText'.$i)
                    ,'is_answer' => $this->input->post('option'.$i)
                    ,'status' => 1 //byDefault Active
                    ,'created_date'=>$now
                    ,'created_by'=>$_SESSION['user_id']
                    ,'updated_date'=>$now
                    ,'updated_by'=>$_SESSION['user_id']
                    );

                    $this->db->where('id', $updateQuestionOpitonId);
                    $this->db->update('question_bank_options', $updateQuestionsOptionns);
                }
            }
        }

        $this->session->set_flashdata('msg', 'Question Added Successfully.');
        redirect('Questionbank');

    }

    public function importquestions(){
        if(!empty($_FILES["file_upload"]["name"])) {
            $file_data = fopen($_FILES["file_upload"]["tmp_name"], 'r');
            fgetcsv($file_data);
            $incree = 0;
            while ($row = fgetcsv($file_data)) {

                    $serialId           =$this->security->xss_clean($row[0]);
                    $subject            =$this->security->xss_clean($row[1]);
                    $topics             =$this->security->xss_clean($row[2]);
                    $tags               =$this->security->xss_clean($row[3]);
                    $questionsType      =$this->security->xss_clean($row[4]);
                    $questionsText      =$this->security->xss_clean($row[5]);

                    $options1           =$this->security->xss_clean($row[6]);
                    $options2           =$this->security->xss_clean($row[7]);
                    $options3           =$this->security->xss_clean($row[8]);
                    $options4           =$this->security->xss_clean($row[9]);

                    $isAnswer           = $this->security->xss_clean($row[10]);
                    $explanations       = $this->security->xss_clean($row[11]);
                    $postvMark          = $this->security->xss_clean($row[12]);
                    $negtvMark          = $this->security->xss_clean($row[13]);

                    $now = date('Y-m-d H:i:s');
                    $insertquestionsData = array('live_test_id'=>0,
                        'type' => $questionsType,
                        'subject'=>$subject,
                        'topic'=>$topics,
                        'question_text'=>$questionsText,
                        'explaination' => $explanations,
                        'order'=>'',
                        'mark'=>$postvMark,
                        'penalty'=>$negtvMark,
                        'created_date'=>$now,
                        'created_by'=>$_SESSION['user_id'],
                        'updated_date'=>$now,
                        'updated_by'=>$_SESSION['user_id'],
                        'status'=>1,
                    );

                $optiosnList=array(
                    '1'=>$options1,
                    '2'=>$options2,
                    '3'=>$options3,
                    '4'=>$options4,
                );


                    $insertNewquestions=$this->Newchapter_model->insertnewquestions($insertquestionsData);

                if($insertNewquestions)
                {
                    //after question has been inserted successfully getting if it and inserting its options
                    $CreatedBy = $_SESSION['user_id'];
                    $newQuestionID = $this->db->query("SELECT id FROM question_bank WHERE status = 1 AND created_by = '$CreatedBy' ORDER BY 1 DESC LIMIT 1")->result();
                    foreach($newQuestionID as $getnewQuestionID){
                        $QuestionID = $getnewQuestionID->id;
                    }

                    foreach ($optiosnList as $key=>$item){
                        $isAnserValue='';
                        if ($key==$isAnswer){
                            $isAnserValue=1;
                        }

                        $insertOptions = array('qsn_bank_id' =>$QuestionID
                        ,'option_no' => $key
                        ,'option_text' =>$item
                        ,'is_answer' =>$isAnserValue
                        ,'status' => 1 //byDefault Active
                        ,'created_date'=>$now
                        ,'created_by'=>$_SESSION['user_id']
                        ,'updated_date'=>$now
                        ,'updated_by'=>$_SESSION['user_id']
                        );

                        $this->db->insert('question_bank_options', $insertOptions);
                    }
                }

            }

            $this->session->set_flashdata('msg', 'Question Added Successfully.');
            redirect('Questionbank');
        }
    }
}
