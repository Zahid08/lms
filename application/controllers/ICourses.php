<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ICourses extends CI_Controller {
    
	public function index()
	{
	    if($_SESSION['role'] == 2 && $_SESSION['status'] == 1){
		    $this->load->view('instructor/icourses');
	    }else{
	        redirect('Login');
	    }
	}
}
