<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lquizquestions extends CI_Controller {
    function __construct()
{
parent::__construct();
$this->load->model('Newquiztest_model');
$this->load->helper(array('form', 'url'));
$this->load->library('form_validation');
}
	public function index()
	{
	    if($_SESSION['role'] == 3 && $_SESSION['status'] == 1){
	        $this->load->view('learner/quizquestions');
	    }else{
	        redirect('Login');
	    }
		
	}
}