<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LiveTest extends CI_Controller {
    function __construct()
{
parent::__construct();
$this->load->model('Newchapter_model');
$this->load->helper(array('form', 'url'));
$this->load->library('form_validation');
}
	public function index()
	{
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
	        $this->load->view('admin/livetest');
	    }else{
	        redirect('Login');
	    }
		
	}

	public function generatedResult(){

        date_default_timezone_set("Asia/Calcutta");

       $quizTestId=$_POST['quizTestId'];
       $currentDateTime=date("Y-m-d h:i:s");
       $quiztest = $this->db->query("SELECT * FROM quiz_test WHERE  id='$quizTestId'")->row();
       if ($quiztest){

           $availableFromDate=$quiztest->available_from;
           $availableTill=$quiztest->available_till;

           $available_from  = date('Y-m-d h:i:s', strtotime($availableFromDate ));
           $available_till  = date('Y-m-d h:i:s', strtotime($availableTill ));

           if ($currentDateTime>=$available_from&& $currentDateTime<$available_till){
                echo 2;
                exit();
           }else{
               $this->db->query("UPDATE quiz_test_attempt SET mark_complete_status =1 WHERE quiztest_id = '". $quizTestId."' ");
               echo 1;
               exit();
           }
       }
    }
}