<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CourseLearners extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('Bigbluebutton_model');
        $this->load->library('form_validation');
    }
    public function index()
    {
        if($_SESSION['role'] == 3 && $_SESSION['status'] == 1){
            $runningStatus=0;
            $itemId   = $_REQUEST['itemid'];
            $courseid   = $_REQUEST['courseid'];
            $chapterItems_1 = $this->db->query("SELECT * FROM sub_chapters WHERE status = 1  AND course_id = '$courseid'  AND id = '$itemId' ORDER BY 1 ASC")->result();


            $bbsettingData = $this->db->query("SELECT * FROM bbb_settings where sub_chapter_id = '$itemId' ORDER BY 1 DESC LIMIT 1")->result();

            foreach ($bbsettingData as $item) {
                $mettingId = $item->meetingId;
            }

            foreach($chapterItems_1 as $getchapterItems_1){
                $subChaptersubType = $getchapterItems_1->sub_type;
            }

            if ($subChaptersubType=='bbb') {
                //Check That Meeitng Are Running Get the URL to join meeting:
                $meetingId = $mettingId;
                $meetingRunningCheckStatus = true;
                try {$resultMeetingRunningCheck = $this->Bigbluebutton_model->isMeetingRunningWithXmlResponseArray($meetingId);}
                catch (Exception $e) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    $meetingRunningCheckStatus = false;
                }

                if ($meetingRunningCheckStatus == true) {
                    if ($resultMeetingRunningCheck['running']=='true'){
                        $runningStatus=1;
                    }else{
                        $runningStatus=0;
                    }
                }
            }

            $runningStatusData['runningStatusData'] =$runningStatus;

            $this->load->view('learner/coursedetails',$runningStatusData);
        }else{
            redirect('Login');
        }
    }

    public function quiz()
    {
        if($_SESSION['role'] == 3 && $_SESSION['status'] == 1){
            $this->load->view('learner/quizoptions');
        }else{
            redirect('Login');
        }
    }

    public function livequiz()
    {
        if($_SESSION['role'] == 3 && $_SESSION['status'] == 1){
            $this->load->view('learner/quizoptionslive');
        }else{
            redirect('Login');
        }
    }


    public function attemptSubjective()
    {
        if($_SESSION['role'] == 3 && $_SESSION['status'] == 1){
            $this->load->view('learner/subjectiveSuccesMessage');
        }else{
            redirect('Login');
        }
    }


    public function quizstart()
    {
        if($_SESSION['role'] == 3 && $_SESSION['status'] == 1){
            $quizId=$_REQUEST['id'];
            $getQuizDetails = $this->db->query("SELECT * FROM quiz_test WHERE status = 1 AND id = '$quizId'")->row();
            if ($getQuizDetails){

                date_default_timezone_set("Asia/Calcutta");

                $availableFromDate=$getQuizDetails->available_from;
                $availableTill=$getQuizDetails->available_till;
                $currentDateTime=date("Y-m-d h:i:s");
                $available_from  = date('Y-m-d h:i:s', strtotime($availableFromDate ));
                $available_till  = date('Y-m-d h:i:s', strtotime($availableTill ));

                if ($currentDateTime>=$available_from && $currentDateTime<=$available_till){
                    $this->load->view('learner/quizstart');
                }else{
                    $this->session->set_flashdata('cm_msg', 'Quiz attempt time over.Please contact with your teacher. Thanks !'); //set success msg if txn successfully
                    if ($getQuizDetails->quiz_type==1){ //live
                        redirect('CourseLearners/livequiz?courseid='.$_REQUEST['courseid'].'&id='.$_REQUEST['id']);
                    }else{
                        redirect('CourseLearners/quiz?courseid='.$_REQUEST['courseid'].'&id='.$_REQUEST['id']);
                    }

                }
            }else{
                redirect('Login');
            }
        }else{
            redirect('Login');
        }
    }

    public function Checkmettingrunning()
    {
        $runningStatus=0;

        $itemId   = $_REQUEST['itemId'];
        $courseid   = $_REQUEST['courseId'];

        $chapterItems_1 = $this->db->query("SELECT * FROM sub_chapters WHERE status = 1  AND course_id = '$courseid'  AND id = '$itemId' ORDER BY 1 ASC")->result();
        foreach($chapterItems_1 as $getchapterItems_1){
            $subChaptersubType = $getchapterItems_1->sub_type;
        }

        $bbsettingData = $this->db->query("SELECT * FROM bbb_settings where sub_chapter_id = '$itemId' ORDER BY 1 DESC LIMIT 1")->result();

        foreach ($bbsettingData as $item) {
            $mettingId = $item->meetingId;
        }


        if ($subChaptersubType=='bbb') {
            //Check That Meeitng Are Running Get the URL to join meeting:
            $meetingId = $mettingId;
            $meetingRunningCheckStatus = true;
            try {$resultMeetingRunningCheck = $this->Bigbluebutton_model->isMeetingRunningWithXmlResponseArray($meetingId);}
            catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                $meetingRunningCheckStatus = false;
            }

            if ($meetingRunningCheckStatus == true) {
                if ($resultMeetingRunningCheck['running']=='true'){
                    $runningStatus=1;
                }else{
                    $runningStatus=0;
                }
            }
        }

        print_r($runningStatus);
        exit();
    }

    public function searchAttempt(){
        if($_SESSION['role'] == 3 && $_SESSION['status'] == 1){
            redirect('CourseLearners/attemptdashboard?courseid='.$_REQUEST['courseId'].'&id='.$_REQUEST['quiztestId'].'&attemptId='.$_REQUEST['attemptSelected']);
        }else{
            redirect('Login');
        }
    }

    public function attemptdashboard(){
        if($_SESSION['role'] == 3 && $_SESSION['status'] == 1){
            $attemptCheck=$this->db->query("SELECT * FROM quiz_test_attempt WHERE quiztest_id = ".$_REQUEST['id']."   AND mark_complete_status =1")->result();
            $markCOmpleted=0;
            if (count($attemptCheck)>0){
                $markCOmpleted=1;
            }
            if ($markCOmpleted==1) {
                $this->load->view('learner/dashboardPanel');
            }else{
                redirect('CourseLearners/attemptMessage?courseid='.$_REQUEST['courseid'].'&id='.$_REQUEST['id']);
            }
        }else{
            redirect('Login');
        }
    }

    public function attemptMessage()
    {
        if($_SESSION['role'] == 3 && $_SESSION['status'] == 1){
            $this->load->view('learner/attemptMessage');
        }else{
            redirect('Login');
        }
    }

    public function removedSubjectiveImage(){
        $questionOptionsId            =$_REQUEST['removedOptionsId'];
        if ($questionOptionsId) {

            $updateDataAllNull = array(
                'answer_option_file' => '',
            );

            $this->db->where('id', $questionOptionsId);
            $this->db->update('quiz_test_question_options', $updateDataAllNull);

            echo 1;
            die();
        }
    }

    public function uploadSubjectiveQuestion(){
        $quizOptionId            =$_REQUEST['questionID'];

        if ($quizOptionId) {
            /*=============uploadimg=============*/
            $config['upload_path'] = './assets/subjective/';
            $config['allowed_types'] = 'jpg|png|jpeg|gif|pdf';
            $config['max_size'] = 20000000000000000000;
            $config['max_width'] = 15000000000000000000000000;
            $config['max_height'] = 1500000000000000000000000;


            $this->load->library('upload', $config);
            $field_name = 'file';
            $this->upload->do_upload($field_name);

            $upload_data = $this->upload->data();
            $data['file_name'] = $upload_data['file_name'];
            /*=============uploadimg=============*/

            $updateDataAllNull = array(
                'answer_option_file'=>$data['file_name'],
            );

            $this->db->where('id', $quizOptionId);
            $this->db->update('quiz_test_question_options', $updateDataAllNull);

            if ($data['file_name']){
                echo $data['file_name'];
                exit();
            }else{
                echo "0";
                exit();
            }
        }
    }

    public function updateQuizTotalAttemptTime(){

        $totalTimeTaken            =$_REQUEST['totalTimeTaken'];
        $quizTestId                =$_REQUEST['quizTestId'];
        $quiztest = $this->db->query("SELECT * FROM quiz_test WHERE status = 1 AND id=$quizTestId")->row();

        $quizTestQuestion = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizTestId." ")->result();

        $answarStatus=1;
        $answarQuestions               = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizTestId." AND answar_status = ".$answarStatus." ")->result();

        $totalQuestions                = count($quizTestQuestion);
        $totalAnswar                   = count($answarQuestions);
        $notAnswar                     = $totalQuestions-$totalAnswar;

        $checkSubjectiveHasOrNot=$this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizTestId."   AND type =4")->result();

        $markCOmpleted=1;
        if (count($checkSubjectiveHasOrNot)>0 || $quiztest->quiz_type==1){
            $markCOmpleted=0;
        }

        $insertQuizAttemptData = array(
            'quiztest_id'               =>$quizTestId,
            'user_id'                   =>$_SESSION['user_id'],
            'mark_total'                =>'',
            'take_attempt_time'         =>$totalTimeTaken,
            'platform'                  =>'web',
            'total_question'            =>$totalQuestions,
            'answar_question'           =>$totalAnswar,
            'mark_complete_status'           =>$markCOmpleted,
        );

        $this->db->insert('quiz_test_attempt', $insertQuizAttemptData);

        $insert_id = $this->db->insert_id();  //Last inserted ID

        if ($insert_id){
            foreach ($quizTestQuestion as $key=>$questionitem){
                $selectedAnswar=1;
                $studentGivenAnswerText=$correctAnserText=$studentAttachFile='';

                if ($questionitem->type==3 || $questionitem->type==4){
                    $getOptionsId = $this->db->query("SELECT id FROM quiz_test_question_options WHERE quiz_test_question_id = " . $questionitem->id . "  ")->result();
                    $coorectOptionsId = $this->db->query("SELECT id FROM quiz_test_question_options WHERE quiz_test_question_id = " . $questionitem->id . "  ")->result();

                    $answerText = $this->db->query("SELECT * FROM quiz_test_question_options WHERE quiz_test_question_id = " . $questionitem->id . "  ")->result();
                    $studentGivenAnswerText=$answerText[0]->answer_option_text;
                    $studentAttachFile=$answerText[0]->answer_option_file;
                    $correctAnserText=$answerText[0]->option_text;

                }else {
                    $getOptionsId = $this->db->query("SELECT id FROM quiz_test_question_options WHERE quiz_test_question_id = " . $questionitem->id . " AND selected_answer = " . $selectedAnswar . " ")->result();
                    $coorectOptionsId = $this->db->query("SELECT id FROM quiz_test_question_options WHERE quiz_test_question_id = " . $questionitem->id . " AND is_answer = " . $selectedAnswar . " ")->result();
                }

                $dataOptionsIdArray=array();
                if (!empty($getOptionsId)) {
                    foreach ($getOptionsId as $key => $itemId) {
                        array_push($dataOptionsIdArray, $itemId->id);
                    }
                }

                $dataOptionsCorrectIdArray=array();
                if (!empty($coorectOptionsId)) {
                    foreach ($coorectOptionsId as $key => $coorectOptionsIdItem) {
                        array_push($dataOptionsCorrectIdArray, $coorectOptionsIdItem->id);
                    }
                }

                if (count($dataOptionsIdArray)>0){

                    //Save Attempt Result
                    $insertQuizAttemptResultData = array(
                        'attempt_id'                =>$insert_id,
                        'quiz_test_id'              =>$quizTestId,
                        'question_type'              =>$questionitem->type,
                        'question_id'               =>$questionitem->id,
                        'chose_option_id'           =>serialize($dataOptionsIdArray),
                        'correct_chose_option_id'   =>serialize($dataOptionsCorrectIdArray),
                        'chose_option_text'         =>$studentGivenAnswerText,
                        'correct_option_text'       =>$correctAnserText,
                        'chose_option_file'         =>$studentAttachFile,
                        'user_id'                   =>$_SESSION['user_id'],
                    );

                    $this->db->insert('quiz_test_attempt_result', $insertQuizAttemptResultData);

                    $insert_id_attempt_result = $this->db->insert_id();  //Last inserted ID

                    if ($insert_id_attempt_result){

                        //Update and empty value set on questions and chose options
                        $updateQuestionSelectedAnswarOptions = array(
                            'selected_answer'=>'',
                            'answer_option_text'=>'',
                            'answer_option_file'=>'',
                        );

                        $this->db->where('quiz_test_question_id', $questionitem->id);
                        $this->db->update('quiz_test_question_options', $updateQuestionSelectedAnswarOptions);


                        $selectedAnswarStatus = array(
                            'answar_status'=>'',
                        );

                        $this->db->where('id', $questionitem->id);
                        $this->db->update('quiz_test_questions', $selectedAnswarStatus);
                    }
                }
            }

            $updatedValue=$this->markCalcualtions($insert_id,$quizTestId);

            $getMark=$updatedValue['get_mark'];
            $correct=$updatedValue['correct'];
            $incorect=$updatedValue['incorect'];
            $mark_total=$updatedValue['mark_total'];
            $skipped=$updatedValue['skipped'];

            $this->db->query("UPDATE quiz_test_attempt SET get_mark =$getMark,correct =$correct,incorect =$incorect,mark_total =$mark_total,skipped =$skipped WHERE id = '". $insert_id."' ");

            if ($markCOmpleted==0){
                echo 2;
            }else {
                echo 1;
                exit();
            }
        }
    }

    public function markCalcualtions($attemptId,$quizTestId){

        $newQuestions           = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizTestId." ORDER BY id DESC ")->result();
        $totalQuestions         =count($newQuestions);
        $countCorrectAnswar     =0;
        $totalAttemptAnswar     =0;

        $countTotalCorrectAnswarMark=0;
        $countPenalityMark=0;
        $totalQuestionsMark=0;
        foreach ($newQuestions as $key=>$getnewQuestions){
            $totalQuestionsMark=$totalQuestionsMark+$getnewQuestions->mark;

            $attemptResult =$this->db->query("SELECT * FROM quiz_test_attempt_result WHERE question_id = ".$getnewQuestions->id." AND attempt_id = ".$attemptId." ")->row();

            if ($attemptResult){

                $found='';

                if ($getnewQuestions->type==3 || $getnewQuestions->type==4){
                    $choseOptionsText = $attemptResult->chose_option_text;
                    $correctOptionsText =$attemptResult->correct_option_text;

                    if ($choseOptionsText==$correctOptionsText){
                        $found = true;
                    }

                }else {
                    if ($attemptResult->chose_option_id==$attemptResult->correct_chose_option_id){
                        $found = true;
                    }else{
                        $found = false;
                    }
                }

                if ($found==true){
                    $countCorrectAnswar++;
                    $countTotalCorrectAnswarMark=$countTotalCorrectAnswarMark+$getnewQuestions->mark;
                }else{
                    $countPenalityMark=$countPenalityMark+$countTotalCorrectAnswarMark+$getnewQuestions->penalty;
                }

                $totalAttemptAnswar++;
            }
        }

        $inCorrectAnswar=$totalAttemptAnswar-$countCorrectAnswar;
        $skippedQuestions=$totalQuestions-$totalAttemptAnswar;

        $markeAnswarTotal=$countTotalCorrectAnswarMark-$countPenalityMark;

        $dataList=array(
            'mark_total'            =>$totalQuestionsMark,
            'incorect'              =>$inCorrectAnswar,
            'skipped'               =>$skippedQuestions,
            'correct'               =>$countCorrectAnswar,
            'get_mark'              =>$markeAnswarTotal,
        );

        return $dataList;
    }


    public function removedAnswar(){

        $questionID            =$_REQUEST['questionID'];
        $quizTestId            =$_REQUEST['quizTestId'];

        $updateDataAllNull = array(
            'selected_answer'=>'',
        );

        $this->db->where('quiz_test_question_id', $questionID);
        $this->db->update('quiz_test_question_options', $updateDataAllNull);


        $updateAnswarStatus = array(
            'answar_status'=>'',
        );

        $this->db->where('id', $questionID);
        $this->db->update('quiz_test_questions', $updateAnswarStatus);


        $newQuestions = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizTestId." ")->result();

        $answarStatus=1;
        $answarQuestions               = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizTestId." AND answar_status = ".$answarStatus." ")->result();

        $totalQuestions                = count($newQuestions);
        $totalAnswar                   = count($answarQuestions);
        $notAnswar                      =$totalQuestions-$totalAnswar;

        $dataArray=array(
            'answar'=>$totalAnswar,
            'notAnswar'=>$notAnswar,
        );

        print_r(json_encode($dataArray));
        exit();
    }


    public function givenAnswar(){

        $OptionsSelectedId     =$_REQUEST['OptionsSelectedId'];
        $questionID            =$_REQUEST['questionID'];
        $quizTestId            =$_REQUEST['quizTestId'];
        $quizTyeId             =$_REQUEST['quizTyeId'];
        $valueOfSubjective     =$_REQUEST['valueOfSubjective'];


        if ($quizTyeId==4 || $quizTyeId==3){
            $updateDataAllNull = array(
                'answer_option_text'=>$valueOfSubjective,
                'selected_answer'=>'1',
            );

            $this->db->where('quiz_test_question_id', $questionID);
            $this->db->update('quiz_test_question_options', $updateDataAllNull);

        }else{
            $updateDataAllNull = array(
                'selected_answer'=>'',
            );

            $this->db->where('quiz_test_question_id', $questionID);
            $this->db->update('quiz_test_question_options', $updateDataAllNull);

            $insertNew = array(
                'selected_answer'=>1,
            );

            if (!empty($OptionsSelectedId)){
                foreach ($OptionsSelectedId as $key=>$itemsId){
                    $this->db->where('id', $itemsId);
                    $this->db->update('quiz_test_question_options', $insertNew);
                }
            }
        }

        //Update Question Answer
        $updateAnswarStatus = array(
            'answar_status'=>1,
        );

        $this->db->where('id', $questionID);
        $this->db->update('quiz_test_questions', $updateAnswarStatus);


        $newQuestions = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizTestId." ")->result();

        $answarStatus=1;
        $answarQuestions               = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizTestId." AND answar_status = ".$answarStatus." ")->result();

        $totalQuestions                = count($newQuestions);
        $totalAnswar                   = count($answarQuestions);
        $notAnswar                      =$totalQuestions-$totalAnswar;

        $dataArray=array(
            'answar'=>$totalAnswar,
            'notAnswar'=>$notAnswar,
        );

        print_r(json_encode($dataArray));
        exit();
    }

    function allquestion($quizTestId,$checkQuestionNumber){

        $newQuestions = $this->db->query("SELECT * FROM quiz_test_questions WHERE status = 1 AND quiz_test_id = ".$quizTestId."  ORDER BY id")->result();
        $count=1;
        $insertPositions='';
        foreach ($newQuestions as $key => $items) {
            if ($items->id==$checkQuestionNumber){
                $insertPositions=$count;
                break;
            }
            $count++;
        }

        return $insertPositions;
    }

    public function singleQuizQuestions(){

        $questionId         = $this->security->xss_clean($_REQUEST['questionId']);
        $quizTestId         = $this->security->xss_clean($_REQUEST['quizTestId']);
        $courseId           = $this->security->xss_clean($_REQUEST['courseId']);

        $singleQuestionsData = $this->db->query("SELECT * FROM quiz_test_questions WHERE id = ".$questionId."")->row();
        $dataList=array();

        if ($singleQuestionsData){
            $questionNumberPositions=$this->allquestion($quizTestId,$singleQuestionsData->id);
            $questionText=$singleQuestionsData->question_text;
            if ($singleQuestionsData){
                $nextQUestionOptions = $this->db->query("SELECT * FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$singleQuestionsData->id." ")->result();
                $html='';

                if ($singleQuestionsData->type==1 || $singleQuestionsData->type==2) {

                    $type = 'checkbox';
                    if ($singleQuestionsData->type == 1) {
                        $type = 'radio';
                    }

                    $f = 'A';
                    foreach ($nextQUestionOptions as $key => $items) {
                        $checked = '';
                        if ($items->selected_answer == 1) {
                            $checked = "checked";
                        }
                        $html .= '<label class="label-container">' . $items->option_text . '
                        <input type="'.$type.'"  name="radio" data-options-id="' . $items->id . '" value="' . $items->id . '" ' . $checked . '>
                        <span class="checkmark">' . $f . '</span>
                    </label>';
                        $f++;
                    }
                }else if ($singleQuestionsData->type==4){
                    $answerValue='';
                    if ($nextQUestionOptions && $nextQUestionOptions[0]->selected_answer==1){
                        $answerValue=$nextQUestionOptions[0]->answer_option_text;
                    }

                    $uploadOptionsId=$nextQUestionOptions[0]->id;
                    $html .='<textarea type="text" name="answarQuestionSubjective" rows="6" id="answarQuestionSubjective" placeholder="Write Answer" class="form-control max-textarea" required>'.$answerValue.'</textarea><p id="errorSubjective" style="color: red"></p>';
                    $baseUrl=base_url();
                    $html .="<div class=\"subjectiveForm\" style=\"margin-top: 0px;margin-bottom: 25px;\">
                            <form class=\"uploadSubjectiveForm mt-2 p-0\" data-id=\"610154140cf2d77bece15465\" action='$baseUrl/CourseLearners/uploadSubjectiveQuestion' method='POST' enctype='multipart/form-data'>";
                    if ($nextQUestionOptions[0]->answer_option_file){
                        $anserFileName=$nextQUestionOptions[0]->answer_option_file;
                        $optionId=$nextQUestionOptions[0]->id;
                        $html .="<div class='image-area'>
                                         <img src='$baseUrl/assets/subjective/$anserFileName' alt='Preview'>
                                         <a class='remove-image' href=\"#\" style=\"display: inline;\" data-quiz-option-id='$optionId'>×</a>
                                     </div>";
                    }else{
                        $html .="<h5>Upload <small class=\"text-gray\">You can upload maximum 100 MB of file size.</small></h5>
                                             <div class='col-8 col-sm-12'>
                                                 <div class='input-group'>
                                                     <input class='form-input' type=\"file\" name=\"subjectveFile\" id=\"fileUpload\" required>
                                                     <input class='form-input' type='hidden' id='dataOptionsId' value='$uploadOptionsId'>
                                                     <button type=\"submit\" class=\"btn btn-outline-primary\">Upload</button>
                                                 </div>
                                             </div>";
                    }
                    $html .="</form></div>";

                }elseif ($singleQuestionsData->type==3){
                    $answerValue='';
                    if ($nextQUestionOptions && $nextQUestionOptions[0]->selected_answer==1){
                        $answerValue=$nextQUestionOptions[0]->answer_option_text;
                    }
                    $questionText = $singleQuestionsData->question_text . '<input type="text" style="margin-top: 25px;" id="answerQuestionsFillIntheBlank" name="answerQuestionsFillIntheBlank" placeholder="Give Answer" class="form-control max-textarea" value="'.$answerValue.'" />';
                }

                $secondNextQuestions =$this->db->query("SELECT * FROM quiz_test_questions WHERE id > ".$singleQuestionsData->id." AND status = 1 AND quiz_test_id = ".$quizTestId."")->row();
                $nextId='';
                if ($secondNextQuestions){
                    $nextId=$secondNextQuestions->id;
                }

                $optionsMessage='';
                if ($singleQuestionsData->type!=3) { //Fill In The Blank
                    $optionsMessage = 'Select the correct answer given below.';
                    if ($singleQuestionsData->type == 4) {
                        $optionsMessage = 'Write your answer given below textBox';
                    }
                }

                $previousNextQUestions = $this->db->query("select * from quiz_test_questions where id = (select max(id) from quiz_test_questions where id < $singleQuestionsData->id) AND status = 1 AND quiz_test_id = ".$quizTestId."")->row();
                $previousId='';
                if ($previousNextQUestions){
                    $previousId=$previousNextQUestions->id;
                }

                $dataList=array(
                    'questionText'  =>$questionText,
                    'mark'          =>$singleQuestionsData->mark,
                    'penality'      =>$singleQuestionsData->penalty,
                    'nextId'=>$nextId,
                    'previousId'=>$previousId,
                    'optionsHtml'=>$html,
                    'optionsMessage'=>$optionsMessage,
                    'type'=>$singleQuestionsData->type,
                    'positions'=>$questionNumberPositions,
                    'currentId'=>$singleQuestionsData->id,
                );

                print_r(json_encode($dataList));
                exit();
            }

        }
    }

    public function subjectiveQuiz(){

        $quizTestId         = $this->security->xss_clean($_REQUEST['quizTestId']);
        $subjectName         = $this->security->xss_clean($_REQUEST['subjectName']);
        $subjectiveQuestions = $this->db->query("SELECT * FROM quiz_test_questions WHERE quiz_test_id = ".$quizTestId." AND subject LIKE '".$subjectName."'")->row();
        $dataList=array();
        if ($subjectiveQuestions){
            $questionNumberPositions=$this->allquestion($quizTestId,$subjectiveQuestions->id);
            $questionText=$subjectiveQuestions->question_text;
            if ($subjectiveQuestions){
                $nextQUestionOptions = $this->db->query("SELECT * FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$subjectiveQuestions->id." ")->result();
                $html='';

                if ($subjectiveQuestions->type==1 || $subjectiveQuestions->type==2) {

                    $type = 'checkbox';
                    if ($subjectiveQuestions->type == 1) {
                        $type = 'radio';
                    }

                    $f = 'A';
                    foreach ($nextQUestionOptions as $key => $items) {
                        $checked = '';
                        if ($items->selected_answer == 1) {
                            $checked = "checked";
                        }
                        $html .= '<label class="label-container">' . $items->option_text . '
                        <input type="'.$type.'"  name="radio" data-options-id="' . $items->id . '" value="' . $items->id . '" ' . $checked . '>
                        <span class="checkmark">' . $f . '</span>
                    </label>';
                        $f++;
                    }
                }else if ($subjectiveQuestions->type==4){
                    $answerValue='';
                    if ($nextQUestionOptions && $nextQUestionOptions[0]->selected_answer==1){
                        $answerValue=$nextQUestionOptions[0]->answer_option_text;
                    }

                    $uploadOptionsId=$nextQUestionOptions[0]->id;
                    $html .='<textarea type="text" name="answarQuestionSubjective" rows="6" id="answarQuestionSubjective" placeholder="Write Answer" class="form-control max-textarea" required>'.$answerValue.'</textarea><p id="errorSubjective" style="color: red"></p>';
                    $baseUrl=base_url();
                    $html .="<div class=\"subjectiveForm\" style=\"margin-top: 0px;margin-bottom: 25px;\">
                            <form class=\"uploadSubjectiveForm mt-2 p-0\" data-id=\"610154140cf2d77bece15465\" action='$baseUrl/CourseLearners/uploadSubjectiveQuestion' method='POST' enctype='multipart/form-data'>";
                    if ($nextQUestionOptions[0]->answer_option_file){
                        $anserFileName=$nextQUestionOptions[0]->answer_option_file;
                        $optionId=$nextQUestionOptions[0]->id;
                        $html .="<div class='image-area'>
                                         <img src='$baseUrl/assets/subjective/$anserFileName' alt='Preview'>
                                         <a class='remove-image' href=\"#\" style=\"display: inline;\" data-quiz-option-id='$optionId'>×</a>
                                     </div>";
                    }else{
                        $html .="<h5>Upload <small class=\"text-gray\">You can upload maximum 100 MB of file size.</small></h5>
                                             <div class='col-8 col-sm-12'>
                                                 <div class='input-group'>
                                                     <input class='form-input' type=\"file\" name=\"subjectveFile\" id=\"fileUpload\" required>
                                                     <input class='form-input' type='hidden' id='dataOptionsId' value='$uploadOptionsId'>
                                                     <button type=\"submit\" class=\"btn btn-outline-primary\">Upload</button>
                                                 </div>
                                             </div>";
                    }
                    $html .="</form></div>";

                }elseif ($subjectiveQuestions->type==3){
                    $answerValue='';
                    if ($nextQUestionOptions && $nextQUestionOptions[0]->selected_answer==1){
                        $answerValue=$nextQUestionOptions[0]->answer_option_text;
                    }
                    $questionText = $subjectiveQuestions->question_text . '<input type="text" style="margin-top: 25px;" id="answerQuestionsFillIntheBlank" name="answerQuestionsFillIntheBlank" placeholder="Give Answer" class="form-control max-textarea" value="'.$answerValue.'" />';
                }

                $secondNextQuestions =$this->db->query("SELECT * FROM quiz_test_questions WHERE id > ".$subjectiveQuestions->id." AND status = 1 AND quiz_test_id = ".$quizTestId."")->row();
                $nextId='';
                if ($secondNextQuestions){
                    $nextId=$secondNextQuestions->id;
                }

                $optionsMessage='';
                if ($subjectiveQuestions->type!=3) { //Fill In The Blank
                    $optionsMessage = 'Select the correct answer given below.';
                    if ($subjectiveQuestions->type == 4) {
                        $optionsMessage = 'Write your answer given below textBox';
                    }
                }

                $previousNextQUestions = $this->db->query("select * from quiz_test_questions where id = (select max(id) from quiz_test_questions where id < $subjectiveQuestions->id) AND status = 1 AND quiz_test_id = ".$quizTestId."")->row();
                $previousId='';
                if ($previousNextQUestions){
                    $previousId=$previousNextQUestions->id;
                }

                $dataList=array(
                    'questionText'  =>$questionText,
                    'mark'          =>$subjectiveQuestions->mark,
                    'penality'      =>$subjectiveQuestions->penalty,
                    'nextId'=>$nextId,
                    'previousId'=>$previousId,
                    'optionsHtml'=>$html,
                    'optionsMessage'=>$optionsMessage,
                    'type'=>$subjectiveQuestions->type,
                    'positions'=>$questionNumberPositions,
                    'currentId'=>$subjectiveQuestions->id,
                );

                print_r(json_encode($dataList));
                exit();
            }

        }
    }

    public function nextQuizQuestions(){

        $nextQuestionId     =$_REQUEST['nextQuestionId'];
        $questionNumber     = $this->security->xss_clean($_REQUEST['questionNumber']);
        $quizTestId         = $this->security->xss_clean($_REQUEST['quizTestId']);
        $courseId           = $this->security->xss_clean($_REQUEST['courseId']);

        $dataList=array();
        if ($nextQuestionId) {
            $nextQuestions = $this->db->query("SELECT * FROM quiz_test_questions WHERE  id = " .$nextQuestionId. " ")->row();
            $questionNumberPositions=$this->allquestion($quizTestId,$nextQuestions->id);

            $questionText=$nextQuestions->question_text;
            if ($nextQuestions){
                $nextQUestionOptions = $this->db->query("SELECT * FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$nextQuestions->id." ")->result();
                $html='';

                if ($nextQuestions->type==1 || $nextQuestions->type==2) {

                    $type = 'checkbox';
                    if ($nextQuestions->type == 1) {
                        $type = 'radio';
                    }

                    $f = 'A';
                    foreach ($nextQUestionOptions as $key => $items) {
                        $checked = '';
                        if ($items->selected_answer == 1) {
                            $checked = "checked";
                        }
                        $html .= '<label class="label-container">' . $items->option_text . '
                        <input type="'.$type.'"  name="radio" data-options-id="' . $items->id . '" value="' . $items->id . '" ' . $checked . '>
                        <span class="checkmark">' . $f . '</span>
                    </label>';
                        $f++;
                    }
                }else if ($nextQuestions->type==4){
                    $answerValue='';
                    if ($nextQUestionOptions && $nextQUestionOptions[0]->selected_answer==1){
                        $answerValue=$nextQUestionOptions[0]->answer_option_text;
                    }

                    $uploadOptionsId=$nextQUestionOptions[0]->id;
                    $html .='<textarea type="text" name="answarQuestionSubjective" rows="6" id="answarQuestionSubjective" placeholder="Write Answer" class="form-control max-textarea" required>'.$answerValue.'</textarea><p id="errorSubjective" style="color: red"></p>';
                    $baseUrl=base_url();
                    $html .="<div class=\"subjectiveForm\" style=\"margin-top: 0px;margin-bottom: 25px;\">
                            <form class=\"uploadSubjectiveForm mt-2 p-0\" data-id=\"610154140cf2d77bece15465\" action='$baseUrl/CourseLearners/uploadSubjectiveQuestion' method='POST' enctype='multipart/form-data'>";
                    if ($nextQUestionOptions[0]->answer_option_file){
                        $anserFileName=$nextQUestionOptions[0]->answer_option_file;
                        $optionId=$nextQUestionOptions[0]->id;
                        $html .="<div class='image-area'>
                                         <img src='$baseUrl/assets/subjective/$anserFileName' alt='Preview'>
                                         <a class='remove-image' href=\"#\" style=\"display: inline;\" data-quiz-option-id='$optionId'>×</a>
                                     </div>";
                    }else{
                        $html .="<h5>Upload <small class=\"text-gray\">You can upload maximum 100 MB of file size.</small></h5>
                                             <div class='col-8 col-sm-12'>
                                                 <div class='input-group'>
                                                     <input class='form-input' type=\"file\" name=\"subjectveFile\" id=\"fileUpload\" required>
                                                     <input class='form-input' type='hidden' id='dataOptionsId' value='$uploadOptionsId'>
                                                     <button type=\"submit\" class=\"btn btn-outline-primary\">Upload</button>
                                                 </div>
                                             </div>";
                    }
                    $html .="</form></div>";

                }elseif ($nextQuestions->type==3){
                    $answerValue='';
                    if ($nextQUestionOptions && $nextQUestionOptions[0]->selected_answer==1){
                        $answerValue=$nextQUestionOptions[0]->answer_option_text;
                    }
                    $questionText = $nextQuestions->question_text . '<input type="text" style="margin-top: 25px;" id="answerQuestionsFillIntheBlank" name="answerQuestionsFillIntheBlank" placeholder="Give Answer" class="form-control max-textarea" value="'.$answerValue.'" />';
                }

                $secondNextQuestions =$this->db->query("SELECT * FROM quiz_test_questions WHERE id > ".$nextQuestions->id." AND status = 1 AND quiz_test_id = ".$quizTestId."")->row();
                $nextId='';
                if ($secondNextQuestions){
                    $nextId=$secondNextQuestions->id;
                }

                $optionsMessage='';
                if ($nextQuestions->type!=3) { //Fill In The Blank
                    $optionsMessage = 'Select the correct answer given below.';
                    if ($nextQuestions->type == 4) {
                        $optionsMessage = 'Write your answer given below textBox';
                    }
                }

                $dataList=array(
                    'questionText'  =>$questionText,
                    'mark'          =>$nextQuestions->mark,
                    'penality'      =>$nextQuestions->penalty,
                    'nextId'=>$nextId,
                    'optionsHtml'=>$html,
                    'optionsMessage'=>$optionsMessage,
                    'type'=>$nextQuestions->type,
                    'positions'=>$questionNumberPositions,
                );

                print_r(json_encode($dataList));
                exit();
            }
        }
    }


    public function previousQuizQuestions(){

        $previousQuestionId     =$_REQUEST['previousQuestionId'];
        $quizTestId         = $this->security->xss_clean($_REQUEST['quizTestId']);
        $dataList=array();
        if ($previousQuestionId) {
            $previousQuestion = $this->db->query("SELECT * FROM quiz_test_questions WHERE  id = " .$previousQuestionId. " ")->row();
            $questionNumberPositions=$this->allquestion($quizTestId,$previousQuestion->id);
            $questionText=$previousQuestion->question_text;
            if ($previousQuestion){
                $nextQUestionOptions = $this->db->query("SELECT * FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$previousQuestion->id." ")->result();
                $html='';
                $f = 'A';

                if ($previousQuestion->type==1 || $previousQuestion->type==2) {

                    $type = 'checkbox';
                    if ($previousQuestion->type == 1) {
                        $type = 'radio';
                    }
                    foreach ($nextQUestionOptions as $key => $items) {
                        $checked = '';
                        if ($items->selected_answer == 1) {
                            $checked = "checked";
                        }

                        $html .= '<label class="label-container">' . $items->option_text . '
                         <input type="' . $type . '"  name="radio" data-options-id="' . $items->id . '" value="' . $items->id . '" ' . $checked . '>
                        <span class="checkmark">' . $f . '</span>
                    </label>';
                        $f++;
                    }
                }else if ($previousQuestion->type==4){
                    $answerValue='';
                    if ($nextQUestionOptions && $nextQUestionOptions[0]->selected_answer==1){
                        $answerValue=$nextQUestionOptions[0]->answer_option_text;
                    }

                    $uploadOptionsId=$nextQUestionOptions[0]->id;
                    $html .='<textarea type="text" name="answarQuestionSubjective" rows="6" id="answarQuestionSubjective" placeholder="Write Answer" class="form-control max-textarea" required>'.$answerValue.'</textarea><p id="errorSubjective" style="color: red"></p>';
                    $baseUrl=base_url();
                    $html .="<div class=\"subjectiveForm\" style=\"margin-top: 0px;margin-bottom: 25px;\">
                            <form class=\"uploadSubjectiveForm mt-2 p-0\" data-id=\"610154140cf2d77bece15465\" action='$baseUrl/CourseLearners/uploadSubjectiveQuestion' method='POST' enctype='multipart/form-data'>";
                    if ($nextQUestionOptions[0]->answer_option_file){
                        $anserFileName=$nextQUestionOptions[0]->answer_option_file;
                        $optionId=$nextQUestionOptions[0]->id;
                        $html .="<div class='image-area'>
                                         <img src='$baseUrl/assets/subjective/$anserFileName' alt='Preview'>
                                         <a class='remove-image' href=\"#\" style=\"display: inline;\" data-quiz-option-id='$optionId'>×</a>
                                     </div>";
                    }else{
                        $html .="<h5>Upload <small class=\"text-gray\">You can upload maximum 100 MB of file size.</small></h5>
                                             <div class='col-8 col-sm-12'>
                                                 <div class='input-group'>
                                                     <input class='form-input' type=\"file\" name=\"subjectveFile\" id=\"fileUpload\" required>
                                                     <input class='form-input' type='hidden' id='dataOptionsId' value='$uploadOptionsId'>
                                                     <button type=\"submit\" class=\"btn btn-outline-primary\">Upload</button>
                                                 </div>
                                             </div>";
                    }
                    $html .="</form></div>";

                }elseif ($previousQuestion->type==3){
                    $answerValue='';
                    if ($nextQUestionOptions && $nextQUestionOptions[0]->selected_answer==1){
                        $answerValue=$nextQUestionOptions[0]->answer_option_text;
                    }
                    $questionText = $previousQuestion->question_text . '<input type="text" style="margin-top: 25px;" id="answerQuestionsFillIntheBlank" name="answerQuestionsFillIntheBlank" placeholder="Give Answer" class="form-control max-textarea" value="'.$answerValue.'" />';
                }

                $previousNextQUestions = $this->db->query("select * from quiz_test_questions where id = (select max(id) from quiz_test_questions where id < $previousQuestion->id) AND status = 1 AND quiz_test_id = ".$quizTestId."")->row();
                $previousId='';
                if ($previousNextQUestions){
                    $previousId=$previousNextQUestions->id;
                }
                $optionsMessage='';
                if ($previousQuestion->type!=3) { //Fill In The Blank
                    $optionsMessage = 'Select the correct answer given below.';
                    if ($previousQuestion->type == 4) {
                        $optionsMessage = 'Write your answer given below textBox';
                    }
                }

                $dataList=array(
                    'questionText'=>$questionText,
                    'mark'          =>$previousQuestion->mark,
                    'penality'      =>$previousQuestion->penalty,
                    'previousQuestionId'=>$previousId,
                    'optionsHtml'=>$html,
                    'optionsMessage'=>$optionsMessage,
                    'type'=>$previousQuestion->type,
                    'positions'=>$questionNumberPositions,

                );

                print_r(json_encode($dataList));
                exit();
            }
        }
    }

    public function CurrentQuizQuestions(){

        $currentId     =$_REQUEST['currentId'];
        $LanguageType     =$_REQUEST['LanguageType'];
        $questionNumber     = $this->security->xss_clean($_REQUEST['questionNumber']);
        $quizTestId         = $this->security->xss_clean($_REQUEST['quizTestId']);
        $courseId           = $this->security->xss_clean($_REQUEST['courseId']);

        $dataList=array();
        if ($currentId && !empty($LanguageType)) {
            $nextQuestions = $this->db->query("SELECT * FROM quiz_test_questions WHERE  id = " .$currentId. " ")->row();
            $questionText=$nextQuestions->question_text;
            $questionNumberPositions=$this->allquestion($quizTestId,$nextQuestions->id);
            if ($nextQuestions){

                $nextQUestionOptions = $this->db->query("SELECT * FROM quiz_test_question_options WHERE status = 1 AND quiz_test_question_id = ".$nextQuestions->id." ")->result();
                $html='';
                $f = 'A';

                if ($nextQuestions->type==1 || $nextQuestions->type==2){ //SIngle CHose Multiple CHose

                    $type='checkbox';
                    if ($nextQuestions->type==1){
                        $type='radio';
                    }

                    foreach ($nextQUestionOptions as $key=>$items){
                        $checked='';
                        if ($items->selected_answer==1){
                            $checked= "checked";
                        }

                        $optionsText=$items->option_text;
                        if ($LanguageType==1){
                            $optionsText=$items->option_text_lang;
                        }

                        $html .='<label class="label-container">'.$optionsText.'
                        <input type="'.$type.'"  name="radio" data-options-id="'.$items->id.'" value="'.$items->id.'" '.$checked.'>
                        <span class="checkmark">'.$f.'</span>
                    </label>';
                        $f++;
                    }
                }elseif ($nextQuestions->type==4){ //Subjective
                    $answerValue='';
                    if ($nextQUestionOptions && $nextQUestionOptions[0]->selected_answer==1){
                        $answerValue=$nextQUestionOptions[0]->answer_option_text;
                    }
                    $html .='<textarea type="text" name="answarQuestionSubjective" rows="6" id="answarQuestionSubjective" placeholder="Write Answer" class="form-control max-textarea" required>'.$answerValue.'</textarea>';
                }elseif ($nextQuestions->type==3){

                    $answerValue='';
                    if ($nextQUestionOptions && $nextQUestionOptions[0]->selected_answer==1){
                        $answerValue=$nextQUestionOptions[0]->answer_option_text;
                    }

                    if ($LanguageType==1){
                        $questionText =strip_tags($nextQuestions->question_text_lang) . '<input style="margin-top: 25px;" type="text" id="answerQuestionsFillIntheBlank" name="answerQuestionsFillIntheBlank" placeholder="Give Answer" class="form-control max-textarea"  value="'.$answerValue.'" />';
                    }else {
                        $questionText = $nextQuestions->question_text . '<input type="text" style="margin-top: 25px;" id="answerQuestionsFillIntheBlank" name="answerQuestionsFillIntheBlank" placeholder="Give Answer" class="form-control max-textarea" value="'.$answerValue.'"  />';
                    }
                }


                $optionsMessage='';
                if ($nextQuestions->type!=3){ //Fill In The Blank
                    $optionsMessage='Select the correct answer given below.';
                    if ($LanguageType==1){
                        $questionText=strip_tags($nextQuestions->question_text_lang);

                        if ($nextQuestions->type==4){
                            $optionsMessage='अपना उत्तर टेक्स्टबॉक्स के नीचे लिखें';
                        }else{
                            $optionsMessage='नीचे दिए गए सही उत्तर का चयन करें।';
                        }
                    }
                }

                $dataList=array(
                    'questionText'  =>$questionText,
                    'mark'          =>$nextQuestions->mark,
                    'penality'      =>$nextQuestions->penalty,
                    'nextId'=>$currentId,
                    'optionsHtml'=>$html,
                    'optionsMessage'=>$optionsMessage,
                    'type'=>$nextQuestions->type,
                    'positions'=>$questionNumberPositions,
                );

                print_r(json_encode($dataList));
                exit();
            }
        }
    }

}
