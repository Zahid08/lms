<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LProfile extends CI_Controller {
    
     function __construct()
	{
		parent::__construct();
		$this->load->helper('form', 'url');
		$this->load->model('User_model', 'USM');
		$this->load->model('Coursedetails_model','CSM');
		
	}
	public function index()
	{
	    $user_info = $this->session->userdata($session_data);
	    $email = $user_info['email'];
	    $id = $user_info['user_id'];
	    $today = date("Y-m-d H:i:s");
	    $data['user'] = $this->USM->get_edit_user($id);
	    $userpass = $data['user'];
	    if(isset($_POST['submit']))
	    {
	        $username= $_POST['name'];
	        $mobile= $_POST['mobile'];
	        $city= $_POST['city'];
	        $dob= $_POST['d_o_b'];
	  
	        $config['upload_path'] = './assets/profile_image/';
	        $config['allowed_types'] = 'gif|jpg|png|jpeg';
		    $config['encrypt_name'] = true;
		    $this->load->library('upload',$config);
		    if(!$this->upload->do_upload('picture'))
	     	{
		    $error =array('error'=>$this->upload->display_errors());
	    	}
	    	 else{
	    	        $uploaddata=$this->upload->data();
		    	    $filename = base_url('assets/profile_image/') . $uploaddata['file_name'];
		    	    $this->USM->insert_edit_profile_user($username,$mobile,$city,$dob,$email,$id,$today,$filename);
	    	     }
	                $this->USM->insert_edit_l_user($username,$mobile,$city,$dob,$email,$id,$today);
	   }
	   if(isset($_POST['passubmit'])){
	       $currentpass = $_POST['currentpass'];
	       $newpass = $_POST['new_pass'];
	       $confpass = $_POST['conf_pass'];
	       foreach($userpass as $p){
	           $user_pass = $p['password'];
	       }
	       $this->USM->update_learner_pass($currentpass,$newpass,$confpass,$user_pass,$id);
	   }
	   
	   
	   
	        if($_SESSION['role'] == 3 && $_SESSION['status'] == 1)
	        {
		      $this->load->view('learner/profile',$data);
	        }  
	    else{
	          redirect('Login');
	        }
	}
}
