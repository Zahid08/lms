<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salesreport extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		$this->load->helper('form', 'url');
		$this->load->model('Coursedetails_model', 'CSM');
		$this->load->model('Salesreport_model');
		
	}
	public function index()
	{
	    $data['luser'] = $this->CSM->get_luser_course();
	    $data['sales_report']= $this->CSM->get_all_course();
	    $data['course_price']= $this->CSM->get_all_price();
	    
	    if(isset($_POST['submit'])){
	        $user_info = $this->session->userdata($session_data);
	        $user_id = $user_info['user_id'];
	        $email = $_POST['email_id'];
	        $course_id = $_POST['course_id'];
	        $payment_mode = $_POST['payment_mode'];
	        $amount = $_POST['amount'];
	        $transactioncode = $_POST['tran_code'];
	        $user['info']=$this->CSM->get_user_info($email);
	        $t = $user['info'];
	        if(count($user) > 0){
	            foreach($t as $user)
	            $learner_id = $user['id'];
	            if(!empty($learner_id)){
	            $this->CSM->insert_transaction_detail($user_id,$learner_id,$course_id,$payment_mode,$amount,$transactioncode);
	        }else{
	            $this->session->set_flashdata('smsg', 'Your Email-ID is incorrect');
	        }
	        }
	        
	        }
	        
	    
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
		$this->load->view('admin/salesreport',$data);
	    }
	    else if($_SESSION['role'] == 3 && $_SESSION['status'] == 1){
		$this->load->view('learner/coursedetails',$insertdata);
	    }else{
	        redirect('Login');
	    }
	}
    public function addcomment()
	{
	        $entitytype = 'Sales Report';
	        $entityid 	= $this->security->xss_clean($this->input->post('trnsid'));
			$comment = $this->security->xss_clean($this->input->post('comment'));
		
			
		
			$now = date('Y-m-d');
 
			$insertData = array(
			                    'entityid'=> $entityid,
			                    'entitytypeid'=> $entitytype,
			                    'comment'=>$comment,
								'status'=>1,
								'created_by'=> $_SESSION['user_id'],
								'created_date'=>$now);
								
			
			
			$insertComment = $this->Salesreport_model->addcomment($insertData);
			
			
				if($insertComment)
				{
				    $this->session->set_flashdata('cm_msg', 'Successfully Commented'); //set success msg if registered successfully
    					redirect('Salesreport');
				    
				}
				else
				{
					$this->session->set_flashdata('cm_msg','Unable to save course. Please try again');
					redirect('Salesreport');
				}
		
	}
	public function addcomments()
	{
	        $entitytype = 'Learner Comment';
	        $entityid 	= $this->security->xss_clean($this->input->post('courseid'));
			$comment = $this->security->xss_clean($this->input->post('comment'));
		    $rating= $this->security->xss_clean($this->input->post('rate'));
			
		
			$now = date('Y-m-d');
 
			$insertData = array(
			                    'entityid'=> $entityid,
			                    'entitytypeid'=> $entitytype,
			                    'comment'=>$comment,
								'status'=>1,
								'rating'=>$rating,
								'created_by'=> $_SESSION['user_id'],
								'created_date'=>$now);
								
			
			
			$insertComments = $this->Salesreport_model->addcomments($insertData);
			
			
				if($insertComments)
				{
				     $entityid 	= $this->security->xss_clean($this->input->post('courseid'));
				    $this->session->set_flashdata('cm_msg', 'Successfully Commented'); //set success msg if registered successfully
    					redirect('CourseLearners?courseid='.$courseId);
				    
				}
				else
				{
					$this->session->set_flashdata('cm_msg','Unable to save course. Please try again');
					redirect('CourseLearners?courseid='.$courseId);
				}
		
	}
	
    public function deletecomment()
	{
			$commentId = $this->security->xss_clean($this->input->post('commentid'));
			
			
			$now = date('Y-m-d');
			$deleteData = array(
								'status'=>2,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
								
			$deleteComment = $this->Salesreport_model->deletecomment($deleteData,$commentId);
			
		    
			
				if($deleteComment)
				{
				    
    				    $this->session->set_flashdata('dc_msg', 'Comment Successfully Deleted'); //set success msg if registered successfully
    					redirect('Salesreport');
				    
				}
				else
				{
					$this->session->set_flashdata('dc_msg','Unable to Delete Comment. Please try again');
					redirect('Salesreport');
				}
		
	}	    
}
