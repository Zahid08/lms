<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Createpromocode extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		$this->load->model('Promocode_model');
		$this->load->helper(array('form', 'url'));
	    $this->load->library('form_validation');
	}
	public function index()
	{
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
		    $this->load->view('admin/createpromocode');
	    }else{
	        redirect('Login');
	    }
	}
	public function addpromocode()
	{
	        $code 	= $this->security->xss_clean($this->input->post('code'));
			$no_of_users = $this->security->xss_clean($this->input->post('no_of_users'));
			$discount = $this->security->xss_clean($this->input->post('discount'));
			$valid_till = $this->security->xss_clean($this->input->post('valid_till'));
			$max_discount = $this->security->xss_clean($this->input->post('max_discount'));
			$min_cart = $this->security->xss_clean($this->input->post('min_cart'));
			$courseid = $this->security->xss_clean($this->input->post('courseid'));
			
			$promocode_id = $this->security->xss_clean($this->input->post('promocode_id'));
			
                
		
			$now = date('Y-m-d');
 
			$insertData = array(
			                    'code'=> $code,
			                    'count'=> $no_of_users,
			                    'discount'=>$discount,
			                    'max_date' => $valid_till,
			                    'min_cart' => $min_cart,
			                    'max_discount' => $max_discount,
			                    'course_id' => $courseid,
								'status'=>1,
								'created_by'=> $_SESSION['user_id'],
								'created_date'=>$now);
								
			
			if($promocode_id == NULL || $promocode_id == ''){
			    $insertPromoCode = $this->Promocode_model->insertpromocode($insertData,0);
			}else{
			    $insertPromoCode = $this->Promocode_model->insertpromocode($insertData,$promocode_id);
			}
			
		    $InsertId = $_SESSION['user_id'];
			
				if($insertPromoCode)
				{
				    if($promocode_id == NULL || $promocode_id == ''){
				        
    				    $getmaxId = $this->db->query("SELECT max(id) as id FROM promo_codes where created_by = '$InsertId' ");
                        foreach($getmaxId->result() as $maxId){
                                $Id = $maxId->id ;  
                        }
				    }else{
				        $Id = $promocode_id ;
				    }
    				    $this->session->set_flashdata('msg', 'Promo Code Successfully Added'); //set success msg if registered successfully
    					redirect('Promocode');
				    
				}
				else
				{
					$this->session->set_flashdata('msg','Unable to save promo code. Please try again');
					redirect('Createpromocode');
				}
		
	}
	public function deletepromocode()
	{
			$courseid = $this->security->xss_clean($this->input->get('courseid'));
			
			$now = date('Y-m-d');
			$deleteData = array(
								'status'=>2,
								'is_published'=>1,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
			$deleteCourse = $this->Coursedetails_model->deletecourse($deleteData,$courseid);
			
		    $InsertId = $_SESSION['user_id'];
			
				if($deleteCourse)
				{
				    
    				    $this->session->set_flashdata('ds_msg', 'Course Successfully Deleted'); //set success msg if registered successfully
    					redirect('Promocode');
				    
				}
				else
				{
					$this->session->set_flashdata('ds_msg','Unable to Delete Course. Please try again');
					redirect('Promocode');
				}
		
	}
	
}
