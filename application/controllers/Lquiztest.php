<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Lquiztest extends CI_Controller {
function __construct()
{
parent::__construct();
$this->load->model('Lquiztest_model');
$this->load->helper(array('form', 'url'));
$this->load->library('form_validation');
}
public function index()
{

 if(($_SESSION['role'] == 3) && $_SESSION['status'] == 1){
$this->load->view('learner/quiz');
}else{
redirect('Login');
}
}
public function result()
{   
    $now=date('Y-m-d h:i:s');
    
    $CourseId = $_GET['courseid'];
    $quizTestid =$_GET['quizid'];
    $CreatedBy = $_SESSION['user_id'];
     
    for($i=1;$i<=6;$i++){
    $insertData= array(
                'course_id'=>$CourseId,
                'quiz_id'=>$quizTestid,
                'quiz_question_id'=>$this->input->post('questionID_'.$i),
                'user_id'=>$_SESSION['user_id'],
                'status' => 1,
                'student_result'=>$this->input->post('optionval_'.$i),
                 'actual_result' =>$this->input->post('answerID_'.$i),
                'created_date'=>$now,
                'created_by'=>$_SESSION['user_id'],
                'updated_date'=>$now,
                'updated_by'=>$_SESSION['user_id']
        );
        
        $this->db->insert('quiz_result', $insertData,$quizquestionid,$quizTestid,$CourseId);
    } 
    redirect('Lquiztest?courseid='.$CourseId.'&quizid='.$quizTestid); 
    
}  

}