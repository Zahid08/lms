<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class QuizReview extends CI_Controller {
    function __construct()
{
parent::__construct();
    $this->load->model('Newchapter_model');
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');
}

    public function index()
    {
        if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
            $this->load->view('admin/quiz_review');
        }else{
            redirect('Login');
        }

    }

    public function markSubmit(){
        if ($_POST){
            $attemptId=$_REQUEST['quiz_attempt_id'];
            $attemptResultId=$_REQUEST['quiz_attempt_result_id'];

            $dataUpdateResult=array(
                'review_id'=>$_SESSION['user_id']
            );

            $this->db->where('id', $attemptResultId);
            $this->db->update('quiz_test_attempt_result', $dataUpdateResult);

            //Mark Complete Attempt Result
            $attempt=array(
                'mark_complete_status'=>1
            );

            $this->db->where('id', $attemptId);
            $this->db->update('quiz_test_attempt', $attempt);

            redirect('QuizReview');
        }
    }
}