<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	    $this->load->library('form_validation');
	    $this->load->model('Promocode_model');
	}
	public function index()
	{
	    if(($_SESSION['role'] == 3) && $_SESSION['status'] == 1){
	  $this->load->view('home/Pay/checkout');
	    }else{
	         redirect('Login');
	    }
	     
	}
	
	public function promocode()
	{
	       $courseId = $_GET['courseid'];
	       $courseData = $this->db->query("SELECT * FROM course_price  WHERE status = 1 AND course_id = '$courseId'  ORDER BY 1 DESC")->result();
           foreach($courseData as $getcourseData){
               $courseData= $getcourseData->payable_price;
           }
	        
	        
			$now = date('Y-m-d');
 
			$insertData = array(
			                    'user_id'=> $_SESSION['user_id'],
			                    'course_id'=> $_GET['courseid'],
			                    'course_price'=>$courseData,
								'status'=>1,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now,
								'created_by'=> $_SESSION['user_id'],
								'created_date'=>$now);
								
			
			
			$insertWishlist = $this->Promocode_model->promocode($insertData,$courseData);
			
			
	    if(($_SESSION['role'] == 3) && $_SESSION['status'] == 1){
	  $this->load->view('home/promocode');
	    }
	    else if(($_SESSION['role'] == 2) && $_SESSION['status'] == 1){
	  $this->load->view('home/promocode');
	    }
	    else if(($_SESSION['role'] == 1) && $_SESSION['status'] == 1){
	  $this->load->view('home/promocode');
	    }else{
	        redirect('Login');
	    }
	     
	}
	
	public function applypromocode($courseId)
	{
	    
	    /*========================Course Info===============================*/
        $courseData = $this->db->query("SELECT * FROM course c LEFT JOIN course_price cp ON cp.course_id = c.course_id WHERE c.status = 1 AND c.is_published = 1 AND c.course_id = '$courseId'  ORDER BY 1 DESC")->result();
        foreach($courseData as $getcourseData){
        $courseTitle = $getcourseData->title;
        $coursePayableAmount = $getcourseData->payable_price;
        }
        /*===================================================================*/

	    
	    $promoCode	= $this->security->xss_clean($this->input->post('promo_code'));
	        $promoCodeData = $this->db->query("SELECT * FROM promo_codes  WHERE code = '$promoCode' AND status = 1 AND course_id = '$courseId'  ORDER BY 1 DESC");
	        
    $existCount = count($promoCodeData);
        if($existCount > 0)  {
            foreach($promoCodeData->result() as $getpromoCodeData){
            $count = $getpromoCodeData->count;
            $usedCount = $getpromoCodeData->used_count;
            $discount = $getpromoCodeData->discount;
            $maxDate = $getpromoCodeData->max_date;
            $maxDiscount = $getpromoCodeData->max_discount;
            $minCart = $getpromoCodeData->min_cart;
            if($currentDate < $maxDate){
                if($usedCount < $count){
                    if($coursePayableAmount >= $minCart){
                        $deduct = ($discount/100)*$coursePayableAmount;
                        if($deduct > $maxDiscount){
                            $deduct = $maxDiscount;
                        }
                        $coursePayableAmount = $coursePayableAmount - $deduct;
                        
                        
                         redirect("Payment?courseid=".$courseId."&amt=".$coursePayableAmount);
                        
                    }else{
                        $this->session->set_flashdata('pm_msg', 'Minimum Amount of purchase should be Rs.'.$minCart);
                        redirect("Payment/promocode?courseid=".$courseId);
                    }
                }else{
                    $this->session->set_flashdata('pm_msg', 'Maximum Users has used this Promo Code');
                    redirect("Payment/promocode?courseid=".$courseId);
                }
            }else{
                $this->session->set_flashdata('pm_msg', 'Promo Code has reached out of Date');
                redirect("Payment/promocode?courseid=".$courseId);
            }
        }
        }
        else{
            $this->session->set_flashdata('pm_msg', 'Invalid Promo Code'); //set success msg if registered successfully
            redirect("Payment/promocode?courseid=".$courseId);
        }
	    
	  
	     
	}


}
