<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Liveclass extends CI_Controller {
    
	public function index()
	{
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
		$this->load->view('admin/liveclass');
	    }else{
	        redirect('Login');
	    }
	}
}
