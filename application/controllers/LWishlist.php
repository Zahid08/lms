<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LWishlist extends CI_Controller {
    
	public function index()
	{
	    if($_SESSION['role'] == 3 && $_SESSION['status'] == 1){
		    $this->load->view('learner/wishlist');
	    }else{
	        redirect('Login');
	    }
	}
}
