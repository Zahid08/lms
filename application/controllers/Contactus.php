<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		$this->load->model('Contactus_model');
	}
	public function index()
	{
	    $this->load->view('home/contactus');
	}
	public function contactUser()
	{      
	        
	        $now = date('Y-m-d h:i:s.ms'); 
	        $userName 	= $this->security->xss_clean($this->input->post('username'));
			$email 		= $this->security->xss_clean($this->input->post('email'));
			$mobile 	= $this->security->xss_clean($this->input->post('mob'));
			$Message 	= $this->security->xss_clean($this->input->post('message')); 
		    $insertData = array(
		                          'Email'=>$email,
		                          'mobile'=>$mobile,
		                          'Name'=>$userName,
		                          'message'=>$Message,
		                         'updated_by'=> $userName,
		                         'updated_date'=>$now,
		                         'created_by'  =>$userName,
		                         'created_date'=>$now);
		   $contactUser = $this->Contactus_model->contactuser($insertData);
		   if($contactUser)
				{
				    
    				    $this->session->set_flashdata('rd_msg', 'Message sent successfully'); //set success msg if registered successfully
    					redirect('Contactus');
				    
				}
				else
				{
					$this->session->set_flashdata('rd_msg','Unable to Place Request. Please try again');
					redirect('Contactus');
				}
	}		
}