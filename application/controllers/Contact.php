<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		
		$this->load->model('Contactus_model');
		
	}
	public function index()
	{
	    $this->load->view('admin/contact');
	}
	 public function deletemessage()
	{       
			$messageId = $this->security->xss_clean($this->input->post('messageid'));
		
			
			$now = date('Y-m-d h:i:s');
			$deleteData = array(
								
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
								
			$deleteMessage = $this->Contactus_model->deletemessage($deleteData,$messageId);
			
		    
			
				if($deleteMessage)
				{
				    
    				    $this->session->set_flashdata('dc_msg', 'Comment Successfully Deleted'); //set success msg if registered successfully
    					redirect('Contact');
				    
				}
				else
				{
					$this->session->set_flashdata('dc_msg','Unable to Delete Comment. Please try again');
					redirect('Contact');
				}
		
	}
	
}	