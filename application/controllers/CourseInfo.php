<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CourseInfo extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		$this->load->model('Coursedetails_model');
		$this->load->helper(array('form', 'url'));
	    $this->load->library('form_validation');
	}
	public function index()
	{
	     if(($_SESSION['role'] == 1 || $_SESSION['role'] == 2) && $_SESSION['status'] == 1){
		    $this->load->view('admin/course_info');
	     }else{
	         redirect('Login');
	     }
	}
	public function addcourse()
	{
	        $title 	= $this->security->xss_clean($this->input->post('title'));
			$category = $this->security->xss_clean($this->input->post('category'));
			$instructor = $this->security->xss_clean($this->input->post('instructor'));
			$tagline = $this->security->xss_clean($this->input->post('tagline'));
			$howToUse 	= $this->security->xss_clean($this->input->post('how_to_use')); 
			$language 	= $this->security->xss_clean($this->input->post('language')); 
			$instructor = $this->security->xss_clean($this->input->post('instructor'));
			$type = $this->security->xss_clean($this->input->post('type'));
			$description = $this->security->xss_clean($this->input->post('description'));
			$courseid = $this->security->xss_clean($this->input->post('course_id'));
			
			/*=============uploadimg=============*/
			$config['upload_path'] = './assets/course_images/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 2000;
            $config['max_width'] = 1500;
            $config['max_height'] = 1500;
    
            
            $this->load->library('upload', $config);
            $field_name = 'coursefile';
            $this->upload->do_upload($field_name);
            
            $upload_data = $this->upload->data();
            $data['file_name'] = $upload_data['file_name'];
			/*=============uploadimg=============*/
			
                
		
			$now = date('Y-m-d');
 
			$insertData = array(
			                    'title'=> $title,
			                    'category'=> $category,
			                    'instructor'=>$instructor,
			                    'type' => $type,
			                    'description'=>$description,
								'tag_line'=>$tagline,
								'how_to_use'=>$howToUse,
								'language'=>$language,
								'image'=>$data['file_name'],
								'status'=>1,
								'is_published'=>1,
								'created_by'=> $_SESSION['user_id'],
								'created_date'=>$now);
								
			
			if($courseid == NULL || $courseid == ''){
			    $insertCourse = $this->Coursedetails_model->insertcourse($insertData,0);
			}else{
			    $insertCourse = $this->Coursedetails_model->insertcourse($insertData,$courseid);
			}
			
		    $InsertId = $_SESSION['user_id'];
			
				if($insertCourse)
				{
				    if($courseid == NULL || $courseid == ''){
				        
    				    $getmaxId = $this->db->query("SELECT max(course_id) as id FROM course where created_by = '$InsertId' ");
                        foreach($getmaxId->result() as $maxId){
                                $Id = $maxId->id ;  
                        }
				    }else{
				        $Id = $courseid ;
				    }
    				    $this->session->set_flashdata('msg', 'Course Successfully Added'); //set success msg if registered successfully
    					redirect('Pricing?courseid='.$Id);
				    
				}
				else
				{
					$this->session->set_flashdata('msg','Unable to save course. Please try again');
					redirect('CourseInfo');
				}
		
	}
	public function deletecourse()
	{
			$courseid = $this->security->xss_clean($this->input->get('courseid'));
			
			$now = date('Y-m-d');
			$deleteData = array(
								'status'=>2,
								'is_published'=>1,
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
			$deleteCourse = $this->Coursedetails_model->deletecourse($deleteData,$courseid);
			
		    $InsertId = $_SESSION['user_id'];
			
				if($deleteCourse)
				{
				    
    				    $this->session->set_flashdata('ds_msg', 'Course Successfully Deleted'); //set success msg if registered successfully
    					redirect('Courses');
				    
				}
				else
				{
					$this->session->set_flashdata('df_msg','Unable to Delete Course. Please try again');
					redirect('Courses');
				}
		
	}
	
}
