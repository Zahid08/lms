<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assetlibrary extends CI_Controller {
    
	public function index()
	{
	    if($_SESSION['role'] == 1 && $_SESSION['status'] == 1){
	        $this->load->view('admin/assetlibrary');
	    }else{
	        redirect('Login');
	    }
		
	}
}
