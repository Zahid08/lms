<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LWallet extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		$this->load->model('Wallet_model');
		$this->load->helper(array('form', 'url'));
	    $this->load->library('form_validation');
	}
	
	public function index()
	{
	    
	     if($_SESSION['role'] == 3 && $_SESSION['status'] == 1){
		    $this->load->view('learner/wallet');
	     }else{
	         redirect('Login');
	     }
	}
   	public function requestcredit()
	{
	         $uniqueId = $_SESSION['unique_id'];
			$creditID = $this->security->xss_clean($this->input->get('credit_id'));
		     $creditS =$this->db->query("SELECT credits_earned as creditS FROM generated_credits WHERE unique_id='$uniqueId'")->result(); 
		    foreach($creditS as $getcreditS){
             $creditS = $getcreditS->creditS;
             }
		    $creditsinRS =$this->db->query("SELECT credits_in_rs as creditsinRS FROM generated_credits WHERE unique_id='$uniqueId'")->result(); 
		    foreach($creditsinRS as $getcreditsinRS){
             $creditsinRS = $getcreditsinRS->creditsinRS;
             }
			$now = date('Y-m-d');
			$InsertData = array(
			                    'credit_id' => $creditID,
			                    'user_id' => $_SESSION['user_id'],
			                    'credits' => $creditS,
			                    'credits_in_rs' => $creditsinRS,
								'status'=>1, //requested
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);
			$updateData = array(
			                    'id'=>$creditID,
								'status'=>1, //requested
								'updated_by'=> $_SESSION['user_id'],
								'updated_date'=>$now);				
							
			$redeemCredit = $this->Wallet_model->redeemcredit($InsertData,$updateData);
           
				if($redeemCredit)
				{
				    
    				    $this->session->set_flashdata('rd_msg', 'Redeem request has been placed successfully'); //set success msg if registered successfully
    					redirect('LWallet');
				    
				}
				else
				{
					$this->session->set_flashdata('rd_msg','Unable to Place Request. Please try again');
					redirect('LWallet');
				}
		
	}
	
}
